package com.mediaiqdigital.doubleclick.batch;

import com.mediaiqdigital.doubleclick.batch.service.TradingCenterBatchRestService;
import com.mediaiqdigital.doubleclick.batch.service.impl.DateParameter;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.net.SocketTimeoutException;
import java.text.ParseException;

/**
 * @author prasannachidire on 22/12/17
 */


@RunWith(SpringJUnit4ClassRunner.class)
@IntegrationTest
@SpringApplicationConfiguration(classes = {DBMBatchServiceApplication.class})
public class TcBatchRestClientTest extends BaseTest{

    @Autowired
    private TradingCenterBatchRestService tradingCenterBatchRestService;

    @Test
    public void testTcBatchRestClientService (){


        try {
            DateParameter startParam = new DateParameter("2017-11-01T00:00:00");
            DateParameter endParam = new DateParameter("2017-11-01T00:00:00");
            System.out.println("*****1 " + startParam.getDate().toString());
            tradingCenterBatchRestService.launchPlacementStatsProcessorInTcBatch(startParam.getDate(), endParam.getDate());
            System.out.println("Completeed");
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }
}
