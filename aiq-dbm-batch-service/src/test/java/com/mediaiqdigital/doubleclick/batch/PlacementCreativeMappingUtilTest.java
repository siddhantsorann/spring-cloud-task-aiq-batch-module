package com.mediaiqdigital.doubleclick.batch;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.util.CollectionUtils;

import com.mediaiq.aiq.domain.Dsp;
import com.mediaiqdigital.aiq.tradingcenter.domain.PlacementCreativeMapping;
import com.mediaiqdigital.aiq.tradingcenter.domain.id.PlacementCreativeId;
import com.mediaiqdigital.doubleclick.batch.service.util.PlacementCreativeMappingUtil;

/**
 * @author prasannachidire on 14/8/18.
 */
public class PlacementCreativeMappingUtilTest extends BaseTest {
  
  
  
  private List<PlacementCreativeMapping> oldList = new ArrayList<>();
  
  private List<PlacementCreativeMapping> newList = new ArrayList<>();
  
  private List<PlacementCreativeMapping> finalList = new ArrayList<>();
  
  Date olderDate = getOlderDate(5);
  Date newDate = getOlderDate(1);
  
  @Before
  public void setup() {
    // Test Data for PCM Job
    
    PlacementCreativeId pc1 = new PlacementCreativeId(1l, 10l, 100l, Dsp.DoubleClick, new Date());
    PlacementCreativeId pc2 = new PlacementCreativeId(1l, 20l, 100l, Dsp.DoubleClick, new Date());
    PlacementCreativeId pc3 = new PlacementCreativeId(2l, 10l, 100l, Dsp.DoubleClick, new Date());
    PlacementCreativeId pc4 = new PlacementCreativeId(2l, 20l, 100l, Dsp.DoubleClick, new Date());
    PlacementCreativeMapping pcm1 = new PlacementCreativeMapping(pc1, olderDate, olderDate);
    PlacementCreativeMapping pcm2 = new PlacementCreativeMapping(pc2, olderDate, olderDate);
    PlacementCreativeMapping pcm3 = new PlacementCreativeMapping(pc3, olderDate, olderDate);
    PlacementCreativeMapping pcm4 = new PlacementCreativeMapping(pc4, olderDate, olderDate);
    oldList.add(pcm1);
    oldList.add(pcm2);
    // To be removed mapping
    oldList.add(pcm3);
    newList.add(pcm1);
    newList.add(pcm2);
    // newly added mapping
    newList.add(pcm4);
    finalList =
        PlacementCreativeMappingUtil.getUpdatedPlacementCreativeMappings(oldList, newList, newDate);
  }
  
  // The Unchanged mappings shouldnt be part of final list
  @Test
  public void testUnchangedMappings() {
    List<PlacementCreativeMapping> placementCreativeMappings = finalList.stream().filter(
        pcm -> (pcm.getId().getCreativeId().equals(20l) || pcm.getId().getCreativeId().equals(10l))
            && pcm.getId().getPlacementId().equals(1l))
        .collect(Collectors.toList());
    
    Assert.assertTrue(CollectionUtils.isEmpty(placementCreativeMappings));
    
  }
  
  
  // Tests if the end date of the removed mappings are updated
  @Test
  public void testRemovedMappings() {
    
    finalList.stream().filter(
        pcm -> pcm.getId().getCreativeId().equals(10l) && pcm.getId().getPlacementId().equals(2l))
        .forEach(pcm -> {
          Assert.assertEquals(pcm.getEndDate().equals(newDate), true);
        });
  }
  
  // Tests if the final List has newly added mappings
  @Test
  public void testAddedMappings() {
    
    List<PlacementCreativeMapping> placementCreativeMappings = finalList.stream().filter(
        pcm -> pcm.getId().getCreativeId().equals(20l) && pcm.getId().getPlacementId().equals(2l))
        .collect(Collectors.toList());
    
    Assert.assertFalse(CollectionUtils.isEmpty(placementCreativeMappings));
  }
}
