package com.mediaiqdigital.doubleclick.batch;

import com.mediaiqdigital.doubleclick.batch.util.StepAttribute;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;

/**
 * Created by prasannachidire on 21/11/17.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@IntegrationTest
@SpringApplicationConfiguration(classes = {DBMBatchServiceApplication.class})
public class PlacementCreativeMappingTest extends BaseTest {
  
  @Autowired
  @Qualifier("dbmPlacementCreativeMappingLoadJob")
  private Job dbmPlacementCreativeMappingLoadJob;
  
  @Before
  public void setup() {
    super.setup();
    jobLauncherTestUtils.setJob(dbmPlacementCreativeMappingLoadJob);
  }
  
  @Test
  public void testPlacementCreativeMappingLoadJob() throws Exception {
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("last_modified", getOlderDate(3)).addDate("date", new Date())
        .addLong(StepAttribute.START_ELEMENT, 0L);
    JobExecution jobExecution =
        jobLauncherTestUtils.launchJob(jobParametersBuilder.toJobParameters());
    Assert.assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());
    System.out.println(" Test Successfully Completed ");
  }
}
