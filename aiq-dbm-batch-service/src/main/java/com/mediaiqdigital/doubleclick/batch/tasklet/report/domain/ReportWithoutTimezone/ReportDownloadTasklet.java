package com.mediaiqdigital.doubleclick.batch.tasklet.report.domain.ReportWithoutTimezone;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.google.api.client.googleapis.media.MediaHttpDownloader;
import com.google.api.client.http.GenericUrl;
import com.mediaiqdigital.doubleclick.batch.util.SecurityUtilities;
import com.mediaiqdigital.doubleclick.batch.util.StepAttribute;

/**
 * Created by piyush on 25/7/16.
 */

/**
 * Created by piyush on 29/5/16.
 */
@Component
@Scope("step")
public class ReportDownloadTasklet implements Tasklet {
  
  final static Logger logger = LoggerFactory.getLogger(ReportDownloadTasklet.class);
  
  
  @Value("${report.statsFile}")
  String basePath;
  
  @Value("#{jobExecutionContext['" + StepAttribute.REPORT_URL + "']}")
  private String reportUrl;
  
  @Value("#{jobExecutionContext['" + StepAttribute.FILE_NAME + "']}")
  private String filename;
  
  @Override
  public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext)
      throws IOException {
    
    GenericUrl reportPath = new GenericUrl(reportUrl);
    String fileName = "DailyPlacementStats";
    if (!StringUtils.isEmpty(filename)) {
      fileName = filename;
    }
    String fileTarget = basePath + System.getProperty("file.separator") + fileName + ".csv";
    OutputStream out = new FileOutputStream(new File(fileTarget));
    System.out.println(out.toString());
    MediaHttpDownloader downloader =
        new MediaHttpDownloader(SecurityUtilities.getHttpTransport(), null);
    downloader.download(reportPath, out);
    System.out.println("Download complete.");
    ExecutionContext executionContext =
        chunkContext.getStepContext().getStepExecution().getJobExecution().getExecutionContext();
    
    System.out.println(" file target path " + fileTarget);
    executionContext.put(StepAttribute.FILE_PATH, fileTarget);
    
    logger.info("Report {} downloaded.", "report 1");
    
    return RepeatStatus.FINISHED;
  }
}
