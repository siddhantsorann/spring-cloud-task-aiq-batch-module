package com.mediaiqdigital.doubleclick.batch.mapper;

import java.util.Date;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import com.mediaiqdigital.doubleclick.batch.domain.FlatLineItem;

public class FlatLineItemMapper implements FieldSetMapper<FlatLineItem> {
  
  @Override
  public FlatLineItem mapFieldSet(FieldSet fieldSet) throws BindException {
    
    Long lineItemId = fieldSet.readLong("Line Item Id");
    String partnerName = fieldSet.readString("Partner Name");
    Long partnerId = fieldSet.readLong("Partner Id");
    String advertiserName = fieldSet.readString("Advertiser Name");
    String ioName = fieldSet.readString("Io Name");;
    String lineItemName = fieldSet.readString("Line Item Name");
    String lineItemTimestamp = fieldSet.readString("Line Item Timestamp");
    String lineItemStatus = fieldSet.readString("Line Item Status");
    Date ioStartDate = fieldSet.readDate("Io Start Date", "MM/dd/yyyy HH:mm");
    Date ioEndDate = fieldSet.readDate("Io End Date", "MM/dd/yyyy HH:mm");
    String ioBudgetType = fieldSet.readString("Io Budget Type");
    String ioBudgetAmount = fieldSet.readString("Io Budget Amount");
    String ioPacing = fieldSet.readString("Io Pacing");
    
    String ioPacingRate = fieldSet.readString("Io Pacing Rate");
    String ioPacingAmount = fieldSet.readString("Io Pacing Amount");
    String lineItemStartDate = fieldSet.readString("Line Item Start Date");
    String lineItemEndDate = fieldSet.readString("Line Item End Date");
    String lineItemBudgetType = fieldSet.readString("Line Item Budget Type");
    String lineItemBudgetAmount = fieldSet.readString("Line Item Budget Amount");
    String lineItemPacing = fieldSet.readString("Line Item Pacing");
    String lineItemPacingRate = fieldSet.readString("Line Item Pacing Rate");
    String lineItemPacingAmount = fieldSet.readString("Line Item Pacing Amount");
    String lineItemFrequencyEnabled = fieldSet.readString("Line Item Frequency Enabled");
    String lineItemFrequencyExposures = fieldSet.readString("Line Item Frequency Exposures");
    String lineItemFrequencyPeriod = fieldSet.readString("Line Item Frequency Period");
    String lineItemFrequencyAmount = fieldSet.readString("Line Item Frequency Amount");
    String bidPrice = fieldSet.readString("Bid Price");
    String partnerRevenueModel = fieldSet.readString("Partner Revenue Model");
    String partnerRevenueAmount = fieldSet.readString("Partner Revenue Amount");
    String currentAudienceTargetingIds = fieldSet.readString("Current Audience Targeting Ids");
    String currentAudienceTargetingNames = fieldSet.readString("Current Audience Targeting Names");
    
    FlatLineItem flatLineItem =
        new FlatLineItem(lineItemId, partnerName, partnerId, advertiserName, ioName, lineItemName,
            lineItemTimestamp, lineItemStatus, ioStartDate, ioEndDate, ioBudgetType, ioBudgetAmount,
            ioPacing, ioPacingRate, ioPacingAmount, lineItemStartDate, lineItemEndDate,
            lineItemBudgetType, lineItemBudgetAmount, lineItemPacing, lineItemPacingRate,
            lineItemPacingAmount, lineItemFrequencyEnabled, lineItemFrequencyExposures,
            lineItemFrequencyPeriod, lineItemFrequencyAmount, bidPrice, partnerRevenueModel,
            partnerRevenueAmount, currentAudienceTargetingIds, currentAudienceTargetingNames);
    
    return flatLineItem;
  }
  
}
