package com.mediaiqdigital.doubleclick.batch.mapper;

import java.util.Date;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import com.mediaiqdigital.doubleclick.Id.DailyPlacementId;
import com.mediaiqdigital.doubleclick.domain.stats.DbmPlacementCreativeStats;

/**
 * Created by piyush on 10/2/16.
 */
public class DBMDailyPlacementStatsMapper implements FieldSetMapper<DbmPlacementCreativeStats> {
  
  
  public DBMDailyPlacementStatsMapper(Date startDate, Date endDate) {}
  
  @Override
  public DbmPlacementCreativeStats mapFieldSet(FieldSet set) throws BindException {
    DbmPlacementCreativeStats stats = new DbmPlacementCreativeStats();
    
    Long creativeId = set.readLong("Creative ID");
    Long lineItemId = set.readLong("Line Item ID");
    Integer timeOfDay = set.readInt("Time of Day");
    Date date = set.readDate("Date", "yyyy/MM/dd");
    Date dateTime = set.readDate("DateTime", "yyyy-MM-dd HH:mm:ss");
    stats.setId(new DailyPlacementId(creativeId, lineItemId, date, timeOfDay));
    stats.setDateTime(dateTime);
    stats.setClicks(set.readLong("Clicks"));
    stats.setImpressions(set.readLong("Impressions"));
    stats.setTotalConversions(set.readLong("Total Conversions"));
    stats.setCtr(set.readDouble("Click Rate (CTR)"));
    stats.setAdvertiserTimeZone(set.readString("Advertiser Time Zone"));
    stats.setCurrency(set.readString("Advertiser Currency"));
    stats.setInsertionOrderId(set.readLong("Insertion Order ID"));
    String totalSpend = set.readString("Total Media Cost (Advertiser Currency)");
    // String spend=totalSpend.substring(1,totalSpend.length());
    
    totalSpend = totalSpend.replace("$", "").replace("€", "").replace("UK£", "").replace("CA", "")
        .replace("A", "").replace("NZ", "").replace("IDR", "").replace("CHF", "").replace("S", "")
        .replace(",", "");
    stats.setTotal_spend(Double.parseDouble(totalSpend));
    
    return stats;
  }
}
