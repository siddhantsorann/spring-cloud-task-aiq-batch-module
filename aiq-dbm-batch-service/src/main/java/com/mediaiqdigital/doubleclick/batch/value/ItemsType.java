package com.mediaiqdigital.doubleclick.batch.value;

/**
 * Created by piyush on 24/11/15.
 */
public enum ItemsType {
  Advertiser, InventorySource, Partner, Pixel, Creative, InsertionOrder, UserList, UniversalChannel, LineItem

}
