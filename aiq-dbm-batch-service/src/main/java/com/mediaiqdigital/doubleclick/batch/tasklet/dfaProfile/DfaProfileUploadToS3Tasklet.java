package com.mediaiqdigital.doubleclick.batch.tasklet.dfaProfile;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.PutObjectRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.File;

/**
 * Created by piyush on 30/11/15.
 */
@Component
@Scope("step")
public class DfaProfileUploadToS3Tasklet implements Tasklet {
  
  private static final Logger LOGGER = LoggerFactory.getLogger(DfaProfileUploadToS3Tasklet.class);
  
  @Autowired
  AmazonClient amazonClient;

  @Value("${dfaReports.s3.bucket}")
  private String bucketName;
  @Value("${dfaReports.s3.path}")
  private String path;
  @Value("${dfaReports.path}")
  private String localPath;
  @Value("#{jobParameters['seat']}")
  private String seat;
  
  @Override
  public RepeatStatus execute(StepContribution contribution, ChunkContext context)
      throws Exception {
    LOGGER.info("uploading dfa profiles to s3 ");
    uploadCsvToS3();
    return RepeatStatus.FINISHED;
  }

  public void uploadCsvToS3() {

    try {
      AmazonS3 s3Client=amazonClient.setS3Client();
      String localFilePath = localPath +"-"+seat + ".csv";
      File file = new File(localFilePath);

      LOGGER.info(" file {}  name {}", file.getTotalSpace(), file.getName());
      s3Client.putObject(new PutObjectRequest(bucketName, path + file.getName(), file));

    } catch (AmazonServiceException ase) {

      LOGGER.info("Caught an AmazonServiceException, which " + "means your request made it "
          + "to Amazon S3, but was rejected with an error response" + " for some reason.");
      LOGGER.info("Error Message:    " + ase.getMessage());
      LOGGER.info("HTTP Status Code: " + ase.getStatusCode());
      LOGGER.info("AWS Error Code:   " + ase.getErrorCode());
      LOGGER.info("Error Type:       " + ase.getErrorType());
      LOGGER.info("Request ID:       " + ase.getRequestId());

    } catch (AmazonClientException ace) {

      LOGGER.info("Caught an AmazonClientException, which " + "means the client encountered "
          + "an internal error while trying to " + "communicate with S3, "
          + "such as not being able to access the network.");
      LOGGER.info("Error Message: " + ace.getMessage());
    }
  }
}

