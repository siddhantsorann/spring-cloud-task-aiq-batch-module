package com.mediaiqdigital.doubleclick.batch.mapper;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindException;

import com.mediaiq.aiq.domain.Dsp;
import com.mediaiqdigital.aiq.tradingcenter.domain.DailyInsertionOrderStats;
import com.mediaiqdigital.aiq.tradingcenter.domain.DailyInsertionOrderStatsId;
import com.mediaiqdigital.aiq.tradingcenter.repo.CampaignIoAssociationRepo;

/**
 * Created by piyush on 10/2/16.
 */
public class DBMDailyInsertionOrderStatsMapper implements FieldSetMapper<DailyInsertionOrderStats> {
  
  
  final static Logger logger = LoggerFactory.getLogger(DBMDailyInsertionOrderStatsMapper.class);
  
  // New model and repo are added.
  // @Autowired
  // DailyIoPlacementAssociationRepo dailyIoPlacementAssociationRepo;
  
  @Autowired
  CampaignIoAssociationRepo campaignIoAssociationRepo;
  
  
  public DBMDailyInsertionOrderStatsMapper(Date startDate, Date endDate) {}
  
  @Override
  public DailyInsertionOrderStats mapFieldSet(FieldSet set) throws BindException {
    
    DailyInsertionOrderStats stats = new DailyInsertionOrderStats();
    Long miqCampaignId = 0L;
    Date date = set.readDate("Date", "yyyy/MM/dd");
    Long insertionOrderId = set.readLong("Insertion Order ID");
    stats.setDailyInsertionOrderStatsId(
        new DailyInsertionOrderStatsId(insertionOrderId, date, Dsp.DoubleClick));
    stats.setClicks(set.readLong("Clicks"));
    stats.setImpressions(set.readLong("Impressions"));
    stats.setConversions(set.readLong("Total Conversions"));
    String totalSpend = set.readString("Media Cost (USD)");
    totalSpend = totalSpend.replace("$", "").replace(",", "");
    // stats.setMediaCost(Double.parseDouble(totalSpend));
    // String spend=totalSpend.substring(1,totalSpend.length());
    // stats.setTotal_spend(spend);
    try {
      // fetching the campaign id from campaignIoAssociation table instead of
      // dailyIoPlacementAssociation table
      
      // miqCampaignId =
      // dailyIoPlacementAssociationRepo.findByIdIoId(insertionOrderId).getId().getCampaignId();
      miqCampaignId = campaignIoAssociationRepo
          .findByIdIoIdAndIdDsp(insertionOrderId, Dsp.DoubleClick).get(0).getId().getCampaignId();
    } catch (IndexOutOfBoundsException e) {
      // TODO: @Piyush: Remove this
      logger.info("Miq Campaign Id Not Found");
    }
    if (miqCampaignId != 0L) {
      stats.setCampaignId(miqCampaignId);
    }
    return stats;
  }
}
