package com.mediaiqdigital.doubleclick.batch.tasklet.dfaProfile;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mediaiqdigital.doubleclick.batch.util.StepAttribute;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import com.google.api.client.util.DateTime;
import com.google.api.client.util.Strings;
import com.google.api.services.dfareporting.Dfareporting;
import com.google.api.services.dfareporting.model.DateRange;
import com.google.api.services.dfareporting.model.Recipient;
import com.google.api.services.dfareporting.model.Report;
import com.google.api.services.dfareporting.model.ReportList;
import com.google.api.services.dfareporting.model.SortedDimension;
import com.google.api.services.dfareporting.model.UserProfileList;
import com.google.common.collect.ImmutableList;
import com.mediaiqdigital.doubleclick.batch.domain.DCMFeedDto;
import com.mediaiqdigital.doubleclick.batch.domain.DcmProfileAccountDto;
import com.mediaiqdigital.doubleclick.domain.DcmProfile;
import com.mediaiqdigital.doubleclick.repo.DcmProfileRepo;

/**
 * Created by piyush on 30/11/15.
 */
@Component
@Scope("step")
public class DfaProfileLoadTasklet implements Tasklet {
  
  private static final Logger LOGGER = LoggerFactory.getLogger(DfaProfileLoadTasklet.class);
  @Autowired
  AmazonClient amazonClient;
  @Autowired
  DfaReportingFactory dfaReportingFactory;
  @Value("${dfaReports.s3.path}")
  private String path;
  @Value("${dfaReports.path}")
  private String localPath;
  @Value("#{jobParameters['directory']}")
  private String dataDirectory;
  @Value("#{jobParameters['secret']}")
  private String clientSecret;
  @Value("#{jobParameters['seat']}")
  private String seat;
  @Value("${dfaReports.mail1}")
  private String mail1;
  @Value("${dfaReports.mail2}")
  private String mail2;

  private static Integer c=0;
  
  @Autowired
  DcmProfileRepo dcmProfileRepo;
  
  private String deliveryType = "ATTACHMENT";
  private String reportKind = "dfareporting#recipient";
  
  @Override
  public RepeatStatus execute(StepContribution contribution, ChunkContext context)
      throws Exception {
    LOGGER.info("starting dfa reporting ");
    
    Dfareporting reporting = getDfaReportingInstance();
    
    List<Long> existingProfileIds = new ArrayList<>();
    List<Long> newProfileIds = new ArrayList<>();
    Map<Long, DcmProfileAccountDto> accountMap = new HashMap<>();

    //Setting up the limit on retries
    if (reporting == null && c<5) {
      c++;
      return RepeatStatus.CONTINUABLE;
    }

    if (reporting == null && c>=5) {
      c=0;
      throw new Exception("Not able to fetch Dfa profiles");
    }

    UserProfileList profiles = reporting.userProfiles().list().execute();
    
    List<DcmProfile> existingDcmProfiles = dcmProfileRepo.findAll();
    
    List<DcmProfile> allDcmProfiles = new ArrayList<>();
    
    profiles.getItems().stream().forEach(p -> {
      existingProfileIds.add(p.getProfileId());
      DcmProfile dcmProfile = new DcmProfile();
      dcmProfile.setId(p.getProfileId());
      accountMap.put(p.getProfileId(),
          new DcmProfileAccountDto(p.getAccountId(), p.getAccountName()));
      allDcmProfiles.add(dcmProfile);
    });
    
    // Creating report for new profiles only
    if (!existingDcmProfiles.isEmpty())
      allDcmProfiles.removeAll(existingDcmProfiles);
    
    if (!allDcmProfiles.isEmpty()) {
      allDcmProfiles.stream().forEach(p -> {
        newProfileIds.add(p.getId());
      });
    }
    existingProfileIds.removeAll(newProfileIds);
    LOGGER.info("creating report for profiles {}", allDcmProfiles);
    createReport(reporting, newProfileIds, existingProfileIds, accountMap);
    dcmProfileRepo.save(allDcmProfiles);
    return RepeatStatus.FINISHED;
    
  }
  
  public void createReport(Dfareporting reporting, List<Long> newProfileIds,
      List<Long> existingProfileIds, Map<Long, DcmProfileAccountDto> accountMap)
      throws IOException {
    
    List<Long> reportIds = new ArrayList<>();
    
    List<DCMFeedDto> dtos = new ArrayList<>();
    List<Long> invalidProfiles = new ArrayList<>();
    
    newProfileIds.stream().forEach(p -> {
      
      DateRange dateRange = new DateRange();
      dateRange.setRelativeDateRange("LAST_7_DAYS");
      
      // Create a dimension to report on.
      List<SortedDimension> dimensions = new ArrayList<>();

      dimensions.add(new SortedDimension().setName("dfa:advertiserId"));
      dimensions.add(new SortedDimension().setName("dfa:advertiser"));
      dimensions.add(new SortedDimension().setName("dfa:campaignId"));
      dimensions.add(new SortedDimension().setName("dfa:campaign"));
      dimensions.add(new SortedDimension().setName("dfa:creativeId"));
      dimensions.add(new SortedDimension().setName("dfa:creative"));
      dimensions.add(new SortedDimension().setName("dfa:site"));
      dimensions.add(new SortedDimension().setName("dfa:date"));
      dimensions.add(new SortedDimension().setName("dfa:hour"));
      dimensions.add(new SortedDimension().setName("dfa:placement"));
      dimensions.add(new SortedDimension().setName("dfa:placementId"));
      dimensions.add(new SortedDimension().setName("dfa:activityId"));
      dimensions.add(new SortedDimension().setName("dfa:activity"));
      
      // Delivery details
      Report.Delivery delivery = new Report.Delivery();
      delivery.setEmailOwner(true);
      delivery.setEmailOwnerDeliveryType(deliveryType);
      delivery.setMessage("DCM Feed Report");
      List<Recipient> recipients = new ArrayList<>();
      recipients
          .add(new Recipient().setEmail(mail1).setDeliveryType(deliveryType).setKind(reportKind));
      recipients
          .add(new Recipient().setEmail(mail2).setDeliveryType(deliveryType).setKind(reportKind));
      delivery.setRecipients(recipients);
      
      
      // Create the criteria for the report.
      Report.Criteria criteria = new Report.Criteria();
      criteria.setDateRange(dateRange);
      criteria.setDimensions(dimensions);
      criteria.setMetricNames(
          ImmutableList.of("dfa:clicks","dfa:impressions","dfa:totalConversions", "dfa:clickRate",
              "dfa:activityClickThroughConversions", "dfa:activityViewThroughConversions"));
      
      Report.Schedule schedule = new Report.Schedule();
      schedule.setActive(true);
      
      schedule.setStartDate(DateTime.parseRfc3339("2018-09-26"));
      schedule.setExpirationDate(DateTime.parseRfc3339("2019-12-25"));
      
      schedule.setEvery(1);
      schedule.setRepeats("DAILY");
      
      // Report Name
      String REPORT_NAME = "DCM_" + p.toString();
      
      // Create the report.
      Report report = new Report();
      report.setCriteria(criteria);
      report.setName(REPORT_NAME);
      report.setFileName(REPORT_NAME);
      report.setType("STANDARD");
      report.setSchedule(schedule);
      report.setDelivery(delivery);
      
      // Insert the report.
      Report result = null;
      try {
        result = reporting.reports().insert(p, report).execute();
        LOGGER.info("Insertion of report succeeded with {} for profile {}", result.getId(), p);
        
        // Display the new report ID.
        LOGGER.info("Standard report with ID {} was created ", result.getId());
        
        reportIds.add(result.getId());
        
        DCMFeedDto dcmFeedDto = new DCMFeedDto(result.getOwnerProfileId(),
            accountMap.get(result.getOwnerProfileId()).getAccountName(), result.getId(), "true",
            new Timestamp(System.currentTimeMillis()));
        
        dtos.add(dcmFeedDto);
      } catch (Exception e) {
        LOGGER.info("Insertion of report fails for profile {}", p);
        invalidProfiles.add(p);
        e.printStackTrace();
      }
    });
    
    existingProfileIds.stream().forEach(p -> {
      
      String fields = "nextPageToken,items(id,name,type)";
      
      ReportList reports = new ReportList();
      String nextPageToken = null;
      
      do {
        // Create and execute the report list request.
        try {
          reports =
              reporting.reports().list(p).setFields(fields).setPageToken(nextPageToken).execute();
        } catch (IOException e) {
          e.printStackTrace();
        }
        
        if (!reports.isEmpty()) {
          reports.getItems().forEach(report -> {
            if(report.getName().startsWith("DCM_")){
            reportIds.add(report.getId());
            DCMFeedDto dcmFeedDto = new DCMFeedDto(p, accountMap.get(p).getAccountName(),
                report.getId(), "true", new Timestamp(System.currentTimeMillis()));
            dtos.add(dcmFeedDto);
            }
          });
        }
        
        // Update the next page token.
        nextPageToken = reports.getNextPageToken();
      } while (!reports.isEmpty() && !reports.getItems().isEmpty()
          && !Strings.isNullOrEmpty(nextPageToken));
      
      
    });
    LOGGER.info("list of profiles which has access issue {}", invalidProfiles);
    generateCsv(dtos);
  }
  
  private void generateCsv(List<DCMFeedDto> dfaReportDtoList) throws IOException {
    
    ICsvBeanWriter beanWriter = null;
    try {
      String fileName = localPath + "-" + seat + ".csv";
      File file = new File(fileName);
      boolean isDirectoryPresent = file.getParentFile().mkdirs();
      if (!isDirectoryPresent)
        LOGGER.info("Creating files inside directory {}", file.getParentFile().mkdirs());
      beanWriter = new CsvBeanWriter(new FileWriter(file), CsvPreference.STANDARD_PREFERENCE);
      beanWriter.writeHeader(getHeaders());
      for (DCMFeedDto dcmFeedDto : dfaReportDtoList)
        beanWriter.write(dcmFeedDto, getHeaders());
      
    } catch (IOException e) {
      LOGGER.info("Writing to csv failed", e);
    } finally {
      if (beanWriter != null)
        beanWriter.close();
    }
  }
  
  private String[] getHeaders() {
    return new String[] {"profileId", "accountName", "reportId", "isActive", "dt"};
  }
  
  private Dfareporting getDfaReportingInstance() {
    Dfareporting reporting = null;
    try {
      reporting = DfaReportingFactory.getInstance(dataDirectory, clientSecret);
    } catch (Exception e) {
      LOGGER.info(" Getting Instance of DfaReporting fails {}",e.getStackTrace());
      e.printStackTrace();
    }
    return reporting;
  }
}
