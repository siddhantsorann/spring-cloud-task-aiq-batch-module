package com.mediaiqdigital.doubleclick.batch.processor;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiqdigital.doubleclick.base.EntityCommonData;
import com.mediaiqdigital.doubleclick.domain.DbmPlacement;

/**
 * @author prasannachidire on 8/2/18.
 */

@Component
@Scope("step")
public class DbmPlacementProcessor implements ItemProcessor<DbmPlacement, DbmPlacement> {
  
  @Override
  public DbmPlacement process(DbmPlacement placement) throws Exception {
    EntityCommonData commonData = placement.getCommonData();
    placement.setId(commonData.getId());
    placement.setName(commonData.getName());
    placement.setIntegrationCode(commonData.getIntegrationCode());
    placement.setActive(commonData.getActive());
    return placement;
  }
}
