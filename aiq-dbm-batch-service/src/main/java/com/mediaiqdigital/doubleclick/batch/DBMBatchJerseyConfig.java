package com.mediaiqdigital.doubleclick.batch;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import com.mediaiq.spring.batch.service.impl.JobHistoryServiceImpl;
import com.mediaiqdigital.doubleclick.batch.service.impl.DBMBatchJobLaunchServiceAsyncImpl;

/**
 * Created by piyush on 25/11/15.
 */

@Component
@ApplicationPath("/api")
public class DBMBatchJerseyConfig extends ResourceConfig {
  
  public DBMBatchJerseyConfig() {
    register(DBMBatchJobLaunchServiceAsyncImpl.class);
    // /register(DBMBatchJobLaunchServiceImpl.class);
    register(JobHistoryServiceImpl.class);
  }
  
}
