package com.mediaiqdigital.doubleclick.batch.processor;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.mediaiq.aiq.domain.Dsp;
import com.mediaiqdigital.aiq.tradingcenter.domain.PlacementCreativeMapping;
import com.mediaiqdigital.aiq.tradingcenter.domain.id.PlacementCreativeId;
import com.mediaiqdigital.aiq.tradingcenter.repo.PlacementCreativeMappingRepo;
import com.mediaiqdigital.aiq.tradingcenter.util.Constants;
import com.mediaiqdigital.doubleclick.batch.service.util.PlacementCreativeMappingUtil;
import com.mediaiqdigital.doubleclick.domain.DbmPlacement;

/**
 * @author prasannachidire on 5/7/18.
 */
@Component
@Scope("job")
public class DbmPlacementCreativeMappingWriter implements ItemWriter<DbmPlacement> {
  
  
  private final Logger LOGGER = LoggerFactory.getLogger(DbmPlacementCreativeMappingWriter.class);
  
  @Autowired
  PlacementCreativeMappingRepo placementCreativeMappingRepo;
  
  @Value("#{jobParameters['last_modified']}")
  protected Date lastModified;
  
  
  @Override
  public void write(List<? extends DbmPlacement> placements) throws Exception {
    
    DateFormat formatter = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
    // This date used as endDate for all the current Mappings
    Date maxEndDate = formatter.parse(Constants.MAX_DATE);
    
    LOGGER.info("Last Modified : {}", lastModified);
    LOGGER.info("maxEndDate : {}", maxEndDate);
    
    
    if (!CollectionUtils.isEmpty(placements)) {
      List<Long> placementIds = placements.stream()
          .map(placement -> Long.valueOf(placement.getId())).collect(Collectors.toList());
      LOGGER.info("{} Placements found", placementIds.size());
      List<PlacementCreativeMapping> placementCreativeMappings = new ArrayList<>();
      
      placements.forEach(placement -> {
        Set<Long> creativeIds = placement.getCreativeIds();
        if (!CollectionUtils.isEmpty(creativeIds)) {
          creativeIds.forEach(creativeId -> {
            PlacementCreativeMapping placementCreativeMapping = new PlacementCreativeMapping(
                new PlacementCreativeId(placement.getCommonData().getId(), creativeId,
                    placement.getInsertionOrderId(), Dsp.DoubleClick, lastModified),
                lastModified, maxEndDate);
            placementCreativeMappings.add(placementCreativeMapping);
          });
        }
      });
      
      LOGGER.info("{} Formed PCM Mappings ", placementCreativeMappings.size());
      
      Set<Long> insertionOrderIdSet =
          placements.stream().map(placement -> Long.valueOf(placement.getInsertionOrderId()))
              .collect(Collectors.toSet());
      List<Long> insertionOrderIds = new ArrayList<>(insertionOrderIdSet);
      
      LOGGER.info("Querying for {} Ios {} placements", insertionOrderIds, placementIds);
      // Using insertionOrderId in the query since the pk starting has ioId before placementId
      List<PlacementCreativeMapping> existingPCMappings =
          placementCreativeMappingRepo.findCurrentPlacementCreativeMappings(
              Dsp.DoubleClick.toString(), insertionOrderIds, placementIds);
      
      LOGGER.info("Found {} mappings from DB", existingPCMappings.size());
      List<PlacementCreativeMapping> updatedList =
          PlacementCreativeMappingUtil.getUpdatedPlacementCreativeMappings(existingPCMappings,
              placementCreativeMappings, lastModified);
      placementCreativeMappingRepo.save(updatedList);
      
    }
    
  }
  
}
