package com.mediaiqdigital.doubleclick.batch.service.impl;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.codehaus.jettison.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.mediaiq.spring.batch.service.impl.AbstractJobLaunchServiceImpl;
import com.mediaiqdigital.aiq.tradingcenter.domain.Campaign;
import com.mediaiqdigital.aiq.tradingcenter.repo.CampaignRepo;
import com.mediaiqdigital.doubleclick.batch.service.DBMBatchJobLaunchService;
import com.mediaiqdigital.doubleclick.batch.service.TradingCenterBatchRestService;
import com.mediaiqdigital.doubleclick.batch.util.JobName;
import com.mediaiqdigital.doubleclick.domain.DbmInsertionOrder;
import com.mediaiqdigital.doubleclick.domain.DbmPlacement;
import com.mediaiqdigital.doubleclick.domain.DbmTimezoneAdvertiser;
import com.mediaiqdigital.doubleclick.repo.DbmInsertionOrderRepo;
import com.mediaiqdigital.doubleclick.repo.DbmPlacementRepo;

@Service
public class DBMBatchJobLaunchServiceImpl extends AbstractJobLaunchServiceImpl
    implements DBMBatchJobLaunchService {
  
  private static final Logger logger = LoggerFactory.getLogger(DBMBatchJobLaunchServiceImpl.class);
  
  @Autowired
  CampaignRepo campaignRepo;
  
  @Autowired
  DbmInsertionOrderRepo dbmInsertionOrderRepo;
  
  @Autowired
  DbmPlacementRepo dbmPlacementRepo;
  
  @Autowired
  TradingCenterBatchRestService tradingCenterBatchRestService;
  
  String startDateParam = "start_date";
  
  String endDateParam = "end_date";
  @Value("${dbmDailyPixelStatsLoadJob.enable}")
  private String dbmDailyPixelStatsLoadJobEnable;
  @Value("${dbmDailyPlacementStatsLoadJob.enable}")
  private String dbmDailyPlacementStatsLoadJobEnable;
  @Value("${dbmDailyTrueViewPlacementStatsLoadJob.enable}")
  private String dbmDailyTrueViewPlacementStatsLoadJobEnable;

  @Value("${datastore.na.directory}")
  private String naReportingDirectory;

  @Value("${datastore.miq.directory}")
  private String miqReportingDirectory;

  @Value("${datastore.na.secret}")
  private String naReportingSecret;

  @Value("${datastore.miq.secret}")
  private String miqReportingSecret;

  @Value("${dfaReports.seat1}")
  private String seat1;

  @Value("${dfaReports.seat2}")
  private String seat2;

  @Scheduled(cron = "0 30 07 * * *")
  @Override
  public void launchSummaryItemJob()
      throws ClassNotFoundException, GeneralSecurityException, JSONException, IOException {

    logger.info("Starting Summary Item load job");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.dbmSummaryLoadJob.name(), jobParametersBuilder.toJobParameters());
  }
  
  @Scheduled(cron = "0 50 07  * * *")
  @Override
  public void launchPlacementLoadJob() {
    logger.info("Starting LineItem load job");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.dbmPlacementLoadJob.name(), jobParametersBuilder.toJobParameters());

  }
  
  @Override
  @Scheduled(cron = "0 32 07 * * *")
  public void launchAdvertiserLoadJob() {

    logger.info("Starting Advertiser load job");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.dbmAdvertiserLoadJob.name(), jobParametersBuilder.toJobParameters());
  }
  
  @Override
  @Scheduled(cron = "0 40 07 * * *")
  public void launchInsertionOrderLoadJob() {

    logger.info("Starting Insertion Order load job");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.dbmInsertionOrderLoadJob.name(), jobParametersBuilder.toJobParameters());
  }
  
  @Override
  // @Scheduled(cron = "0 30 07 * * *")
  public void launchPartnerLoadJob() {

    logger.info("Starting Partner load job");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.dbmPartnerLoadJob.name(), jobParametersBuilder.toJobParameters());
  }
  
  @Override
  @Scheduled(cron = "0 10 08 * * *")
  public void launchPixelLoadJob() {

    logger.info("Starting Pixel load job");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.dbmPixelLoadJob.name(), jobParametersBuilder.toJobParameters());
  }
  
  @Override
  @Scheduled(cron = "0 0 8 * * *")
  public void launchCreativeLoadJob() {

    logger.info("Starting Creative load job");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.dbmCreativeLoadJob.name(), jobParametersBuilder.toJobParameters());
  }
  
  @Override
  @Scheduled(cron = "0 47 07 * * *")
  public void launchBrowserLoadJob() {

    logger.info("Starting Browser load job");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.dbmBrowserLoadJob.name(), jobParametersBuilder.toJobParameters());
  }
  
  @Override
  @Scheduled(cron = "0 17 08 * * *")
  public void launchLanguageLoadJob() {

    logger.info("Starting Language load job");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.dbmLanguageLoadJob.name(), jobParametersBuilder.toJobParameters());
  }
  
  @Override
  @Scheduled(cron = "0 27 08 * * *")
  public void launchDeviceCriteriaLoadJob() {

    logger.info("Starting DeviceCriteria load job");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.dbmDeviceCriteriaLoadJob.name(), jobParametersBuilder.toJobParameters());
  }
  
  @Override
  @Scheduled(cron = "0 37 08 * * *")
  public void launchIspLoadJob() {

    logger.info("Starting Isp load job");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.dbmIspLoadJob.name(), jobParametersBuilder.toJobParameters());
  }
  
  @Override
  @Scheduled(cron = "0 47 08 * * *")
  public void launchGeoLocationLoadJob() {

    logger.info("Starting GeoLocation load job");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.dbmGeoLocationLoadJob.name(), jobParametersBuilder.toJobParameters());
  }
  
  @Override
  @Scheduled(cron = "0 0 8 * * *")
  public void launchLineItemLoadJob() {

    logger.info("Starting LineItem load job");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.dbmLineItemLoadJob.name(), jobParametersBuilder.toJobParameters());
  }
  
  // Without triggering the placement stats processor in tc-batch
  @Override
  @Scheduled(cron = "0 45 0,18 * * ?")
  public void launchDailyPlacementStatsLoadCronJob()
      throws ParseException, ExecutionException, InterruptedException {
    logger.info("daily placement stats  ");
    launchDailyPlacementStatsLoadJob(getPrevDate(1), getFutureDate(1));
    logger.info("DBM raw stats job is complete");
  }
  
  // triggering the placement stats processor in tc-batch
  // once dbm raw is done.
  @Override
  @Scheduled(cron = "0 0 7 * * ?")
  public void launchDailyPlacementStatsAndTriggerTcLoadCronJob()
      throws ParseException, ExecutionException, InterruptedException {
    logger.info("daily placement stats in dbm + tc  ");
    launchDailyPlacementStatsAndTriggerTcLoadCronJob(getPrevDate(1), getFutureDate(1));
    logger.info("DBM raw stats job is complete and triggering is done");
  }
  
  @Override
  public void launchDailyPlacementStatsAndTriggerTcLoadCronJob(DateParameter startDate,
      DateParameter endDate) throws ExecutionException, InterruptedException {
    logger.info("daily placement stats  ");
    launchDailyPlacementStatsLoadJob(startDate, endDate);
    logger.info("DBM raw stats job is complete and triggering placement stats processor");
    tradingCenterBatchRestService.launchPlacementStatsProcessorInTcBatch(startDate.getDate(),
        endDate.getDate());
    logger.info("Triggering done");
  }
  
  @Override
  @Scheduled(cron = "0 36 16 * * ?")
  public void launchDailyTrueViewPlacementStatsLoadCronJob() throws ParseException {
    logger.info("launching daily true view placement stats  ");
    launchDailyTrueViewPlacementStatsLoadJob(getPrevDate(1), getFutureDate(1));

  }
  
  @Override
  // @Scheduled(cron = "0 30 08/8 * * *")
  public void launchDailyPlacementStatsTimezoneLoadCronJob() throws ParseException {

    logger.info("daily placement timezone stats");
    launchDailyPlacementTimeZoneStatsLoadJob(getPrevDate(1), getPrevDate(0));
  }
  
  @Override
  // @Scheduled(cron = "0 0 7 * * *")
  public void launchUniversalSiteLoadJob() {

    logger.info("Starting dbmUniversalSiteLoadJob");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.dbmUniversalSiteLoadJob.name(), jobParametersBuilder.toJobParameters());
  }
  
  @Override
  @Scheduled(cron = "0 20 7 * * *")
  public void launchInventorySourceLoadJob() {

    logger.info("Starting dbmInventorySourceLoadJob");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.dbmInventorySourceLoadJob.name(), jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public void launchDailyInsertionOrderStatsLoadCronJob() throws ParseException {
    logger.info("Starting Stats Load Job");
    launchDailyInsertionOrderStatsLoadJob(getPrevDate(1), getPrevDate(0));
  }
  
  @Override
  public void launchDailyPlacementStatsLoadJob(DateParameter startDate, DateParameter endDate)
      throws InterruptedException, ExecutionException {
    
    if (!Boolean.valueOf(dbmDailyPlacementStatsLoadJobEnable))
      return;
    
    logger.info("Starting Stats Load Job");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    jobParametersBuilder.addDate(startDateParam, startDate.getDate());
    jobParametersBuilder.addDate(endDateParam, endDate.getDate());
    Future<JobExecution> dbmDailyPlacementStatsLoadJob = launchJob(
        JobName.dbmDailyPlacementStatsLoadJob.name(), jobParametersBuilder.toJobParameters());
    dbmDailyPlacementStatsLoadJob.get();
    
  }
  
  @Override
  public void launchDailyTrueViewPlacementStatsLoadJob(DateParameter start_date,
      DateParameter end_date) {
    
    if (!Boolean.valueOf(dbmDailyTrueViewPlacementStatsLoadJobEnable))
      return;
    
    logger.info("Starting True View Stats Load Job");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    jobParametersBuilder.addDate(startDateParam, start_date.getDate());
    jobParametersBuilder.addDate(endDateParam, end_date.getDate());
    launchJob(JobName.dbmDailyTrueViewPlacementStatsLoadJob.name(),
        jobParametersBuilder.toJobParameters());
    
  }
  
  @Override
  @Scheduled(cron = "0 30 0/6 * * ?")
  public void launchDailyPixelLevelStatsLoadJob() throws ParseException {
    
    logger.info("Starting pixel Stats Load Job");
    launchDailyPixelLevelStatsLoadJob(getPrevDate(1), getFutureDate(1));
    
  }
  
  @Override
  public void launchDailyPixelLevelStatsLoadJob(DateParameter start_date, DateParameter end_date) {
    
    logger.info("Starting Stats Load Job");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    jobParametersBuilder.addDate(startDateParam, start_date.getDate());
    jobParametersBuilder.addDate(endDateParam, end_date.getDate());
    launchJob(JobName.dbmDailyPixelLevelStatsLoadJob.name(),
        jobParametersBuilder.toJobParameters());
    
  }
  
  @Override
  public void launchDailyPlacementTimeZoneStatsLoadJob(DateParameter start_date,
      DateParameter end_date) {
    
    logger.info("Starting Stats Load Job");
    new HashMap<String, List<Long>>(0);
    List<Campaign> campaignList = campaignRepo.findByTimezoneIsNotNull();
    
    logger.info(" campaign list " + campaignList);
    if (!campaignList.isEmpty()) {
      
      Set<DbmTimezoneAdvertiser> dbmTimezoneAdvertisers = new HashSet<>();
      campaignList.stream().forEach(list -> {
        dbmTimezoneAdvertisers.addAll(list.getMiqAdvertiser().getDbmAdvertisers());
      });
      logger.info(" dbmTimeAdv " + dbmTimezoneAdvertisers);
      Map<String, Set<Long>> dbmAdvertiserIdsMap = new HashMap<>();
      Map<String, Set<Long>> dbmPlacementIdsMap = new HashMap<>();
      if (!dbmTimezoneAdvertisers.isEmpty()) {
        dbmTimezoneAdvertisers.forEach((k) -> {
          Set<Long> advIds;
          advIds = dbmAdvertiserIdsMap.get(k.getTimezone());
          if (null == advIds)
            advIds = new HashSet<Long>();
          advIds.add(k.getDbmAdvertiser().getId());
          dbmAdvertiserIdsMap.put(k.getTimezone(), advIds);
        });
      }
      
      logger.info(" dbmAdvIdsMap " + dbmAdvertiserIdsMap);
      if (!dbmAdvertiserIdsMap.isEmpty()) {
        for (String timezone : dbmAdvertiserIdsMap.keySet()) {
          Set<Long> ioIds = new HashSet<>();
          Set<Long> placementIds = new HashSet<>();
          Set<Long> advIds = dbmAdvertiserIdsMap.get(timezone);
          Set<DbmInsertionOrder> dbmInsertionOrderSet =
              dbmInsertionOrderRepo.findByAdvertiserIdIn(advIds);
          if (!dbmInsertionOrderSet.isEmpty()) {
            dbmInsertionOrderSet.stream().forEach((k) -> {
              ioIds.add(k.getId());
            });
            
            logger.info("for timezone ===== " + timezone + " ioIDs " + ioIds);
            Set<DbmPlacement> dbmPlacements = dbmPlacementRepo.findByInsertionOrderIdIn(ioIds);
            if (!dbmPlacements.isEmpty()) {
              dbmPlacements.stream().forEach((k) -> {
                placementIds.add(k.getId());
                
              });
              logger.info("for timezone ===== " + timezone + " placementIDs " + placementIds);
              if (!placementIds.isEmpty()) {
                dbmPlacementIdsMap.put(timezone, placementIds);
              }
              logger.info(" placement Map " + dbmPlacementIdsMap);
            }
          }
        }
      }
      if (!dbmPlacementIdsMap.isEmpty()) {
        dbmPlacementIdsMap.entrySet().stream().forEach((k) -> {
          Future<JobExecution> triggerJob =
              triggerJob(start_date, end_date, k.getKey(), k.getValue());
          try {
            triggerJob.get();
          } catch (Exception e) {
            e.printStackTrace();
          }
        });
      }
      
    }
  }
  
  public Future<JobExecution> triggerJob(DateParameter startDate, DateParameter endDate,
      String timezone, Set<Long> placementIds) {
    
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    jobParametersBuilder.addDate(startDateParam, startDate.getDate());
    jobParametersBuilder.addDate(endDateParam, endDate.getDate());
    jobParametersBuilder.addString("placementIds", placementIds.toString());
    jobParametersBuilder.addString("timezone", timezone);
    return launchJob(JobName.dbmDailyPlacementTimezoneStatsLoadJob.name(),
        jobParametersBuilder.toJobParameters());
  }
  // launchJob(JobName.dbmDailyPlacementTimeZoneStatsLoadJob.name(),jobParametersBuilder.toJobParameters());
  
  
  @Override
  public void launchDailyInsertionOrderStatsLoadJob(DateParameter start_date,
      DateParameter end_date) {
    
    logger.info("Starting Stats Load Job");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    jobParametersBuilder.addDate(startDateParam, start_date.getDate());
    jobParametersBuilder.addDate(endDateParam, end_date.getDate());
    launchJob(JobName.dbmDailyInsertionOrderStatsLoadJob.name(),
        jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public void launchDailyIOPlacementStatsLoadJob(DateParameter start_date, DateParameter end_date) {
    
    logger.info("Starting Stats Load Job");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    jobParametersBuilder.addDate(startDateParam, start_date.getDate());
    jobParametersBuilder.addDate(endDateParam, end_date.getDate());
    launchJob(JobName.dbmDailyIOPlacementStatsLoadJob.name(),
        jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public void launchDailyPixelStatsLoadJob(DateParameter start_date, DateParameter end_date) {
    
    if (!Boolean.valueOf(dbmDailyPixelStatsLoadJobEnable))
      return;
    
    logger.info("starting Pixel StatsLoadJob");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    jobParametersBuilder.addDate(startDateParam, start_date.getDate());
    jobParametersBuilder.addDate(endDateParam, end_date.getDate());
    launchJob(JobName.dbmDailyPixelStatsLoadJob.name(), jobParametersBuilder.toJobParameters());
    
  }
  
  @Override
  public void uploadFileLoadJob() {
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.dbmDailyPlacementStatsPersistLoadJob.name(),
        jobParametersBuilder.toJobParameters());
  }
  
  // @Scheduled(cron = "0 40 07 * * *")
  public void launchDailyPixelStatsCronJob() throws ParseException {
    launchDailyPixelStatsLoadJob(getPrevDate(1), getPrevDate(1));
  }
  
  private DateParameter getPrevDate(int days) throws ParseException {
    String DATE_FORMAT = "yyyy-MM-dd";
    DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
    Calendar cal = Calendar.getInstance();
    cal.add(Calendar.DATE, -days);
    DateParameter dateParameter = new DateParameter(dateFormat.format(cal.getTime()));
    
    return dateParameter;
  }
  
  private DateParameter getFutureDate(int days) throws ParseException {
    String DATE_FORMAT = "yyyy-MM-dd";
    DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
    Calendar cal = Calendar.getInstance();
    cal.add(Calendar.DATE, +days);
    DateParameter dateParameter = new DateParameter(dateFormat.format(cal.getTime()));
    
    return dateParameter;
  }
  
  @Override
  @Scheduled(cron = "0 47 08 * * *")
  public void launchExchangeLoadJob() {
    logger.info("Starting Exchange load job");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.dbmExchangeLoadJob.name(), jobParametersBuilder.toJobParameters());
  }
  
  @Override
  @Scheduled(cron = "0 30 8 * * ?")
  public void launchPlacementCreativeMappingLoadJob() {
    logger.info("Starting DBM placement creative mapping load job Scheduled");
    launchPlacementCreativeMappingLoadJob(null);
  }
  
  @Override
  public void launchPlacementCreativeMappingLoadJob(DateParameter lastModified) {
    logger.info("Starting DBM placement creative mapping load job");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    if (lastModified != null) {
      jobParametersBuilder.addDate("last_modified", lastModified.getDate());
    }
    else{
      try {
        jobParametersBuilder.addDate("last_modified",getPrevDate(1).getDate());
      } catch (ParseException e) {
        e.printStackTrace();
      }
    }
    launchJob(JobName.dbmPlacementCreativeMappingLoadJob.name(),
        jobParametersBuilder.toJobParameters());
  }
  
  @Override
  @Scheduled(cron = "0 30 1 * * ?")
  public void launchDfaProfileForSeat1LoadJob() {
    logger.info("Starting DFA Profile load job");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    logger.info("new data directory {}",naReportingDirectory);
    jobParametersBuilder.addString("directory",naReportingDirectory);
    jobParametersBuilder.addString("secret",naReportingSecret);
    jobParametersBuilder.addString("seat",seat1);
    launchJob(JobName.dfaProfileLoadJob.name(), jobParametersBuilder.toJobParameters());
  }

  @Override
  @Scheduled(cron = "0 0 2 * * ?")
  public void launchDfaProfileForSeat2LoadJob() {
    logger.info("Starting DFA Profile load job");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    jobParametersBuilder.addString("directory",miqReportingDirectory);
    jobParametersBuilder.addString("secret",miqReportingSecret);
    jobParametersBuilder.addString("seat",seat2);
    launchJob(JobName.dfaProfileLoadJob.name(), jobParametersBuilder.toJobParameters());
  }
  
}
