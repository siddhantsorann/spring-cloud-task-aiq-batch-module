package com.mediaiqdigital.doubleclick.batch.tasklet.report;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.google.api.client.googleapis.media.MediaHttpDownloader;
import com.google.api.client.http.GenericUrl;
import com.google.api.services.doubleclickbidmanager.DoubleClickBidManager;
import com.google.api.services.doubleclickbidmanager.model.Query;
import com.google.api.services.doubleclickbidmanager.model.RunQueryRequest;
import com.mediaiqdigital.doubleclick.batch.util.SecurityUtilities;
import com.mediaiqdigital.doubleclick.batch.util.StepAttribute;

/**
 * Created by piyush on 9/2/16.
 */
@Component
@Scope("step")
public class PlacementCreativeReportDownloadTasklet implements Tasklet {
  
  
  final static Logger logger =
      LoggerFactory.getLogger(PlacementCreativeReportDownloadTasklet.class);
  @Value("#{jobParameters['start_date']}")
  protected Date startDate;
  @Value("#{jobParameters['end_date']}")
  protected Date endDate;
  @Value("${report.statsFile}")
  String basePath;
  @Autowired
  DoubleClickBidManager dbm;
  
  @Value("#{new java.lang.Long('${report.placementQueryId}')}")
  private Long queryId;
  
  @Override
  public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext)
      throws Exception {
    
    logger.info(" report part ddsds ");
    System.out.println(basePath);
    
    
    logger.info(" *********** chch *****");
    
    System.out.println(" start time " + startDate.getTime());
    System.out.println(" end time " + endDate.getTime());
    
    RunQueryRequest runQueryRequest = new RunQueryRequest();
    runQueryRequest.setDataRange("CUSTOM_DATES");
    runQueryRequest.setReportDataStartTimeMs(startDate.getTime());
    runQueryRequest.setReportDataEndTimeMs(endDate.getTime());
    dbm.queries().runquery(queryId, runQueryRequest).execute();
    
    Query queryResponse = dbm.queries().getquery(queryId).execute();
    System.out.println(" queryresp " + queryResponse);
    // Grab the report.
    GenericUrl reportPath =
        new GenericUrl(queryResponse.getMetadata().getGoogleCloudStoragePathForLatestReport());
    
    String fileTarget =
        basePath + System.getProperty("file.separator") + "DailyPlacementStats" + ".csv";
    OutputStream out = new FileOutputStream(new File(fileTarget));
    System.out.println(out.toString());
    MediaHttpDownloader downloader =
        new MediaHttpDownloader(SecurityUtilities.getHttpTransport(), null);
    downloader.download(reportPath, out);
    System.out.println("Download complete.");
    ExecutionContext executionContext =
        chunkContext.getStepContext().getStepExecution().getJobExecution().getExecutionContext();
    
    System.out.println(" file target path " + fileTarget);
    executionContext.put(StepAttribute.FILE_PATH, fileTarget);
    
    logger.info("Report {} downloaded.", "report 1");
    return RepeatStatus.FINISHED;
  }
  
}
