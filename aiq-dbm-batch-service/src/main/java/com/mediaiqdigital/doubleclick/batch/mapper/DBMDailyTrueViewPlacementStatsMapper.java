package com.mediaiqdigital.doubleclick.batch.mapper;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import com.mediaiqdigital.doubleclick.Id.DailyPlacementId;
import com.mediaiqdigital.doubleclick.domain.stats.DbmPlacementCreativeStats;

/**
 * Created by prasannachidire on 10-Jul-2017
 */
public class DBMDailyTrueViewPlacementStatsMapper
    implements FieldSetMapper<DbmPlacementCreativeStats> {
  
  public DBMDailyTrueViewPlacementStatsMapper(Date startDate, Date endDate) {}
  
  
  @Override
  public DbmPlacementCreativeStats mapFieldSet(FieldSet set) throws BindException {
    DbmPlacementCreativeStats stats = new DbmPlacementCreativeStats();
    
    
    Long lineItemId = set.readLong("Line Item ID");
    Integer timeOfDay = set.readInt("Time of Day");
    Date date = set.readDate("Date", "yyyy/MM/dd");
    Date dateTime = set.readDate("DateTime", "yyyy-MM-dd HH:mm:ss");
    // Entering placementId as the creative id since true view placements dont have any creatives
    stats.setId(new DailyPlacementId(lineItemId, lineItemId, date, timeOfDay));
    stats.setDateTime(dateTime);
    stats.setClicks(set.readLong("Clicks"));
    stats.setImpressions(set.readLong("Impressions"));
    stats.setTotalConversions(set.readLong("TrueView: Views"));
    stats.setCtr(set.readDouble("Click Rate (CTR)"));
    // Setting some default timezone since dbm doesnt provide advertiser timezone in the reports
    stats.setAdvertiserTimeZone("Europe/London");
    stats.setCurrency(set.readString("Advertiser Currency"));
    stats.setInsertionOrderId(set.readLong("Insertion Order ID"));
    String totalSpend = set.readString("Total Media Cost (Advertiser Currency)");
    // String spend=totalSpend.substring(1,totalSpend.length());
    String[] currencies = {"$", "€", "UK£", "CA", "A", "NZ", "IDR", "CHF", "S", ","};
    
    totalSpend = StringUtils.replaceEach(totalSpend, currencies,
        new String[] {"", "", "", "", "", "", "", "", "", ""});
    stats.setTotal_spend(Double.parseDouble(totalSpend));
    
    return stats;
  }
}
