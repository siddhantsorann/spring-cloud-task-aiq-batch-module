package com.mediaiqdigital.doubleclick.batch.tasklet;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.storage.Storage;
import com.google.api.services.storage.Storage.Objects.Get;
import com.mediaiqdigital.doubleclick.batch.util.Constants;
import com.mediaiqdigital.doubleclick.batch.util.DateFormatUtil;
import com.mediaiqdigital.doubleclick.batch.util.FileDownloadUtil;
import com.mediaiqdigital.doubleclick.batch.util.SecurityCredentials;
import com.mediaiqdigital.doubleclick.domain.DbmSummaryItems;
import com.mediaiqdigital.doubleclick.repo.DbmSummaryItemsRepo;

/**
 * Created by piyush on 20/11/15.
 */
@Component
@Scope("step")
public class SummaryDownloadTasklet implements Tasklet {
  
  static final NetHttpTransport HTTP_TRANSPORT = new NetHttpTransport();
  
  static final JacksonFactory JSON_FACTORY = new JacksonFactory();
  
  private static final Logger LOGGER = LoggerFactory.getLogger(SummaryDownloadTasklet.class);
  @Autowired
  DbmSummaryItemsRepo summaryItemsRepo;
  String[] items = {"Advertiser", "InventorySource", "Partner", "Pixel", "Creative",
      "InsertionOrder", "UserList", "UniversalChannel", "LineItem", "Language", "Isp", "Browser",
      "DeviceCriteria", "GeoLocation", "SupportedExchange", "UniversalSiteLoadTasklet",
      "DataPartner"};
  @Value("#{jobParameters['date']}")
  private Date date;
  @Value("${report.file}")
  private String filePath;
  @Value("${google.serviceAccount}")
  private String serviceAccount;
  @Value("${privateId}")
  private String privateId;
  
  @SuppressWarnings("unused")
  public RepeatStatus execute(StepContribution contribution, ChunkContext context)
      throws Exception {

    Storage storage = SecurityCredentials.getCredentials(filePath, serviceAccount);

    String current_date = DateFormatUtil.format(new Date());

    Calendar cal = Calendar.getInstance();
    cal.add(Calendar.DATE, -7);
    Date todate1 = cal.getTime();

    Get get = storage.objects().get(privateId, "entity/" + current_date + ".0.summary.json");

    Get get1 =
        storage.objects().get(Constants.GDBM_PUBLIC, "entity/" + current_date + ".0.summary.json");

    // TODO for multiple summary files
    try {
      ByteArrayOutputStream out = FileDownloadUtil.executeAndReturnByteStream(get);
      convertFromBytes(out.toString());
    } catch (GoogleJsonResponseException e) {
      LOGGER.error("Error in fetching private entity summary file", e);
    }

    try {
      ByteArrayOutputStream out1 = FileDownloadUtil.executeAndReturnByteStream(get1);
      convertFromBytes(out1.toString());
    } catch (GoogleJsonResponseException e) {
      LOGGER.error("Error in fetching public entity summary file", e);
    }

    return RepeatStatus.FINISHED;
  }
  
  private void convertFromBytes(String bytes)
      throws IOException, ClassNotFoundException, JSONException {

    JSONObject jsonObject = new JSONObject(bytes);
    JSONArray jsonArray = jsonObject.getJSONArray("file");

    for (int index = 0; index < jsonArray.length(); index++) {
      JSONObject summaryEntry = jsonArray.getJSONObject(index);
      String fileName = summaryEntry.getString("name");
      storeFiles(fileName);
    }

  }
  
  private void storeFiles(String data) {
    LOGGER.info(" data " + data);
    for (String item : items) {
      if (data.contains(item))
        summaryItemsRepo.save(new DbmSummaryItems(item, data));
    }
    
  }
}
