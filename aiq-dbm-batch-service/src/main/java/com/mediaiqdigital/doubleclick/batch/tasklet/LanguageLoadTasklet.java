package com.mediaiqdigital.doubleclick.batch.tasklet;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.LinkedList;
import java.util.List;

import org.codehaus.jettison.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.storage.Storage;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.mediaiqdigital.doubleclick.batch.util.Constants;
import com.mediaiqdigital.doubleclick.batch.util.FileDownloadUtil;
import com.mediaiqdigital.doubleclick.batch.util.SecurityCredentials;
import com.mediaiqdigital.doubleclick.domain.DbmLanguage;
import com.mediaiqdigital.doubleclick.domain.DbmSummaryItems;
import com.mediaiqdigital.doubleclick.repo.DbmLanguageRepo;
import com.mediaiqdigital.doubleclick.repo.DbmSummaryItemsRepo;

@Component
@Scope("step")
public class LanguageLoadTasklet implements Tasklet {
  
  static final NetHttpTransport HTTP_TRANSPORT = new NetHttpTransport();
  
  static final JacksonFactory JSON_FACTORY = new JacksonFactory();
  
  private static final Logger LOGGER = LoggerFactory.getLogger(LanguageLoadTasklet.class);
  
  @Autowired
  DbmSummaryItemsRepo summaryItemsRepo;
  
  @Autowired
  DbmLanguageRepo languageRepo;
  
  @Value("${report.file}")
  private String filePath;
  
  @Value("${google.serviceAccount}")
  private String serviceAccount;
  
  @Override
  public RepeatStatus execute(StepContribution contribution, ChunkContext context) throws Exception,
      GeneralSecurityException, IOException, ClassNotFoundException, JSONException {
    
    Storage storage = SecurityCredentials.getCredentials(filePath, serviceAccount);
    
    List<DbmSummaryItems> summaryItemsList = summaryItemsRepo.findByItemName("Language");
    
    for (DbmSummaryItems summaryItems : summaryItemsList) {
      
      String fileName = summaryItems.getFileName();
      LOGGER.info("FileName is " + fileName);
      Storage.Objects.Get get = storage.objects().get(Constants.GDBM_PUBLIC, "entity/" + fileName);
      JsonArray jsonArray = FileDownloadUtil.execute(get);
      Gson gson = new Gson();
      List<DbmLanguage> languages = new LinkedList<>();
      for (int i = 0; i < jsonArray.size(); i++) {
        JsonObject str = jsonArray.get(i).getAsJsonObject();
        DbmLanguage obj = gson.fromJson(str, DbmLanguage.class);
        obj.setId(obj.getId());
        obj.setCode(obj.getCode());
        
        LOGGER.info(" Object is " + obj);
        LOGGER.info(" String is " + str);
        LOGGER.info("-------");
        languages.add(obj);
      }
      
      languageRepo.save(languages);
    }
    
    return RepeatStatus.FINISHED;
    
  }
  
}
