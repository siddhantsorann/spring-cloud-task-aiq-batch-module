package com.mediaiqdigital.doubleclick.batch.processor;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.mediaiqdigital.doubleclick.domain.DbmLineItem;

/**
 * @author prasannachidire on 6/2/18.
 */

@Component
public class DbmLineItemProcessor implements ItemProcessor<String, DbmLineItem> {
  
  @Override
  public DbmLineItem process(String dbmLineItemJson) throws Exception {
    
    Gson gson = new Gson();
    DbmLineItem dbmLineItem = gson.fromJson(dbmLineItemJson, DbmLineItem.class);
    dbmLineItem.setId(dbmLineItem.getCommonData().getId());
    dbmLineItem.setName(dbmLineItem.getCommonData().getName());
    dbmLineItem.setIntegrationCode(dbmLineItem.getCommonData().getIntegrationCode());
    dbmLineItem.setActive(dbmLineItem.getCommonData().getActive());
    return dbmLineItem;
  }
  
}
