package com.mediaiqdigital.doubleclick.batch.tasklet.report.domain.ReportWithoutTimezone;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiqdigital.doubleclick.batch.util.StepAttribute;

/**
 * Created by piyush on 4/8/16.
 */
@Component
@Scope("step")
public class ReportPersistTasklet implements Tasklet {
  
  final static Logger logger = LoggerFactory.getLogger(ReportPersistTasklet.class);
  
  
  @Value("${report.statsFile}")
  String basePath;
  
  @Override
  public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext)
      throws IOException {
    
    String fileTarget =
        basePath + System.getProperty("file.separator") + "DailyPlacementStats" + ".csv";
    ExecutionContext executionContext =
        chunkContext.getStepContext().getStepExecution().getJobExecution().getExecutionContext();
    
    System.out.println(" file target path " + fileTarget);
    executionContext.put(StepAttribute.FILE_PATH, fileTarget);
    
    logger.info("Report {} downloaded.", "report 1");
    
    return RepeatStatus.FINISHED;
  }
}
