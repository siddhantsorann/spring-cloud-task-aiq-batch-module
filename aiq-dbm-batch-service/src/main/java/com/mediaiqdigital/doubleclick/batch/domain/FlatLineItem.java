package com.mediaiqdigital.doubleclick.batch.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.mediaiqdigital.doubleclick.batch.domain.id.FlatLineItemId;

@Entity
@Table(name = "flat_line_item", catalog = "doubleclick")
public class FlatLineItem {
  
  @EmbeddedId
  private FlatLineItemId id;
  
  private String partnerName;
  private String advertiserName;
  private String ioName;
  private String lineItemName;
  private String lineItemTimestamp;
  private String lineItemStatus;
  private Date ioStartDate;
  private Date ioEndDate;
  private String ioBudgetType;
  private String ioBudgetAmount;
  private String ioPacing;
  private String ioPacingRate;
  private String ioPacingAmount;
  private String lineItemStartDate;
  private String lineItemEndDate;
  private String lineItemBudgetType;
  private String lineItemBudgetAmount;
  private String lineItemPacing;
  private String lineItemPacingRate;
  private String lineItemPacingAmount;
  private String lineItemFrequencyEnabled;
  private String lineItemFrequencyExposures;
  private String lineItemFrequencyPeriod;
  private String lineItemFrequencyAmount;
  private String bidPrice;
  private String partnerRevenueModel;
  private String partnerRevenueAmount;
  private String currentAudienceTargetingIds;
  
  @Column(length = 10000)
  private String currentAudienceTargetingNames;
  
  public FlatLineItem(Long lineItemId, String partnerName, Long partnerId, String advertiserName,
      String ioName, String lineItemName, String lineItemTimestamp, String lineItemStatus,
      Date ioStartDate, Date ioEndDate, String ioBudgetType, String ioBudgetAmount, String ioPacing,
      String ioPacingRate, String ioPacingAmount, String lineItemStartDate, String lineItemEndDate,
      String lineItemBudgetType, String lineItemBudgetAmount, String lineItemPacing,
      String lineItemPacingRate, String lineItemPacingAmount, String lineItemFrequencyEnabled,
      String lineItemFrequencyExposures, String lineItemFrequencyPeriod,
      String lineItemFrequencyAmount, String bidPrice, String partnerRevenueModel,
      String partnerRevenueAmount, String currentAudienceTargetingIds,
      String currentAudienceTargetingNames) {
    super();
    this.id = new FlatLineItemId();
    this.id.setLineItemId(lineItemId);
    this.id.setPartnerId(partnerId);


    this.partnerName = partnerName;
    this.advertiserName = advertiserName;
    this.ioName = ioName;
    this.lineItemName = lineItemName;
    this.lineItemTimestamp = lineItemTimestamp;
    this.lineItemStatus = lineItemStatus;
    this.ioStartDate = ioStartDate;
    this.ioEndDate = ioEndDate;
    this.ioBudgetType = ioBudgetType;
    this.ioBudgetAmount = ioBudgetAmount;
    this.ioPacing = ioPacing;
    this.ioPacingRate = ioPacingRate;
    this.ioPacingAmount = ioPacingAmount;
    this.lineItemStartDate = lineItemStartDate;
    this.lineItemEndDate = lineItemEndDate;
    this.lineItemBudgetType = lineItemBudgetType;
    this.lineItemBudgetAmount = lineItemBudgetAmount;
    this.lineItemPacing = lineItemPacing;
    this.lineItemPacingRate = lineItemPacingRate;
    this.lineItemPacingAmount = lineItemPacingAmount;
    this.lineItemFrequencyEnabled = lineItemFrequencyEnabled;
    this.lineItemFrequencyExposures = lineItemFrequencyExposures;
    this.lineItemFrequencyPeriod = lineItemFrequencyPeriod;
    this.lineItemFrequencyAmount = lineItemFrequencyAmount;
    this.bidPrice = bidPrice;
    this.partnerRevenueModel = partnerRevenueModel;
    this.partnerRevenueAmount = partnerRevenueAmount;
    this.currentAudienceTargetingIds = currentAudienceTargetingIds;
    this.currentAudienceTargetingNames = currentAudienceTargetingNames;
  }
  
  public FlatLineItem() {}
  
  public String getPartnerName() {
    return partnerName;
  }
  
  public void setPartnerName(String partnerName) {
    this.partnerName = partnerName;
  }
  
  public String getAdvertiserName() {
    return advertiserName;
  }
  
  public void setAdvertiserName(String advertiserName) {
    this.advertiserName = advertiserName;
  }
  
  public String getIoName() {
    return ioName;
  }
  
  public void setIoName(String ioName) {
    this.ioName = ioName;
  }
  
  public String getLineItemName() {
    return lineItemName;
  }
  
  public void setLineItemName(String lineItemName) {
    this.lineItemName = lineItemName;
  }
  
  public String getLineItemTimestamp() {
    return lineItemTimestamp;
  }
  
  public void setLineItemTimestamp(String lineItemTimestamp) {
    this.lineItemTimestamp = lineItemTimestamp;
  }
  
  public String getLineItemStatus() {
    return lineItemStatus;
  }
  
  public void setLineItemStatus(String lineItemStatus) {
    this.lineItemStatus = lineItemStatus;
  }
  
  public Date getIoStartDate() {
    return ioStartDate;
  }
  
  public void setIoStartDate(Date ioStartDate) {
    this.ioStartDate = ioStartDate;
  }
  
  public Date getIoEndDate() {
    return ioEndDate;
  }
  
  public void setIoEndDate(Date ioEndDate) {
    this.ioEndDate = ioEndDate;
  }
  
  public String getIoBudgetType() {
    return ioBudgetType;
  }
  
  public void setIoBudgetType(String ioBudgetType) {
    this.ioBudgetType = ioBudgetType;
  }
  
  public String getIoBudgetAmount() {
    return ioBudgetAmount;
  }
  
  public void setIoBudgetAmount(String ioBudgetAmount) {
    this.ioBudgetAmount = ioBudgetAmount;
  }
  
  public String getIoPacing() {
    return ioPacing;
  }
  
  public void setIoPacing(String ioPacing) {
    this.ioPacing = ioPacing;
  }
  
  public String getIoPacingRate() {
    return ioPacingRate;
  }
  
  public void setIoPacingRate(String ioPacingRate) {
    this.ioPacingRate = ioPacingRate;
  }
  
  public String getIoPacingAmount() {
    return ioPacingAmount;
  }
  
  public void setIoPacingAmount(String ioPacingAmount) {
    this.ioPacingAmount = ioPacingAmount;
  }
  
  public String getLineItemStartDate() {
    return lineItemStartDate;
  }
  
  public void setLineItemStartDate(String lineItemStartDate) {
    this.lineItemStartDate = lineItemStartDate;
  }
  
  public String getLineItemEndDate() {
    return lineItemEndDate;
  }
  
  public void setLineItemEndDate(String lineItemEndDate) {
    this.lineItemEndDate = lineItemEndDate;
  }
  
  public String getLineItemBudgetType() {
    return lineItemBudgetType;
  }
  
  public void setLineItemBudgetType(String lineItemBudgetType) {
    this.lineItemBudgetType = lineItemBudgetType;
  }
  
  public String getLineItemBudgetAmount() {
    return lineItemBudgetAmount;
  }
  
  public void setLineItemBudgetAmount(String lineItemBudgetAmount) {
    this.lineItemBudgetAmount = lineItemBudgetAmount;
  }
  
  public String getLineItemPacing() {
    return lineItemPacing;
  }
  
  public void setLineItemPacing(String lineItemPacing) {
    this.lineItemPacing = lineItemPacing;
  }
  
  public String getLineItemPacingRate() {
    return lineItemPacingRate;
  }
  
  public void setLineItemPacingRate(String lineItemPacingRate) {
    this.lineItemPacingRate = lineItemPacingRate;
  }
  
  public String getLineItemPacingAmount() {
    return lineItemPacingAmount;
  }
  
  public void setLineItemPacingAmount(String lineItemPacingAmount) {
    this.lineItemPacingAmount = lineItemPacingAmount;
  }
  
  public String getLineItemFrequencyEnabled() {
    return lineItemFrequencyEnabled;
  }
  
  public void setLineItemFrequencyEnabled(String lineItemFrequencyEnabled) {
    this.lineItemFrequencyEnabled = lineItemFrequencyEnabled;
  }
  
  public String getLineItemFrequencyExposures() {
    return lineItemFrequencyExposures;
  }
  
  public void setLineItemFrequencyExposures(String lineItemFrequencyExposures) {
    this.lineItemFrequencyExposures = lineItemFrequencyExposures;
  }
  
  public String getLineItemFrequencyPeriod() {
    return lineItemFrequencyPeriod;
  }
  
  public void setLineItemFrequencyPeriod(String lineItemFrequencyPeriod) {
    this.lineItemFrequencyPeriod = lineItemFrequencyPeriod;
  }
  
  public String getLineItemFrequencyAmount() {
    return lineItemFrequencyAmount;
  }
  
  public void setLineItemFrequencyAmount(String lineItemFrequencyAmount) {
    this.lineItemFrequencyAmount = lineItemFrequencyAmount;
  }
  
  public String getBidPrice() {
    return bidPrice;
  }
  
  public void setBidPrice(String bidPrice) {
    this.bidPrice = bidPrice;
  }
  
  public String getPartnerRevenueModel() {
    return partnerRevenueModel;
  }
  
  public void setPartnerRevenueModel(String partnerRevenueModel) {
    this.partnerRevenueModel = partnerRevenueModel;
  }
  
  public String getPartnerRevenueAmount() {
    return partnerRevenueAmount;
  }
  
  public void setPartnerRevenueAmount(String partnerRevenueAmount) {
    this.partnerRevenueAmount = partnerRevenueAmount;
  }
  
  public String getCurrentAudienceTargetingIds() {
    return currentAudienceTargetingIds;
  }
  
  public void setCurrentAudienceTargetingIds(String currentAudienceTargetingIds) {
    this.currentAudienceTargetingIds = currentAudienceTargetingIds;
  }
  
  public String getCurrentAudienceTargetingNames() {
    return currentAudienceTargetingNames;
  }
  
  public void setCurrentAudienceTargetingNames(String currentAudienceTargetingNames) {
    this.currentAudienceTargetingNames = currentAudienceTargetingNames;
  }
}
