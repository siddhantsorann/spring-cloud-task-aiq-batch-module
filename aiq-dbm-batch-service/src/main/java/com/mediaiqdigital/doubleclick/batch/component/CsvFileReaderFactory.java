package com.mediaiqdigital.doubleclick.batch.component;

import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

import com.mediaiqdigital.doubleclick.batch.domain.FlatLineItem;
import com.mediaiqdigital.doubleclick.batch.mapper.FlatLineItemMapper;
import com.mediaiqdigital.doubleclick.batch.util.StepAttribute;

@Configuration
public class CsvFileReaderFactory {
  
  @Bean(name = "lineItemReader")
  @Scope("job")
  public FlatFileItemReader<FlatLineItem> provideT(
      @Value("#{jobExecutionContext['" + StepAttribute.FILE_PATH + "']}") String filePath) {
    String[] names = {"Line Item Id", "Partner Name", "Partner Id", "Advertiser Name", "Io Name",
        "Line Item Name", "Line Item Timestamp", "Line Item Status", "Io Start Date", "Io End Date",
        "Io Budget Type", "Io Budget Amount", "Io Pacing", "Io Pacing Rate", "Io Pacing Amount",
        "Line Item Start Date", "Line Item End Date", "Line Item Budget Type",
        "Line Item Budget Amount", "Line Item Pacing", "Line Item Pacing Rate",
        "Line Item Pacing Amount", "Line Item Frequency Enabled", "Line Item Frequency Exposures",
        "Line Item Frequency Period", "Line Item Frequency Amount", "Bid Price",
        "Partner Revenue Model", "Partner Revenue Amount", "Current Audience Targeting Ids",
        "Current Audience Targeting Names"};
    
    FlatFileItemReader<FlatLineItem> reader = new FlatFileItemReader<>();
    DefaultLineMapper<FlatLineItem> mapper = new DefaultLineMapper<>();
    FlatLineItemMapper fieldSetMapper = new FlatLineItemMapper();
    commonProvider(names, reader, filePath, mapper, fieldSetMapper);
    return reader;
  }
  
  @SuppressWarnings({"rawtypes", "unchecked"})
  private void commonProvider(String[] names, FlatFileItemReader reader, String filePath,
      DefaultLineMapper mapper, FieldSetMapper fieldSetMapper) {
    System.out.println(filePath);
    Resource resource = new FileSystemResource(filePath);
    reader.setResource(resource);
    reader.setLinesToSkip(1);
    DelimitedLineTokenizer tokenizer = new DelimitedLineTokenizer();
    tokenizer.setNames(names);
    mapper.setLineTokenizer(tokenizer);
    mapper.setFieldSetMapper(fieldSetMapper);
    reader.setLineMapper(mapper);
  }
  
}
