package com.mediaiqdigital.doubleclick.batch.domain.id;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class FlatLineItemId implements Serializable {
  
  private static final long serialVersionUID = -671174627695378222L;
  
  private Long lineItemId;
  private Long partnerId;
  
  public Long getLineItemId() {
    return lineItemId;
  }
  
  public void setLineItemId(Long lineItemId) {
    this.lineItemId = lineItemId;
  }
  
  public Long getPartnerId() {
    return partnerId;
  }
  
  public void setPartnerId(Long partnerId) {
    this.partnerId = partnerId;
  }
}
