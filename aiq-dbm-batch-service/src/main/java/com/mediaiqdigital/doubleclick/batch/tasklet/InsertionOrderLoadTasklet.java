package com.mediaiqdigital.doubleclick.batch.tasklet;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.codehaus.jettison.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.storage.Storage;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.mediaiq.aiq.domain.Currency;
import com.mediaiq.spring.batch.aspect.Retry;
import com.mediaiqdigital.doubleclick.batch.util.FileDownloadUtil;
import com.mediaiqdigital.doubleclick.batch.util.SecurityCredentials;
import com.mediaiqdigital.doubleclick.domain.DbmInsertionOrder;
import com.mediaiqdigital.doubleclick.domain.DbmSummaryItems;
import com.mediaiqdigital.doubleclick.repo.DbmAdvertiserRepo;
import com.mediaiqdigital.doubleclick.repo.DbmInsertionOrderRepo;
import com.mediaiqdigital.doubleclick.repo.DbmSummaryItemsRepo;
import com.mediaiqdigital.doubleclick.value.State;

/**
 * Created by piyush on 30/11/15.
 */
@Component
public class InsertionOrderLoadTasklet implements Tasklet {
  
  static final NetHttpTransport HTTP_TRANSPORT = new NetHttpTransport();
  
  static final JacksonFactory JSON_FACTORY = new JacksonFactory();
  
  private static final Logger LOGGER = LoggerFactory.getLogger(InsertionOrderLoadTasklet.class);
  
  @Autowired
  DbmSummaryItemsRepo summaryItemsRepo;
  
  @Autowired
  DbmInsertionOrderRepo insertionOrderRepo;
  
  @Autowired
  DbmAdvertiserRepo dbmAdvertiserRepo;
  
  @Value("${report.file}")
  private String filePath;
  
  @Value("${google.serviceAccount}")
  private String serviceAccount;
  
  @Value("${privateId}")
  private String privateId;
  
  @Retry
  @Override
  public RepeatStatus execute(StepContribution contribution, ChunkContext context) throws Exception,
      GeneralSecurityException, IOException, ClassNotFoundException, JSONException {
    
    
    
    List<DbmInsertionOrder> dbmInsertionOrderList = insertionOrderRepo.findAll();
    Map<Long, DbmInsertionOrder> dbmIoMap = new HashMap<>();
    Map<Long, String> advertiserToTimezoneMap = new HashMap<>();
    dbmInsertionOrderList.stream().forEach((k) -> {
      dbmIoMap.put(k.getId(), k);
      if (k.getMiqTimezone() != null)
        advertiserToTimezoneMap.put(k.getAdvertiserId(), k.getMiqTimezone());
    });
    
    
    
    Storage storage = SecurityCredentials.getCredentials(filePath, serviceAccount);
    
    List<DbmSummaryItems> summaryItemsList = summaryItemsRepo.findByItemName("InsertionOrder");
    
    for (DbmSummaryItems summaryItems : summaryItemsList) {
      
      
      String fileName = summaryItems.getFileName();
      LOGGER.info("FileName is " + fileName);
      Storage.Objects.Get get = storage.objects().get(privateId, "entity/" + fileName);
      JsonArray jsonArray = FileDownloadUtil.execute(get);
      Gson gson = new Gson();
      List<DbmInsertionOrder> insertionOrders = new LinkedList<>();
      for (int i = 0; i < jsonArray.size(); i++) {
        JsonElement str = jsonArray.get(i);
        DbmInsertionOrder insertionOrder = gson.fromJson(str, DbmInsertionOrder.class);
        Long advertiserId = insertionOrder.getAdvertiserId();
        String miq_timezone = null;
        if (dbmIoMap.containsKey(insertionOrder.getCommonData().getId())) {
          miq_timezone = dbmIoMap.get(insertionOrder.getCommonData().getId()).getMiqTimezone();
          insertionOrder.setMiqTimezone(miq_timezone);
        } else {
          if (advertiserToTimezoneMap.containsKey(advertiserId))
            insertionOrder.setMiqTimezone(advertiserToTimezoneMap.get(advertiserId));
        }
        String timezoneCode = dbmAdvertiserRepo.findById(advertiserId).getTimezoneCode();
        String currency = dbmAdvertiserRepo.findOne(advertiserId).getCurrencyCode();
        if (timezoneCode != null)
          insertionOrder.setTimezone(timezoneCode);
        insertionOrder.setId(insertionOrder.getCommonData().getId());
        insertionOrder.setName(insertionOrder.getCommonData().getName());
        insertionOrder.setIntegrationCode(insertionOrder.getCommonData().getIntegrationCode());
        insertionOrder.setActive(insertionOrder.getCommonData().getActive());
        insertionOrder.setCurrency(Currency.valueOf(currency));
        if (insertionOrder.getCommonData().getActive().equals(true))
          insertionOrder.setState(State.active);
        else
          insertionOrder.setState(State.inactive);
        LOGGER.info(" Object is " + insertionOrder);
        LOGGER.info(" String is " + str);
        LOGGER.info("-------");
        
        insertionOrders.add(insertionOrder);
      }
      
      insertionOrderRepo.save(insertionOrders);
      
    }
    
    
    return RepeatStatus.FINISHED;
    
    
  }
}
