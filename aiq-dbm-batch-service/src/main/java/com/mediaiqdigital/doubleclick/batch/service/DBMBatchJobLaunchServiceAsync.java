package com.mediaiqdigital.doubleclick.batch.service;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.concurrent.ExecutionException;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONException;

import com.mediaiqdigital.doubleclick.batch.service.impl.DateParameter;

/**
 * Created by piyush on 25/11/15.
 */
@Path("/load")
@Produces(MediaType.APPLICATION_JSON)
public interface DBMBatchJobLaunchServiceAsync {
  
  @POST
  @Path("/summary")
  void launchSummaryItemJob()
      throws ClassNotFoundException, GeneralSecurityException, JSONException, IOException;
  
  @POST
  @Path("/advertiser")
  void launchAdvertiserLoadJob();
  
  @POST
  @Path("/browser")
  void launchBrowserLoadJob();
  
  @POST
  @Path("/language")
  void launchLanguageLoadJob();
  
  @POST
  @Path("/device_criteria")
  void launchDeviceCriteriaLoadJob();
  
  @POST
  @Path("/isp")
  void launchIspLoadJob();
  
  @POST
  @Path("/geo_location")
  void launchGeoLocationLoadJob();
  
  @POST
  @Path("/insertion_order")
  void launchInsertionOrderLoadJob();
  
  @POST
  @Path("/partner")
  void launchPartnerLoadJob();
  
  @POST
  @Path("/placement")
  void launchPlacementLoadJob();
  
  @POST
  @Path("/pixel")
  void launchPixelLoadJob();
  
  @POST
  @Path("/creative")
  void launchCreativeLoadJob();
  
  @POST
  @Path("/line-item")
  void launchLineItemLoadJob();
  
  @POST
  @Path("/exchange")
  void launchExchangeLoadJob();
  
  @POST
  @Path("/universal-site")
  void launchUniversalSiteLoadJob();
  
  @POST
  @Path("/inventory-source")
  void launchInventorySourceLoadJob();
  
  @POST
  @Path("/fileUpload")
  void uploadFileLoadJob();
  
  
  @POST
  @Path("/daily-placement-stats")
  void launchDailyPlacementStatsLoadJob(@QueryParam("start_date") DateParameter startDate,
      @QueryParam("end_date") DateParameter endDate,
      @QueryParam("trigger_tc_api") @DefaultValue("true") Boolean triggerTcApi)
      throws InterruptedException, ExecutionException;
  
  @POST
  @Path("/daily-pixel-level-stats")
  void launchDailyPixelLevelStatsLoadJob(@QueryParam("start_date") DateParameter startDate,
      @QueryParam("end_date") DateParameter endDate);
  
  
  @POST
  @Path("/daily-io-placement-stats")
  void launchDailyIOPlacementStatsLoadJob(@QueryParam("start_date") DateParameter startDate,
      @QueryParam("end_date") DateParameter endDate);
  
  
  @POST
  @Path("/daily-insertion-order-stats")
  void launchDailyInsertionOrderStatsLoadJob(@QueryParam("start_date") DateParameter startDate,
      @QueryParam("end_date") DateParameter endDate);
  
  
  @POST
  @Path("/daily-pixel-stats")
  void launchDailyPixelStatsLoadJob(@QueryParam("start_date") DateParameter startDate,
      @QueryParam("end_date") DateParameter endDate);
  
  @POST
  @Path("/daily-placement-timezone-stats")
  void launchDailyPlacementTimeZoneStatsLoadJob(@QueryParam("start_date") DateParameter startDate,
      @QueryParam("end_date") DateParameter endDate);
  
  @POST
  @Path("/daily-true-view-placement-stats")
  void launchDailyTrueViewPlacementStatsLoadJob(@QueryParam("start_date") DateParameter start_date,
      @QueryParam("end_date") DateParameter end_date);
  
  @POST
  @Path("/placement-creative-mapping")
  void launchPlacementCreativeMappingLoadJob(
      @QueryParam("last_modified") DateParameter last_modified);
  
  @POST
  @Path("/na-profile")
  void launchDfaProfileForSeat1LoadJob();

  @POST
  @Path("/miq-profile")
  void launchDfaProfileForSeat2LoadJob();


}
