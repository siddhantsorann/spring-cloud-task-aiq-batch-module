package com.mediaiqdigital.doubleclick.batch.tasklet.report.domain;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.google.api.services.doubleclickbidmanager.DoubleClickBidManager;
import com.google.api.services.doubleclickbidmanager.model.Query;
import com.google.api.services.doubleclickbidmanager.model.RunQueryRequest;
import com.mediaiqdigital.doubleclick.batch.util.StepAttribute;

/**
 * Created by piyush on 29/5/16.
 */
@Component
@Scope("step")
public class ReportStatusCheckTimezoneTasklet implements Tasklet {
  
  final static Logger logger = LoggerFactory.getLogger(ReportStatusCheckTimezoneTasklet.class);
  @Value("#{jobParameters['start_date']}")
  protected Date startDate;
  @Value("#{jobParameters['timezone']}")
  protected String timezone;
  @Value("#{jobParameters['end_date']}")
  protected Date endDate;
  @Value("${report.statsFile}")
  String basePath;
  @Autowired
  DoubleClickBidManager dbm;
  @Value("#{jobExecutionContext['" + StepAttribute.QUERY_ID + "']}")
  private String queryId;
  
  public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext)
      throws Exception {
    
    logger.info(" timezone " + timezone);
    
    
    
    System.out.println(" start time " + startDate.getTime());
    System.out.println(" end time " + endDate.getTime());
    
    RunQueryRequest runQueryRequest = new RunQueryRequest();
    runQueryRequest.setDataRange("CUSTOM_DATES");
    runQueryRequest.setReportDataStartTimeMs(startDate.getTime());
    runQueryRequest.setReportDataEndTimeMs(endDate.getTime());
    runQueryRequest.setTimezoneCode(timezone);
    // Grab the report.
    String reportPathS = null;
    while (reportPathS == null) {
      dbm.queries().runquery(Long.valueOf(queryId).longValue(), runQueryRequest).execute();
      Query queryResponse = dbm.queries().getquery(Long.valueOf(queryId).longValue()).execute();
      System.out.println(" queryresp " + queryResponse);
      System.out.println("*****************");
      reportPathS = queryResponse.getMetadata().getGoogleCloudStoragePathForLatestReport();
      Thread.sleep(10000);
    }
    logger.info(" report URL " + reportPathS);
    ExecutionContext executionContext =
        chunkContext.getStepContext().getStepExecution().getJobExecution().getExecutionContext();
    executionContext.put(StepAttribute.REPORT_URL, reportPathS);
    
    return RepeatStatus.FINISHED;
  }
  
}
