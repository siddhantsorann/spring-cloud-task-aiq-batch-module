package com.mediaiqdigital.doubleclick.batch.util;

import java.io.File;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.storage.Storage;
import com.google.api.services.storage.StorageScopes;

/**
 * Created by piyush on 30/11/15.
 */
public abstract class SecurityCredentials {
  
  static final NetHttpTransport HTTP_TRANSPORT = new NetHttpTransport();
  
  static final JacksonFactory JSON_FACTORY = new JacksonFactory();
  
  private static final String APPLICATION_NAME = "MediaiQ-DBM-Integration/1.0";
  
  public SecurityCredentials() {
    
  }
  
  public static Storage getCredentials(String filePath, String serviceAccount) throws Exception {
    
    File privateKey = new File(filePath);
    
    JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
    Credential credential =
        new GoogleCredential.Builder().setTransport(httpTransport).setJsonFactory(JSON_FACTORY)
            .setServiceAccountId(serviceAccount).setServiceAccountScopes(StorageScopes.all())
            .setServiceAccountPrivateKeyFromP12File(privateKey).build();
    
    Storage storage = new Storage.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential)
        .setApplicationName(APPLICATION_NAME).build();
    
    return storage;
    
    
  }
}
