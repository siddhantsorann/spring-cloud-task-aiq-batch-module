package com.mediaiqdigital.doubleclick.batch.tasklet;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.google.api.services.storage.Storage;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.mediaiq.aiq.domain.Dsp;
import com.mediaiqdigital.aiq.tradingcenter.domain.PlacementCreativeMapping;
import com.mediaiqdigital.aiq.tradingcenter.domain.id.PlacementCreativeId;
import com.mediaiqdigital.aiq.tradingcenter.repo.PlacementCreativeMappingRepo;
import com.mediaiqdigital.doubleclick.batch.util.DateFormatUtil;
import com.mediaiqdigital.doubleclick.batch.util.FileDownloadUtil;
import com.mediaiqdigital.doubleclick.batch.util.SecurityCredentials;
import com.mediaiqdigital.doubleclick.domain.DbmPlacement;
import com.mediaiqdigital.doubleclick.domain.DbmSummaryItems;
import com.mediaiqdigital.doubleclick.repo.DbmSummaryItemsRepo;

/**
 * Created by prasannachidire on 8/11/17.
 */

@Component
@Scope("step")
public class PlacementCreativeMappingLoadTasklet implements Tasklet {
  
  
  private final Logger LOGGER = LoggerFactory.getLogger(PlacementCreativeMappingLoadTasklet.class);
  
  @Value("#{jobParameters['last_modified']}")
  protected Date lastModified;
  @Autowired
  DbmSummaryItemsRepo summaryItemsRepo;
  @Autowired
  PlacementCreativeMappingRepo placementCreativeMappingRepo;
  @Value("${report.file}")
  private String filePath;
  @Value("${google.serviceAccount}")
  private String serviceAccount;
  @Value("${privateId}")
  private String privateId;
  
  @Override
  public RepeatStatus execute(StepContribution arg0, ChunkContext arg1) throws Exception {
    
    List<PlacementCreativeMapping> placementCreativeMappings = new ArrayList<>();
    List<DbmSummaryItems> summaryItemsList = summaryItemsRepo.findByItemName("LineItem");
    String endDate = "2100-01-01 00:00:00";
    DateFormat formatter = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
    Date maxEndDate = formatter.parse(endDate);
    String fileName = "";
    
    if (lastModified == null) {
      if (!CollectionUtils.isEmpty(summaryItemsList)) {
        fileName = summaryItemsList.get(0).getFileName();
      } else {
        LOGGER.info("No item in the summery item list");
      }
    } else {
      String dateString = DateFormatUtil.format(lastModified);
      fileName = dateString + ".0.LineItem.json";
    }
    
    Storage storage = SecurityCredentials.getCredentials(filePath, serviceAccount);
    
    LOGGER.info("Running Dbm placement creative mapping for dbmFile: {}", fileName);
    Storage.Objects.Get get = storage.objects().get(privateId, "entity/" + fileName);
    JsonReader reader = FileDownloadUtil.executAndGetReader(get);
    List<DbmPlacement> placements = new ArrayList<>();
    
    Gson gson = new Gson();
    reader.beginArray();
    while (reader.hasNext()) {
      DbmPlacement dbmPlacement = gson.fromJson(reader, DbmPlacement.class);
      dbmPlacement.setId(dbmPlacement.getCommonData().getId());
      placements.add(dbmPlacement);
    }
    reader.close();
    
    if (!CollectionUtils.isEmpty(placements)) {
      List<Long> placementIds =
          placements.stream().map(DbmPlacement::getId).collect(Collectors.toList());
      LOGGER.info("{} Placements found", placementIds.size());
      
      placements.forEach(placement -> {
        if (!CollectionUtils.isEmpty(placement.getCreativeIds())) {
          placement.getCreativeIds().forEach(creativeId -> {
            PlacementCreativeMapping placementCreativeMapping = new PlacementCreativeMapping(
                new PlacementCreativeId(placement.getCommonData().getId(),creativeId,
                    placement.getInsertionOrderId(), Dsp.DoubleClick, lastModified),
                lastModified, maxEndDate);
            placementCreativeMappings.add(placementCreativeMapping);
          });
        }
      });
      List<PlacementCreativeMapping> existingPCMappings =
          placementCreativeMappingRepo.findByIdPlacementIdInAndIdDsp(placementIds, Dsp.DoubleClick);
      
      existingPCMappings.removeAll(placementCreativeMappings);
      LOGGER.info("{} Placement Creative Mapping removed ", existingPCMappings.size());
      
      existingPCMappings.stream().forEach(p -> {
        p.setEndDate(lastModified);
      });
      LOGGER.info("{} new Placement Creative Mapping added ", placementCreativeMappings.size());
      
      placementCreativeMappings.addAll(existingPCMappings);
    }
    
    LOGGER.info(" Found {} mappings", placementCreativeMappings.size());
    placementCreativeMappingRepo.save(placementCreativeMappings);
    
    return RepeatStatus.FINISHED;
  }
}
