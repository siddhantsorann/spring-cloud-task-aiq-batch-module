package com.mediaiqdigital.doubleclick.batch.tasklet.dfaProfile;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

/**
 * Created by piyushagarwal on 10/5/18.
 */
@Service
public class AmazonClient {
  
  private static final Logger LOGGER = LoggerFactory.getLogger(AmazonClient.class);
  private AmazonS3 s3Client;
  
  public AmazonS3 setS3Client() {
    s3Client = AmazonS3ClientBuilder.defaultClient();
//    Belew client can be used to test in local instance
//    s3Client = AmazonS3ClientBuilder.standard().withRegion("us-east-1").build();
    LOGGER.info("Connection established {}", s3Client.toString());
    return s3Client;
  }
}
