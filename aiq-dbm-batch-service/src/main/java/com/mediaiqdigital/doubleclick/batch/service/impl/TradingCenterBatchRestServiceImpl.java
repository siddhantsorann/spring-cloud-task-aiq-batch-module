package com.mediaiqdigital.doubleclick.batch.service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

import com.mediaiq.aiq.retrofit.RetrofitAdapterProvider;
import com.mediaiqdigital.doubleclick.batch.service.TradingCenterBatchRestService;
import com.mediaiqdigital.doubleclick.batch.service.TradingCenterBatchRetrofitService;


/**
 * A Service to call trading center REST Apis.
 *
 * @author prasannachidire on 15/12/17
 */
@Service
@EnableAsync
public class TradingCenterBatchRestServiceImpl implements TradingCenterBatchRestService {
  
  private static final Logger LOGGER =
      LoggerFactory.getLogger(TradingCenterBatchRestServiceImpl.class);
  DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
  @Autowired
  RetrofitAdapterProvider<TradingCenterBatchRetrofitService> restAdapterProvider;
  @Value("${miqClientToken}")
  private String token;
  @Value("${tcBatchEndPoint}")
  private String tcBatchEndPoint;
  
  private TradingCenterBatchRetrofitService getRestClient() {
    return restAdapterProvider.getRestClient(tcBatchEndPoint, token,
        TradingCenterBatchRetrofitService.class);
  }
  
  @Async
  @Override
  public void launchPlacementStatsProcessorInTcBatch(Date startDate, Date endDate) {
    
    LOGGER.info("Launching placement stats processor for startDate:{}, endDate:{}", startDate,
        endDate);
    
    String startDateString = dateFormat.format(startDate);
    String endDateString = dateFormat.format(endDate);
    getRestClient().launchPlacementStatsProcessor(startDateString, endDateString);
  }
}
