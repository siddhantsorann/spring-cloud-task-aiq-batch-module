package com.mediaiqdigital.doubleclick.batch.util;

public interface StepAttribute {
  String FILE_PATH = "file_path";
  String START_ELEMENT = "start_element";
  String NUM_ELEMENTS = "num_elements";
  String COUNT = "count";
  String REPORT_ID = "report_id";
  String REPORT_NAME = "name";
  String LAST_MODIFIED = "last_modified";
  String CAMPAIGN_IDS = "CAMPAIGN_IDS";
  String DATE = "DATE";
  String QUERY_ID = "query_id";
  String REPORT_URL = "report_url";
  String FILE_NAME = "file_name";
}
