package com.mediaiqdigital.doubleclick.batch.tasklet;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.api.services.storage.Storage;
import com.mediaiqdigital.doubleclick.batch.util.JsonConverterUtil;
import com.mediaiqdigital.doubleclick.batch.util.SecurityCredentials;
import com.mediaiqdigital.doubleclick.batch.util.StepAttribute;
import com.mediaiqdigital.doubleclick.domain.DbmCreative;
import com.mediaiqdigital.doubleclick.domain.DbmSummaryItems;
import com.mediaiqdigital.doubleclick.repo.DbmSummaryItemsRepo;

/**
 * Created by piyush on 1/12/15.
 */
@Component
public class CreativeLoadTasklet implements Tasklet {
  
  private static final Logger LOGGER = LoggerFactory.getLogger(CreativeLoadTasklet.class);
  
  @Autowired
  DbmSummaryItemsRepo summaryItemsRepo;
  @Value("${report.statsFile}")
  String basePath;
  @Value("${report.file}")
  private String filePath;
  @Value("${google.serviceAccount}")
  private String serviceAccount;
  @Value("${privateId}")
  private String privateId;
  
  @Override
  public RepeatStatus execute(StepContribution contribution, ChunkContext context)
      throws Exception {
    
    Storage storage = SecurityCredentials.getCredentials(filePath, serviceAccount);
    
    List<DbmSummaryItems> summaryItemsList = summaryItemsRepo.findByItemName("Creative");
    
    for (DbmSummaryItems summaryItems : summaryItemsList) {
      
      String fileName = summaryItems.getFileName();
      LOGGER.info("FileName is " + fileName);
      Storage.Objects.Get get = storage.objects().get(privateId, "entity/" + fileName);
      String fileTarget = basePath + System.getProperty("file.separator") + "DbmCreative.csv";
      JsonConverterUtil.writeToLocalFromGoogleStorage(get, DbmCreative.class, fileTarget);
      ExecutionContext executionContext =
          context.getStepContext().getStepExecution().getJobExecution().getExecutionContext();
      executionContext.put(StepAttribute.FILE_PATH, fileTarget);
    }
    
    return RepeatStatus.FINISHED;
    
  }
}
