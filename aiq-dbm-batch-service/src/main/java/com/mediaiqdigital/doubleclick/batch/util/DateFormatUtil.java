package com.mediaiqdigital.doubleclick.batch.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormatUtil {
  
  public static String format(Date date) {
    DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
    return dateFormat.format(date);
  }
  
}
