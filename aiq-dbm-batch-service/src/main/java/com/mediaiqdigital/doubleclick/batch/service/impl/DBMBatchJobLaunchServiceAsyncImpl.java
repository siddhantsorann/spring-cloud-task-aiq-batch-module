package com.mediaiqdigital.doubleclick.batch.service.impl;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.concurrent.ExecutionException;

import org.codehaus.jettison.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mediaiqdigital.doubleclick.batch.service.DBMBatchJobLaunchService;
import com.mediaiqdigital.doubleclick.batch.service.DBMBatchJobLaunchServiceAsync;

/**
 * Created by piyush on 25/11/15.
 */
@Service
public class DBMBatchJobLaunchServiceAsyncImpl implements DBMBatchJobLaunchServiceAsync {
  
  private static final Logger logger = LoggerFactory.getLogger(DBMBatchJobLaunchServiceAsync.class);
  @Autowired
  DBMBatchJobLaunchService dbmBatchJobLaunchService;
  
  @Override
  public void launchSummaryItemJob()
      throws ClassNotFoundException, GeneralSecurityException, JSONException, IOException {
    logger.info("Async Service");
    dbmBatchJobLaunchService.launchSummaryItemJob();
  }
  
  @Override
  public void launchAdvertiserLoadJob() {
    logger.info("Async Advertiser Service");
    dbmBatchJobLaunchService.launchAdvertiserLoadJob();
    
  }
  
  @Override
  public void launchUniversalSiteLoadJob() {
    logger.info("Async universal site Service");
    dbmBatchJobLaunchService.launchUniversalSiteLoadJob();
    
  }
  
  @Override
  public void launchInventorySourceLoadJob() {
    logger.info("Async inventory source Service");
    dbmBatchJobLaunchService.launchInventorySourceLoadJob();
    
  }
  
  @Override
  public void launchBrowserLoadJob() {
    logger.info("Async Browser Service");
    dbmBatchJobLaunchService.launchBrowserLoadJob();
    
  }
  
  @Override
  public void launchLanguageLoadJob() {
    logger.info("Async Language Service");
    dbmBatchJobLaunchService.launchLanguageLoadJob();
    
  }
  
  @Override
  public void launchDeviceCriteriaLoadJob() {
    logger.info("Async DeviceCriteria Service");
    dbmBatchJobLaunchService.launchDeviceCriteriaLoadJob();
    
  }
  
  @Override
  public void launchIspLoadJob() {
    logger.info("Async Isp Service");
    dbmBatchJobLaunchService.launchIspLoadJob();
    
  }
  
  @Override
  public void launchGeoLocationLoadJob() {
    logger.info("Async GeoLocation Service");
    dbmBatchJobLaunchService.launchGeoLocationLoadJob();
    
  }
  
  @Override
  public void uploadFileLoadJob() {
    logger.info("uploading file");
    dbmBatchJobLaunchService.uploadFileLoadJob();
  }
  
  @Override
  public void launchInsertionOrderLoadJob() {
    logger.info("Async Insertion Order Service");
    dbmBatchJobLaunchService.launchInsertionOrderLoadJob();
    
  }
  
  @Override
  public void launchPartnerLoadJob() {
    logger.info("Async Partner Service");
    dbmBatchJobLaunchService.launchPartnerLoadJob();
    
  }
  
  @Override
  public void launchPlacementLoadJob() {
    logger.info("Async LineItem Service ");
    dbmBatchJobLaunchService.launchPlacementLoadJob();
  }
  
  @Override
  public void launchPixelLoadJob() {
    logger.info("Async Pixel Service ");
    dbmBatchJobLaunchService.launchPixelLoadJob();
  }
  
  @Override
  public void launchCreativeLoadJob() {
    logger.info("Async Creative Service ");
    dbmBatchJobLaunchService.launchCreativeLoadJob();
  }
  
  @Override
  public void launchDailyPlacementStatsLoadJob(DateParameter startDate, DateParameter endDate,
      Boolean triggerTcApi) throws InterruptedException, ExecutionException
  
  {
    logger.info(" Async Placement Stats Load Job");
    if (!triggerTcApi) {
      dbmBatchJobLaunchService.launchDailyPlacementStatsLoadJob(startDate, endDate);
    } else {
      logger.info(" Async Placement Stats Load Job with trigger to tc placment load job");
      dbmBatchJobLaunchService.launchDailyPlacementStatsAndTriggerTcLoadCronJob(startDate, endDate);
    }
  }
  
  @Override
  public void launchDailyTrueViewPlacementStatsLoadJob(DateParameter start_date,
      DateParameter end_date) {
    logger.info(" Async True View Placement Stats Load Job");
    dbmBatchJobLaunchService.launchDailyTrueViewPlacementStatsLoadJob(start_date, end_date);
  }
  
  @Override
  public void launchDailyPixelLevelStatsLoadJob(DateParameter start_date, DateParameter end_date) {
    logger.info(" Async Placement Stats Load Job");
    dbmBatchJobLaunchService.launchDailyPixelLevelStatsLoadJob(start_date, end_date);
  }
  
  @Override
  public void launchDailyInsertionOrderStatsLoadJob(DateParameter start_date,
      DateParameter end_date) {
    logger.info(" Async IO Stats Load Job");
    dbmBatchJobLaunchService.launchDailyInsertionOrderStatsLoadJob(start_date, end_date);
  }
  
  @Override
  public void launchDailyIOPlacementStatsLoadJob(DateParameter start_date, DateParameter end_date) {
    logger.info(" Async IO Placement Stats Load Job");
    dbmBatchJobLaunchService.launchDailyIOPlacementStatsLoadJob(start_date, end_date);
  }
  
  @Override
  public void launchDailyPixelStatsLoadJob(DateParameter start_date, DateParameter end_date) {
    logger.info(" Async IO Placement Stats Load Job");
    dbmBatchJobLaunchService.launchDailyPixelStatsLoadJob(start_date, end_date);
  }
  
  @Override
  public void launchDailyPlacementTimeZoneStatsLoadJob(DateParameter start_date,
      DateParameter end_date) {
    logger.info(" Async Placement Stats Load Job");
    dbmBatchJobLaunchService.launchDailyPlacementTimeZoneStatsLoadJob(start_date, end_date);
  }
  
  @Override
  public void launchLineItemLoadJob() {
    logger.info("Line Item Load Job");
    dbmBatchJobLaunchService.launchLineItemLoadJob();
  }
  
  @Override
  public void launchExchangeLoadJob() {
    logger.info("Line Item Load Job");
    dbmBatchJobLaunchService.launchExchangeLoadJob();
  }
  
  @Override
  public void launchPlacementCreativeMappingLoadJob(DateParameter last_modified) {
    logger.info("DBM placement creative mapping load Job");
    dbmBatchJobLaunchService.launchPlacementCreativeMappingLoadJob(last_modified);
  }
  
  @Override
  public void launchDfaProfileForSeat1LoadJob() {
    dbmBatchJobLaunchService.launchDfaProfileForSeat1LoadJob();
  }

  @Override
  public void launchDfaProfileForSeat2LoadJob() {
    dbmBatchJobLaunchService.launchDfaProfileForSeat2LoadJob();
  }

}
