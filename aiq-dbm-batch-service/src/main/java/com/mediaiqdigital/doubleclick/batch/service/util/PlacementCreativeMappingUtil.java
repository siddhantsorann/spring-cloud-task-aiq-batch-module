package com.mediaiqdigital.doubleclick.batch.service.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mediaiqdigital.aiq.tradingcenter.domain.PlacementCreativeMapping;

/**
 * @author prasannachidire on 13/8/18.
 */
public class PlacementCreativeMappingUtil {
  
  private static final Logger LOGGER = LoggerFactory.getLogger(PlacementCreativeMappingUtil.class);
  
  public static List<PlacementCreativeMapping> getUpdatedPlacementCreativeMappings(
      List<PlacementCreativeMapping> placementCreativeMappingsFromDB,
      List<PlacementCreativeMapping> placementCreativeMappingsFromDBM, Date lastModified) {
    
    Map<Long, Map<Long, List<PlacementCreativeMapping>>> mapFromTable =
        placementCreativeMappingsFromDB.stream()
            .collect(Collectors.groupingBy(pcm -> pcm.getId().getPlacementId(),
                Collectors.groupingBy(pcm -> pcm.getId().getCreativeId())));
    LOGGER.info("Formed {} map from DB", mapFromTable.size());
    Map<Long, Map<Long, List<PlacementCreativeMapping>>> mapFromDBM =
        placementCreativeMappingsFromDBM.stream()
            .collect(Collectors.groupingBy(pcm -> pcm.getId().getPlacementId(),
                Collectors.groupingBy(pcm -> pcm.getId().getCreativeId())));
    LOGGER.info("Formed {} map from DBM", mapFromDBM.size());
    
    List<PlacementCreativeMapping> newPlacementCreativeMappings =
        placementCreativeMappingsFromDBM.stream().filter(pcm -> {
          Long creativeId = pcm.getId().getCreativeId();
          Long placementId = pcm.getId().getPlacementId();
          return !(mapFromTable.containsKey(placementId)
              && mapFromTable.get(placementId).containsKey(creativeId));
        }).collect(Collectors.toList());
    
    LOGGER.info("Created new  {} mappings ", newPlacementCreativeMappings.size());
    List<PlacementCreativeMapping> removedPlacementCreativeMappings =
        placementCreativeMappingsFromDB.stream().filter(pcm -> {
          Long creativeId = pcm.getId().getCreativeId();
          Long placementId = pcm.getId().getPlacementId();
          return !(mapFromDBM.containsKey(placementId)
              && mapFromDBM.get(placementId).containsKey(creativeId));
        }).map(pcm -> {
          pcm.setEndDate(lastModified);
          return pcm;
        }).collect(Collectors.toList());
    LOGGER.info("Updated old {} mappings ", removedPlacementCreativeMappings.size());
    
    // save all the updated mappings
    newPlacementCreativeMappings.addAll(removedPlacementCreativeMappings);
    LOGGER.info(" Saved {} mappings", newPlacementCreativeMappings.size());
    return newPlacementCreativeMappings;
  }
}
