package com.mediaiqdigital.doubleclick.batch.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.auth.oauth2.CredentialRefreshListener;
import com.google.api.client.auth.oauth2.TokenErrorResponse;
import com.google.api.client.auth.oauth2.TokenResponse;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.doubleclickbidmanager.DoubleClickBidManager;
import com.google.common.collect.ImmutableSet;

/**
 * Created by piyush on 9/12/15.
 */
@Configuration
public class SecurityUtilities {

  // HTTP Transport object used for automatically refreshing access tokens.
  static final NetHttpTransport HTTP_TRANSPORT = new NetHttpTransport();

  private static final Logger LOGGER = LoggerFactory.getLogger(SecurityUtilities.class);
  /**
   * Be sure to specify the name of your application. If the application name is {@code null} or
   * blank, the application will log a warning. Suggested format is "MyCompany-ProductName/1.0".
   */
  private static final String APPLICATION_NAME = "MediaiQ-DBM-Integration/1.0";
  // Scopes for the OAuth token
  private static final Set<String> SCOPES =
      ImmutableSet.of("https://www.googleapis.com/auth/doubleclickbidmanager");
  private static FileDataStoreFactory dataStoreFactory;
  // JSON factory used for parsing refresh token responses.
  final JacksonFactory JSON_FACTORY = new JacksonFactory();
  // Directory to store the OAuth Refresh Token if using User Credentials .
  @Value("${datastore.directory}")
  private String storedCredentialDirectoryPath;
  @Value("${datastore.oauth-userId}")
  private String userId;

  public static NetHttpTransport getHttpTransport() {
    return HTTP_TRANSPORT;
  }

  public Credential getUserCredential() throws Exception {

    File datastoreDirectory = new File(storedCredentialDirectoryPath);

    GoogleClientSecrets clientSecrets =
        GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(
            SecurityUtilities.class.getResourceAsStream("/client_secret_jarvis_reporting.json")));
    dataStoreFactory = new FileDataStoreFactory(datastoreDirectory);

    // set up authorization code flow.
    GoogleAuthorizationCodeFlow flow =
        new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
            .setDataStoreFactory(dataStoreFactory).setApprovalPrompt("auto")
            .setAccessType("offline").addRefreshListener(new CredentialRefreshListener() {
              @Override
              public void onTokenResponse(Credential credential, TokenResponse tokenResponse)
                  throws IOException {
                
                LOGGER.info("Token Refreshed Succesfully");
              }
              
              @Override
              public void onTokenErrorResponse(Credential credential,
                  TokenErrorResponse tokenErrorResponse) throws IOException {
                
                LOGGER.info("ERROR WITH TOKEN ");
              }
            }).build();

    // authorize and get credentials.
    Credential credential = new AuthorizationCodeInstalledApp(flow,
        new LocalServerReceiver.Builder().setHost("localhost").setPort(10000).build())
            .authorize(userId).setExpiresInSeconds(31536000L);

    return credential;
  }

  private HttpRequestInitializer setHttpTimeout(final HttpRequestInitializer requestInitializer) {
    return new HttpRequestInitializer() {
      public void initialize(HttpRequest httpRequest) throws IOException {
        requestInitializer.initialize(httpRequest);
        httpRequest.setReadTimeout(3 * 60000); // 3 minutes read timeout.
      }
    };
  }

  @Bean
  public DoubleClickBidManager getAPIService() throws Exception {
    DoubleClickBidManager bmService = new DoubleClickBidManager.Builder(HTTP_TRANSPORT,
        JSON_FACTORY, setHttpTimeout(getUserCredential()))
            .setApplicationName(APPLICATION_NAME + "_JavaSamples").build();
    return bmService;
  }
}
