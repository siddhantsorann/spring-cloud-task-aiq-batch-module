package com.mediaiqdigital.doubleclick.batch.service;

import java.util.Date;

/**
 * An Interface for trading center rest client.
 *
 * @author prasannachidire on 15/12/17
 */
public interface TradingCenterBatchRestService {
  
  void launchPlacementStatsProcessorInTcBatch(Date startDate, Date endDate);
}
