package com.mediaiqdigital.doubleclick.batch.domain;

/**
 * Created by piyushagarwal on 18/6/18.
 */
public class DcmProfileAccountDto {
  
  private Long accountId;
  private String accountName;
  
  public DcmProfileAccountDto(Long accountId, String accountName) {
    this.accountId = accountId;
    this.accountName = accountName;
  }
  
  public Long getAccountId() {
    return accountId;
  }
  
  public void setAccountId(Long accountId) {
    this.accountId = accountId;
  }
  
  public String getAccountName() {
    return accountName;
  }
  
  public void setAccountName(String accountName) {
    this.accountName = accountName;
  }
}
