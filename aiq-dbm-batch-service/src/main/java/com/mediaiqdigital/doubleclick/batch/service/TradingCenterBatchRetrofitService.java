package com.mediaiqdigital.doubleclick.batch.service;

import retrofit.client.Response;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * A Retrofit Interface for trading center batch.
 *
 * @author prasannachidire on 15/12/17
 */
public interface TradingCenterBatchRetrofitService {
  
  @POST("/load/placement-stats")
  Response launchPlacementStatsProcessor(@Query("start_date") String startDateParameter,
      @Query("end_date") String endDateParameter);
}
