package com.mediaiqdigital.doubleclick.batch.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ChunkListener;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.stereotype.Component;

/**
 * @author prasannachidire on 6/2/18.
 */
@Component
public class ChunkCountListener implements ChunkListener {
  
  private static final Logger LOGGER = LoggerFactory.getLogger(ChunkCountListener.class);
  
  private int loggingInterval = 1000;
  
  @Override
  public void beforeChunk(ChunkContext context) {
    // Nothing to do here
  }
  
  @Override
  public void afterChunk(ChunkContext context) {
    
    int count = context.getStepContext().getStepExecution().getReadCount();
    
    // If the number of records processed so far is a multiple
    // of the logging interval then output a log message.
    if (count > 0 && count % loggingInterval == 0) {
      LOGGER.info("{} items processed", count);
    }
  }
  
  @Override
  public void afterChunkError(ChunkContext context) {
    // Nothing to do here
  }
  
  public void setLoggingInterval(int loggingInterval) {
    this.loggingInterval = loggingInterval;
  }
}
