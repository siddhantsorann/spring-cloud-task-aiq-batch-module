package com.mediaiqdigital.doubleclick.batch.tasklet;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.LinkedList;
import java.util.List;

import org.codehaus.jettison.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.storage.Storage;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.mediaiqdigital.doubleclick.batch.util.FileDownloadUtil;
import com.mediaiqdigital.doubleclick.batch.util.SecurityCredentials;
import com.mediaiqdigital.doubleclick.domain.DbmAdvertiser;
import com.mediaiqdigital.doubleclick.domain.DbmSummaryItems;
import com.mediaiqdigital.doubleclick.repo.DbmAdvertiserRepo;
import com.mediaiqdigital.doubleclick.repo.DbmSummaryItemsRepo;

/**
 * Created by piyush on 24/11/15.
 */
@Component
@Scope("step")
public class AdvertiserLoadTasklet implements Tasklet {
  
  static final NetHttpTransport HTTP_TRANSPORT = new NetHttpTransport();
  
  static final JacksonFactory JSON_FACTORY = new JacksonFactory();
  
  private static final Logger LOGGER = LoggerFactory.getLogger(AdvertiserLoadTasklet.class);
  
  @Autowired
  DbmSummaryItemsRepo summaryItemsRepo;
  
  @Autowired
  DbmAdvertiserRepo advertiseRepo;
  
  @Value("${report.file}")
  private String filePath;
  
  @Value("${google.serviceAccount}")
  private String serviceAccount;
  
  @Value("${privateId}")
  private String privateId;
  
  @Override
  public RepeatStatus execute(StepContribution contribution, ChunkContext context) throws Exception,
      GeneralSecurityException, IOException, ClassNotFoundException, JSONException {
    
    Storage storage = SecurityCredentials.getCredentials(filePath, serviceAccount);
    
    List<DbmSummaryItems> summaryItemsList = summaryItemsRepo.findByItemName("Advertiser");
    
    for (DbmSummaryItems summaryItems : summaryItemsList) {
      
      String fileName = summaryItems.getFileName();
      LOGGER.info("FileName is " + fileName);
      Storage.Objects.Get get = storage.objects().get(privateId, "entity/" + fileName);
      JsonArray jsonArray = FileDownloadUtil.execute(get);
      Gson gson = new Gson();
      List<DbmAdvertiser> advertisers = new LinkedList<>();
      for (int i = 0; i < jsonArray.size(); i++) {
        JsonObject str = jsonArray.get(i).getAsJsonObject();
        DbmAdvertiser obj = gson.fromJson(str, DbmAdvertiser.class);
        obj.setId(obj.getCommonData().getId());
        obj.setName(obj.getCommonData().getName());
        obj.setIntegrationCode(obj.getCommonData().getIntegrationCode());
        obj.setActive(obj.getCommonData().getActive());
        
        LOGGER.info(" Object is " + obj);
        LOGGER.info(" String is " + str);
        LOGGER.info("-------");
        advertisers.add(obj);
      }
      advertiseRepo.save(advertisers);
    }
    
    return RepeatStatus.FINISHED;
    
  }
  
}
