package com.mediaiqdigital.doubleclick.batch.tasklet;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.google.api.client.util.Value;
import com.google.api.services.doubleclickbidmanager.DoubleClickBidManager;
import com.google.api.services.doubleclickbidmanager.model.DownloadLineItemsRequest;
import com.google.api.services.doubleclickbidmanager.model.DownloadLineItemsResponse;
import com.google.common.base.Charsets;
import com.google.common.io.Files;
import com.mediaiqdigital.doubleclick.batch.util.StepAttribute;

/**
 * Created by piyush on 20/11/15.
 */
@Component
@Scope("step")
public class DownloadLineItemTasklet implements Tasklet {
  
  final static Logger logger = LoggerFactory.getLogger(DownloadLineItemTasklet.class);
  @Autowired
  DoubleClickBidManager doubleClickBidManagerService;
  @Value("${report.directory}")
  private String reportDir = "/tmp";
  
  @Override
  public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext)
      throws Exception {
    
    Long startTime = System.currentTimeMillis();
    String fileTarget = reportDir + System.getProperty("file.separator") + "line-items.csv";
    
    // Get an authenticated connection to the API.
    
    // Setup any filtering on the API request
    DownloadLineItemsRequest dliRequest = new DownloadLineItemsRequest();
    
    // Call the API, getting the (filtered) list of line items.
    DownloadLineItemsResponse dliResponse =
        doubleClickBidManagerService.lineitems().downloadlineitems(dliRequest).execute();
    File to = new File(fileTarget);
    Files.write(dliResponse.getLineItems(), to, Charsets.UTF_8);
    logger.info("File downloaded at {}", fileTarget);
    
    Long endTime = System.currentTimeMillis();
    
    ExecutionContext executionContext =
        chunkContext.getStepContext().getStepExecution().getJobExecution().getExecutionContext();
    executionContext.put(StepAttribute.FILE_PATH, fileTarget);
    logger.debug("Time taken::" + (endTime - startTime));
    
    return RepeatStatus.FINISHED;
  }
  
}
