package com.mediaiqdigital.doubleclick.batch.mapper;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.mapping.PassThroughLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

import com.mediaiqdigital.aiq.tradingcenter.domain.DailyInsertionOrderStats;
import com.mediaiqdigital.aiq.tradingcenter.domain.stats.DailyPixelLevelStats;
import com.mediaiqdigital.doubleclick.batch.util.StepAttribute;
import com.mediaiqdigital.doubleclick.domain.stats.DbmPixelStats;
import com.mediaiqdigital.doubleclick.domain.stats.DbmPlacementCreativeStats;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

/**
 * Created by piyush on 15/12/15.
 */
@Configuration
public class CsvFileItemReaderFactory {
  
  
  private static final Logger LOGGER = LoggerFactory.getLogger(CsvFileItemReaderFactory.class);
  private static final String format = "UTF-8";
  @Value("${report.statsFile}")
  String basePath;
  SimpleDateFormat sdf = new SimpleDateFormat("yyyy/mm/dd");
  SimpleDateFormat output = new SimpleDateFormat("yyyy-mm-dd");
  
  @Bean(name = "dbmDailyPlacementStatsReader")
  @Scope("job")
  public FlatFileItemReader<DbmPlacementCreativeStats> provideTT(
      @Value("#{jobExecutionContext['" + StepAttribute.FILE_PATH + "']}") String filePath,
      @Value("#{jobParameters['start_date']}") Date startDate,
      @Value("#{jobParameters['end_date']}") Date endDate) throws IOException {
    
    String[] names = {"Creative", "Creative ID", "DCM Placement ID", "Creative Status",
        "Creative Source", "Creative Integration Code", "Line Item", "Line Item ID",
        "Line Item Status", "Line Item Integration Code", "Targeted Data Providers",
        "Advertiser Time Zone", "Date", "Time of Day", "Insertion Order", "Insertion Order ID",
        "Insertion Order Status", "Insertion Order Integration Code", "Advertiser Currency",
        "Clicks", "Click Rate (CTR)", "Impressions", "Total Conversions",
        "Total Media Cost (Advertiser Currency)", "DateTime"};
    
    String[] names1 = {"Date", "Insertion Order", "Insertion Order ID", "Insertion Order Status",
        "Insertion Order Integration Code", "Line Item", "Line Item ID", "Line Item Status",
        "Line Item Integration Code", "Targeted Data Providers", "Creative", "Creative ID",
        "DCM Placement ID", "Creative Status", "Creative Source", "Creative Integration Code",
        "Advertiser Time Zone", "Advertiser Currency", "Time of Day", "Impressions", "Clicks",
        "Total Conversions", "Total Media Cost (Advertiser Currency)", "Click Rate (CTR)",
        "DateTime"};
    
    
    String fileWpath = basePath + "/dbmDailyPlacementStats.csv";
    fileWriter(filePath, fileWpath);
    FlatFileItemReader<DbmPlacementCreativeStats> reader = new FlatFileItemReader<>();
    DefaultLineMapper<DbmPlacementCreativeStats> mapper = new DefaultLineMapper<>();
    DBMDailyPlacementStatsMapper fieldSetMapper =
        new DBMDailyPlacementStatsMapper(startDate, endDate);
    commonProvider(names1, reader, fileWpath, mapper, fieldSetMapper);
    return reader;
    
  }
  
  @Bean(name = "dbmDailyPixelLevelStatsReader")
  @Scope("job")
  public FlatFileItemReader<DailyPixelLevelStats> provideTIT(
      @Value("#{jobExecutionContext['" + StepAttribute.FILE_PATH + "']}") String filePath,
      @Value("#{jobParameters['start_date']}") Date startDate,
      @Value("#{jobParameters['end_date']}") Date endDate) throws IOException {
    
    String[] names = {"Creative", "Creative ID", "DCM Placement ID", "Creative Status",
        "Creative Source", "Creative Integration Code", "Line Item", "Line Item ID",
        "Line Item Status", "Line Item Integration Code", "Targeted Data Providers",
        "Advertiser Time Zone", "Conversion Pixel", "Conversion Pixel ID",
        "Conversion Pixel DCM ID", "Conversion Pixel Status", "Conversion Pixel Integration Code",
        "Date", "Time of Day", "Insertion Order", "Insertion Order ID", "Insertion Order Status",
        "Insertion Order Integration Code", "Clicks", "Click Rate (CTR)", "Impressions",
        "Total Conversions", "DateTime"};
    
    String[] names1 = {"Date", "Insertion Order", "Insertion Order ID", "Insertion Order Status",
        "Insertion Order Integration Code", "Line Item", "Line Item ID", "Line Item Status",
        "Line Item Integration Code", "Targeted Data Providers", "Creative", "Creative ID",
        "DCM Placement ID", "Creative Status", "Creative Source", "Creative Integration Code",
        "Advertiser Time Zone", "Conversion Pixel", "Conversion Pixel ID",
        "Conversion Pixel DCM ID", "Conversion Pixel Status", "Conversion Pixel Integration Code",
        "Time of Day", "Impressions", "Clicks", "Total Conversions", "Click Rate (CTR)",
        "DateTime"};
    
    String fileTarget = basePath + System.getProperty("file.separator")
        + "DailyPlacementCreativePixelStats" + ".csv";
    
    String fileWpath = basePath + "/dbmDailyPixelLevelStats.csv";
    fileWriterForPixelLevelStats(fileTarget, fileWpath);
    FlatFileItemReader<DailyPixelLevelStats> reader = new FlatFileItemReader<>();
    DefaultLineMapper<DailyPixelLevelStats> mapper = new DefaultLineMapper<>();
    DBMDailyPlacementCreativePixelStatsMapper fieldSetMapper =
        new DBMDailyPlacementCreativePixelStatsMapper(startDate, endDate);
    commonProvider(names1, reader, fileWpath, mapper, fieldSetMapper);
    return reader;
    
  }
  
  
  
  @Bean(name = "dbmDailyPixelStatsReader")
  @Scope("job")
  public FlatFileItemReader<DbmPixelStats> provideT(
      @Value("#{jobExecutionContext['" + StepAttribute.FILE_PATH + "']}") String filePath,
      @Value("#{jobParameters['start_date']}") Date startDate,
      @Value("#{jobParameters['end_date']}") Date endDate) throws IOException {
    
    String[] names = {"Pixel", "Pixel Id", "Pixel DCM ID", "Pixel Status", "Pixel Integration Code",
        "Pixel Load Count"};
    
    String fileWpath = basePath + "/dbmDailyPixelStats.csv";
    fileWriterForPixelLevelStats(filePath, fileWpath);
    FlatFileItemReader<DbmPixelStats> reader = new FlatFileItemReader<DbmPixelStats>();
    DefaultLineMapper<DbmPixelStats> mapper = new DefaultLineMapper<>();
    DbmDailyPixelStatsMapper fieldSetMapper = new DbmDailyPixelStatsMapper(startDate, endDate);
    commonProvider(names, reader, fileWpath, mapper, fieldSetMapper);
    return reader;
    
  }
  
  
  @Bean(name = "dbmDailyInsertionOrderStatsReader")
  @Scope("job")
  public FlatFileItemReader<DailyInsertionOrderStats> provideTTT(
      @Value("#{jobExecutionContext['" + StepAttribute.FILE_PATH + "']}") String filePath,
      @Value("#{jobParameters['start_date']}") Date startDate,
      @Value("#{jobParameters['end_date']}") Date endDate) throws IOException {
    
    
    String[] names = {"Insertion Order", "Insertion Order ID", "Insertion Order Status",
        "Insertion Order Integration Code", "Advertiser Time Zone", "Date", "Clicks",
        "Click Rate (CTR)", "Impressions", "Total Conversions", "Media Cost (USD)"};
    
    String fileWpath = basePath + "/dbmDailyInsertionOrderStats.csv";
    fileWriter(filePath, fileWpath);
    
    FlatFileItemReader<DailyInsertionOrderStats> reader = new FlatFileItemReader<>();
    DefaultLineMapper<DailyInsertionOrderStats> mapper = new DefaultLineMapper<>();
    DBMDailyInsertionOrderStatsMapper fieldSetMapper =
        new DBMDailyInsertionOrderStatsMapper(startDate, endDate);
    commonProvider(names, reader, fileWpath, mapper, fieldSetMapper);
    return reader;
    
  }
  
  @Bean(name = "dbmDailyTrueViewPlacementStatsReader")
  @Scope("job")
  public FlatFileItemReader<DbmPlacementCreativeStats> provideTrueViewStats(
      @Value("#{jobExecutionContext['" + StepAttribute.FILE_PATH + "']}") String filePath,
      @Value("#{jobParameters['start_date']}") Date startDate,
      @Value("#{jobParameters['end_date']}") Date endDate) throws IOException {
    
    String[] names = {"Line Item", "Line Item ID", "Line Item Status", "Line Item Integration Code",
        "Date", "Time of Day", "Insertion Order", "Insertion Order ID", "Insertion Order Status",
        "Insertion Order Integration Code", "Advertiser Currency", "Clicks", "Click Rate (CTR)",
        "Impressions", "TrueView: Views", "Total Media Cost (Advertiser Currency)", "DateTime"};
    String fileWpath = basePath + "/dbmDailyTrueViewPlacementStats.csv";
    fileWriterTrueViewReport(filePath, fileWpath);
    FlatFileItemReader<DbmPlacementCreativeStats> reader = new FlatFileItemReader<>();
    DefaultLineMapper<DbmPlacementCreativeStats> mapper = new DefaultLineMapper<>();
    DBMDailyTrueViewPlacementStatsMapper fieldSetMapper =
        new DBMDailyTrueViewPlacementStatsMapper(startDate, endDate);
    commonProvider(names, reader, fileWpath, mapper, fieldSetMapper);
    return reader;
    
  }
  
  @Bean(name = "dbmItemReader")
  @Scope("job")
  public FlatFileItemReader<String> provideLineItemReader(
      @Value("#{jobExecutionContext['" + StepAttribute.FILE_PATH + "']}") String filePath) {
    FlatFileItemReader<String> reader = new FlatFileItemReader<>();
    Resource resource = new FileSystemResource(filePath);
    reader.setResource(resource);
    PassThroughLineMapper passThroughLineMapper = new PassThroughLineMapper();
    reader.setLineMapper(passThroughLineMapper);
    return reader;
  }
  
  
  private void fileWriter(String fileRpath, String fileWpath) throws IOException {
    
    CSVReader csvReader =
        new CSVReader(new InputStreamReader(new FileInputStream(fileRpath), format));
    CSVWriter csvWriter =
        new CSVWriter(new OutputStreamWriter(new FileOutputStream(fileWpath), format));
    String[] row = null;
    int count = 0;
    while ((row = csvReader.readNext()) != null) {
      count++;
      if (row[11].equalsIgnoreCase("Unknown"))
        continue;
      
      if (row[11].isEmpty()) {
        break;
      } else {
        String[] list = new String[25];
        for (int i = 0; i < row.length; i++) {
          list[i] = row[i];
        }
        if (count != 1) {
          Date data = null;
          try {
            System.out.println("list[0].toString() " + list[0].toString());
            data = sdf.parse(list[0].toString());
            System.out.println("**********************");
            System.out.println("data   " + data);
          } catch (ParseException e) {
            e.printStackTrace();
          }
          String formattedTime = output.format(data);
          list[24] = String.valueOf(convertIntoDateTime(formattedTime, list[18].toString()));
        } else
          list[24] = "DateTime";
        csvWriter.writeNext(list);
      }
    }
    csvReader.close();
    csvWriter.close();
  }
  
  private void fileWriterTrueViewReport(String fileRpath, String fileWpath) throws IOException {
    
    CSVReader csvReader =
        new CSVReader(new InputStreamReader(new FileInputStream(fileRpath), format));
    CSVWriter csvWriter =
        new CSVWriter(new OutputStreamWriter(new FileOutputStream(fileWpath), format));
    String[] row = null;
    int count = 0;
    while ((row = csvReader.readNext()) != null) {
      count++;
      if (row[0].equalsIgnoreCase("Unknown"))
        continue;
      
      if (row[0].isEmpty()) {
        break;
      } else {
        String[] list = new String[17];
        for (int i = 0; i < row.length; i++) {
          list[i] = row[i];
        }
        if (count != 1) {
          Date data = null;
          try {
            LOGGER.info("True view stat with date before parse {}", list[4].toString());
            data = sdf.parse(list[4].toString());
            LOGGER.info("True view stat with date after parse {}", data);
          } catch (ParseException e) {
            LOGGER.error("Error while parsing the date", e);
          }
          String formattedTime = output.format(data);
          list[16] = String.valueOf(convertIntoDateTime(formattedTime, list[5].toString()));
        } else
          list[16] = "DateTime";
        csvWriter.writeNext(list);
      }
    }
    csvReader.close();
    csvWriter.close();
  }
  
  
  private void fileWriterForPixelLevelStats(String fileRpath, String fileWpath) throws IOException {
    
    CSVReader csvReader =
        new CSVReader(new InputStreamReader(new FileInputStream(fileRpath), format));
    CSVWriter csvWriter =
        new CSVWriter(new OutputStreamWriter(new FileOutputStream(fileWpath), format));
    String[] row = null;
    int count = 0;
    while ((row = csvReader.readNext()) != null) {
      count++;
      if (row[11].isEmpty())
        break;
      if (row[12].isEmpty())
        continue;
      if (row[11].equalsIgnoreCase("Unknown"))
        continue;
      if (row[17].equalsIgnoreCase("Total"))
        continue;
      if (row[18].isEmpty())
        continue;
      else {
        String[] list = new String[28];
        
        for (int i = 0; i < row.length; i++) {
          list[i] = row[i];
        }
        // System.out.println("row length is {} "+row.length+ "and row is {}" +row[1].toString()+" "
        // +row[7].toString()+" "+row[12].toString()+" "+row[13].toString()+" "+row[17].toString()
        // +" "+row[18].toString()+" "+row[20].toString());
        if (count != 1) {
          Date data = null;
          try {
            System.out.println("list[0].toString() " + list[0].toString());
            data = sdf.parse(list[0].toString());
            System.out.println("**********************");
            System.out.println("data   " + data);
          } catch (ParseException e) {
            e.printStackTrace();
          }
          String formattedTime = output.format(data);
          list[27] = String.valueOf(convertIntoDateTime(formattedTime, list[22].toString()));
        } else
          list[27] = "DateTime";
        csvWriter.writeNext(list);
      }
    }
    csvReader.close();
    csvWriter.close();
  }
  
  public String convertIntoDateTime(String date, String time) {
    String myDate = null;
    if (time.length() > 1)
      myDate = date + " " + time + ":00:00";
    else
      myDate = date + " " + "0" + time + ":00:00";
    
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    new java.util.Date();
    try {
      LOGGER.info("parsing date {}, time {}", date, time);
      sdf.parse(myDate);
    } catch (ParseException pe) {
      pe.printStackTrace();
    }
    return myDate;
  }
  
  private void commonProvider(String[] names, FlatFileItemReader reader, String filePath,
      DefaultLineMapper mapper, FieldSetMapper fieldSetMapper) {
    System.out.println("file path " + filePath);
    Resource resource = new FileSystemResource(filePath);
    reader.setResource(resource);
    reader.setLinesToSkip(1);
    DelimitedLineTokenizer tokenizer = new DelimitedLineTokenizer();
    tokenizer.setNames(names);
    mapper.setLineTokenizer(tokenizer);
    mapper.setFieldSetMapper(fieldSetMapper);
    reader.setLineMapper(mapper);
  }
  
  
}
