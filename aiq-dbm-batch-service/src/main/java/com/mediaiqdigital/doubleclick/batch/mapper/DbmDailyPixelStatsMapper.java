package com.mediaiqdigital.doubleclick.batch.mapper;

import java.util.Date;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import com.mediaiqdigital.doubleclick.domain.stats.DbmPixelStats;

/**
 * Created by piyush on 5/5/16.
 */
public class DbmDailyPixelStatsMapper implements FieldSetMapper<DbmPixelStats> {
  
  
  private Date startDate;
  
  public DbmDailyPixelStatsMapper(Date startDate, Date endDate) {
    this.startDate = startDate;
  }
  
  @Override
  public DbmPixelStats mapFieldSet(FieldSet set) throws BindException {
    DbmPixelStats stats = new DbmPixelStats();
    
    stats.setPixel(set.readString("Pixel"));
    stats.setPixelId(set.readLong("Pixel Id"));
    stats.setPixelLoadCount(set.readLong("Pixel Load Count"));
    stats.setPixelStatus(set.readString("Pixel Status"));
    stats.setDate(startDate);
    return stats;
  }
}
