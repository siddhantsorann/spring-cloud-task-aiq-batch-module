package com.mediaiqdigital.doubleclick.batch.service;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Created by piyusha on 13/6/17.
 */
@Service
public class DBMRegularExpressionUtil {
  
  private static final Logger LOGGER = LoggerFactory.getLogger(DBMRegularExpressionUtil.class);
  
  private static final Pattern SIZEM1 = Pattern.compile("DFA_([0-9]*)");
  private static final Pattern SIZEM2 = Pattern.compile("DCM_([0-9]*)");
  private static final Pattern SIZEM3 = Pattern.compile("Flashtalking_([0-9]*)");
  private static final Pattern SIZEM4 = Pattern.compile("Sizmek_([0-9]*)");
  private static final Pattern SIZEM5 = Pattern.compile("Mediaplex_([0-9]*)");
  private static final Pattern SIZEM6 = Pattern.compile("Ebay_([0-9]*)");
  private static final Pattern SIZEM7 = Pattern.compile("Weborama_([0-9]*)");
  // Just update this map whenever new keywords are added
  private static final Map<String, Pattern> m = new HashMap<String, Pattern>() {
    {
      put("DFA_", SIZEM1);
      put("DCM_", SIZEM2);
      put("Flashtalking_", SIZEM3);
      put("Sizmek_", SIZEM4);
      put("Mediaplex_", SIZEM5);
      put("Ebay_", SIZEM6);
      put("Weborama_", SIZEM7);
    }
  };
  String[] keywords =
      {"DFA_", "DCM_", "Flashtalking_", "Sizmek_", "Mediaplex_", "Ebay_", "Weborama_"};
  
  public String findPattern(String tagScript) {
    String result = null;
    for (String k : keywords) {
      if (tagScript.contains(k)) {
        Matcher matcher = m.get(k).matcher(tagScript);
        result = matcher.find() ? StringUtils.remove(matcher.group(0), k) : null;
        break;
      }
    }
    return result;
  }
}
