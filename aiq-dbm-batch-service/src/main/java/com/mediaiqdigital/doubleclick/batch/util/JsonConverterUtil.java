package com.mediaiqdigital.doubleclick.batch.util;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import com.google.api.client.http.HttpResponse;
import com.google.api.services.storage.Storage;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

/**
 * @author prasannachidire on 9/2/18.
 */
public class JsonConverterUtil {
  
  public static void writeToLocalFromGoogleStorage(Storage.Objects.Get get, Class<?> type,
      String localFilePath) throws IOException {
    JsonReader reader = FileDownloadUtil.executAndGetReader(get);
    Gson gson = new Gson();
    reader.beginArray();
    BufferedWriter bw =
        new BufferedWriter(new OutputStreamWriter(new FileOutputStream(localFilePath), "UTF-8"));
    while (reader.hasNext()) {
      bw.write(gson.toJson(type.cast(gson.fromJson(reader, type))));
      bw.newLine();
    }
    bw.flush();
    bw.close();
    reader.close();
  }
  
  public static void writeToLocalFromGoogleStorageWithoutBuffer(Storage.Objects.Get get,
      String localFilePath) throws IOException {
    
    HttpResponse httpResponse = get.executeMedia();
    OutputStream outputStream = new FileOutputStream(localFilePath);
    get.getMediaHttpDownloader().setDirectDownloadEnabled(true);
    httpResponse.download(outputStream);
  }
  
  
}
