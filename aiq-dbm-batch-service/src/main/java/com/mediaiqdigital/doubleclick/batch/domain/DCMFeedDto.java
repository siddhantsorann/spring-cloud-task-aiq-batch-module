package com.mediaiqdigital.doubleclick.batch.domain;

import java.sql.Timestamp;

/**
 * Created by piyushagarwal on 10/5/18.
 */
public class DCMFeedDto {
  
  private Long profileId;
  private String accountName;
  private Long reportId;
  private String isActive;
  private Timestamp dt;
  
  public DCMFeedDto(Long profileId, String accountName, Long reportId, Timestamp dt) {
    this.profileId = profileId;
    this.accountName = accountName;
    this.reportId = reportId;
    this.dt = dt;
  }
  
  public DCMFeedDto(Long profileId, String accountName, Long reportId, String isActive,
      Timestamp dt) {
    this.profileId = profileId;
    this.accountName = accountName;
    this.reportId = reportId;
    this.isActive = isActive;
    this.dt = dt;
  }
  
  public Long getProfileId() {
    return profileId;
  }
  
  public void setProfileId(Long profileId) {
    this.profileId = profileId;
  }
  
  public String getAccountName() {
    return accountName;
  }
  
  public void setAccountName(String accountName) {
    this.accountName = accountName;
  }
  
  public Long getReportId() {
    return reportId;
  }
  
  public void setReportId(Long reportId) {
    this.reportId = reportId;
  }
  
  public Timestamp getDt() {
    return dt;
  }
  
  public void setDt(Timestamp dt) {
    this.dt = dt;
  }
  
  public String getIsActive() {
    return isActive;
  }
  
  public void setIsActive(String isActive) {
    this.isActive = isActive;
  }
  
  @Override
  public String toString() {
    return "DCMFeedDto{" + "profileId=" + profileId + ", accountName='" + accountName + '\''
        + ", reportId=" + reportId + ", isActive='" + isActive + '\'' + ", dt=" + dt + '}';
  }
}
