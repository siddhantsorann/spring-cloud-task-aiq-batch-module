package com.mediaiqdigital.doubleclick.batch.tasklet;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;

import org.mapdb.DBMaker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.JpaRepository;

import com.google.api.services.storage.Storage;
import com.google.gson.JsonArray;
import com.mediaiqdigital.doubleclick.batch.util.FileDownloadUtil;
import com.mediaiqdigital.doubleclick.batch.util.SecurityCredentials;
import com.mediaiqdigital.doubleclick.domain.DbmSummaryItems;
import com.mediaiqdigital.doubleclick.repo.DbmSummaryItemsRepo;

public abstract class AbstractLoadTasklet<TYPE, REPO extends JpaRepository<TYPE, ?>>
    implements Tasklet {
  
  private static final Logger LOGGER = LoggerFactory.getLogger(AbstractLoadTasklet.class);
  @Autowired
  DbmSummaryItemsRepo summaryItemsRepo;
  @Value("${report.file}")
  private String filePath;
  @Value("${google.serviceAccount}")
  private String serviceAccount;
  @Value("${privateId}")
  private String privateId;
  
  @Value("${mapDbDirectory}")
  private String mapDbDirectory;
  
  @Override
  public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext)
      throws Exception {
    
    Storage storage = SecurityCredentials.getCredentials(filePath, serviceAccount);
    
    List<DbmSummaryItems> summaryItemsList = summaryItemsRepo.findByItemName(getTypeName());
    
    for (DbmSummaryItems summaryItems : summaryItemsList) {
      String fileName = summaryItems.getFileName();
      LOGGER.info("FileName is " + fileName);
      
      Set<TYPE> items = createTreeSet(getTypeName());
      Storage.Objects.Get get = storage.objects().get(privateId, "entity/" + fileName);
      JsonArray jsonArray = FileDownloadUtil.execute(get);
      createItems(items, jsonArray);
      
      getRepository().save(items);
    }
    
    return RepeatStatus.FINISHED;
  }
  
  public abstract void createItems(Set<TYPE> items, JsonArray jsonArray);
  
  private Set<TYPE> createTreeSet(String typeName) throws IOException {
    File mapDbDirPath = new File(mapDbDirectory);
    File createTempFile = File.createTempFile("mapdb-" + typeName, "db", mapDbDirPath);
    
    Set<TYPE> items = DBMaker.newFileDB(createTempFile).deleteFilesAfterClose().closeOnJvmShutdown()
        .transactionDisable().make().getHashSet("temp");
    return items;
  }
  
  public abstract REPO getRepository();
  
  public abstract String getTypeName();
  
  public abstract Class<TYPE> getTypeClass();
}
