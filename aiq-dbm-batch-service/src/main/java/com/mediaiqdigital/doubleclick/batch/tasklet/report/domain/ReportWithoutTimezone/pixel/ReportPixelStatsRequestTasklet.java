package com.mediaiqdigital.doubleclick.batch.tasklet.report.domain.ReportWithoutTimezone.pixel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.google.api.services.doubleclickbidmanager.DoubleClickBidManager;
import com.google.api.services.doubleclickbidmanager.model.*;
import com.mediaiqdigital.doubleclick.batch.util.StepAttribute;

/**
 * Created by piyush on 22/6/16.
 */
@Component
@Scope("step")
public class ReportPixelStatsRequestTasklet implements Tasklet {
  
  
  final static Logger logger = LoggerFactory.getLogger(
      com.mediaiqdigital.doubleclick.batch.tasklet.report.domain.ReportWithoutTimezone.ReportRequestTasklet.class);
  @Value("#{jobParameters['start_date']}")
  protected Date startDate;
  @Value("#{jobParameters['end_date']}")
  protected Date endDate;
  @Value("${report.statsFile}")
  String basePath;
  @Value("${report.pixelStatsQueryId}")
  String queryId;
  
  @Autowired
  DoubleClickBidManager dbm;
  
  
  @Override
  public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext)
      throws IOException {
    
    // Query query = new Query();
    // query = createQuery();
    //
    // DoubleClickBidManager.Queries.Createquery createquery = dbm.queries().createquery(query);
    // Long queryId = createquery.execute().getQueryId();
    
    RunQueryRequest runQueryRequest = new RunQueryRequest();
    runQueryRequest.setDataRange("CUSTOM_DATES");
    runQueryRequest.setReportDataStartTimeMs(startDate.getTime());
    runQueryRequest.setReportDataEndTimeMs(endDate.getTime());
    runQueryRequest.setTimezoneCode("Europe/London");
    
    dbm.queries().runquery(Long.valueOf(queryId), runQueryRequest).execute();
    
    logger.info(" query Id " + queryId);
    
    
    ExecutionContext executionContext =
        chunkContext.getStepContext().getStepExecution().getJobExecution().getExecutionContext();
    executionContext.put(StepAttribute.QUERY_ID, queryId);
    
    return RepeatStatus.FINISHED;
  }
  
  public Query createQuery() {
    
    List<String> groupList = new ArrayList<>();
    groupList.add("FILTER_CREATIVE_ID");
    groupList.add("FILTER_LINE_ITEM");
    groupList.add("FILTER_ADVERTISER_TIMEZONE");
    groupList.add("FILTER_ACTIVITY_ID");
    groupList.add("FILTER_DATE");
    groupList.add("FILTER_TIME_OF_DAY");
    groupList.add("FILTER_INSERTION_ORDER");
    
    List<String> metrics = new ArrayList<>();
    metrics.add("METRIC_CLICKS");
    metrics.add("METRIC_CTR");
    metrics.add("METRIC_IMPRESSIONS");
    metrics.add("METRIC_TOTAL_CONVERSIONS");
    
    
    FilterPair filterPair = new FilterPair();
    filterPair.setType("FILTER_PARTNER");
    filterPair.setValue("485089");
    List<FilterPair> filterPairList = new ArrayList<>();
    filterPairList.add(filterPair);
    
    
    Query query = new Query();
    query.setKind("doubleclickbidmanager#query");
    query.setMetadata(new QueryMetadata().setFormat("CSV").setDataRange("CURRENT_DAY")
        .setLocale("en").setTitle("DAILY_STATS_REPORT").setLatestReportRunTimeMs(36000000L));
    
    query.setParams(new Parameters().setType("TYPE_GENERAL").setGroupBys(groupList)
        .setFilters(filterPairList).setMetrics(metrics));
    
    query.setSchedule(new QuerySchedule().setFrequency("DAILY").setNextRunMinuteOfDay(875));
    query.setQueryId(22L);
    
    
    return query;
  }
}
