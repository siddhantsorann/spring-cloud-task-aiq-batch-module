package com.mediaiqdigital.doubleclick.batch.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import com.google.api.client.http.HttpResponse;
import com.google.api.services.storage.Storage.Objects.Get;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

public class FileDownloadUtil {
  
  public static JsonArray execute(Get get) throws IOException {
    HttpResponse httpResponse = get.executeMedia();
    
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    get.getMediaHttpDownloader().setDirectDownloadEnabled(true);
    httpResponse.download(out);
    
    JsonArray jsonArray = new JsonParser().parse(out.toString()).getAsJsonArray();
    return jsonArray;
  }
  
  public static JsonReader executAndGetReader(Get get) throws IOException {
    HttpResponse httpResponse = get.executeMedia();
    
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    get.getMediaHttpDownloader().setDirectDownloadEnabled(true);
    httpResponse.download(out);
    
    ByteArrayInputStream inStream = new ByteArrayInputStream(out.toByteArray());
    JsonReader reader = new JsonReader(new InputStreamReader(inStream, "UTF-8"));
    return reader;
  }
  
  public static ByteArrayOutputStream executeAndReturnByteStream(Get get) throws IOException {
    HttpResponse httpResponse = get.executeMedia();
    
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    get.getMediaHttpDownloader().setDirectDownloadEnabled(true);
    httpResponse.download(out);
    
    return out;
  }
}
