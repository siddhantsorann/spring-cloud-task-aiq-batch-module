package com.mediaiqdigital.doubleclick.batch.tasklet.report.domain.ReportWithoutTimezone;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.google.api.services.doubleclickbidmanager.DoubleClickBidManager;
import com.google.api.services.doubleclickbidmanager.model.Query;
import com.mediaiq.spring.batch.aspect.Retry;
import com.mediaiqdigital.doubleclick.batch.exception.ReportNotReadyException;
import com.mediaiqdigital.doubleclick.batch.tasklet.report.domain.ReportStatusCheckTimezoneTasklet;
import com.mediaiqdigital.doubleclick.batch.util.StepAttribute;

/**
 * Created by piyush on 22/7/16.
 */

@Component
@Scope("step")
public class ReportStatusCheckTasklet implements Tasklet {
  
  final static Logger logger = LoggerFactory.getLogger(ReportStatusCheckTimezoneTasklet.class);
  @Value("#{jobParameters['start_date']}")
  protected Date startDate;
  @Value("#{jobParameters['end_date']}")
  protected Date endDate;
  @Value("${report.statsFile}")
  String basePath;
  @Autowired
  DoubleClickBidManager dbm;
  @Value("#{jobExecutionContext['" + StepAttribute.QUERY_ID + "']}")
  private String queryId;
  private Integer attempt = 1;
  
  @Retry
  @Override
  public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext)
      throws Exception {
    
    System.out.println(" start time " + startDate.getTime());
    System.out.println(" end time " + endDate.getTime());
    logger.info("Attempt {} to check status of report with queryID: {}", attempt, queryId);
    // Grab the report.
    String reportPathS = null;
    Query queryResponse = dbm.queries().getquery(Long.valueOf(queryId).longValue()).execute();
    System.out.println(" queryresp " + queryResponse);
    reportPathS = queryResponse.getMetadata().getGoogleCloudStoragePathForLatestReport();
    boolean isRunning = queryResponse.getMetadata().getRunning();
    if (reportPathS == null || reportPathS.isEmpty() || isRunning) {
      logger.warn("Report not ready yet. Trying again.");
      attempt++;
      throw new ReportNotReadyException("Invalid attempt");
    }
    logger.info(" report URL " + reportPathS);
    ExecutionContext executionContext =
        chunkContext.getStepContext().getStepExecution().getJobExecution().getExecutionContext();
    executionContext.put(StepAttribute.REPORT_URL, reportPathS);
    
    return RepeatStatus.FINISHED;
  }
  
}
