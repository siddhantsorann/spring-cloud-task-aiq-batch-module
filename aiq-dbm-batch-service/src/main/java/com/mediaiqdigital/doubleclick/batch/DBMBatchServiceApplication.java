package com.mediaiqdigital.doubleclick.batch;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableAutoConfiguration
@EnableScheduling
@ComponentScan(basePackages = {"com.mediaiqdigital.doubleclick.batch", "com.mediaiq.spring.batch","com.mediaiq.aiq.retrofit"})
@EntityScan(basePackages = {"com.mediaiqdigital.doubleclick.domain",
    "com.mediaiqdigital.aiq.tradingcenter.domain", "com.mediaiq.aiq.miq.domain",
    "com.mediaiq.appnexus.domain", "com.mediaiq.aiq.mediamath.domain"})
@EnableJpaRepositories(basePackages = {"com.mediaiqdigital.doubleclick.repo",
    "com.mediaiqdigital.aiq.tradingcenter.repo"

})

@ImportResource({"classpath:spring-integration-config.xml", "classpath:spring-batch-config.xml",
    "doubleclick-batch-jobs.xml", "classpath:dbm-batch-service-retry-config.xml"})
// @EnableOAuth2Resource

@EnableTransactionManagement
public class DBMBatchServiceApplication {
  public static void main(String[] args) {
    
    new SpringApplicationBuilder(DBMBatchServiceApplication.class).web(true).run(args);
  }
  
}
