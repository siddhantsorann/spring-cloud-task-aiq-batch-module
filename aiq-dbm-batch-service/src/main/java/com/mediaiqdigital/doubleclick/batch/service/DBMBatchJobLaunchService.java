package com.mediaiqdigital.doubleclick.batch.service;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.ParseException;
import java.util.concurrent.ExecutionException;

import org.codehaus.jettison.json.JSONException;

import com.mediaiqdigital.doubleclick.batch.service.impl.DateParameter;

public interface DBMBatchJobLaunchService {
  
  void launchSummaryItemJob()
      throws ClassNotFoundException, GeneralSecurityException, JSONException, IOException;
  
  void launchAdvertiserLoadJob();
  
  void launchBrowserLoadJob();
  
  void launchLanguageLoadJob();
  
  void launchDeviceCriteriaLoadJob();
  
  void launchIspLoadJob();
  
  void launchGeoLocationLoadJob();
  
  void launchInsertionOrderLoadJob();
  
  void launchPartnerLoadJob();
  
  void launchPlacementLoadJob();
  
  void launchPixelLoadJob();
  
  void launchCreativeLoadJob();
  
  void launchLineItemLoadJob();
  
  void uploadFileLoadJob();
  
  void launchUniversalSiteLoadJob();
  
  void launchInventorySourceLoadJob();
  
  void launchDailyPlacementStatsLoadJob(DateParameter startDate, DateParameter endDate)
      throws InterruptedException, ExecutionException;
  
  void launchDailyPixelLevelStatsLoadJob() throws ParseException;
  
  void launchDailyPixelLevelStatsLoadJob(DateParameter start_date, DateParameter end_date);
  
  void launchDailyPlacementTimeZoneStatsLoadJob(DateParameter start_date, DateParameter end_date);
  
  
  void launchDailyInsertionOrderStatsLoadJob(DateParameter start_date, DateParameter end_date);
  
  void launchDailyIOPlacementStatsLoadJob(DateParameter start_date, DateParameter end_date);
  
  void launchDailyPixelStatsLoadJob(DateParameter start_date, DateParameter end_date);
  
  void launchDailyPlacementStatsLoadCronJob()
      throws ParseException, ExecutionException, InterruptedException;
  
  void launchDailyPlacementStatsAndTriggerTcLoadCronJob()
      throws ParseException, ExecutionException, InterruptedException;
  
  void launchDailyPlacementStatsAndTriggerTcLoadCronJob(DateParameter startDate,
      DateParameter endDate) throws ExecutionException, InterruptedException;
  
  void launchDailyPlacementStatsTimezoneLoadCronJob() throws ParseException;
  
  
  void launchDailyInsertionOrderStatsLoadCronJob() throws ParseException;
  
  void launchExchangeLoadJob();
  
  void launchDailyTrueViewPlacementStatsLoadCronJob() throws ParseException;
  
  void launchDailyTrueViewPlacementStatsLoadJob(DateParameter start_date, DateParameter end_date);
  
  void launchPlacementCreativeMappingLoadJob();
  
  void launchPlacementCreativeMappingLoadJob(DateParameter lastModified);
  
  void launchDfaProfileForSeat1LoadJob();

  void launchDfaProfileForSeat2LoadJob();

}
