package com.mediaiqdigital.doubleclick.batch.tasklet.dfaProfile;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;


import com.google.api.client.auth.oauth2.CredentialRefreshListener;
import com.google.api.client.auth.oauth2.TokenErrorResponse;
import com.google.api.client.auth.oauth2.TokenResponse;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.mediaiqdigital.doubleclick.batch.util.SecurityUtilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.dfareporting.Dfareporting;
import com.google.api.services.dfareporting.DfareportingScopes;

/**
 * Utility methods used by all DFA Reporting and Trafficking API samples.
 */
@Service
public class DfaReportingFactory {

  private static final Logger LOGGER = LoggerFactory.getLogger(DfaReportingFactory.class);
  private static String dataDirectory;
  private static String clientSecret;

  // HTTP Transport object used for automatically refreshing access tokens.
  static final NetHttpTransport HTTP_TRANSPORT = new NetHttpTransport();

  private static FileDataStoreFactory dataStoreFactory;
  // JSON factory used for parsing refresh token responses.
  static final JacksonFactory JSON_FACTORY = new JacksonFactory();


  public static NetHttpTransport getHttpTransport() {
    return HTTP_TRANSPORT;
  }

  public static Credential getUserCredential() throws Exception {

    File datastoreDirectory = new File(dataDirectory);

    LOGGER.info("final dataDir {} and client {}",datastoreDirectory,clientSecret);

    GoogleClientSecrets clientSecrets =
        GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(
            SecurityUtilities.class.getResourceAsStream(clientSecret)));
    dataStoreFactory = new FileDataStoreFactory(datastoreDirectory);

    LOGGER.info("final secrets {}",clientSecrets);

    LOGGER.info("final dataStoreFactory {}",dataStoreFactory);

    // set up authorization code flow.
    GoogleAuthorizationCodeFlow flow =
        new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY, clientSecrets,DfareportingScopes.all())
            .setDataStoreFactory(dataStoreFactory).setApprovalPrompt("auto")
            .setAccessType("offline").addRefreshListener(new CredentialRefreshListener() {
          @Override
          public void onTokenResponse(Credential credential, TokenResponse tokenResponse)
              throws IOException {

            LOGGER.info("Token Refreshed Succesfully");
          }

          @Override
          public void onTokenErrorResponse(Credential credential,
              TokenErrorResponse tokenErrorResponse) throws IOException {

            LOGGER.info("ERROR WITH TOKEN ");
          }
        }).build();

    // authorize and get credentials.
    Credential credential = new AuthorizationCodeInstalledApp(flow,
        new LocalServerReceiver.Builder().setHost("localhost").setPort(10000).build())
        .authorize("jarvis@mediaiqdigital.com").setExpiresInSeconds(31536000L);

    return credential;
  }

  private HttpRequestInitializer setHttpTimeout(final HttpRequestInitializer requestInitializer) {
    return new HttpRequestInitializer() {
      public void initialize(HttpRequest httpRequest) throws IOException {
        requestInitializer.initialize(httpRequest);
        httpRequest.setReadTimeout(3 * 60000); // 3 minutes read timeout.
      }
    };
  }


  /**
   * Performs all necessary setup steps for running requests against the API.
   *
   * @return An initialized {@link Dfareporting} service object.
   */
  public static Dfareporting getInstance(String directory, String secret) throws Exception {

    dataDirectory=directory;
    clientSecret=secret;
    Credential credential = getUserCredential();

    LOGGER.info("credential status {}",credential.getExpirationTimeMilliseconds());

    // Create Dfareporting client.
    return new Dfareporting.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential)
        .setApplicationName("TestUserHDDfa").build();
  }
}
