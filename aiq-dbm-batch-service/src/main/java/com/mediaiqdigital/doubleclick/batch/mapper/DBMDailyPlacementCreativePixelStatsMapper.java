package com.mediaiqdigital.doubleclick.batch.mapper;

import java.util.Date;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import com.mediaiq.aiq.domain.Dsp;
import com.mediaiqdigital.aiq.tradingcenter.domain.id.PixelLevelStatsId;
import com.mediaiqdigital.aiq.tradingcenter.domain.stats.DailyPixelLevelStats;

/**
 * Created by piyusha on 9/6/17.
 */
public class DBMDailyPlacementCreativePixelStatsMapper
    implements FieldSetMapper<DailyPixelLevelStats> {
  
  public DBMDailyPlacementCreativePixelStatsMapper(Date startDate, Date endDate) {}
  
  @Override
  public DailyPixelLevelStats mapFieldSet(FieldSet set) throws BindException {
    DailyPixelLevelStats stats = new DailyPixelLevelStats();
    
    Long creativeId = set.readLong("Creative ID");
    Long lineItemId = set.readLong("Line Item ID");
    
    Long pixelDCMID = set.readLong("Conversion Pixel DCM ID");
    Long placementDCMID = set.readLong("DCM Placement ID");
    
    Integer timeOfDay = set.readInt("Time of Day");
    Date date = set.readDate("Date", "yyyy/MM/dd");
    Date dateTime = set.readDate("DateTime", "yyyy-MM-dd HH:mm:ss");
    
    Long pixelId = set.readLong("Conversion Pixel ID");
    stats.setInsertionOrderId(set.readLong("Insertion Order ID"));
    stats.setId(
        new PixelLevelStatsId(creativeId, lineItemId, pixelId, date, timeOfDay, Dsp.DoubleClick));
    stats.setDateTime(dateTime);
    stats.setPixelDCMId(pixelDCMID);
    stats.setPlacementDCMId(placementDCMID);
    stats.setClicks(set.readInt("Clicks"));
    stats.setImpressions(set.readInt("Impressions"));
    stats.setTotalConversions(set.readInt("Total Conversions"));
    
    return stats;
  }
  
}
