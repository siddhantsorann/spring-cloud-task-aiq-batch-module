package com.mediaiqdigital.doubleclick.batch.processor;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.file.ResourceAwareItemReaderItemStream;
import org.springframework.batch.item.support.AbstractItemCountingItemStreamItemReader;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.util.ClassUtils;

import com.google.gson.Gson;

import sun.reflect.generics.reflectiveObjects.ParameterizedTypeImpl;

/**
 * @author prasannachidire on 30/7/18.
 */
@SuppressWarnings("rawtypes")
@Component
public class JsonFileListItemReader extends AbstractItemCountingItemStreamItemReader
    implements ResourceAwareItemReaderItemStream, InitializingBean {
  
  private static final Logger LOGGER = LoggerFactory.getLogger(JsonFileListItemReader.class);
  
  private Resource resource;
  
  private List items;
  private String classToBound;
  private int index = 0;
  private Gson gson = new Gson();
  
  public void setClassToBound(String classToBound) {
    this.classToBound = classToBound;
  }
  
  ObjectMapper mapper = new ObjectMapper();
  
  public JsonFileListItemReader() {
    setName(ClassUtils.getShortName(JsonFileListItemReader.class));
  }
  
  /**
   * Public setter for the input resource.
   */
  public void setResource(Resource resource) {
    this.resource = resource;
  }
  
  
  @Override
  protected Object doRead() throws Exception {
    return (this.items == null || index >= this.items.size()) ? null : this.items.get(index++);
  }
  
  @Override
  protected void doClose() throws Exception {}
  
  @Override
  protected void doOpen() throws IOException, ClassNotFoundException {
    Assert.notNull(resource, "Input resource must be set");
    
    if (!resource.exists()) {
      LOGGER.warn("Input resource does not exist " + resource.getDescription());
      return;
    }
    
    if (!resource.isReadable()) {
      LOGGER.warn("Input resource is not readable " + resource.getDescription());
      return;
    }
    Reader reader = new InputStreamReader(resource.getInputStream(), "UTF-8");
    Type type =
        ParameterizedTypeImpl.make(List.class, new Type[] {Class.forName(this.classToBound)}, null);
    this.items = gson.fromJson(reader, type);
    LOGGER.info("Items processed** {}", this.getCurrentItemCount());
    
  }
  
  @Override
  public void afterPropertiesSet() throws Exception {
    
  }
}
