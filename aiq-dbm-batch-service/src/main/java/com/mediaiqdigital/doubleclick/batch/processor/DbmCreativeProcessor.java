package com.mediaiqdigital.doubleclick.batch.processor;

import org.apache.commons.lang.StringUtils;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.mediaiqdigital.doubleclick.batch.service.DBMRegularExpressionUtil;
import com.mediaiqdigital.doubleclick.domain.DbmCreative;

/**
 * @author prasannachidire on 8/2/18.
 */
@Component
public class DbmCreativeProcessor implements ItemProcessor<String, DbmCreative> {
  
  @Autowired
  DBMRegularExpressionUtil dbmRegularExpressionUtil;
  
  
  @Override
  public DbmCreative process(String dbmCreativeJson) throws Exception {
    
    Gson gson = new Gson();
    DbmCreative dbmCreative = gson.fromJson(dbmCreativeJson, DbmCreative.class);
    dbmCreative.setId(dbmCreative.getCommonData().getId());
    dbmCreative.setName(dbmCreative.getCommonData().getName());
    dbmCreative.setIntegrationCode(dbmCreative.getCommonData().getIntegrationCode());
    dbmCreative.setActive(dbmCreative.getCommonData().getActive());
    if (StringUtils.isNotEmpty(dbmCreative.getName())) {
      String clientCreativeId = dbmRegularExpressionUtil.findPattern(dbmCreative.getName());
      if (StringUtils.isNotEmpty(clientCreativeId))
        dbmCreative.setClientCreativeId(clientCreativeId);
      
    }
    return dbmCreative;
  }
}
