package com.mediaiqdigital.doubleclick.batch.tasklet;

import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.api.services.storage.Storage;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.mediaiqdigital.doubleclick.batch.util.FileDownloadUtil;
import com.mediaiqdigital.doubleclick.batch.util.SecurityCredentials;
import com.mediaiqdigital.doubleclick.domain.DbmPixel;
import com.mediaiqdigital.doubleclick.domain.DbmSummaryItems;
import com.mediaiqdigital.doubleclick.repo.DbmPixelRepo;
import com.mediaiqdigital.doubleclick.repo.DbmSummaryItemsRepo;

/**
 * Created by piyush on 1/12/15.
 */
@Component
public class PixelLoadTasklet implements Tasklet {
  
  
  private static final Logger LOGGER = LoggerFactory.getLogger(PixelLoadTasklet.class);
  
  
  @Autowired
  DbmSummaryItemsRepo summaryItemsRepo;
  
  @Autowired
  DbmPixelRepo pixelRepo;
  
  @Value("${report.file}")
  private String filePath;
  
  @Value("${google.serviceAccount}")
  private String serviceAccount;
  
  @Value("${privateId}")
  private String privateId;
  
  @Override
  public RepeatStatus execute(StepContribution contribution, ChunkContext context)
      throws Exception {
    
    Storage storage = SecurityCredentials.getCredentials(filePath, serviceAccount);
    
    List<DbmSummaryItems> summaryItemsList = summaryItemsRepo.findByItemName("Pixel");
    
    for (DbmSummaryItems summaryItems : summaryItemsList) {
      
      String fileName = summaryItems.getFileName();
      LOGGER.info("FileName is " + fileName);
      Storage.Objects.Get get = storage.objects().get(privateId, "entity/" + fileName);
      JsonArray jsonArray = FileDownloadUtil.execute(get);
      Gson gson = new Gson();
      List<DbmPixel> pixels = new LinkedList<>();
      for (int i = 0; i < jsonArray.size(); i++) {
        JsonElement str = jsonArray.get(i);
        DbmPixel pixel = gson.fromJson(str, DbmPixel.class);
        pixel.setId(pixel.getPixelCommonData().getId());
        pixel.setName(pixel.getPixelCommonData().getName());
        pixel.setIntegrationCode(pixel.getPixelCommonData().getIntegrationCode());
        
        LOGGER.info(" Object is " + pixel);
        LOGGER.info(" String is " + str);
        LOGGER.info("-------");
        pixels.add(pixel);
      }
      
      pixelRepo.save(pixels);
    }
    
    
    return RepeatStatus.FINISHED;
    
  }
}
