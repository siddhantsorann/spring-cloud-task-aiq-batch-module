package com.mediaiq.aiq.redshift.batch.processor.dbm;


import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import com.mediaiqdigital.doubleclick.domain.DbmCreative;

@Component
public class DbmCreativePushToRedshiftProcessor implements ItemProcessor<DbmCreative, DbmCreative> {
  
  @Override
  public DbmCreative process(DbmCreative dbmCreative) throws Exception {
    String name = dbmCreative.getName();
    dbmCreative.setName((name != null) ? name.trim().replaceAll("\t", " ") : name);
    return dbmCreative;
  }
  
  
}
