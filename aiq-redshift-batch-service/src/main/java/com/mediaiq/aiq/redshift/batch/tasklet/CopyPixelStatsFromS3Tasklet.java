package com.mediaiq.aiq.redshift.batch.tasklet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.aiq.aws.s3.S3Service;

/**
 * Created by prasannachidire on 16-Jun-2017
 */

@Component
@Scope("step")
public class CopyPixelStatsFromS3Tasklet implements Tasklet {
  
  final static Logger LOGGER = LoggerFactory.getLogger(CopyPixelStatsFromS3Tasklet.class);
  
  @Autowired
  S3Service s3Client;
  
  @Value("${s3.pixel.destination}")
  private String downloadFolderNameLocal;
  
  @Value("#{'${s3.pixel.path}'}")
  private String keyPrefix;
  
  @Value("#{'${s3.pixel.bucket}'}")
  private String bucketName;
  
  @Override
  public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext)
      throws Exception {
    
    LOGGER.info("Started downloading file from s3 : bucket:{}, path:{}", bucketName, keyPrefix);
    s3Client.downloadDirectory(bucketName, keyPrefix, downloadFolderNameLocal);
    LOGGER.info("Downloaded file from s3 : bucket:{}, path:{}", bucketName, keyPrefix);
    return RepeatStatus.FINISHED;
  }
  
}
