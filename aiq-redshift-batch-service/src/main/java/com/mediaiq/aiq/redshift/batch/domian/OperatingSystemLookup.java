package com.mediaiq.aiq.redshift.batch.domian;

public class OperatingSystemLookup {
  private Long Id;
  private String osFamily;
  private String osName;
  
  public Long getId() {
    return Id;
  }
  
  public void setId(Long id) {
    Id = id;
  }
  
  public String getOsFamily() {
    return osFamily;
  }
  
  public void setOsFamily(String osFamily) {
    this.osFamily = osFamily;
  }
  
  public String getOsName() {
    return osName;
  }
  
  public void setOsName(String osName) {
    this.osName = osName;
  }
  
  @Override
  public String toString() {
    return "OperatingSystemLookup [Id=" + Id + ", osFamily=" + osFamily + ", osName=" + osName
        + "]";
  }
  
  
}
