package com.mediaiq.aiq.redshift.batch.processor;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import com.mediaiq.aiq.redshift.batch.domian.AdvertiserLookup;

@Component
public class AdvertiserProcessor implements ItemProcessor<AdvertiserLookup, AdvertiserLookup> {
  
  @Override
  public AdvertiserLookup process(AdvertiserLookup miqAdvertiser) throws Exception {
    
    return miqAdvertiser;
  }
  
}
