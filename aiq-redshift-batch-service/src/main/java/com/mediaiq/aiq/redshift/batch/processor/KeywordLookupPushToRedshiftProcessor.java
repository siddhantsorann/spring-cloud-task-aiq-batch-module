package com.mediaiq.aiq.redshift.batch.processor;


import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import com.mediaiq.aiq.miq.domain.lookup.KeywordLookup;
import com.mediaiq.aiq.redshift.batch.domian.RedshiftKeywordLookup;

@Component
public class KeywordLookupPushToRedshiftProcessor
    implements ItemProcessor<KeywordLookup, RedshiftKeywordLookup> {
  
  @Override
  public RedshiftKeywordLookup process(KeywordLookup keywordLookup) throws Exception {
    
    RedshiftKeywordLookup redshiftKeywordLookup = new RedshiftKeywordLookup(keywordLookup.getId(),
        keywordLookup.getKeyword(), keywordLookup.getIsBlacklisted(),
        (keywordLookup.getIabCategory() == null || keywordLookup.getIabCategory().getName() == null)
            ? ""
            : keywordLookup.getIabCategory().getName(),
        (keywordLookup.getLookupCategory() == null
            || keywordLookup.getLookupCategory().getName() == null) ? ""
                : keywordLookup.getLookupCategory().getName(),
        (keywordLookup.getKeywordLookupCategory() == null
            || keywordLookup.getKeywordLookupCategory().getName() == null) ? ""
                : keywordLookup.getKeywordLookupCategory().getName());
    
    return redshiftKeywordLookup;
  }
  
  
}
