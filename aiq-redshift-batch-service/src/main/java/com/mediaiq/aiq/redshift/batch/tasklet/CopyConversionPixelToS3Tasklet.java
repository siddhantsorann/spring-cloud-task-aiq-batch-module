
package com.mediaiq.aiq.redshift.batch.tasklet;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("step")
public class CopyConversionPixelToS3Tasklet extends CopyToS3Tasklet {
  
  @Value("${s3.conversion-pixel.source.filename}")
  private String uploadFileName;
  
  @Value("#{'${s3.conversion-pixel.bucket}' + '${s3.conversion-pixel.path}'}")
  private String bucketName;
  
  @Value("${s3.conversion-pixel.destination.filename}")
  private String keyName;
  
  @Override
  protected String getKeyName() {
    return keyName;
  }
  
  @Override
  protected String getBucketName() {
    return bucketName;
  }
  
  @Override
  protected String getUploadFileName() {
    return uploadFileName;
  }
}
