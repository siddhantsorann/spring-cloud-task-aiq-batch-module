package com.mediaiq.aiq.redshift.batch.domian;

public class AdvertiserLookup {
  
  public Long getId() {
    return id;
  }
  
  public void setId(Long id) {
    this.id = id;
  }
  
  public String getName() {
    return name;
  }
  
  public void setName(String name) {
    this.name = name;
  }
  
  public String getCategory() {
    return category;
  }
  
  public void setCategory(String category) {
    this.category = category;
  }
  
  public String getSubcategory() {
    return subcategory;
  }
  
  public void setSubcategory(String subcategory) {
    this.subcategory = subcategory;
  }
  
  public Long getAppnexusAdvertiserId() {
    return appnexusAdvertiserId;
  }
  
  public void setAppnexusAdvertiserId(Long appnexusAdvertiserId) {
    this.appnexusAdvertiserId = appnexusAdvertiserId;
  }
  
  private Long id;
  private String name;
  private String category;
  private String subcategory;
  private Long appnexusAdvertiserId;
  
}
