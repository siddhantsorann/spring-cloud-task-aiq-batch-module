package com.mediaiq.aiq.redshift.batch.processor;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.domain.LineItemAudit;

@Component
public class LineItemAuditPushToRedshiftProcessor
    implements ItemProcessor<LineItemAudit, LineItemAudit> {
  
  @Override
  public LineItemAudit process(LineItemAudit lineItemAudit) throws Exception {
    
    return lineItemAudit;
  }
  
}
