package com.mediaiq.aiq.redshift.batch.row.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.RowMapper;

import com.mediaiq.aiq.redshift.batch.domian.OperatingSystemLookup;

@Configuration(value = "OperatingSystemRowMapper")
public class OperatingSystemRowMapper implements RowMapper<OperatingSystemLookup> {
  
  @Override
  public OperatingSystemLookup mapRow(ResultSet rs, int rowNum) throws SQLException {
    
    OperatingSystemLookup operatingSystemLookup = new OperatingSystemLookup();
    
    operatingSystemLookup.setId(rs.getLong("os_id"));
    operatingSystemLookup.setOsFamily(rs.getString("os_family"));
    operatingSystemLookup.setOsName(rs.getString("os_name"));
    
    return operatingSystemLookup;
  }
  
}
