package com.mediaiq.aiq.redshift.batch.processor;

import org.apache.commons.lang.StringUtils;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import com.mediaiq.aiq.miq.domain.lookup.AdvertiserSegmentsLookup;
import com.mediaiq.aiq.redshift.batch.domian.RedshiftAdvertiserSegment;

@Component
public class AdvertiserSegmentPushToRedshiftProcessor
    implements ItemProcessor<AdvertiserSegmentsLookup, RedshiftAdvertiserSegment> {
  
  @Override
  public RedshiftAdvertiserSegment process(AdvertiserSegmentsLookup advertiserSegmentLookup)
      throws Exception {
    
    RedshiftAdvertiserSegment redshiftAdvertiserSegment =
        new RedshiftAdvertiserSegment(advertiserSegmentLookup.getSegment().getId(),
            StringUtils.trim(advertiserSegmentLookup.getSegment().getName()),
            advertiserSegmentLookup.getPixelType() == null ? ""
                : advertiserSegmentLookup.getPixelType().name(),
            advertiserSegmentLookup.getSegment().getAdvertiserId(),
            (advertiserSegmentLookup.getLookupCategory() == null
                || advertiserSegmentLookup.getLookupCategory().getName() == null) ? ""
                    : advertiserSegmentLookup.getLookupCategory().getName(),
            advertiserSegmentLookup.isRequiredForSrs());
    
    return redshiftAdvertiserSegment;
  }
  
}
