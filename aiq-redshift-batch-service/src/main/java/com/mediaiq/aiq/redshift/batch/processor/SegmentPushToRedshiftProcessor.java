package com.mediaiq.aiq.redshift.batch.processor;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import com.mediaiq.aiq.redshift.batch.domian.RedshiftSegment;
import com.mediaiq.appnexus.domain.Segment;

@Component
public class SegmentPushToRedshiftProcessor implements ItemProcessor<Segment, RedshiftSegment> {
  
  @Override
  public RedshiftSegment process(Segment item) throws Exception {
    
    String name = item.getName();
    
    name = (name == null) ? null : name.replaceAll("	", "");
    RedshiftSegment redshiftSegment = new RedshiftSegment(item.getId(), item.getCode(),
        item.getState(), name, item.getDescription(), item.getMemberId(), item.getCategory(),
        item.getAdvertiserId(), item.getLastModified(), item.getParentSegmentId(), item.getSeat());
    return redshiftSegment;
  }
}
