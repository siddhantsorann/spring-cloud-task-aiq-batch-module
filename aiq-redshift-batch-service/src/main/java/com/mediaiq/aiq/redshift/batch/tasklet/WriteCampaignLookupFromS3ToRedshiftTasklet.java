package com.mediaiq.aiq.redshift.batch.tasklet;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("step")
public class WriteCampaignLookupFromS3ToRedshiftTasklet extends WriteFromS3ToSrsRedshiftTasklet {
  
  @Value("${s3.campaign-lookup.query.delete}")
  private String deleteQuery;
  
  @Value("${s3.campaign-lookup.query.copy}")
  private String copyQuery;
  
  @Override
  protected String getDeleteQuery() {
    return deleteQuery;
  }
  
  @Override
  protected String getCopyQuery() {
    return copyQuery;
  }
}
