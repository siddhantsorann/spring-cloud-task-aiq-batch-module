package com.mediaiq.aiq.redshift.batch.service;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.ws.rs.PathParam;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.mediaiqdigital.aiq.tradingcenter.domain.stats.DailyPixelLevelStats;

/**
 * Created by prasannachidire on 12-Jun-2017
 */
@Service
public interface RedshiftPixelStatService {
  
  public void uploadByDates(@PathParam("start_date") Date startDate,
      @PathParam("end_date") Date endDate) throws DataAccessException, IOException;
  
  public void save(List<DailyPixelLevelStats> dailyPixelLevelStats);
  
  public void uploadFile(String filePath);
}
