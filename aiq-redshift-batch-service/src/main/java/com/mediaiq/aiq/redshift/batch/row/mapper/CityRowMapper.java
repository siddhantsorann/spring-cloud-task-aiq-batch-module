package com.mediaiq.aiq.redshift.batch.row.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.RowMapper;

import com.mediaiq.aiq.redshift.batch.domian.CityLookup;

@Configuration(value = "cityRowMapper")
public class CityRowMapper implements RowMapper<CityLookup> {
  
  @Override
  public CityLookup mapRow(ResultSet rs, int rowNum) throws SQLException {
    
    CityLookup city = new CityLookup();
    city.setCountryCode(rs.getString("country_code"));
    city.setCountryId(rs.getLong("country_id"));
    city.setCountryName(rs.getString("country_name"));
    city.setRegionId(rs.getLong("region_id"));
    city.setRegionCode(rs.getString("region_code"));
    city.setRegionName(rs.getString("region_name"));
    city.setCityName(rs.getString("city_name"));
    
    return city;
  }
}
