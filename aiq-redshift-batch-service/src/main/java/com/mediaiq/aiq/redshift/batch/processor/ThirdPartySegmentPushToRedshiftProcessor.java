package com.mediaiq.aiq.redshift.batch.processor;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import com.mediaiq.aiq.miq.domain.lookup.ThirdPartySegmentsLookup;
import com.mediaiq.aiq.redshift.batch.domian.RedshiftThirdPartySegment;

@Component
public class ThirdPartySegmentPushToRedshiftProcessor
    implements ItemProcessor<ThirdPartySegmentsLookup, RedshiftThirdPartySegment> {
  
  @Override
  public RedshiftThirdPartySegment process(ThirdPartySegmentsLookup thirdPartySegmentsLookup)
      throws Exception {
    
    RedshiftThirdPartySegment redshiftThirdPartySegment =
        new RedshiftThirdPartySegment(thirdPartySegmentsLookup.getSegment().getId(),
            thirdPartySegmentsLookup.getSegment().getName() == null ? ""
                : thirdPartySegmentsLookup.getSegment().getName().replaceAll("\\s+", ""),
            (thirdPartySegmentsLookup.getIabCategory() == null
                || thirdPartySegmentsLookup.getIabCategory().getName() == null) ? ""
                    : thirdPartySegmentsLookup.getIabCategory().getName(),
            (thirdPartySegmentsLookup.getLookupCategory() == null
                || thirdPartySegmentsLookup.getLookupCategory().getName() == null) ? ""
                    : thirdPartySegmentsLookup.getLookupCategory().getName(),
            (thirdPartySegmentsLookup.getThirdPartyCategory() == null
                || thirdPartySegmentsLookup.getThirdPartyCategory().getName() == null) ? ""
                    : thirdPartySegmentsLookup.getThirdPartyCategory().getName(),
            (thirdPartySegmentsLookup.getDataProvider() == null ? ""
                : thirdPartySegmentsLookup.getDataProvider().getName()),
            thirdPartySegmentsLookup.getSegment().getState());
    
    return redshiftThirdPartySegment;
  }
  
}
