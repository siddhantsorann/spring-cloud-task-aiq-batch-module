package com.mediaiq.aiq.redshift.batch.domian;

public class CampaignLookup {
  
  private Long advertiserId;
  private String advertiserName;
  private Long ioId;
  private String ioName;
  private Long lineItemId;
  private String lineItemName;
  private Long placementId;
  private String placementName;
  
  public Long getAdvertiserId() {
    return advertiserId;
  }
  
  public void setAdvertiserId(Long advertiserId) {
    this.advertiserId = advertiserId;
  }
  
  public String getAdvertiserName() {
    return advertiserName;
  }
  
  public void setAdvertiserName(String advertiserName) {
    this.advertiserName = advertiserName;
  }
  
  public Long getIoId() {
    return ioId;
  }
  
  public void setIoId(Long ioId) {
    this.ioId = ioId;
  }
  
  public String getIoName() {
    return ioName;
  }
  
  public void setIoName(String ioName) {
    this.ioName = ioName;
  }
  
  public Long getLineItemId() {
    return lineItemId;
  }
  
  public void setLineItemId(Long lineItemId) {
    this.lineItemId = lineItemId;
  }
  
  public String getLineItemName() {
    return lineItemName;
  }
  
  public void setLineItemName(String lineItemName) {
    this.lineItemName = lineItemName;
  }
  
  public Long getPlacementId() {
    return placementId;
  }
  
  public void setPlacementId(Long placementId) {
    this.placementId = placementId;
  }
  
  public String getPlacementName() {
    return placementName;
  }
  
  public void setPlacementName(String placementName) {
    this.placementName = placementName;
  }
  
  @Override
  public String toString() {
    return "CampaignLookup [advertiserId=" + advertiserId + ", advertiserName=" + advertiserName
        + ", ioId=" + ioId + ", ioName=" + ioName + ", lineItemId=" + lineItemId + ", lineItemName="
        + lineItemName + ", placementId=" + placementId + ", placementName=" + placementName + "]";
  }
}
