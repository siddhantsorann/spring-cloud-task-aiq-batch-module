package com.mediaiq.aiq.redshift.batch.domian;

public class OfficeClientAdvertiserLookup {
  
  public Long getCountryId() {
    return countryId;
  }
  
  public void setCountryId(Long countryId) {
    this.countryId = countryId;
  }
  
  public String getCountryName() {
    return countryName;
  }
  
  public void setCountryName(String countryName) {
    this.countryName = countryName;
  }
  
  public Long getMarketRegionId() {
    return marketRegionId;
  }
  
  public void setMarketRegionId(Long marketRegionId) {
    this.marketRegionId = marketRegionId;
  }
  
  public String getMarketRegionName() {
    return marketRegionName;
  }
  
  public void setMarketRegionName(String marketRegionName) {
    this.marketRegionName = marketRegionName;
  }
  
  private Long countryId;
  private String countryName;
  private Long marketRegionId;
  private String marketRegionName;
  private Long clientId;
  private String clientName;
  private Long miqAdvertiserId;
  private String miqAdvertiserName;
  private Long officeId;
  private String officeName;
  
  public Long getClientId() {
    return clientId;
  }
  
  public void setClientId(Long clientId) {
    this.clientId = clientId;
  }
  
  public String getClientName() {
    return clientName;
  }
  
  public void setClientName(String clientName) {
    this.clientName = clientName;
  }
  
  public Long getMiqAdvertiserId() {
    return miqAdvertiserId;
  }
  
  public void setMiqAdvertiserId(Long miqAdvertiserId) {
    this.miqAdvertiserId = miqAdvertiserId;
  }
  
  public String getMiqAdvertiserName() {
    return miqAdvertiserName;
  }
  
  public void setMiqAdvertiserName(String miqAdvertiserName) {
    this.miqAdvertiserName = miqAdvertiserName;
  }
  
  public Long getOfficeId() {
    return officeId;
  }
  
  public void setOfficeId(Long officeId) {
    this.officeId = officeId;
  }
  
  public String getOfficeName() {
    return officeName;
  }
  
  public void setOfficeName(String officeName) {
    this.officeName = officeName;
  }
  
}
