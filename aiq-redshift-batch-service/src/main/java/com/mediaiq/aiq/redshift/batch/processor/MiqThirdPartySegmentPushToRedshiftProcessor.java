package com.mediaiq.aiq.redshift.batch.processor;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import com.mediaiq.aiq.miq.domain.lookup.MiqThirdPartySegmentsLookup;
import com.mediaiq.aiq.redshift.batch.domian.RedshiftMiqThirdPartySegment;

@Component
public class MiqThirdPartySegmentPushToRedshiftProcessor
    implements ItemProcessor<MiqThirdPartySegmentsLookup, RedshiftMiqThirdPartySegment> {
  
  @Override
  public RedshiftMiqThirdPartySegment process(
      MiqThirdPartySegmentsLookup miqThirdPartySegmentsLookup) throws Exception {
    
    RedshiftMiqThirdPartySegment miqThirdPartySegment =
        new RedshiftMiqThirdPartySegment(miqThirdPartySegmentsLookup.getSegment().getId(),
            miqThirdPartySegmentsLookup.getSegment().getName(),
            miqThirdPartySegmentsLookup.getSegmentType() == null ? ""
                : miqThirdPartySegmentsLookup.getSegmentType().name(),
            (miqThirdPartySegmentsLookup.getLookupCategory() == null
                || miqThirdPartySegmentsLookup.getLookupCategory().getName() == null) ? ""
                    : miqThirdPartySegmentsLookup.getLookupCategory().getName());
    return miqThirdPartySegment;
  }
  
}
