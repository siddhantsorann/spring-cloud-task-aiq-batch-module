package com.mediaiq.aiq.redshift.batch.tasklet;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.aiq.plan.redshift.response.RedshiftPlacementConversionResponse;
import com.mediaiq.aiq.redshift.batch.processor.PlacementConversionProcessor;
import com.mediaiq.aiq.redshift.batch.service.RedshiftPlacementConversionStatService;
import com.mediaiq.appnexus.domain.report.stats.raw.AppnexusConversionStats;
import com.mediaiq.appnexus.repo.AppnexusPlacementPixelConversionStatsRepo;

@Component
@Scope("step")
public class PlacementConversionStatLoadTasklet implements Tasklet {
  final static Logger logger = LoggerFactory.getLogger(PlacementConversionStatLoadTasklet.class);
  
  @Autowired
  RedshiftPlacementConversionStatService redshiftPlacementConversionStatService;
  
  @Autowired
  AppnexusPlacementPixelConversionStatsRepo appnexusPlacementPixelConversionStatsRepo;
  
  @Autowired
  PlacementConversionProcessor placementConversionProcessor;
  
  @Value("#{jobParameters['date']}")
  private Date date;
  
  public RepeatStatus execute(StepContribution arg0, ChunkContext arg1) throws Exception {
    logger.info("Got date param: {}", date);
    List<RedshiftPlacementConversionResponse> byDate =
        redshiftPlacementConversionStatService.getByDate(date).get();
    logger.info("Fetched {} records.", byDate.size());
    List<AppnexusConversionStats> appnexusConversionStats =
        placementConversionProcessor.convertList(byDate);
    logger.info("{} Records to save", appnexusConversionStats.size());
    
    appnexusPlacementPixelConversionStatsRepo.save(appnexusConversionStats);
    
    return RepeatStatus.FINISHED;
  }
}
