package com.mediaiq.aiq.redshift.batch.service;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/redshift")
@Produces(MediaType.APPLICATION_JSON)
public interface RedshiftJobLaunchService {
  
  @POST
  @Path("/push/placement")
  void launchPlacementPushJob();
  
  @POST
  @Path("/push/segment")
  void launchSegmentPushJob();
  
  @POST
  @Path("/push/sitedomain")
  void launchSiteDomainPushJob();
  
  @POST
  @Path("/push/mobileApp")
  void launchMobileAppPushJob();
  
  @POST
  @Path("/push/category")
  void launchCategoryPushJob();
  
  @POST
  @Path("/push/deviceModel")
  void launchDeviceModelPushJob();
  
  @POST
  @Path("/push/creative")
  void launchCreativePushJob(
      @QueryParam("last_modified_start_date") DateParameter lastModifiedStartDate,
      @QueryParam("last_modified_end_date") DateParameter lastModifiedEndDate);
  
  @POST
  @Path("/push/keyword")
  void launchKeywordLookupPushJob();
  
  @POST
  @Path("/push/browser")
  void launchBrowserLookupPushJob();
  
  @POST
  @Path("/push/language")
  void launchLanguageLookupPushJob();
  
  @POST
  @Path("/push/operating-system")
  void launchOperatingSystemLookupPushJob();
  
  @POST
  @Path("/push/carrier")
  void launchCarrierLookupPushJob();
  
  @POST
  @Path("/push/seller")
  void launchSellerLookupPushJob();
  
  @POST
  @Path("/push/advertiser-segment")
  void launchAdvertiserSegmentPushJob();
  
  @POST
  @Path("/push/miq-third-party-segment")
  void launchMiqThirdPartyLookupSegmentPushJob();
  
  @POST
  @Path("/push/third-party-segment")
  void launchThirdPartyLookupSegmentPushJob();
  
  @POST
  @Path("/push/insertion-order-audit")
  void launchInsertionOrderAuditPushJob();
  
  @POST
  @Path("/push/line-item-audit")
  void launchLineItemAuditPushJob();
  
  @POST
  @Path("/push/placement-audit")
  void launchPlacementAuditPushJob();
  
  @POST
  @Path("/push/profile-segment-lookup")
  void launchProfileSegmentLookupPushJob(
      @QueryParam("last_modified_start_date") DateParameter lastModifiedStartDate,
      @QueryParam("last_modified_end_date") DateParameter lastModifiedEndDate);
  
  @POST
  @Path("/pull/placement-conversion-stats")
  void launchPlacementConversionStatsPullJob(@QueryParam("date") DateParameter date);
  
  @POST
  @Path("/push/campaign-lookup")
  void launchCampaignLookupPushJob();
  
  @POST
  @Path("/push/advertiser")
  void launchAdvertiserPushJob();
  
  @POST
  @Path("/push/conversion-pixel")
  void launchPixelPushJob();
  
  @POST
  @Path("/push/geo")
  void launchGeoLookupPushJob();
  
  @POST
  @Path("/push/office-client-advertiser-lookup")
  void launchOfficeClientAdvertiserLookupPushJob();
  
  @POST
  @Path("/push/advertiser-category")
  void launchAdvertiserCategoryPushJob();
  ////////////////////////////
  // DBM
  
  
  @POST
  @Path("/push/dbm/pixel")
  void launchDbmPixelPushJob();
  
  
  @POST
  @Path("/push/dbm/advertiser")
  void launchDbmAdvertiserPushJob();
  
  @POST
  @Path("/push/dbm/creative")
  void launchDbmCreativePushJob();
  
  @POST
  @Path("/push/dbm/geo_location")
  void launchDbmGeoLocationPushJob();
  
  @POST
  @Path("/push/dbm/insertion_order")
  void launchDbmInsertionOrderPushJob();
  
  @POST
  @Path("/push/dbm/isp")
  void launchDbmIspPushJob();
  
  @POST
  @Path("/push/dbm/language")
  void launchDbmLanguagePushJob();
  
  @POST
  @Path("/push/dbm/partner")
  void launchDbmPartnerPushJob();
  
  @POST
  @Path("/push/dbm/placement")
  void launchDbmPlacementPushJob();
  
  @POST
  @Path("/push/dbm/line_item")
  void launchDbmLineItemPushJob();
  
  @POST
  @Path("/push/dbm/browser")
  void launchDbmBrowserPushJob();
  
  @POST
  @Path("/push/dbm/device_criteria")
  void launchDbmDeviceCriteriaPushJob();
  
  @POST
  @Path("/push/dbm/timezone_advertiser")
  void launchDbmTimezoneAdvertiserPushJob();
  
  @POST
  @Path("/push/dbm/exchange")
  void launchDbmExchangePushJob();
  
  @POST
  @Path("/pull/pixel-level-stats")
  void launchPixelStatsLoadJob(@QueryParam("start_date") DateParameter startDate,
      @QueryParam("end_date") DateParameter endDate);
  
  @POST
  @Path("/pull/ip_additional_pulse")
  void launchPostCodeNameToIdMapping();
  
}
