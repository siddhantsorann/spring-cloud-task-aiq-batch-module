package com.mediaiq.aiq.redshift.batch;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import com.mediaiq.aiq.plan.redshift.response.SitedomainBlacklistInfo;

@Component
public class AdsafeSitedomainInfoProcessor
    implements ItemProcessor<AdsafeSiteDomainInfo, SitedomainBlacklistInfo> {
  
  @Override
  public SitedomainBlacklistInfo process(AdsafeSiteDomainInfo item) throws Exception {
    
    if (item == null)
      return null;
    
    boolean blocked = item.traqBucket.toUpperCase() == "VERY HIGH TRAQ"
        || item.traqBucket.toUpperCase() == "VERY HIGH TRAQ" || item.sadScore != 0
        || item.bsScore <= 1000;
    
    return new SitedomainBlacklistInfo(item.getSiteDomain(), blocked);
  }
}
