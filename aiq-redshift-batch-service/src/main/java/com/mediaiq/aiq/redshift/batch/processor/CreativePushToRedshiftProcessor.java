package com.mediaiq.aiq.redshift.batch.processor;


import org.apache.commons.lang.StringUtils;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.domain.AdxAudit;
import com.mediaiq.appnexus.domain.Creative;

@Component
public class CreativePushToRedshiftProcessor implements ItemProcessor<Creative, Creative> {
  
  @Override
  public Creative process(Creative creative) throws Exception {
    
    String name = creative.getName();
    creative.setName((name != null) ? name.trim().replaceAll("\t", " ") : name);
    creative
        .setContent((creative.getContent() != null) ? creative.getContent().replaceAll("\t", " ")
            : creative.getContent());
    creative
        .setContent((creative.getContent() != null) ? creative.getContent().replaceAll("\r", " ")
            : creative.getContent());
    creative
        .setContent((creative.getContent() != null) ? creative.getContent().replaceAll("\n", " ")
            : creative.getContent());
    creative.setFacebookAuditFeedback(
        StringUtils.abbreviate(creative.getFacebookAuditFeedback(), 65535));
    creative.setLandingPageUrl(StringUtils.abbreviate(creative.getLandingPageUrl(), 65535));
    if (creative.getAdxAudit() == null)
      creative.setAdxAudit(new AdxAudit());
    return creative;
  }
  
  
}
