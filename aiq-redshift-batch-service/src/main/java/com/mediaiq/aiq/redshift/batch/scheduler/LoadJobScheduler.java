package com.mediaiq.aiq.redshift.batch.scheduler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

import com.mediaiq.aiq.redshift.batch.service.RedshiftJobLaunchService;

@Configuration
public class LoadJobScheduler {
  
  private static final Logger logger = LoggerFactory.getLogger(LoadJobScheduler.class);
  
  @Autowired
  private RedshiftJobLaunchService redshiftJobLaunchService;
  
  // @Scheduled(cron = "0 0 10 * * ?")
  private void launchPlacementPushJob() {
    logger.info("Scheduled load job: Executing launchPlacementPushJob");
    redshiftJobLaunchService.launchPlacementPushJob();
  }
  
  @Scheduled(cron = "0 30 5 * * ?")
  private void launchSiteDomainPushJob() {
    logger.info("Scheduled load job: Executing launchSiteDomainPushJob");
    redshiftJobLaunchService.launchSiteDomainPushJob();
  }
  
  @Scheduled(cron = "0 30 5 * * ?")
  private void launchMobileAppPushJob() {
    logger.info("Scheduled load job: Executing launchMobileAppPushJob");
    redshiftJobLaunchService.launchMobileAppPushJob();
  }
  
  @Scheduled(cron = "0 30 5 * * ?")
  private void launchCategoryPushJob() {
    logger.info("Scheduled load job: Executing launchCategoryPushJob");
    redshiftJobLaunchService.launchCategoryPushJob();
  }
  
  @Scheduled(cron = "0 30 5 * * ?")
  private void launchDeviceModelPushJob() {
    logger.info("Scheduled load job: Executing launchDeviceModelPushJob");
    redshiftJobLaunchService.launchDeviceModelPushJob();
  }
  
  @Scheduled(cron = "0 30 5 * * ?")
  private void launchCreativePushJob() {
    logger.info("Scheduled load job: Executing launchCreativePushJob");
    redshiftJobLaunchService.launchCreativePushJob(null, null);
  }
  
  @Scheduled(cron = "0 30 5 * * ?")
  private void launchKeywordLookupPushJob() {
    logger.info("Scheduled load job: Executing launchKeywordLookupPushJob");
    redshiftJobLaunchService.launchKeywordLookupPushJob();
  }
  
  @Scheduled(cron = "0 0 3 ? * SUN")
  private void launchGeoLookupPushJob() {
    logger.info("Scheduled load job: Executing launchGeoLookupPushJob");
    redshiftJobLaunchService.launchGeoLookupPushJob();
  }
  
  @Scheduled(cron = "0 30 5 * * ?")
  private void launchAdvertiserSegmentPushJob() {
    logger.info("Scheduled load job: Executing launchAdvertiserSegmentPushJob");
    redshiftJobLaunchService.launchAdvertiserSegmentPushJob();
  }
  
  @Scheduled(cron = "0 40 5 * * ?")
  private void launchSegmentPushJob() {
    logger.info("Scheduled load job: Executing launchSegmentPushJob");
    redshiftJobLaunchService.launchSegmentPushJob();
  }
  
  // @Scheduled(cron = "0 30 1 * * ?")
  // private void launchMiqThirdPartyLookupSegmentPushJob() {
  // logger.info("Scheduled load job: Executing launchMiqThirdPartyLookupSegmentPushJob");
  // redshiftJobLaunchService.launchMiqThirdPartyLookupSegmentPushJob();
  // }
  
  // @Scheduled(cron = "0 30 1 * * ?")
  // private void launchThirdPartyLookupSegmentPushJob() {
  // logger.info("Scheduled load job: Executing launchThirdPartyLookupSegmentPushJob");
  // redshiftJobLaunchService.launchThirdPartyLookupSegmentPushJob();
  // }
  
  // @Scheduled(cron = "0 30 1 * * ?")
  // private void launchInsertionOrderAuditPushJob() {
  // logger.info("Scheduled load job: Executing launchInsertionOrderAuditPushJob");
  // redshiftJobLaunchService.launchInsertionOrderAuditPushJob();
  // }
  
  // @Scheduled(cron = "0 30 1 * * ?")
  // private void launchLineItemAuditPushJob() {
  // logger.info("Scheduled load job: Executing launchLineItemAuditPushJob");
  // redshiftJobLaunchService.launchLineItemAuditPushJob();
  // }
  
  // @Scheduled(cron = "0 30 1 * * ?")
  // private void launchPlacementAuditPushJob() {
  // logger.info("Scheduled load job: Executing launchPlacementAuditPushJob");
  // redshiftJobLaunchService.launchPlacementAuditPushJob();
  // }
  
  @Scheduled(cron = "0 20 6 ? * *")
  private void launchProfileSegmentLookupPushJob() {
    logger.info("Scheduled load job: Executing launchProfileSegmentLookupPushJob");
    redshiftJobLaunchService.launchProfileSegmentLookupPushJob(null, null);
  }
  
  @Scheduled(cron = "0 30 6 ? * *")
  private void launchOfficeClientAdvertiserLookupPushJob() {
    logger.info("Scheduled load job: Executing launchOfficeClientAdvertiserLookupPushJob");
    redshiftJobLaunchService.launchOfficeClientAdvertiserLookupPushJob();
  }
  
  // TODO: Correct this Job. @Scheduled(cron = "0 1 * * * ?")
  // private void launchPlacementConversionStatsPullJob(){
  // logger.info("Scheduled load job: Executing launchPlacementConversionStatsPullJob");
  // redshiftJobLaunchService.launchPlacementConversionStatsPullJob(null);
  // }
  
  @Scheduled(cron = "0 0 6 * * ?")
  private void launchSellerPushJob() {
    logger.info("Scheduled load job: Executing SellerPushJob");
    redshiftJobLaunchService.launchSellerLookupPushJob();
  }
  
  @Scheduled(cron = "0 0 6 * * ?")
  private void launchCarrierPushJob() {
    logger.info("Scheduled load job: Executing CarrierPushJob");
    redshiftJobLaunchService.launchCarrierLookupPushJob();
  }
  
  @Scheduled(cron = "0 10 6 * * ?")
  private void launchCampaignLookupPushJob() {
    logger.info("Scheduled load job: Executing Campaign Lookup Push Job");
    redshiftJobLaunchService.launchCampaignLookupPushJob();
  }
  
  @Scheduled(cron = "0 30 6 * * ?")
  private void launchLanguagePushJob() {
    logger.info("Scheduled load job: Executing LanguagePushJob");
    redshiftJobLaunchService.launchLanguageLookupPushJob();
  }
  
  @Scheduled(cron = "0 30 6 * * ?")
  private void launchBrowserPushJob() {
    logger.info("Scheduled load job: Executing BrowserPushJob");
    redshiftJobLaunchService.launchBrowserLookupPushJob();
  }
  
  @Scheduled(cron = "0 30 6 * * ?")
  private void launchOperatingSystemPushJob() {
    logger.info("Scheduled load job: Executing operating system PushJob");
    redshiftJobLaunchService.launchOperatingSystemLookupPushJob();
  }
  
  @Scheduled(cron = "0 30 7 * * ?")
  private void launchAdvertiserPushJob() {
    logger.info("Scheduled load job: Executing AdvertiserPushJob");
    redshiftJobLaunchService.launchAdvertiserPushJob();
  }
  
  @Scheduled(cron = "0 30 7 * * ?")
  private void launchPixelPushJob() {
    logger.info("Scheduled load job: Executing PixelPushJob");
    redshiftJobLaunchService.launchPixelPushJob();
  }
  
  @Scheduled(cron = "0 5 7 * * ?")
  private void launchAdvertiserCategoryPushJob() {
    logger.info("Scheduled load job: Executing AdvertiserCategoryJob");
    redshiftJobLaunchService.launchAdvertiserCategoryPushJob();
  }
  
  /*
   * Dbm Lookups
   */
  @Scheduled(cron = "0 30 5 * * ?")
  private void launchDbmPixelPushJob() {
    logger.info("Scheduled load job: Executing launchDbmPixelPushJob");
    redshiftJobLaunchService.launchDbmPixelPushJob();
  }
  
  @Scheduled(cron = "0 30 5 * * ?")
  private void launchDbmAdvertiserPushJob() {
    logger.info("Scheduled load job: Executing launchDbmAdvertiserPushJob");
    redshiftJobLaunchService.launchDbmAdvertiserPushJob();
  }
  
  @Scheduled(cron = "0 30 5 * * ?")
  private void launchDbmCreativePushJob() {
    logger.info("Scheduled load job: Executing launchDbmCreativePushJob");
    redshiftJobLaunchService.launchDbmCreativePushJob();
  }
  
  @Scheduled(cron = "0 30 5 * * ?")
  private void launchDbmGeoLocationPushJob() {
    logger.info("Scheduled load job: Executing launchDbmGeoLocationPushJob");
    redshiftJobLaunchService.launchDbmGeoLocationPushJob();
  }
  
  @Scheduled(cron = "0 30 5 * * ?")
  private void launchDbmInsertionOrderPushJob() {
    logger.info("Scheduled load job: Executing launchDbmInsertionOrderPushJob");
    redshiftJobLaunchService.launchDbmInsertionOrderPushJob();
  }
  
  @Scheduled(cron = "0 30 5 * * ?")
  private void launchDbmIspPushJob() {
    logger.info("Scheduled load job: Executing launchDbmIspPushJob");
    redshiftJobLaunchService.launchDbmIspPushJob();
  }
  
  @Scheduled(cron = "0 30 5 * * ?")
  private void launchDbmLanguagePushJob() {
    logger.info("Scheduled load job: Executing launchDbmLanguagePushJob");
    redshiftJobLaunchService.launchDbmLanguagePushJob();
  }
  
  @Scheduled(cron = "0 30 5 * * ?")
  private void launchDbmPartnerPushJob() {
    logger.info("Scheduled load job: Executing launchDbmPartnerPushJob");
    redshiftJobLaunchService.launchDbmPartnerPushJob();
  }
  
  @Scheduled(cron = "0 30 5 * * ?")
  private void launchDbmPlacementPushJob() {
    logger.info("Scheduled load job: Executing launchDbmPlacementPushJob");
    redshiftJobLaunchService.launchDbmPlacementPushJob();
  }
  
  @Scheduled(cron = "0 0 6 * * ?")
  private void launchDbmLineItemPushJob() {
    logger.info("Scheduled load job: Executing launchDbmLineItemPushJob");
    redshiftJobLaunchService.launchDbmLineItemPushJob();
  }
  
  @Scheduled(cron = "0 30 5 * * ?")
  private void launchDbmBrowserPushJob() {
    logger.info("Scheduled load job: Executing launchDbmBrowserPushJob");
    redshiftJobLaunchService.launchDbmBrowserPushJob();
  }
  
  @Scheduled(cron = "0 30 5 * * ?")
  private void launchDbmDeviceCriteriaPushJob() {
    logger.info("Scheduled load job: Executing launchDbmDeviceCriteriaPushJob");
    redshiftJobLaunchService.launchDbmDeviceCriteriaPushJob();
  }
  
  @Scheduled(cron = "0 5 5 * * ?")
  private void launchDbmExchangePushJob() {
    logger.info("Scheduled load job: Executing launchDbmExchangePushJob");
    redshiftJobLaunchService.launchDbmExchangePushJob();
  }
  
  // Schedule @2:30PM everyday
  // @Scheduled(cron = "0 30 22 * * ?")
  private void launchPixelStatsLoadJob() {
    logger.info("Scheduled load job: Executing launchPixelStatsLoadJob");
    redshiftJobLaunchService.launchPixelStatsLoadJob(null, null);
  }
  
  //DPI sends data to ip_additional_pulse table every saturday, so running this on sundays
  @Scheduled(cron = "0 15 13 ? * SUN")
  private void launchPostCodeNameToIdMapping() {
    logger.info("Scheduled load job: Executing: launchPostCodeNameToIdMapping");
    redshiftJobLaunchService.launchPostCodeNameToIdMapping();
  }
}
