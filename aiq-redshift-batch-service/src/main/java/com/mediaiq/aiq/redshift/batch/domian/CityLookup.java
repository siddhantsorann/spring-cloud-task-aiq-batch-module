package com.mediaiq.aiq.redshift.batch.domian;

public class CityLookup {
  
  private String countryCode;
  private Long countryId;
  private String countryName;
  private Long regionId;
  private String regionCode;
  private String regionName;
  private String cityName;
  
  public String getCountryCode() {
    return countryCode;
  }
  
  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }
  
  public Long getCountryId() {
    return countryId;
  }
  
  public void setCountryId(Long countryId) {
    this.countryId = countryId;
  }
  
  public String getCountryName() {
    return countryName;
  }
  
  public void setCountryName(String countryName) {
    this.countryName = countryName;
  }
  
  public Long getRegionId() {
    return regionId;
  }
  
  public void setRegionId(Long regionId) {
    this.regionId = regionId;
  }
  
  public String getRegionCode() {
    return regionCode;
  }
  
  public void setRegionCode(String regionCode) {
    this.regionCode = regionCode;
  }
  
  public String getRegionName() {
    return regionName;
  }
  
  public void setRegionName(String regionName) {
    this.regionName = regionName;
  }
  
  public String getCityName() {
    return cityName;
  }
  
  public void setCityName(String cityName) {
    this.cityName = cityName;
  }
  
  @Override
  public String toString() {
    return "CityLookup [countryCode=" + countryCode + ", countryId=" + countryId + ", countryName="
        + countryName + ", regionId=" + regionId + ", regionCode=" + regionCode + ", regionName="
        + regionName + ", cityName=" + cityName + "]";
  }
  
}
