package com.mediaiq.aiq.redshift.batch.domian.elasticsearch;

import org.springframework.stereotype.Component;

@Component
public class PostcodeElasticsearch {
  private String postalCode;
  private Long postCodeId;
  private String regionName;
  private Long regionCode;
  private Long cityCode;
  private String city;
  private Long countryCode;
  private String countryName;
  private String twoLetterCountry;
  public String getPostalCode() {
    return postalCode;
  }
  public void setPostalCode(String postalCode) {
    this.postalCode = postalCode;
  }
  public Long getPostCodeId() {
    return postCodeId;
  }
  public void setPostCodeId(Long postCodeId) {
    this.postCodeId = postCodeId;
  }
  public String getRegionName() {
    return regionName;
  }
  public void setRegionName(String regionName) {
    this.regionName = regionName;
  }
  public Long getRegionCode() {
    return regionCode;
  }
  public void setRegionCode(Long regionCode) {
    this.regionCode = regionCode;
  }
  public Long getCityCode() {
    return cityCode;
  }
  public void setCityCode(Long cityCode) {
    this.cityCode = cityCode;
  }
  public String getCity() {
    return city;
  }
  public void setCity(String city) {
    this.city = city;
  }
  public Long getCountryCode() {
    return countryCode;
  }
  public void setCountryCode(Long countryCode) {
    this.countryCode = countryCode;
  }
  public String getCountryName() {
    return countryName;
  }
  public void setCountryName(String countryName) {
    this.countryName = countryName;
  }
  public String getTwoLetterCountry() {
    return twoLetterCountry;
  }
  public void setTwoLetterCountry(String twoLetterCountry) {
    this.twoLetterCountry = twoLetterCountry;
  }
  @Override
  public String toString() {
    return "PostcodeElasticsearch [postalCode=" + postalCode + ", postCodeId=" + postCodeId
        + ", regionName=" + regionName + ", regionCode=" + regionCode + ", cityCode=" + cityCode
        + ", city=" + city + ", countryCode=" + countryCode + ", countryName=" + countryName
        + ", twoLetterCountry=" + twoLetterCountry + "]";
  }
  
}
