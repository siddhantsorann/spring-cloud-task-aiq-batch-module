package com.mediaiq.aiq.redshift.batch.row.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.RowMapper;

import com.mediaiq.aiq.redshift.batch.domian.AdvertiserLookup;

@Configuration(value = "advertiserRowMapper")
public class AdvertiserRowMapper implements RowMapper<AdvertiserLookup> {
  
  @Override
  public AdvertiserLookup mapRow(ResultSet rs, int rowNum) throws SQLException {
    
    AdvertiserLookup advertiser = new AdvertiserLookup();
    advertiser.setId(rs.getLong("id"));
    advertiser.setName(rs.getString("name"));
    advertiser.setAppnexusAdvertiserId(rs.getLong("appnexus_advertiser_id"));
    advertiser.setCategory(rs.getString("category"));
    advertiser.setSubcategory(rs.getString("subcategory"));
    
    return advertiser;
  }
  
}
