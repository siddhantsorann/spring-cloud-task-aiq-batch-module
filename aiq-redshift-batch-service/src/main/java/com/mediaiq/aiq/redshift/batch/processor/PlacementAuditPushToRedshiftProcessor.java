package com.mediaiq.aiq.redshift.batch.processor;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.domain.PlacementAudit;

@Component
public class PlacementAuditPushToRedshiftProcessor
    implements ItemProcessor<PlacementAudit, PlacementAudit> {
  
  @Override
  public PlacementAudit process(PlacementAudit placementAudit) throws Exception {
    
    
    String name = placementAudit.getName();
    placementAudit.setName((name != null) ? name.trim().replaceAll("\t", " ") : name);
    return placementAudit;
  }
  
}
