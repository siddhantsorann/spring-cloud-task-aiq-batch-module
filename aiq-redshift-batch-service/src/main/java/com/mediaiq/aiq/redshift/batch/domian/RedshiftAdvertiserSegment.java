package com.mediaiq.aiq.redshift.batch.domian;

public class RedshiftAdvertiserSegment {
  
  private Long segmentId;
  private String segmentName;
  private String pixelType;
  private Long advertiserId;
  private String category;
  private Boolean requiredForSrs;
  
  public RedshiftAdvertiserSegment(Long segmentId, String segmentName, String pixelType,
      Long advertiserId, String category, Boolean requiredForSrs) {
    super();
    this.segmentId = segmentId;
    this.segmentName = segmentName;
    this.pixelType = pixelType;
    this.advertiserId = advertiserId;
    this.category = category;
    this.setRequiredForSrs(requiredForSrs);
  }
  
  public Long getSegmentId() {
    return segmentId;
  }
  
  public void setSegmentId(Long segmentId) {
    this.segmentId = segmentId;
  }
  
  public String getSegmentName() {
    return segmentName;
  }
  
  public void setSegmentName(String segmentName) {
    this.segmentName = segmentName;
  }
  
  public String getPixelType() {
    return pixelType;
  }
  
  public void setPixelType(String pixelType) {
    this.pixelType = pixelType;
  }
  
  public Long getAdvertiserId() {
    return advertiserId;
  }
  
  public void setAdvertiserId(Long advertiserId) {
    this.advertiserId = advertiserId;
  }
  
  public String getCategory() {
    return category;
  }
  
  public void setLookupCategory(String category) {
    this.category = category;
  }
  
  public Boolean getRequiredForSrs() {
    return requiredForSrs;
  }
  
  public void setRequiredForSrs(Boolean requiredForSrs) {
    this.requiredForSrs = requiredForSrs;
  }
}
