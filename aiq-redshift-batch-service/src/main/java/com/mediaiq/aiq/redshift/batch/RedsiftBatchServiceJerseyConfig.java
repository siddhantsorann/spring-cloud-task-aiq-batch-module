package com.mediaiq.aiq.redshift.batch;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import com.mediaiq.aiq.elasticsearch.service.ElasticsearchService;
import com.mediaiq.aiq.elasticsearch.service.ElasticsearchServiceImpl;
import com.mediaiq.aiq.redshift.batch.service.impl.RedshiftJobLaunchServiceImpl;

@Component
@ApplicationPath("/api")
public class RedsiftBatchServiceJerseyConfig extends ResourceConfig {
  
  public RedsiftBatchServiceJerseyConfig() {
    
    register(RedshiftJobLaunchServiceImpl.class);
    register(ElasticsearchServiceImpl.class);
    register(ElasticsearchService.class);
  }
  
}
