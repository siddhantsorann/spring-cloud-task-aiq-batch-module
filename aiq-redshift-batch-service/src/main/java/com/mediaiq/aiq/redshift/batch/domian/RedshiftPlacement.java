package com.mediaiq.aiq.redshift.batch.domian;

import java.util.Date;

import com.mediaiq.appnexus.value.SupplyTypeAction;

public class RedshiftPlacement {
  private Long id;
  private String code;
  private Long advertiserId;
  private String name;
  private String profileId;
  private String lineItemId;
  private Date startDate;
  private Date endDate;
  private Date lastModified;
  private String cpmBidType;
  private Double cpcGoal;
  private Double baseBid;
  private String state;
  private String inventoryType;
  private String seat;
  private String timezone;
  private String supplyType;
  private SupplyTypeAction supplyTypeAction;
  private Boolean enablePacing;
  private Boolean lifetimePacing;
  private Boolean cadenceModifierEnabled;
  private Double minBid;
  private Double maxBid;
  private Long segmentModifierId;
  private Long segmentPriceId;
  private Long segmentModifierWeight;
  
  public RedshiftPlacement(Long id, String code, Long advertiserId, String name, String profileId,
      String lineItemId, Date startDate, Date endDate, Date lastModified, String cpmBidType,
      Double cpcGoal, Double baseBid, String state, String inventoryType, String seat,
      String timezone, String supplyType, SupplyTypeAction supplyTypeAction, Boolean enablePacing,
      Boolean lifetimePacing, Boolean cadenceModifierEnabled, Double minBid, Double maxBid,
      Long segmentModifierId, Long segmentPriceId, Long segmentModifierWeight) {
    super();
    this.id = id;
    this.code = code;
    this.advertiserId = advertiserId;
    this.name = name;
    this.profileId = profileId;
    this.lineItemId = lineItemId;
    this.startDate = startDate;
    this.endDate = endDate;
    this.lastModified = lastModified;
    this.cpmBidType = cpmBidType;
    this.cpcGoal = cpcGoal;
    this.baseBid = baseBid;
    this.state = state;
    this.inventoryType = inventoryType;
    this.seat = seat;
    this.timezone = timezone;
    this.supplyType = supplyType;
    this.supplyTypeAction = supplyTypeAction;
    this.enablePacing = enablePacing;
    this.lifetimePacing = lifetimePacing;
    this.cadenceModifierEnabled = cadenceModifierEnabled;
    this.minBid = minBid;
    this.maxBid = maxBid;
    this.segmentModifierId = segmentModifierId;
    this.segmentPriceId = segmentPriceId;
    this.setSegmentModifierWeight(segmentModifierWeight);
  }
  
  public RedshiftPlacement() {}
  
  public String getCpmBidType() {
    return cpmBidType;
  }
  
  public void setCpmBidType(String cpmBidType) {
    this.cpmBidType = cpmBidType;
  }
  
  public Double getCpcGoal() {
    return cpcGoal;
  }
  
  public void setCpcGoal(Double cpcGoal) {
    this.cpcGoal = cpcGoal;
  }
  
  public Double getBaseBid() {
    return baseBid;
  }
  
  public void setBaseBid(Double baseBid) {
    this.baseBid = baseBid;
  }
  
  public String getState() {
    return state;
  }
  
  public void setState(String state) {
    this.state = state;
  }
  
  public String getInventoryType() {
    return inventoryType;
  }
  
  public void setInventoryType(String inventoryType) {
    this.inventoryType = inventoryType;
  }
  
  public String getSeat() {
    return seat;
  }
  
  public void setSeat(String seat) {
    this.seat = seat;
  }
  
  public Long getId() {
    return id;
  }
  
  public void setId(Long id) {
    this.id = id;
  }
  
  public String getCode() {
    return code;
  }
  
  public void setCode(String code) {
    this.code = code;
  }
  
  public Long getAdvertiserId() {
    return advertiserId;
  }
  
  public void setAdvertiserId(Long advertiserId) {
    this.advertiserId = advertiserId;
  }
  
  public String getName() {
    return name;
  }
  
  public void setName(String name) {
    this.name = name;
  }
  
  public String getProfileId() {
    return profileId;
  }
  
  public void setProfileId(String profileId) {
    this.profileId = profileId;
  }
  
  public String getLineItemId() {
    return lineItemId;
  }
  
  public void setLineItemId(String lineItemId) {
    this.lineItemId = lineItemId;
  }
  
  public Date getStartDate() {
    return startDate;
  }
  
  public void setStartDate(Date startDate) {
    this.startDate = startDate;
  }
  
  public Date getEndDate() {
    return endDate;
  }
  
  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }
  
  public Date getLastModified() {
    return lastModified;
  }
  
  public void setLastModified(Date lastModified) {
    this.lastModified = lastModified;
  }
  
  public String getTimezone() {
    return timezone;
  }
  
  public void setTimezone(String timezone) {
    this.timezone = timezone;
  }
  
  public String getSupplyType() {
    return supplyType;
  }
  
  public void setSupplyType(String supplyType) {
    this.supplyType = supplyType;
  }
  
  public SupplyTypeAction getSupplyTypeAction() {
    return supplyTypeAction;
  }
  
  public void setSupplyTypeAction(SupplyTypeAction supplyTypeAction) {
    this.supplyTypeAction = supplyTypeAction;
  }
  
  public Boolean getEnablePacing() {
    return enablePacing;
  }
  
  public void setEnablePacing(Boolean enablePacing) {
    this.enablePacing = enablePacing;
  }
  
  public Boolean getLifetimePacing() {
    return lifetimePacing;
  }
  
  public void setLifetimePacing(Boolean lifetimePacing) {
    this.lifetimePacing = lifetimePacing;
  }
  
  public Boolean getCadenceModifierEnabled() {
    return cadenceModifierEnabled;
  }
  
  public void setCadenceModifierEnabled(Boolean cadenceModifierEnabled) {
    this.cadenceModifierEnabled = cadenceModifierEnabled;
  }
  
  public Double getMinBid() {
    return minBid;
  }
  
  public void setMinBid(Double minBid) {
    this.minBid = minBid;
  }
  
  public Double getMaxBid() {
    return maxBid;
  }
  
  public void setMaxBid(Double maxBid) {
    this.maxBid = maxBid;
  }
  
  public Long getSegmentModifierId() {
    return segmentModifierId;
  }
  
  public void setSegmentModifierId(Long segmentModifierId) {
    this.segmentModifierId = segmentModifierId;
  }
  
  public Long getSegmentPriceId() {
    return segmentPriceId;
  }
  
  public void setSegmentPriceId(Long segmentPriceId) {
    this.segmentPriceId = segmentPriceId;
  }
  
  public Long getSegmentModifierWeight() {
    return segmentModifierWeight;
  }
  
  public void setSegmentModifierWeight(Long segmentModifierWeight) {
    this.segmentModifierWeight = segmentModifierWeight;
  }
}
