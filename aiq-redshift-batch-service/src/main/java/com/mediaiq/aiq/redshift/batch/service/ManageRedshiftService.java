package com.mediaiq.aiq.redshift.batch.service;

import com.mediaiq.aiq.plan.redshift.service.base.AbstractRedshiftService;

public interface ManageRedshiftService<R> extends AbstractRedshiftService<R> {
}
