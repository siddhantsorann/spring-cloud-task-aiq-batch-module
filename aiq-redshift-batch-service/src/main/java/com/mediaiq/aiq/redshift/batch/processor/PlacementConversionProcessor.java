package com.mediaiq.aiq.redshift.batch.processor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.mediaiq.aiq.domain.Cost;
import com.mediaiq.aiq.domain.Currency;
import com.mediaiq.aiq.plan.redshift.response.RedshiftPlacementConversionResponse;
import com.mediaiq.aiq.redshift.batch.value.EventType;
import com.mediaiq.appnexus.domain.report.stats.id.AppnexusPlacementPixelConversionStatsId;
import com.mediaiq.appnexus.domain.report.stats.raw.AppnexusConversionStats;

@Component
public class PlacementConversionProcessor {
  
  private final static Logger LOGGER = LoggerFactory.getLogger(PlacementConversionProcessor.class);
  
  private Currency currency = Currency.USD;
  private Map<AppnexusPlacementPixelConversionStatsId, AppnexusConversionStats> placementPixelConversionObjectMap =
      new HashMap<AppnexusPlacementPixelConversionStatsId, AppnexusConversionStats>();
  
  public List<AppnexusConversionStats> convertList(
      List<RedshiftPlacementConversionResponse> listToConvert) {
    
    LOGGER.info(
        "Starting processing of redshift response to the AppnexusPlacementPixelConversionStat model");
    
    for (RedshiftPlacementConversionResponse redshiftResponse : listToConvert) {
      
      // Creating the key
      AppnexusPlacementPixelConversionStatsId primaryKey =
          new AppnexusPlacementPixelConversionStatsId(redshiftResponse.getImpDate(),
              redshiftResponse.getCampaignId(), redshiftResponse.getPixelId());
      LOGGER.info(redshiftResponse.toString());
      
      // An object with either PC_CONV or PV_CONV stats is already in the
      // map.
      // Now an object with same key but diff conversion stats is landing.
      // Handle it and update the required fields.
      
      if (placementPixelConversionObjectMap.containsKey(primaryKey)) {
        AppnexusConversionStats pixelConversionStatsObj =
            placementPixelConversionObjectMap.get(primaryKey);
        BigDecimal currentCostValue = new BigDecimal(0);
        // update the object
        if (redshiftResponse.getEventType().equalsIgnoreCase(EventType.pc_conv.name())) {
          pixelConversionStatsObj.setPostClickConversions(redshiftResponse.getConversions());
          pixelConversionStatsObj
              .setPostClickRevenue(new Cost(redshiftResponse.getPostClickRevenue(), currency));
          currentCostValue = redshiftResponse.getPostClickRevenue();
          
        } else if (redshiftResponse.getEventType().equalsIgnoreCase(EventType.pv_conv.name())) {
          pixelConversionStatsObj.setPostViewConversions(redshiftResponse.getConversions());
          pixelConversionStatsObj
              .setPostViewRevenue(new Cost(redshiftResponse.getPostViewRevenue(), currency));
          currentCostValue = redshiftResponse.getPostViewRevenue();
        }
        // update the totalConversions;
        Long initialTotalConversions = pixelConversionStatsObj.getTotalConversions();
        Long finalTotalConversions = initialTotalConversions + redshiftResponse.getConversions();
        pixelConversionStatsObj.setTotalConversions(finalTotalConversions);
        
        // update the totalCost
        Cost initialTotalCost = pixelConversionStatsObj.getTotalCost();
        BigDecimal result = currentCostValue.add(initialTotalCost.getValue());
        Cost finalTotalCost = new Cost(result, currency);
        pixelConversionStatsObj.setTotalCost(finalTotalCost);
      } else {
        
        // Creating the entire object
        AppnexusConversionStats appnexusPlacementPixelConversionStatsObj =
            new AppnexusConversionStats(primaryKey,
                new Cost(redshiftResponse.getPostClickRevenue(), currency),
                new Cost(redshiftResponse.getPostViewRevenue(), currency),
                redshiftResponse.getConversions(), redshiftResponse.getEventType());
        
        // the object is yet not in the map. Add it to the map
        placementPixelConversionObjectMap.put(primaryKey, appnexusPlacementPixelConversionStatsObj);
      }
    }
    // finally return the list which consists of the values in the map
    return new ArrayList<AppnexusConversionStats>(placementPixelConversionObjectMap.values());
  }
}
