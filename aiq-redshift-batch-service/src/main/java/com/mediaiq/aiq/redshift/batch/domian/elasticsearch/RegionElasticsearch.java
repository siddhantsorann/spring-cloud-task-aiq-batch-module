package com.mediaiq.aiq.redshift.batch.domian.elasticsearch;

import org.springframework.stereotype.Component;

@Component
public class RegionElasticsearch {
  private String regionName;
  private Long regionCode;
  private Long countryCode;
  private String twoLetterCountry;
  
  public String getRegionName() {
    return regionName;
  }
  
  public void setRegionName(String regionName) {
    this.regionName = regionName;
  }
  
  public Long getRegionCode() {
    return regionCode;
  }
  
  public void setRegionCode(Long regionCode) {
    this.regionCode = regionCode;
  }
  
  public Long getCountryCode() {
    return countryCode;
  }
  
  public void setCountryCode(Long countryCode) {
    this.countryCode = countryCode;
  }
  
  public String getTwoLetterCountry() {
    return twoLetterCountry;
  }
  
  public void setTwoLetterCountry(String twoLetterCountry) {
    this.twoLetterCountry = twoLetterCountry;
  }
  
}
