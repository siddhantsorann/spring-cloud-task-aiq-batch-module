package com.mediaiq.aiq.redshift.batch.processor;


import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.mediaiq.aiq.redshift.batch.domian.RedshiftPlacement;
import com.mediaiq.appnexus.domain.CampaignModifier;
import com.mediaiq.appnexus.domain.Placement;

@Component
public class PlacementPushToRedshiftProcessor
    implements ItemProcessor<Placement, RedshiftPlacement> {
  
  @Override
  public RedshiftPlacement process(Placement placement) throws Exception {
    
    RedshiftPlacement redshiftPlacement = new RedshiftPlacement();
    BeanUtils.copyProperties(placement, redshiftPlacement);
    redshiftPlacement.setName(placement.getName().replaceAll("\t", ""));
    
    if (!CollectionUtils.isEmpty(placement.getCampaignModifiers())) {
      for (CampaignModifier campaignModifier : placement.getCampaignModifiers()) {
        if (campaignModifier.getType().equals("segment_modifier_id"))
          redshiftPlacement.setSegmentModifierId(campaignModifier.getValue());
        if (campaignModifier.getType().equals("segment_price_id"))
          redshiftPlacement.setSegmentPriceId(campaignModifier.getValue());
        if (campaignModifier.getType().equals("segment_modifier_weight"))
          redshiftPlacement.setSegmentModifierWeight(campaignModifier.getValue());
      }
    }
    
    return redshiftPlacement;
  }
}
