package com.mediaiq.aiq.redshift.batch.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.mediaiq.aiq.plan.redshift.service.impl.AbstractRedshiftServiceImpl;
import com.mediaiq.aiq.redshift.batch.service.ManageRedshiftService;

public abstract class ManageRedshiftServiceImpl<R> extends AbstractRedshiftServiceImpl<R>
    implements ManageRedshiftService<R> {
  
  @Autowired(required = false)
  private NamedParameterJdbcTemplate wareHouse2NamedParameterJdbcTemplate;
  
  @Override
  public NamedParameterJdbcTemplate getJdbcTemplate() {
    return wareHouse2NamedParameterJdbcTemplate;
  }
  
}
