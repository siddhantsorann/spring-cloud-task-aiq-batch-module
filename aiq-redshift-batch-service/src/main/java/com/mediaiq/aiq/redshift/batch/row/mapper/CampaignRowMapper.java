package com.mediaiq.aiq.redshift.batch.row.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.RowMapper;

import com.mediaiq.aiq.redshift.batch.domian.CampaignLookup;

@Configuration(value = "campaignRowMapper")
public class CampaignRowMapper implements RowMapper<CampaignLookup> {
  
  @Override
  public CampaignLookup mapRow(ResultSet rs, int rowNum) throws SQLException {
    
    CampaignLookup campaignLookup = new CampaignLookup();
    
    campaignLookup.setAdvertiserId(rs.getLong("advertiser_id"));
    campaignLookup.setAdvertiserName(rs.getString("advertiser_name"));
    campaignLookup.setIoId(rs.getLong("io_id"));
    campaignLookup.setIoName(rs.getString("io_name"));
    campaignLookup.setLineItemId(rs.getLong("li_id"));
    campaignLookup.setLineItemName(rs.getString("li_name"));
    campaignLookup.setPlacementId(rs.getLong("placement_id"));
    campaignLookup.setPlacementName(rs.getString("placement_name").replaceAll("\\s+", " "));
    
    return campaignLookup;
  }
  
}
