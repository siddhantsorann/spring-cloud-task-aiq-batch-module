
package com.mediaiq.aiq.redshift.batch.tasklet;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("step")
public class CopyOperatingSystemToS3Tasklet extends CopyToS3Tasklet {
  
  @Value("${s3.operating-system.source.filename}")
  private String uploadFileName;
  
  @Value("#{'${s3.operating-system.bucket}' + '${s3.operating-system.path}'}")
  private String bucketName;
  
  @Value("${s3.operating-system.destination.filename}")
  private String keyName;
  
  @Override
  protected String getKeyName() {
    return keyName;
  }
  
  @Override
  protected String getBucketName() {
    return bucketName;
  }
  
  @Override
  protected String getUploadFileName() {
    return uploadFileName;
  }
}
