package com.mediaiq.aiq.redshift.batch.service;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Future;

import javax.ws.rs.PathParam;

import org.springframework.dao.DataAccessException;

import com.mediaiq.aiq.plan.redshift.response.RedshiftPlacementConversionResponse;

public interface RedshiftPlacementConversionStatService
    extends ManageRedshiftService<RedshiftPlacementConversionResponse> {
  
  public Future<List<RedshiftPlacementConversionResponse>> getByDate(@PathParam("date") Date date)
      throws DataAccessException, IOException;
}
