package com.mediaiq.aiq.redshift.batch.domian;

public class RedshiftMiqThirdPartySegment {
  
  private Long segmentId;
  private String segmentName;
  private String segmentType;
  private String category;
  
  public RedshiftMiqThirdPartySegment(Long segmentId, String segmentName, String segmentType,
      String category) {
    super();
    this.segmentId = segmentId;
    this.segmentName = segmentName;
    this.segmentType = segmentType;
    this.category = category;
  }
  
  public Long getSegmentId() {
    return segmentId;
  }
  
  public void setSegmentId(Long segmentId) {
    this.segmentId = segmentId;
  }
  
  public String getSegmentName() {
    return segmentName;
  }
  
  public void setSegmentName(String segmentName) {
    this.segmentName = segmentName;
  }
  
  public String getSegmentType() {
    return segmentType;
  }
  
  public void setSegmentType(String segmentType) {
    this.segmentType = segmentType;
  }
  
  public String getCategory() {
    return category;
  }
  
  public void setCategory(String category) {
    this.category = category;
  }
  
}
