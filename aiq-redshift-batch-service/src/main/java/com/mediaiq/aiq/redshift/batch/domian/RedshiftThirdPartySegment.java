package com.mediaiq.aiq.redshift.batch.domian;

public class RedshiftThirdPartySegment {
  
  private Long segmentId;
  private String segmentName;
  private String iabCategory;
  private String category1;
  private String category2;
  private String dataProvider;
  private String state;
  
  public RedshiftThirdPartySegment(Long segmentId, String segmentName, String iabCategory,
      String category1, String category2, String dataProvider, String state) {
    super();
    this.segmentId = segmentId;
    this.segmentName = segmentName;
    this.iabCategory = iabCategory;
    this.category1 = category1;
    this.category2 = category2;
    this.dataProvider = dataProvider;
    this.state = state;
  }
  
  public Long getSegmentId() {
    return segmentId;
  }
  
  public void setSegmentId(Long segmentId) {
    this.segmentId = segmentId;
  }
  
  public String getSegmentName() {
    return segmentName;
  }
  
  public void setSegmentName(String segmentName) {
    this.segmentName = segmentName;
  }
  
  public String getIabCategory() {
    return iabCategory;
  }
  
  public void setIabCategory(String iabCategory) {
    this.iabCategory = iabCategory;
  }
  
  public String getDataProvider() {
    return dataProvider;
  }
  
  public void setDataProvider(String dataProvider) {
    this.dataProvider = dataProvider;
  }
  
  public String getState() {
    return state;
  }
  
  public void setState(String state) {
    this.state = state;
  }
  
  public String getCategory1() {
    return category1;
  }
  
  public void setCategory1(String category1) {
    this.category1 = category1;
  }
  
  public String getCategory2() {
    return category2;
  }
  
  public void setCategory2(String category2) {
    this.category2 = category2;
  }
}
