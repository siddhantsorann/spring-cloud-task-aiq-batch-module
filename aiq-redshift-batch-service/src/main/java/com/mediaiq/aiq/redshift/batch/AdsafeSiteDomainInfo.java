package com.mediaiq.aiq.redshift.batch;

public class AdsafeSiteDomainInfo {
  
  String siteDomain;
  int sadScore;
  int bsScore;
  String traqBucket;
  
  public String getSiteDomain() {
    return siteDomain;
  }
  
  public void setSiteDomain(String siteDomain) {
    this.siteDomain = siteDomain;
  }
  
  public int getSadScore() {
    return sadScore;
  }
  
  public void setSadScore(int sadScore) {
    this.sadScore = sadScore;
  }
  
  public int getBsScore() {
    return bsScore;
  }
  
  public void setBsScore(int bsScore) {
    this.bsScore = bsScore;
  }
  
  public String getTraqBucket() {
    return traqBucket;
  }
  
  public void setTraqBucket(String traqBucket) {
    this.traqBucket = traqBucket;
  }
}
