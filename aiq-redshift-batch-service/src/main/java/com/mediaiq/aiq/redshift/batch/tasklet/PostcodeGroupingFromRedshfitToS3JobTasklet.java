package com.mediaiq.aiq.redshift.batch.tasklet;

import java.io.IOException;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.mediaiq.aiq.miq.domain.DEGeoCountry;
import com.mediaiq.aiq.miq.repo.DEGeoCountryRepo;
import com.mysql.jdbc.StringUtils;

@Component
@Scope("step")
public class PostcodeGroupingFromRedshfitToS3JobTasklet implements Tasklet {
  
  static final Logger LOGGER =
      LoggerFactory.getLogger(PostcodeGroupingFromRedshfitToS3JobTasklet.class);
  
  private final String Sqlfile_Group_By_Postal_Id = "/sql/group_by_postal_id.sql";
  
  @Value("${s3.postcode.source}")
  private String s3KeyPrefix;

  @Value("${s3.iam}")
  private String iam;
  
  @Autowired
  JdbcTemplate wareHouse2JdbcTemplate;
  
  @Autowired
  private DEGeoCountryRepo deGeoService;
  
  
  
  @Override
  public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext)
      throws Exception {
    
    LOGGER.info(
        "Starting grouping postcode data from ip_additional_pulse table "
        + "in redshift and sending to s3");
    uploadByQuery();
    LOGGER.info("Uploaded grouped post code data sto s3");
    return RepeatStatus.FINISHED;
  }
  
  private void uploadByQuery() throws IOException {
    
    String countryCodes = getCountyListFromSegmentManager();
    String query =
        IOUtils.toString(new ClassPathResource(Sqlfile_Group_By_Postal_Id).getInputStream());
    query = query.replace(":country_codes",countryCodes);
    query = query.replace(":s3KeyPrefix", s3KeyPrefix);
    query = query.replace(":iam", iam);
    LOGGER.info("Starting to run the query {}", query);
    wareHouse2JdbcTemplate.execute(query);
    LOGGER.info("Uploaded grouped redshift data from ip_additional_pulse to s3");
    
  }
  
  private String getCountyListFromSegmentManager() {
    List<DEGeoCountry> countries = deGeoService.findAll();
    String commaSepratedList = "";
    for (DEGeoCountry country : countries) {
      commaSepratedList += "\\\'" + country.getDigitalElementCountryCode().toLowerCase() + "\\\',";
    }
    commaSepratedList=commaSepratedList.substring(0, commaSepratedList.length() - 1);
    LOGGER.info("Getting postcodes for country codes: {}", commaSepratedList);
    return commaSepratedList;
  }
}
