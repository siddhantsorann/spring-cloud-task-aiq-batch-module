package com.mediaiq.aiq.redshift.batch.processor;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import com.mediaiq.aiq.miq.domain.lookup.SiteDomainLookup;
import com.mediaiq.aiq.redshift.batch.domian.RedshiftSiteDomain;

@Component
public class SitedomainPushToRedshiftProcessor
    implements ItemProcessor<SiteDomainLookup, RedshiftSiteDomain> {
  
  @Override
  public RedshiftSiteDomain process(SiteDomainLookup item) throws Exception {
    
    RedshiftSiteDomain redshiftSiteDomain =
        new RedshiftSiteDomain(item.getName(), item.getTopLevelCategory(),
            item.getSecondLevelCategory(), item.getPlatformAuditedImpressions(),
            item.getSellerAuditedImpressions(), item.getLastModified(), item.getIsBlacklisted(),
            (item.getIabCategory() == null || item.getIabCategory().getName() == null) ? ""
                : item.getIabCategory().getName(),
            (item.getIabSubCategory() == null || item.getIabSubCategory().getName() == null) ? ""
                : item.getIabSubCategory().getName());
    return redshiftSiteDomain;
  }
}
