
package com.mediaiq.aiq.redshift.batch.tasklet;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("step")
public class CopyOfficeClientAdvertiserLookupToS3Tasklet extends CopyToS3Tasklet {
  
  @Value("${s3.office-client-advertiser-lookup.source.filename}")
  private String uploadFileName;
  
  @Value("#{'${s3.office-client-advertiser-lookup.bucket}' + '${s3.office-client-advertiser-lookup.path}'}")
  private String bucketName;
  
  @Value("${s3.office-client-advertiser-lookup.destination.filename}")
  private String keyName;
  
  @Override
  protected String getKeyName() {
    return keyName;
  }
  
  @Override
  protected String getBucketName() {
    return bucketName;
  }
  
  @Override
  protected String getUploadFileName() {
    return uploadFileName;
  }
}
