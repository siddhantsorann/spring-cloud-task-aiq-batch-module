
package com.mediaiq.aiq.redshift.batch.tasklet.dbm;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("step")
public class CopyDbmLanguageToS3Tasklet extends CopyToS3Tasklet {
  
  @Value("${s3.dbmLanguage.source.filename}")
  private String uploadFileName;
  
  @Value("#{'${s3.dbmLanguage.bucket}' + '${s3.dbmLanguage.path}'}")
  private String bucketName;
  
  @Value("${s3.dbmLanguage.destination.filename}")
  private String keyName;
  
  @Override
  protected String getKeyName() {
    return keyName;
  }
  
  @Override
  protected String getBucketName() {
    return bucketName;
  }
  
  @Override
  protected String getUploadFileName() {
    return uploadFileName;
  }
}
