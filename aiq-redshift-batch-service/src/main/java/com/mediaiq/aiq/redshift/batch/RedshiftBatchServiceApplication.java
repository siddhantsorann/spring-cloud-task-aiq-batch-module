package com.mediaiq.aiq.redshift.batch;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.security.oauth2.OAuth2AutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.cloud.security.oauth2.resource.EnableOAuth2Resource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.mediaiq.aiq.miq.service.impl.DEGeoCountryServiceImpl;
import com.mediaiq.aiq.redshift.batch.service.impl.RedshiftPixelStatServiceImpl;

@Configuration
@EnableAutoConfiguration(
    exclude = {DataSourceAutoConfiguration.class, OAuth2AutoConfiguration.class})
@EnableScheduling
@EnableIntegration
@ComponentScan(
    basePackages = {"com.mediaiq.spring.batch", "com.mediaiq.aiq.redshift.batch",
        "com.mediaiq.aiq.redshift.batch.service", "com.mediaiq.aiq.aws.s3" ,
        "com.mediaiq.aiq.elasticsearch.service"},
    basePackageClasses = {RedshiftPixelStatServiceImpl.class})
@EntityScan(basePackages = {"com.mediaiq.spring.batch.domain", "com.mediaiq.appnexus.domain",
    "com.mediaiq.aiq.mediamath.domain", "com.mediaiq.aiq.miq.domain",
    "com.mediaiqdigital.aiq.tradingcenter.domain", "com.mediaiqdigital.doubleclick.domain"})
@EnableJpaRepositories(basePackages = {"com.mediaiq.spring.batch.repo", "com.mediaiq.appnexus.repo",
    "com.mediaiq.aiq.miq.repo", "com.mediaiqdigital.aiq.tradingcenter.repo"})
@EnableMongoRepositories(basePackages = "com.mediaiq.appnexus.mongo.repo")
@ImportResource({"classpath:spring-integration-config.xml", "classpath:spring-batch-config.xml",
    "classpath:redshift-batch-jobs.xml"})
@EnableOAuth2Resource
@EnableTransactionManagement
public class RedshiftBatchServiceApplication {
  public static void main(String[] args) {
    new SpringApplicationBuilder(RedshiftBatchServiceApplication.class).web(true).run(args);
  }
  
  @Bean
  public static PropertySourcesPlaceholderConfigurer properties() {
    PropertySourcesPlaceholderConfigurer configurer = new PropertySourcesPlaceholderConfigurer();
    configurer.setIgnoreUnresolvablePlaceholders(true);
    return configurer;
  }
}
