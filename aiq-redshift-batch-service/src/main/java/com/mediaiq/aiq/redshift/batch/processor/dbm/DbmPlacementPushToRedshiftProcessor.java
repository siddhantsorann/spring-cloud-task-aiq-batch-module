package com.mediaiq.aiq.redshift.batch.processor.dbm;


import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import com.mediaiqdigital.doubleclick.domain.DbmPlacement;

@Component
public class DbmPlacementPushToRedshiftProcessor
    implements ItemProcessor<DbmPlacement, DbmPlacement> {
  
  @Override
  public DbmPlacement process(DbmPlacement placement) throws Exception {
    
    placement.setName(placement.getName().replaceAll("\t", ""));
    placement.setName(placement.getName().replaceAll(",", " "));
    return placement;
  }
}
