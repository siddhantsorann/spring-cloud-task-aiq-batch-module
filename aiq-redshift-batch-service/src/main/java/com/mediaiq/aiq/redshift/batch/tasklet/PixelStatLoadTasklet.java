package com.mediaiq.aiq.redshift.batch.tasklet;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.aiq.redshift.batch.service.RedshiftPixelStatService;

/**
 * Created by prasannachidire on 12-Jun-2017
 */

@Component
@Scope("step")
public class PixelStatLoadTasklet implements Tasklet {
  
  final static Logger LOGGER = LoggerFactory.getLogger(PixelStatLoadTasklet.class);
  
  @Autowired
  RedshiftPixelStatService redshiftPixelStatService;
  
  @Value("#{jobParameters['start_date']}")
  private Date startDate;
  
  @Value("#{jobParameters['end_date']}")
  private Date endDate;
  
  @Override
  public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext)
      throws Exception {
    
    LOGGER.info("Got date params: {} , {}", startDate, endDate);
    redshiftPixelStatService.uploadByDates(startDate, endDate);
    LOGGER.info("Uploaded stats to s3");
    return RepeatStatus.FINISHED;
  }
  
}
