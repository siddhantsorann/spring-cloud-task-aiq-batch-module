package com.mediaiq.aiq.redshift.batch.tasklet;

import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("step")
public class WriteProfileSegmentLookupFromS3ToRedshiftTasklet extends WriteFromS3ToRedshiftTasklet {
  
  @Value("${s3.profile-segment-lookup.query.delete}")
  private String deleteQuery;
  
  @Value("${s3.profile-segment-lookup.query.copy}")
  private String copyQuery;
  
  @Value("#{jobParameters['last_modified_after']}")
  protected Date lastModifiedAfter;
  
  @Value("#{jobParameters['last_modified_before']}")
  protected Date lastModifiedBefore;
  
  @Override
  protected String getDeleteQuery() {
    return deleteQuery;
  }
  
  @Override
  protected String getCopyQuery() {
    return copyQuery;
  }
  
  @Override
  protected Object[] getBindParameters() {
    return new Object[] {lastModifiedAfter, lastModifiedBefore};
  }
}
