package com.mediaiq.aiq.redshift.batch.service.impl;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Future;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.stereotype.Service;

import com.mediaiq.aiq.plan.redshift.request.RedshiftDailyPlacementStatsRequest;
import com.mediaiq.aiq.plan.redshift.response.RedshiftPlacementConversionResponse;
import com.mediaiq.aiq.redshift.batch.service.RedshiftPlacementConversionStatService;

@Service
public class RedshiftPlacementConversionStatServiceImpl
    extends ManageRedshiftServiceImpl<RedshiftPlacementConversionResponse>
    implements RedshiftPlacementConversionStatService {
  
  final private String SQLFILE_GET_PLACEMENT_CONV_BY_DATE = "/sql/placement_conv_by_date.sql";
  SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
  
  public Future<List<RedshiftPlacementConversionResponse>> getByDate(Date date)
      throws DataAccessException, IOException {
    System.out.println(getDateFormat().format(date));
    return query(SQLFILE_GET_PLACEMENT_CONV_BY_DATE, // new DateDTO(getDateFormat().format(date))
        new BeanPropertySqlParameterSource(
            new RedshiftDailyPlacementStatsRequest(getDateFormat().format(date))),
        new BeanPropertyRowMapper<RedshiftPlacementConversionResponse>(
            RedshiftPlacementConversionResponse.class));
  }
  
  public SimpleDateFormat getDateFormat() {
    return dateFormat;
  }
  
  public void setDateFormat(SimpleDateFormat dateFormat) {
    this.dateFormat = dateFormat;
  }
  
  @Override
  protected Class<RedshiftPlacementConversionResponse> getResponseType() {
    return RedshiftPlacementConversionResponse.class;
  }
}
