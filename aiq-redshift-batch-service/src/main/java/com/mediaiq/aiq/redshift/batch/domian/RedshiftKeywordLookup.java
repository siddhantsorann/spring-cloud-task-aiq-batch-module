package com.mediaiq.aiq.redshift.batch.domian;

public class RedshiftKeywordLookup {
  public RedshiftKeywordLookup(Long id, String keyword, Boolean isBlacklisted, String iabCategory,
      String lookupCategory, String keywordLookupCategory) {
    super();
    this.id = id;
    this.setKeyword(keyword);
    this.isBlacklisted = isBlacklisted;
    this.iabCategory = iabCategory;
    this.lookupCategory = lookupCategory;
    this.keywordLookupCategory = keywordLookupCategory;
  }
  
  private Long id;
  private String keyword;
  private Boolean isBlacklisted;
  private String iabCategory;
  private String lookupCategory;
  private String keywordLookupCategory;
  
  public Long getId() {
    return id;
  }
  
  public void setId(Long id) {
    this.id = id;
  }
  
  public Boolean getIsBlacklisted() {
    return isBlacklisted;
  }
  
  public void setIsBlacklisted(Boolean isBlacklisted) {
    this.isBlacklisted = isBlacklisted;
  }
  
  public String getIabCategory() {
    return iabCategory;
  }
  
  public void setIabCategory(String iabCategory) {
    this.iabCategory = iabCategory;
  }
  
  public String getLookupCategory() {
    return lookupCategory;
  }
  
  public void setLookupCategory(String lookupCategory) {
    this.lookupCategory = lookupCategory;
  }
  
  public String getKeywordLookupCategory() {
    return keywordLookupCategory;
  }
  
  public void setKeywordLookupCategory(String keywordLookupCategory) {
    this.keywordLookupCategory = keywordLookupCategory;
  }
  
  public String getKeyword() {
    return keyword;
  }
  
  public void setKeyword(String keyword) {
    this.keyword = keyword;
  }
  
}
