package com.mediaiq.aiq.redshift.batch.domian;

import java.util.Date;

import com.mediaiq.appnexus.value.AppnexusSeat;

public class RedshiftSegment {
  
  private Long id;
  private String code;
  private String state;
  private String name;
  private String description;
  private Long memberId;
  private String category;
  private Long advertiserId;
  private Date lastModified;
  private Long parentSegmentId;
  private AppnexusSeat seat;
  // private SegmentGroup segmentGroup;
  // private DataProviderType dataProvider;
  // private String segmentTypeName;
  
  public RedshiftSegment(Long id, String code, String state, String name, String description,
      Long memberId, String category, Long advertiserId, Date lastModified, Long parentSegmentId,
      AppnexusSeat seat) {
    super();
    this.id = id;
    this.code = code;
    this.state = state;
    this.name = name;
    this.description = description;
    this.memberId = memberId;
    this.category = category;
    this.advertiserId = advertiserId;
    this.lastModified = lastModified;
    this.parentSegmentId = parentSegmentId;
    this.seat = seat;
  }
  
  public Long getId() {
    return id;
  }
  
  public void setId(Long id) {
    this.id = id;
  }
  
  public String getCode() {
    return code;
  }
  
  public void setCode(String code) {
    this.code = code;
  }
  
  public String getState() {
    return state;
  }
  
  public void setState(String state) {
    this.state = state;
  }
  
  public String getName() {
    return name;
  }
  
  public void setName(String name) {
    this.name = name;
  }
  
  public String getDescription() {
    return description;
  }
  
  public void setDescription(String description) {
    this.description = description;
  }
  
  public Long getMemberId() {
    return memberId;
  }
  
  public void setMemberId(Long memberId) {
    this.memberId = memberId;
  }
  
  public String getCategory() {
    return category;
  }
  
  public void setCategory(String category) {
    this.category = category;
  }
  
  public Long getAdvertiserId() {
    return advertiserId;
  }
  
  public void setAdvertiserId(Long advertiserId) {
    this.advertiserId = advertiserId;
  }
  
  public Date getLastModified() {
    return lastModified;
  }
  
  public void setLastModified(Date lastModified) {
    this.lastModified = lastModified;
  }
  
  public Long getParentSegmentId() {
    return parentSegmentId;
  }
  
  public void setParentSegmentId(Long parentSegmentId) {
    this.parentSegmentId = parentSegmentId;
  }
  
  public AppnexusSeat getSeat() {
    return seat;
  }
  
  public void setSeat(AppnexusSeat seat) {
    this.seat = seat;
  }
}
