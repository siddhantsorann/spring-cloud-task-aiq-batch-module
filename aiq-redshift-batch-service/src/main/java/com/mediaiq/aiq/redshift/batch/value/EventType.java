package com.mediaiq.aiq.redshift.batch.value;

public enum EventType {
  pc_conv, pv_conv;
}
