package com.mediaiq.aiq.redshift.batch.row.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.RowMapper;

import com.mediaiqdigital.doubleclick.domain.DbmPlacement;

@Configuration(value = "dbmPlacementRowMapper")
public class DbmPlacementRowMapper implements RowMapper<DbmPlacement> {
  
  @Override
  public DbmPlacement mapRow(ResultSet rs, int rowNum) throws SQLException {
    
    DbmPlacement dbmPlacement = new DbmPlacement();
    dbmPlacement.setId(rs.getLong("id"));
    dbmPlacement.setName(rs.getString("name"));
    dbmPlacement.setInsertionOrderId(rs.getLong("insertion_order_id"));
    dbmPlacement.setActive(rs.getBoolean("active"));
    
    return dbmPlacement;
  }
}
