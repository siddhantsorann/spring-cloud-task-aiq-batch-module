package com.mediaiq.aiq.redshift.batch.processor;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.domain.InsertionOrder;
import com.mediaiq.appnexus.domain.InsertionOrderAudit;

@Component
public class InsertionOrderProcessor implements ItemProcessor<InsertionOrder, InsertionOrderAudit> {
  
  @Override
  public InsertionOrderAudit process(InsertionOrder insertionOrder) throws Exception {
    
    InsertionOrderAudit insertionOrderAudit = new InsertionOrderAudit(insertionOrder);
    
    return insertionOrderAudit;
  }
  
}
