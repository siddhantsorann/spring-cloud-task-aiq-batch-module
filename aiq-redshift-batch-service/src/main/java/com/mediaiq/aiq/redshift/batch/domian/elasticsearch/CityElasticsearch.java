package com.mediaiq.aiq.redshift.batch.domian.elasticsearch;

import org.springframework.stereotype.Component;

@Component
public class CityElasticsearch {
  
  private Long cityCode;
  private String city;
  private Long countryCode;
  private String twoLetterCountry;
  private Long regionCode;
  
  public Long getRegionCode() {
    return regionCode;
  }

  public void setRegionCode(Long regionCode) {
    this.regionCode = regionCode;
  }

  public Long getCityCode() {
    return cityCode;
  }
  
  public void setCityCode(Long cityCode) {
    this.cityCode = cityCode;
  }
  
  public String getCity() {
    return city;
  }
  
  public void setCity(String city) {
    this.city = city;
  }
  
  public Long getCountryCode() {
    return countryCode;
  }
  
  public void setCountryCode(Long countryCode) {
    this.countryCode = countryCode;
  }
  
  public String getTwoLetterCountry() {
    return twoLetterCountry;
  }
  
  public void setTwoLetterCountry(String twoLetterCountry) {
    this.twoLetterCountry = twoLetterCountry;
  }
  
}
