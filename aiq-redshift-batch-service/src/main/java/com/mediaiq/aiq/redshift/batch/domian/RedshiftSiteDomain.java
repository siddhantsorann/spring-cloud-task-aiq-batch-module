package com.mediaiq.aiq.redshift.batch.domian;

import java.util.Date;

public class RedshiftSiteDomain {
  
  private String name;
  private String topLevelCategory;
  private String secondLevelCategory;
  private Long platformAuditedImpressions;
  private Long sellerAuditedImpressions;
  private Date lastModified;
  private Boolean isBlacklisted;
  private String iabCategory;
  private String iabSubCategory;
  
  public RedshiftSiteDomain(String name, String topLevelCategory, String secondLevelCategory,
      Long platformAuditedImpressions, Long sellerAuditedImpressions, Date lastModified,
      Boolean isBlacklisted, String iabCategory, String iabSubCategory) {
    super();
    this.name = name;
    this.topLevelCategory = topLevelCategory;
    this.secondLevelCategory = secondLevelCategory;
    this.platformAuditedImpressions = platformAuditedImpressions;
    this.sellerAuditedImpressions = sellerAuditedImpressions;
    this.lastModified = lastModified;
    this.setIsBlacklisted(isBlacklisted);
    this.iabCategory = iabCategory;
    this.iabSubCategory = iabSubCategory;
  }
  
  public String getName() {
    return name;
  }
  
  public void setName(String name) {
    this.name = name;
  }
  
  public String getTopLevelCategory() {
    return topLevelCategory;
  }
  
  public void setTopLevelCategory(String topLevelCategory) {
    this.topLevelCategory = topLevelCategory;
  }
  
  public String getSecondLevelCategory() {
    return secondLevelCategory;
  }
  
  public void setSecondLevelCategory(String secondLevelCategory) {
    this.secondLevelCategory = secondLevelCategory;
  }
  
  public Long getPlatformAuditedImpressions() {
    return platformAuditedImpressions;
  }
  
  public void setPlatformAuditedImpressions(Long platformAuditedImpressions) {
    this.platformAuditedImpressions = platformAuditedImpressions;
  }
  
  public Long getSellerAuditedImpressions() {
    return sellerAuditedImpressions;
  }
  
  public void setSellerAuditedImpressions(Long sellerAuditedImpressions) {
    this.sellerAuditedImpressions = sellerAuditedImpressions;
  }
  
  public Date getLastModified() {
    return lastModified;
  }
  
  public void setLastModified(Date lastModified) {
    this.lastModified = lastModified;
  }
  
  public String getIabCategory() {
    return iabCategory;
  }
  
  public void setIabCategory(String iabCategory) {
    this.iabCategory = iabCategory;
  }
  
  public String getIabSubCategory() {
    return iabSubCategory;
  }
  
  public void setIabSubCategory(String iabSubCategory) {
    this.iabSubCategory = iabSubCategory;
  }
  
  @Override
  public String toString() {
    return "RedshiftSiteDomain [name=" + name + ", topLevelCategory=" + topLevelCategory
        + ", secondLevelCategory=" + secondLevelCategory + ", platformAuditedImpressions="
        + platformAuditedImpressions + ", sellerAuditedImpressions=" + sellerAuditedImpressions
        + ", lastModified=" + lastModified + ", isBlacklisted=" + isBlacklisted + ", iabCategory="
        + iabCategory + ", iabSubCategory=" + iabSubCategory + "]";
  }
  
  public Boolean getIsBlacklisted() {
    return isBlacklisted;
  }
  
  public void setIsBlacklisted(Boolean isBlacklisted) {
    this.isBlacklisted = isBlacklisted;
  }
  
}
