package com.mediaiq.aiq.redshift.batch.tasklet;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.mediaiq.spring.batch.domain.StatsJobLastRunHour;
import com.mediaiq.spring.batch.domain.id.StatsJobLastRunHourId;
import com.mediaiq.spring.batch.repo.StatsJobLastRunHourRepo;

@Component
@Scope("step")
public class CheckStatsInRedshiftTasklet implements Tasklet {
  private static final String FEED_SERVICE_LAST_MODIFIED_API =
      "http://aiq.mediaiqdigital.com/feed/lastModified?feedname=appnexusLastHourlyUpdate";
  
  private static final String PLACEMENT_CONVERSION_STATS_JOB = "PLACEMENT_CONVERSION_STATS";
  
  Logger logger = LoggerFactory.getLogger(CheckStatsInRedshiftTasklet.class);
  
  @Autowired
  StatsJobLastRunHourRepo statsJobLastRunHourRepo;
  
  @Value("#{jobParameters['date']}")
  private Date date;
  
  @Override
  public RepeatStatus execute(StepContribution arg0, ChunkContext arg1) throws Exception {
    
    logger.info("Check if new stats are ready...");
    RestTemplate restTemplate = new RestTemplate();
    String content = restTemplate.getForObject(FEED_SERVICE_LAST_MODIFIED_API, String.class);
    
    // Get from repo the last hour for the job
    StatsJobLastRunHour lastRunInfo =
        statsJobLastRunHourRepo.findByIdDateAndIdJobName(date, PLACEMENT_CONVERSION_STATS_JOB);
    if (lastRunInfo == null || Integer.valueOf(content) > lastRunInfo.getMaxHour()) {
      arg0.setExitStatus(ExitStatus.COMPLETED);
      StatsJobLastRunHour statsJobLastRunHour =
          new StatsJobLastRunHour(new StatsJobLastRunHourId(date, PLACEMENT_CONVERSION_STATS_JOB),
              Integer.valueOf(content));
      statsJobLastRunHourRepo.save(statsJobLastRunHour);
    } else {
      logger.info("New stats are not available... Stopping...");
      arg0.setExitStatus(ExitStatus.STOPPED);
    }
    
    return RepeatStatus.FINISHED;
  }
}
