package com.mediaiq.aiq.redshift.batch.service.impl;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.mediaiq.aiq.domain.Dsp;
import com.mediaiq.aiq.redshift.batch.service.RedshiftPixelStatService;
import com.mediaiqdigital.aiq.tradingcenter.domain.stats.DailyPixelLevelStats;
import com.mediaiqdigital.aiq.tradingcenter.repo.DailyPixelLevelStatsRepo;

/**
 * Created by prasannachidire on 12-Jun-2017
 */
@Service
public class RedshiftPixelStatServiceImpl implements RedshiftPixelStatService {
  
  final static Logger LOGGER = LoggerFactory.getLogger(RedshiftPixelStatServiceImpl.class);
  
  
  final private String SQLFILE_GET_PIXEL_STATS_BY_DATE = "/sql/pixel_stats_by_date.sql";
  
  SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
  
  @Autowired
  JdbcTemplate srsWareHouseJdbcTemplate;
  
  @Autowired
  DailyPixelLevelStatsRepo dailyPixelLevelStatsRepo;
  
  @Value("${s3.pixel.source}")
  private String s3KeyPrefix;

  @Value("${s3.iam}")
  private String iam;
  
  @Override
  public void uploadByDates(Date startDate, Date endDate) throws DataAccessException, IOException {
    
    LOGGER.info("Running pixel stats job fromDate : {}, endDate: {}",
        getDateFormat().format(startDate), getDateFormat().format(endDate));
    
    String query =
        IOUtils.toString(new ClassPathResource(SQLFILE_GET_PIXEL_STATS_BY_DATE).getInputStream());
    query = query.replace(":startDate", getDateFormat().format(startDate));
    query = query.replace(":endDate", getDateFormat().format(endDate));
    query = query.replace(":s3KeyPrefix", s3KeyPrefix);
    query = query.replace(":iam", iam);

    LOGGER.info("Starting to run the query {}", query);
    srsWareHouseJdbcTemplate.execute(query);
    LOGGER.info("Uploaded pixel stats to s3");
  }
  
  
  @Override
  public void save(List<DailyPixelLevelStats> dailyPixelLevelStats) {
    dailyPixelLevelStatsRepo.save(dailyPixelLevelStats);
  }
  
  @Override
  public void uploadFile(String filePath) {
    dailyPixelLevelStatsRepo.uploadFile(Dsp.Appnexus.name(), filePath);
    
  }
  
  public SimpleDateFormat getDateFormat() {
    return dateFormat;
  }
  
  public void setDateFormat(SimpleDateFormat dateFormat) {
    this.dateFormat = dateFormat;
  }
  
}
