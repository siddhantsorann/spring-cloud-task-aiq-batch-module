package com.mediaiq.aiq.redshift.batch.tasklet.dbm;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;

import com.mediaiq.aiq.aws.s3.S3Service;

public abstract class CopyToS3Tasklet implements Tasklet {
  
  @Autowired
  S3Service s3Client;
  
  @Override
  public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext)
      throws Exception {
    s3Client.copy(getUploadFileName(), getBucketName(), getKeyName());
    return RepeatStatus.FINISHED;
  }
  
  protected abstract String getKeyName();
  
  protected abstract String getBucketName();
  
  protected abstract String getUploadFileName();
}
