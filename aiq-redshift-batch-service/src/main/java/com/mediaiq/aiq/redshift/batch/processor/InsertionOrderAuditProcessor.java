package com.mediaiq.aiq.redshift.batch.processor;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.domain.InsertionOrderAudit;

@Component
public class InsertionOrderAuditProcessor
    implements ItemProcessor<InsertionOrderAudit, InsertionOrderAudit> {
  
  @Override
  public InsertionOrderAudit process(InsertionOrderAudit insertionOrderAudit) throws Exception {
    
    
    return insertionOrderAudit;
  }
  
}
