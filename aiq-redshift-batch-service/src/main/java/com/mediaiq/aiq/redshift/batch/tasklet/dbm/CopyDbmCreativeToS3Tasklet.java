
package com.mediaiq.aiq.redshift.batch.tasklet.dbm;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("step")
public class CopyDbmCreativeToS3Tasklet extends CopyToS3Tasklet {
  
  @Value("${s3.dbmCreative.source.filename}")
  private String uploadFileName;
  
  @Value("#{'${s3.dbmCreative.bucket}' + '${s3.dbmCreative.path}'}")
  private String bucketName;
  
  @Value("${s3.dbmCreative.destination.filename}")
  private String keyName;
  
  @Override
  protected String getKeyName() {
    return keyName;
  }
  
  @Override
  protected String getBucketName() {
    return bucketName;
  }
  
  @Override
  protected String getUploadFileName() {
    return uploadFileName;
  }
}
