package com.mediaiq.aiq.redshift.batch.tasklet;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.aiq.redshift.batch.service.impl.RedshiftJobLaunchServiceImpl;
import com.mediaiq.appnexus.domain.Placement;
import com.mediaiq.appnexus.domain.ProfileSegmentLookup;
import com.mediaiq.appnexus.domain.id.ProfileSegmentLookupId;
import com.mediaiq.appnexus.domain.profile.AppnexusProfile;
import com.mediaiq.appnexus.domain.profile.SegmentGroupTarget;
import com.mediaiq.appnexus.domain.profile.SegmentTarget;
import com.mediaiq.appnexus.mongo.repo.AppnexusProfileRepo;
import com.mediaiq.appnexus.repo.PlacementRepo;
import com.mediaiq.appnexus.repo.ProfileSegmentLookupRepo;

@Component
@Scope("step")
public class UpdateProfileSegmentLookupTasklet implements Tasklet {
  
  private final Logger logger = LoggerFactory.getLogger(RedshiftJobLaunchServiceImpl.class);
  
  @Autowired
  private PlacementRepo placementRepo;
  
  @Value("#{jobParameters['last_modified_after']}")
  protected Date lastModifiedAfter;
  
  @Value("#{jobParameters['last_modified_before']}")
  protected Date lastModifiedBefore;
  
  @Autowired
  private ProfileSegmentLookupRepo profileSegmentLookupRepo;
  
  @Autowired
  private AppnexusProfileRepo appnexusProfileRepo;
  
  @Override
  public RepeatStatus execute(StepContribution arg0, ChunkContext arg1) throws Exception {
    
    List<AppnexusProfile> profiles =
        appnexusProfileRepo.findByLastModifiedBetween(lastModifiedAfter, lastModifiedBefore);
    logger.info("Total profiles: {}", profiles.size());
    
    List<String> profileIds =
        profiles.stream().map(p -> p.getId().toString()).collect(Collectors.toList());
    if (profileIds.isEmpty())
      return RepeatStatus.FINISHED;
    
    List<Placement> placements = placementRepo.findByProfileIdIn(profileIds);
    logger.info("Total no. of placements found: {}", placements.size());
    
    Map<String, Long> profileIdToPlacementIdMap =
        placements.stream().collect(Collectors.toMap(Placement::getProfileId, Placement::getId));
    logger.info("ProfileId to PlacementId map done");
    
    List<ProfileSegmentLookup> lookups = new ArrayList<ProfileSegmentLookup>();
    for (AppnexusProfile profile : profiles) {
      if (profile.getSegmentGroupTargets() == null)
        continue;
      
      for (SegmentGroupTarget segmentGroupTarget : profile.getSegmentGroupTargets()) {
        if (segmentGroupTarget == null || segmentGroupTarget.getSegments() == null)
          continue;
        for (SegmentTarget segmentTarget : segmentGroupTarget.getSegments()) {
          if (segmentTarget == null)
            continue;
          
          ProfileSegmentLookup profileSegmentLoopkup = new ProfileSegmentLookup();
          profileSegmentLoopkup.setLastModified(profile.getLastModified());
          profileSegmentLoopkup.setSegmentGroupAction(profile.getSegmentGroupAction().name());
          profileSegmentLoopkup.setSegmentBool(profile.getSegmentBooleanOperator().name());
          
          ProfileSegmentLookupId id = new ProfileSegmentLookupId(profile.getId(),
              profile.getAdvertiserId(), profile.getSeat().name(), segmentTarget.getSegmentId());
          profileSegmentLoopkup.setId(id);
          
          if (profileIdToPlacementIdMap.containsKey(profile.getId().toString()))
            profileSegmentLoopkup
                .setCampaignId(profileIdToPlacementIdMap.get(profile.getId().toString()));
          
          lookups.add(profileSegmentLoopkup);
        }
      }
    }
    
    logger.info("Saving lookup...");
    // save
    profileSegmentLookupRepo.save(lookups);
    logger.info("Lookup saved");
    
    return RepeatStatus.FINISHED;
  }
  
}
