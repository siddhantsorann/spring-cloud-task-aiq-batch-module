package com.mediaiq.aiq.redshift.batch;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class AdsafeSiteDomainInfoRowMapper implements RowMapper<AdsafeSiteDomainInfo> {
  
  @Override
  public AdsafeSiteDomainInfo mapRow(ResultSet rs, int rowNum) throws SQLException {
    AdsafeSiteDomainInfo adsafeSiteDomainInfo = new AdsafeSiteDomainInfo();
    adsafeSiteDomainInfo.setSiteDomain(rs.getString("site_domain"));
    adsafeSiteDomainInfo.setSadScore(rs.getInt("sad_score"));
    adsafeSiteDomainInfo.setBsScore(rs.getInt("bs_score"));
    adsafeSiteDomainInfo.setTraqBucket(rs.getString("traq_bucket"));
    if (adsafeSiteDomainInfo.siteDomain == null)
      throw new IllegalArgumentException("Sitedomain should not be null.");
    return adsafeSiteDomainInfo;
  }
  
}
