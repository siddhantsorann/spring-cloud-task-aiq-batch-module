package com.mediaiq.aiq.redshift.batch.row.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.RowMapper;

import com.mediaiq.aiq.redshift.batch.domian.OfficeClientAdvertiserLookup;

@Configuration(value = "officeClientAdvertiserLookupRowMapper")
public class OfficeClientAdvertiserLookupRowMapper
    implements RowMapper<OfficeClientAdvertiserLookup> {
  
  @Override
  public OfficeClientAdvertiserLookup mapRow(ResultSet rs, int rowNum) throws SQLException {
    
    OfficeClientAdvertiserLookup lookup = new OfficeClientAdvertiserLookup();
    lookup.setCountryId(rs.getLong("country_id"));
    lookup.setCountryName(rs.getString("country_name"));
    lookup.setMarketRegionId(rs.getLong("market_region_id"));
    lookup.setMarketRegionName(rs.getString("market_region_name"));
    lookup.setOfficeId(rs.getLong("office_id"));
    lookup.setOfficeName(rs.getString("office_name"));
    lookup.setMiqAdvertiserId(rs.getLong("miq_advertiser_id"));
    lookup.setMiqAdvertiserName(rs.getString("miq_advertiser_name"));
    lookup.setClientId(rs.getLong("client_id"));
    lookup.setClientName(rs.getString("client_name"));
    
    return lookup;
  }
  
}
