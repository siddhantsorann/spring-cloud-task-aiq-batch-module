package com.mediaiq.aiq.redshift.batch.service.impl;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import com.mediaiq.aiq.redshift.batch.service.DateParameter;
import com.mediaiq.aiq.redshift.batch.service.RedshiftJobLaunchService;
import com.mediaiq.aiq.redshift.batch.value.JobName;
import com.mediaiq.spring.batch.integration.component.JobLaunchGateway;
import com.mediaiq.spring.batch.service.impl.AbstractJobLaunchServiceImpl;

@Configuration
@Service
public class RedshiftJobLaunchServiceImpl extends AbstractJobLaunchServiceImpl
    implements RedshiftJobLaunchService {
  
  private static final Logger logger = LoggerFactory.getLogger(RedshiftJobLaunchServiceImpl.class);
  
  @Autowired
  JobLaunchGateway jobLaunchGateway;
  
  @Override
  public void launchPlacementPushJob() {
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.lookupPlacementPushToRedshiftJob.name(),
        jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public void launchSegmentPushJob() {
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.lookupSegmentPushToRedshiftJob.name(),
        jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public void launchSiteDomainPushJob() {
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("last_modified", new LocalDate().minusWeeks(2).toDate())
        .addDate("date", new Date());
    launchJob(JobName.lookupSitedomainPushToRedshiftJob.name(),
        jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public void launchMobileAppPushJob() {
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("last_modified", new LocalDate().minusWeeks(2).toDate())
        .addDate("date", new Date());
    launchJob(JobName.lookupMobileAppPushToRedshiftJob.name(),
        jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public void launchCategoryPushJob() {
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.categoryPushToRedshiftJob.name(), jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public void launchDeviceModelPushJob() {
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.deviceModelPushToRedshiftJob.name(), jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public void launchCreativePushJob(DateParameter lastModifiedStartDate,
      DateParameter lastModifiedEndDate) {
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    
    if (lastModifiedStartDate != null && lastModifiedStartDate.getDate() != null)
      jobParametersBuilder.addDate("last_modified_after", lastModifiedStartDate.getDate());
    else
      jobParametersBuilder.addDate("last_modified_after", getOlderDate(2));
    
    if (lastModifiedEndDate != null && lastModifiedEndDate.getDate() != null)
      jobParametersBuilder.addDate("last_modified_before", lastModifiedEndDate.getDate());
    else
      jobParametersBuilder.addDate("last_modified_before", getOlderDate(0));
    
    launchJob(JobName.creativePushToRedshiftJob.name(), jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public void launchKeywordLookupPushJob() {
    logger.info("Launching keyword lookup push job");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.keywordLookupPushToRedshiftJob.name(),
        jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public void launchAdvertiserSegmentPushJob() {
    logger.info("Launching Advertiser Segment push job");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.advertiserSegmentPushToRedshiftJob.name(),
        jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public void launchMiqThirdPartyLookupSegmentPushJob() {
    logger.info("Launching Miq-Third Party Segment push job");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.miqThirdPartySegmentPushToRedshiftJob.name(),
        jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public void launchThirdPartyLookupSegmentPushJob() {
    logger.info("Launching Third Party Segment push job");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.thirdPartySegmentPushToRedshiftJob.name(),
        jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public void launchInsertionOrderAuditPushJob() {
    logger.info("Launching Insertion Order Audit push job");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.insertionOrderAuditPushToRedshiftJob.name(),
        jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public void launchLineItemAuditPushJob() {
    logger.info("Launching LineItem Audit push job");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.lineItemAuditPushToRedshiftJob.name(),
        jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public void launchPlacementAuditPushJob() {
    logger.info("Launching Placement Audit push job");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.placementAuditPushToRedshiftJob.name(),
        jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public void launchProfileSegmentLookupPushJob(DateParameter lastModifiedStartDate,
      DateParameter lastModifiedEndDate) {
    logger.info("Launching Profile Segment Lookup Push Job");
    
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    
    if (lastModifiedStartDate != null && lastModifiedStartDate.getDate() != null)
      jobParametersBuilder.addDate("last_modified_after", lastModifiedStartDate.getDate());
    else
      jobParametersBuilder.addDate("last_modified_after", getOlderDate(2));
    
    if (lastModifiedEndDate != null && lastModifiedEndDate.getDate() != null)
      jobParametersBuilder.addDate("last_modified_before", lastModifiedEndDate.getDate());
    else
      jobParametersBuilder.addDate("last_modified_before", getOlderDate(0));
    
    launchJob(JobName.profileSegmentLookupPushToRedshiftJob.name(),
        jobParametersBuilder.toJobParameters());
    
  }
  
  @Override
  public void launchPlacementConversionStatsPullJob(DateParameter date) {
    logger.info("Launching Placement Conversion Stats Pull Job");
    
    JobParametersBuilder jobParameterBuilder = new JobParametersBuilder();
    
    if (date != null && date.getDate() != null)
      jobParameterBuilder.addDate("date", date.getDate());
    else
      jobParameterBuilder.addDate("date", new Date());
    launchJob(JobName.placementConversionStatsLoadJob.name(),
        jobParameterBuilder.toJobParameters());
  }
  
  @Override
  public void launchCampaignLookupPushJob() {
    logger.info("Launching CampaignLookup Push Job");
    
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.campaignLookupPushToRedshiftJob.name(),
        jobParametersBuilder.toJobParameters());
    
  }
  
  private Date getOlderDate(int days) {
    Calendar cal = Calendar.getInstance();
    cal.add(Calendar.DATE, -days);
    cal.add(Calendar.HOUR_OF_DAY, 0);
    Date date = cal.getTime();
    return date;
  }
  
  @Override
  public void launchBrowserLookupPushJob() {
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.browserPushToRedshiftJob.name(), jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public void launchLanguageLookupPushJob() {
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.languagePushToRedshiftJob.name(), jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public void launchOperatingSystemLookupPushJob() {
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.operatingSystemPushToRedshiftJob.name(),
        jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public void launchCarrierLookupPushJob() {
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.carrierPushToRedshiftJob.name(), jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public void launchAdvertiserPushJob() {
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.advertiserPushToRedshiftJob.name(), jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public void launchSellerLookupPushJob() {
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.sellerPushToRedshiftJob.name(), jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public void launchPixelPushJob() {
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.conversionPixelPushToRedshiftJob.name(),
        jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public void launchGeoLookupPushJob() {
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.cityPushToRedshiftJob.name(), jobParametersBuilder.toJobParameters());
  }
  
  /////////////////////////////////
  // DBM LOOKUPS
  
  @Override
  public void launchDbmPixelPushJob() {
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.dbmPixelPushToRedshiftJob.name(), jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public void launchDbmAdvertiserPushJob() {
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.dbmAdvertiserPushToRedshiftJob.name(),
        jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public void launchDbmCreativePushJob() {
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.dbmCreativePushToRedshiftJob.name(), jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public void launchDbmGeoLocationPushJob() {
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.dbmGeoLocationPushToRedshiftJob.name(),
        jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public void launchDbmInsertionOrderPushJob() {
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.dbmInsertionOrderPushToRedshiftJob.name(),
        jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public void launchDbmIspPushJob() {
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.dbmIspPushToRedshiftJob.name(), jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public void launchDbmLanguagePushJob() {
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.dbmLanguagePushToRedshiftJob.name(), jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public void launchDbmPartnerPushJob() {
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.dbmPartnerPushToRedshiftJob.name(), jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public void launchDbmPlacementPushJob() {
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.dbmPlacementPushToRedshiftJob.name(), jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public void launchDbmBrowserPushJob() {
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.dbmBrowserPushToRedshiftJob.name(), jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public void launchDbmDeviceCriteriaPushJob() {
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.dbmDeviceCriteriaPushToRedshiftJob.name(),
        jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public void launchDbmTimezoneAdvertiserPushJob() {
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.dbmTimezoneAdvertiserPushToRedshiftJob.name(),
        jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public void launchDbmLineItemPushJob() {
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.dbmLineItemPushToRedshiftJob.name(), jobParametersBuilder.toJobParameters());
    
  }
  
  @Override
  public void launchDbmExchangePushJob() {
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.dbmExchangePushToRedshiftJob.name(), jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public void launchAdvertiserCategoryPushJob() {
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.advertiserCategoryPushToRedshiftJob.name(),
        jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public void launchPixelStatsLoadJob(DateParameter startDate, DateParameter endDate) {
    logger.info("Launching Pixel Stats Load Job");
    
    JobParametersBuilder jobParameterBuilder = new JobParametersBuilder();
    
    if (startDate != null && startDate.getDate() != null)
      jobParameterBuilder.addDate("start_date", startDate.getDate());
    else
      jobParameterBuilder.addDate("start_date", DateUtils.addDays(new Date(), -1));
    if (endDate != null && endDate.getDate() != null)
      jobParameterBuilder.addDate("end_date", endDate.getDate());
    else
      jobParameterBuilder.addDate("end_date", DateUtils.addDays(new Date(), -1));
    
    launchJob(JobName.pixelStatsLoadJob.name(), jobParameterBuilder.toJobParameters());
  }
  
  @Override
  public void launchOfficeClientAdvertiserLookupPushJob() {
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.officeClientAdvertiserLookupPushToRedshiftJob.name(),
        jobParametersBuilder.toJobParameters());
  }

  @Override
  public void launchPostCodeNameToIdMapping() {
    logger.info("Launching Postcode job to write from Redshift to S3 to Elasticsearch");
    JobParametersBuilder jobParameterBuilder = new JobParametersBuilder();
    jobParameterBuilder.addDate("date", new Date());
    launchJob(JobName.postcodeNameToIdMapping.name(), jobParameterBuilder.toJobParameters());
  }
  
}
