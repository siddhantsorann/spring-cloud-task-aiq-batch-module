package com.mediaiq.aiq.redshift.batch.tasklet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
@Scope("step")
public abstract class WriteFromS3ToSrsRedshiftTasklet implements Tasklet {
  
  private static final Logger logger =
      LoggerFactory.getLogger(WriteFromS3ToSrsRedshiftTasklet.class);
  
  @Autowired(required = false)
  @Qualifier("wareHouse2JdbcTemplate")
  private JdbcTemplate redshiftWareHouse2JdbcTemplate;
  
  @Autowired(required = false)
  @Qualifier("srsWareHouseJdbcTemplate")
  private JdbcTemplate srsWareHouseJdbcTemplate;
  
  @Autowired(required = false)
  @Qualifier("amnetWareHouseJdbcTemplate")
  private JdbcTemplate amnetWareHouseJdbcTemplate;
  
  @Override
  public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext)
      throws Exception {
    
    // writing to warehouse 2
    if (redshiftWareHouse2JdbcTemplate != null) {
      logger.info("warehouse 2 : {}, {}", getDeleteQuery());
      redshiftWareHouse2JdbcTemplate.update(getDeleteQuery());
      redshiftWareHouse2JdbcTemplate.execute("commit;");
      logger.info("warehouse 2 : " + getCopyQuery());
      redshiftWareHouse2JdbcTemplate.execute(getCopyQuery());
      redshiftWareHouse2JdbcTemplate.execute("commit;");
    }
    
    // writing to srs Warehouse
    if (srsWareHouseJdbcTemplate != null) {
      logger.info("srs warehouse : {}, {}", getDeleteQuery());
      srsWareHouseJdbcTemplate.execute(getDeleteQuery());
      srsWareHouseJdbcTemplate.execute("commit;");
      logger.info("srs warehouse : {}, {}", getCopyQuery());
      srsWareHouseJdbcTemplate.execute(getCopyQuery());
      srsWareHouseJdbcTemplate.execute("commit;");
    }
    
    if (amnetWareHouseJdbcTemplate != null) {
      logger.info("warehouse amnet: " + getCopyQuery());
      amnetWareHouseJdbcTemplate.execute(getDeleteQuery());
      amnetWareHouseJdbcTemplate.execute("commit;");
      amnetWareHouseJdbcTemplate.execute(getCopyQuery());
      amnetWareHouseJdbcTemplate.execute("commit;");
    }
    
    return RepeatStatus.FINISHED;
  }
  
  protected abstract String getDeleteQuery();
  
  protected abstract String getCopyQuery();
}
