package com.mediaiq.aiq.redshift.batch.tasklet;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("step")
public class WriteThirdPartySegmentFromS3ToRedshiftTasklet extends WriteFromS3ToRedshiftTasklet {
  
  @Value("${s3.third-party-segment.query.delete}")
  private String deleteQuery;
  
  @Value("${s3.third-party-segment.query.copy}")
  private String copyQuery;
  
  @Override
  protected String getDeleteQuery() {
    return deleteQuery;
  }
  
  @Override
  protected String getCopyQuery() {
    return copyQuery;
  }
  
  @Override
  protected Object[] getBindParameters() {
    return null;
  }
}
