package com.mediaiq.aiq.redshift.batch.domian;

public class AdvertiserCategoryLookup {
  private Long miqAdvertiserId;
  private String miqAdvertiserName;
  private String category;
  private String subcategory;
  private String dsp;
  private Long advertiserId;
  private String advertiserName;
  private String timezone;
  
  public String getTimezone() {
    return timezone;
  }
  
  public void setTimezone(String timezone) {
    this.timezone = timezone;
  }
  
  public Long getMiqAdvertiserId() {
    return miqAdvertiserId;
  }
  
  public void setMiqAdvertiserId(Long miqAdvertiserId) {
    this.miqAdvertiserId = miqAdvertiserId;
  }
  
  public String getMiqAdvertiserName() {
    return miqAdvertiserName;
  }
  
  public void setMiqAdvertiserName(String miqAdvertiserName) {
    this.miqAdvertiserName = miqAdvertiserName;
  }
  
  public String getCategory() {
    return category;
  }
  
  public void setCategory(String category) {
    this.category = category;
  }
  
  public String getSubcategory() {
    return subcategory;
  }
  
  public void setSubcategory(String subcategory) {
    this.subcategory = subcategory;
  }
  
  public String getDsp() {
    return dsp;
  }
  
  public void setDsp(String dsp) {
    this.dsp = dsp;
  }
  
  public Long getAdvertiserId() {
    return advertiserId;
  }
  
  public void setAdvertiserId(Long advertiserId) {
    this.advertiserId = advertiserId;
  }
  
  public String getAdvertiserName() {
    return advertiserName;
  }
  
  public void setAdvertiserName(String advertiserName) {
    this.advertiserName = advertiserName;
  }
  
}
