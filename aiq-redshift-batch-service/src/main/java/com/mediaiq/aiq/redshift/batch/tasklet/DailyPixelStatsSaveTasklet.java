package com.mediaiq.aiq.redshift.batch.tasklet;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.aiq.redshift.batch.service.RedshiftPixelStatService;

/**
 * Created by prasannachidire on 16-Jun-2017
 */
@Component
@Scope("step")
public class DailyPixelStatsSaveTasklet implements Tasklet {
  
  
  final static Logger LOGGER = LoggerFactory.getLogger(DailyPixelStatsSaveTasklet.class);
  
  @Value("${s3.pixel.destination}")
  private String downloadFolderNameLocal;
  
  @Value("#{'${s3.pixel.path}'}")
  private String keyPrefix;
  
  @Value("#{'${s3.pixel.bucket}'}")
  private String bucketName;
  
  @Autowired
  RedshiftPixelStatService redshiftPixelStatService;
  
  @Override
  public RepeatStatus execute(StepContribution arg0, ChunkContext arg1) throws Exception {
    
    String downloadFileDirectory = downloadFolderNameLocal + "/" + keyPrefix;
    File[] files = new File(downloadFileDirectory).listFiles();
    for (File file : files) {
      String filePath = file.getAbsolutePath();
      LOGGER.info("saving records from file {}", filePath);
      redshiftPixelStatService.uploadFile(filePath);
    }
    
    return RepeatStatus.FINISHED;
  }
  
}
