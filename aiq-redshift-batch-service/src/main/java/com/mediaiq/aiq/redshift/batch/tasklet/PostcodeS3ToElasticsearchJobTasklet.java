package com.mediaiq.aiq.redshift.batch.tasklet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.mediaiq.aiq.elasticsearch.service.ElasticsearchService;
import com.mediaiq.aiq.redshift.batch.domian.elasticsearch.CityElasticsearch;
import com.mediaiq.aiq.redshift.batch.domian.elasticsearch.PostcodeElasticsearch;
import com.mediaiq.aiq.redshift.batch.domian.elasticsearch.RegionElasticsearch;

@Component
@Scope("step")
public class PostcodeS3ToElasticsearchJobTasklet implements Tasklet {
  
  @Autowired
  ElasticsearchService elasticsearchService;
  
  static final Logger logger = LoggerFactory.getLogger(PostcodeS3ToElasticsearchJobTasklet.class);
  static final int BATCH_SIZE = 500;
  @Autowired
  AmazonS3 s3Client;
  
  @Value("${s3.postcode.path}")
  private String s3Path;
  
  @Value("${s3.postcode.bucket}")
  private String bucket;
  
  @Value("${s3.postcode.file}")
  private String file;
  
  
  @Override
  public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext)
      throws Exception {
    logger.info("Indexing S3 data in {} to ES:", s3Path + file);
    S3Object object = s3Client.getObject(new GetObjectRequest(bucket, s3Path + file));
    System.out.println("Content-Type: " + object.getObjectMetadata().getContentType());
    saveTextInputStream(object.getObjectContent());
    return null;
  }
  
  private void saveTextInputStream(InputStream input) throws IOException{
    logger.info("Start reading from csv");
    BufferedReader br2 = null;
    String line = "";
    String cvsSplitBy = "\\|";
    // Read one text line at a time and display.
    InputStreamReader rd;
    List<String[]> batchOfLines = new ArrayList<>();
    try {
      rd = new InputStreamReader(input, "UTF-8");
      br2 = new BufferedReader(rd);
      int batch = 0;
      while ((line = br2.readLine()) != null) {
        batch++;
        line = line.replaceAll(",,", ",NA,");
        String[] object = line.split(cvsSplitBy);
        batchOfLines.add(object);
        if (batch == BATCH_SIZE) {
          //indexing 100 postcodes in a single bulk call
          logger.info("Indexing batch of: {}",BATCH_SIZE);
          createElasticsearchEntities(batchOfLines);
          batchOfLines = new ArrayList<>();
          batch = 0;
        }
      }
      //for last set of batch which is less than 500
      if(!batchOfLines.isEmpty()) {
        logger.info("Indexing last batch of size : {}",batch);
        createElasticsearchEntities(batchOfLines);
      }
    } catch (IOException e) {
      logger.error("ERROR--->: {}",e.getMessage());
      throw new  IOException(e);
    }
    
  }
  
  private void createElasticsearchEntities(List<String[]> batchOfLines) throws IOException {
    List<PostcodeElasticsearch> EsPostcodes = createPostCodeElaticsearchEntity(batchOfLines);
    createCityElaticsearchEntity(EsPostcodes);
    createRegionElaticsearchEntity(EsPostcodes);
  }
  
  private void createRegionElaticsearchEntity(
      List<PostcodeElasticsearch> postcodeElasticsearchList) throws IOException {
    List<RegionElasticsearch> regionElasticsearchList =
        postcodeElasticsearchList.stream().map(postcodeElasticsearch -> {
          RegionElasticsearch regionElasticsearch = new RegionElasticsearch();
          regionElasticsearch.setRegionCode(postcodeElasticsearch.getRegionCode());
          regionElasticsearch.setRegionName(postcodeElasticsearch.getRegionName());
          regionElasticsearch.setCountryCode(postcodeElasticsearch.getCityCode());
          regionElasticsearch.setTwoLetterCountry(postcodeElasticsearch.getTwoLetterCountry());
          return regionElasticsearch;
        }).collect(Collectors.toList());
    
    try {
      elasticsearchService.createIndexRegion(regionElasticsearchList);
    } catch (IOException e) {
      logger.error("Failed to save to elasticsearch regions", e.getMessage());
      throw new  IOException(e);
    }
  }
  
  private void createCityElaticsearchEntity(List<PostcodeElasticsearch> postcodeElasticsearchList) throws IOException {
    
    List<CityElasticsearch> cityCodeElasticsearchList =
        postcodeElasticsearchList.stream().map(postcodeElasticsearch -> {
          CityElasticsearch cityElasticsearch = new CityElasticsearch();
          cityElasticsearch.setCity(postcodeElasticsearch.getCity());
          cityElasticsearch.setCityCode(postcodeElasticsearch.getCityCode());
          cityElasticsearch.setCountryCode(postcodeElasticsearch.getCityCode());
          cityElasticsearch.setTwoLetterCountry(postcodeElasticsearch.getTwoLetterCountry());
          cityElasticsearch.setRegionCode(postcodeElasticsearch.getRegionCode());
          return cityElasticsearch;
        }).collect(Collectors.toList());
    try {
      elasticsearchService.createIndexCity(cityCodeElasticsearchList);
    } catch (IOException e) {
      logger.error("Failed to save to elasticsearch city:", e.getMessage());
      throw new  IOException(e);
    }
  }
  
  private List<PostcodeElasticsearch> createPostCodeElaticsearchEntity(
      List<String[]> batchOfLines) throws IOException {
    List<PostcodeElasticsearch> postcodeElasticsearchList = batchOfLines.stream().map(line -> {
      PostcodeElasticsearch postcodeElasticsearch = new PostcodeElasticsearch();
      postcodeElasticsearch.setCountryName(line[7]);
      postcodeElasticsearch.setPostalCode(line[0]);
      postcodeElasticsearch.setRegionName(line[2]);
      postcodeElasticsearch.setRegionCode(new Long(line[3]));
      postcodeElasticsearch.setCityCode(new Long(line[4]));
      postcodeElasticsearch.setCity(line[5]);
      postcodeElasticsearch.setCountryCode(new Long(line[6]));
      postcodeElasticsearch.setTwoLetterCountry(line[8]);
      postcodeElasticsearch.setPostCodeId(new Long(line[1]));
      return postcodeElasticsearch;
    }).collect(Collectors.toList());
    
    try {
      elasticsearchService.createIndexPostcode(postcodeElasticsearchList);
    } catch (IOException e) {
      logger.error("Failed to save to elasticsearch object {}", e.getMessage());
      throw new  IOException(e);
    }
    
    return postcodeElasticsearchList;
  }
  
}
