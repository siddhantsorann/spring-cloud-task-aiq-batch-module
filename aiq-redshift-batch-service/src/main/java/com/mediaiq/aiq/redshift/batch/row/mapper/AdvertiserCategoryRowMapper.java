package com.mediaiq.aiq.redshift.batch.row.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.RowMapper;

import com.mediaiq.aiq.redshift.batch.domian.AdvertiserCategoryLookup;

@Configuration(value = "advertiserCategoryRowMapper")
public class AdvertiserCategoryRowMapper implements RowMapper<AdvertiserCategoryLookup> {
  
  @Override
  public AdvertiserCategoryLookup mapRow(ResultSet rs, int rowNum) throws SQLException {
    
    AdvertiserCategoryLookup advertiserCategory = new AdvertiserCategoryLookup();
    advertiserCategory.setMiqAdvertiserId(rs.getLong("miq_advertiser_id"));
    advertiserCategory.setMiqAdvertiserName(rs.getString("miq_advertiser_name"));
    advertiserCategory.setCategory(rs.getString("category"));
    advertiserCategory.setSubcategory(rs.getString("subcategory"));
    advertiserCategory.setDsp(rs.getString("dsp"));
    advertiserCategory.setAdvertiserId(rs.getLong("advertiser_id"));
    advertiserCategory.setAdvertiserName(rs.getString("advertiser_name"));
    advertiserCategory.setTimezone(rs.getString("timezone"));
    
    return advertiserCategory;
  }
  
}
