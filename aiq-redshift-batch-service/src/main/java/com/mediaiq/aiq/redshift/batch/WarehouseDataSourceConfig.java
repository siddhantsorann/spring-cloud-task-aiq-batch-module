package com.mediaiq.aiq.redshift.batch;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.mchange.v2.c3p0.ComboPooledDataSource;

@Configuration
public class WarehouseDataSourceConfig {
  
  @ConditionalOnProperty(prefix = "wh2.datasource", name = "user")
  @Bean(name = "wh2DataSource")
  @ConfigurationProperties(prefix = "wh2.datasource")
  public ComboPooledDataSource wh2DataSource() {
    ComboPooledDataSource comboPooledDataSource = new ComboPooledDataSource();
    return comboPooledDataSource;
  }
  
  @ConditionalOnBean(name = "wh2DataSource")
  @Bean(name = "wareHouse2JdbcTemplate")
  public JdbcTemplate wareHouse2JdbcTemplate() {
    JdbcTemplate wareHouse2JdbcTemplate = new JdbcTemplate(wh2DataSource(), true);
    return wareHouse2JdbcTemplate;
  }
  
  @ConditionalOnBean(name = "wh2DataSource")
  @Bean(name = "wareHouse2NamedParameterJdbcTemplate")
  public NamedParameterJdbcTemplate wareHouse2NamedParameterJdbcTemplate() {
    NamedParameterJdbcTemplate wareHouse2NamedParameterJdbcTemplate =
        new NamedParameterJdbcTemplate(wh2DataSource());
    return wareHouse2NamedParameterJdbcTemplate;
  }
  
  @ConditionalOnProperty(prefix = "wh3.datasource", name = "user")
  @Bean(name = "wh3DataSource")
  @ConfigurationProperties(prefix = "wh3.datasource")
  public ComboPooledDataSource wh3DataSource() {
    ComboPooledDataSource comboPooledDataSource = new ComboPooledDataSource();
    return comboPooledDataSource;
  }
  
  @ConditionalOnBean(name = "wh3DataSource")
  @Bean(name = "wareHouse3JdbcTemplate")
  public JdbcTemplate wareHouse3JdbcTemplate() {
    JdbcTemplate wareHouse3JdbcTemplate = new JdbcTemplate(wh3DataSource(), true);
    return wareHouse3JdbcTemplate;
  }
  
  @ConditionalOnBean(name = "wh3DataSource")
  @Bean(name = "wareHouse3NamedParameterJdbcTemplate")
  public NamedParameterJdbcTemplate wareHouse3NamedParameterJdbcTemplate() {
    NamedParameterJdbcTemplate wareHouse3NamedParameterJdbcTemplate =
        new NamedParameterJdbcTemplate(wh3DataSource());
    return wareHouse3NamedParameterJdbcTemplate;
  }
  
  @ConditionalOnProperty(prefix = "srsWh.datasource", name = "user")
  @Bean(name = "srsWhDataSource")
  @ConfigurationProperties(prefix = "srsWh.datasource")
  public ComboPooledDataSource srsWhDataSource() {
    ComboPooledDataSource comboPooledDataSource = new ComboPooledDataSource();
    return comboPooledDataSource;
  }
  
  @ConditionalOnBean(name = "srsWhDataSource")
  @Bean(name = "srsWareHouseJdbcTemplate")
  public JdbcTemplate srsWareHouseJdbcTemplate() {
    JdbcTemplate wareHouse2JdbcTemplate = new JdbcTemplate(srsWhDataSource(), true);
    return wareHouse2JdbcTemplate;
  }
  
  @ConditionalOnBean(name = "srsWhDataSource")
  @Bean(name = "srsWareHouseNamedParameterJdbcTemplate")
  public NamedParameterJdbcTemplate srsWareHouseNamedParameterJdbcTemplate() {
    NamedParameterJdbcTemplate srsWareHouseNamedParameterJdbcTemplate =
        new NamedParameterJdbcTemplate(srsWhDataSource());
    return srsWareHouseNamedParameterJdbcTemplate;
  }
  
  @ConditionalOnProperty(prefix = "amnetWh.datasource", name = "user")
  @Bean(name = "amnetWhDataSource")
  @ConfigurationProperties(prefix = "amnetWh.datasource")
  public ComboPooledDataSource amnetWhDataSource() {
    ComboPooledDataSource comboPooledDataSource = new ComboPooledDataSource();
    return comboPooledDataSource;
  }
  
  @ConditionalOnBean(name = "amnetWhDataSource")
  @Bean(name = "amnetWareHouseJdbcTemplate")
  public JdbcTemplate amnetWhWareHouseJdbcTemplate() {
    JdbcTemplate wareHouse2JdbcTemplate = new JdbcTemplate(amnetWhDataSource(), true);
    return wareHouse2JdbcTemplate;
  }
  
  @ConditionalOnBean(name = "amnetWhDataSource")
  @Bean(name = "amnetWareHouseNamedParameterJdbcTemplate")
  public NamedParameterJdbcTemplate amnetWareHouseNamedParameterJdbcTemplate() {
    NamedParameterJdbcTemplate amnetWareHouseNamedParameterJdbcTemplate =
        new NamedParameterJdbcTemplate(amnetWhDataSource());
    return amnetWareHouseNamedParameterJdbcTemplate;
  }
}
