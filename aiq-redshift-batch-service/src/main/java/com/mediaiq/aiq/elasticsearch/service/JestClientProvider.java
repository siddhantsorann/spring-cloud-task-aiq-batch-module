package com.mediaiq.aiq.elasticsearch.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.searchbox.client.JestClient;
import io.searchbox.client.JestClientFactory;
import io.searchbox.client.config.HttpClientConfig;

@Configuration
public class JestClientProvider {
  @Value("${elasticsearch.endpoint}")
  private String url;
  @Bean
  public JestClient getJestClient() {
    JestClientFactory factory = null;
    factory = new JestClientFactory();
    factory.setHttpClientConfig(new HttpClientConfig.Builder(url).multiThreaded(true).build());
    JestClient jestClient = factory.getObject();
    return jestClient;
  }
}
