package com.mediaiq.aiq.elasticsearch.service;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.mediaiq.aiq.redshift.batch.domian.elasticsearch.CityElasticsearch;
import com.mediaiq.aiq.redshift.batch.domian.elasticsearch.PostcodeElasticsearch;
import com.mediaiq.aiq.redshift.batch.domian.elasticsearch.RegionElasticsearch;

import io.searchbox.client.JestClient;
import io.searchbox.core.Bulk;
import io.searchbox.core.Index;

@Service
public class ElasticsearchServiceImpl implements ElasticsearchService {
  
  @Autowired
  private JestClient jestClient;
  @Value("${elasticsearch.index}")
  private String esIndex;
  @Value("${elasticsearch.type}")
  private String esType;
  @Value("${elasticsearch.type.region}")
  private String esRegionTable;
  @Value("${elasticsearch.type.postcode}")
  private String esPostcodeTable;
  @Value("${elasticsearch.type.citycode}")
  private String esCityTable;
  
  static final Logger logger = LoggerFactory.getLogger(ElasticsearchServiceImpl.class);
  
  @Override
  public void createIndexRegion(List<RegionElasticsearch> regionElasticsearchList)
      throws IOException {
    List<Index> newIndexBuiderList = regionElasticsearchList.stream().map(item -> {
      Index index = new Index.Builder(item).id(item.getRegionCode().toString()).build();
      return index;
    }).collect(Collectors.toList());
    indexToElasticDb(newIndexBuiderList, esRegionTable);
  }
  
  @Override
  public void createIndexCity(List<CityElasticsearch> cityElasticsearchList) throws IOException {
    List<Index> newIndexBuiderList = cityElasticsearchList.stream().map(item -> {
      Index index = new Index.Builder(item).id(item.getCityCode().toString()).build();
      return index;
    }).collect(Collectors.toList());
    indexToElasticDb(newIndexBuiderList, esCityTable);
  }
  
  @Override
  public void createIndexPostcode(List<PostcodeElasticsearch> postcodeElasticsearchList)
      throws IOException {
    List<Index> newIndexBuiderList = postcodeElasticsearchList.stream().map(item -> {
      Index index = new Index.Builder(item).id(item.getPostCodeId().toString()).build();
      return index;
    }).collect(Collectors.toList());
    indexToElasticDb(newIndexBuiderList, esPostcodeTable);
  }
  
  private void indexToElasticDb(List<Index> newIndexBuiderList, String tableName) throws IOException {
    try {
      Bulk bulk = new Bulk.Builder().defaultIndex(esIndex).defaultType(tableName)
          .addAction(newIndexBuiderList).build();
      jestClient.execute(bulk);
    } catch (IOException e) {
      logger.error("ERROR--->{}", e.getMessage());
      throw new IOException(e);
    }
  }
}
