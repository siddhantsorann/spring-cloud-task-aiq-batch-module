package com.mediaiq.aiq.elasticsearch.service;

import java.io.IOException;
import java.util.List;

import org.springframework.stereotype.Service;

import com.mediaiq.aiq.redshift.batch.domian.elasticsearch.CityElasticsearch;
import com.mediaiq.aiq.redshift.batch.domian.elasticsearch.PostcodeElasticsearch;
import com.mediaiq.aiq.redshift.batch.domian.elasticsearch.RegionElasticsearch;


@Service
public interface ElasticsearchService {

  void createIndexRegion(List<RegionElasticsearch> regionElasticsearch) throws IOException;

  void createIndexCity(List<CityElasticsearch> cityElasticsearch) throws IOException;

  void createIndexPostcode(List<PostcodeElasticsearch> postcodeElasticsearch) throws IOException;
  
  
}
