
UNLOAD ('select
	campaign_id,
	creative_id,
	pixel_id,
	pixel_name,
	sum( pcconversions + pvconversions ) as conversions,
	date(
		dateadd(
			hour,
			cast(
				user_tz_offset as integer
			),
			cast(
				dt as timestamp
			)
		)
	) as date
from
	public.report_attributed_conversions
where
	date(
		dateadd(
			hour,
			cast(
				user_tz_offset as integer
			),
			cast(
				dt as timestamp
			)
		)
	) between \':startDate\' and \':endDate\' 
group by
	campaign_id,
	creative_id,
	pixel_id,
	pixel_name,
	date(
		dateadd(
			hour,
			cast(
				user_tz_offset as integer
			),
			cast(
				dt as timestamp
			)
		)
	)
')
to ':s3KeyPrefix'
iam_role ':iam'
ALLOWOVERWRITE    
ESCAPE  
PARALLEL OFF; 