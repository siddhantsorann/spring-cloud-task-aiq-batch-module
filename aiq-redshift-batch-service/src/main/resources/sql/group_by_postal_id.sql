UNLOAD ('select postal_code,
			post_code_id,
			region_name,
			region_code,
			city_code,
			city,
			country_code,
			country_name,
			two_letter_country
			from public.ip_additional_pulse
			where two_letter_country in (:country_codes) and postal_code!=\'?\' and post_code_id!=\'-1\' and region_name!=\'?\' and city!=\'?\'and  country_name!=\'?\'
			group by postal_code,
			post_code_id,
			region_name,
			region_code,
			city_code,
			city,
			country_code,
			country_name,
			two_letter_country;
		')
to ':s3KeyPrefix'
iam_role ':iam'
ALLOWOVERWRITE    
ESCAPE  
PARALLEL OFF;
