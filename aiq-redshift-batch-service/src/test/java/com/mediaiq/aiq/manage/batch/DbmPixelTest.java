package com.mediaiq.aiq.manage.batch;

import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class DbmPixelTest extends BaseTest {
  
  @Autowired
  @Qualifier("dbmPixelPushToRedshiftJob")
  Job dbmPixelPushToRedshiftJob;
  
  @Before
  public void setup() {
    super.setup();
    jobLauncherTestUtils.setJob(dbmPixelPushToRedshiftJob);
  }
  
  @Test
  public void testDbmPixelQUery() throws Exception {
    
    // JobExecution jobExecution = jobLauncherTestUtils.launchJob(jobParameters);
    JobExecution jobExecution = jobLauncherTestUtils.launchStep("downloadDbmPixelAsCsvStep");
    Assert.assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());
    
    // Test the step if the file is copied correctly to S3.
    JobExecution jobExecutionCopyS3 = jobLauncherTestUtils.launchStep("copyDbmPixelToS3Step");
    Assert.assertEquals(ExitStatus.COMPLETED, jobExecutionCopyS3.getExitStatus());
    
  }
  
  @Test
  public void testDbmPixelJob() throws Exception {
    
    JobParameters jobParameters =
        new JobParametersBuilder().addDate("date", new Date()).toJobParameters();
    
    JobExecution jobExecution = jobLauncherTestUtils.launchJob(jobParameters);
    Assert.assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());
    
  }
  
}

