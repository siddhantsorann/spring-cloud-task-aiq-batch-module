package com.mediaiq.aiq.manage.batch;

import java.util.Calendar;
import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * Created by prasannachidire on 19-Jun-2017
 */
public class PixelStatsLoadJobTest extends BaseTest {
  
  @Autowired
  @Qualifier("pixelStatsLoadJob")
  Job pixelStatsLoadJob;
  
  @Before
  public void setup() {
    super.setup();
    jobLauncherTestUtils.setJob(pixelStatsLoadJob);
  }
  
  @Test
  public void testInsertionOrderAuditPushJob() throws Exception {
    
    // Running the pixel stats job for yesterday
    
    JobParameters jobParameters = new JobParametersBuilder().addDate("start_date", yesterday())
        .addDate("end_date", yesterday()).toJobParameters();
    
    JobExecution jobExecution = jobLauncherTestUtils.launchJob(jobParameters);
    Assert.assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());
    
  }
  
  private Date yesterday() {
    final Calendar cal = Calendar.getInstance();
    cal.add(Calendar.DATE, -1);
    return cal.getTime();
  }
}
