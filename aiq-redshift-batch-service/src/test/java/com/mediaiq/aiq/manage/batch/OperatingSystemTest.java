package com.mediaiq.aiq.manage.batch;

import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class OperatingSystemTest extends BaseTest {
  
  @Autowired
  @Qualifier("operatingSystemPushToRedshiftJob")
  Job operatingSystemPushToRedshiftJob;
  
  @Before
  public void setup() {
    super.setup();
    jobLauncherTestUtils.setJob(operatingSystemPushToRedshiftJob);
  }
  
  @Test
  public void testOperatingSystemQUery() throws Exception {
    
    // JobExecution jobExecution = jobLauncherTestUtils.launchJob(jobParameters);
    JobExecution jobExecution = jobLauncherTestUtils.launchStep("downloadoperatingSystemAsCsvStep");
    Assert.assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());
    
    // Test the step if the file is copied correctly to S3.
    JobExecution jobExecutionCopyS3 =
        jobLauncherTestUtils.launchStep("copyOperatingSystemToS3Step");
    Assert.assertEquals(ExitStatus.COMPLETED, jobExecutionCopyS3.getExitStatus());
    
    // Test the step if the file is copied correctly from S3 to redshift.
    JobExecution jobExecutionCopyRedshift =
        jobLauncherTestUtils.launchStep("writeOperatingSystemFromS3ToRedshiftStep");
    Assert.assertEquals(ExitStatus.COMPLETED, jobExecutionCopyRedshift.getExitStatus());
    
  }
  
  @Test
  public void testOperatingSystemJob() throws Exception {
    
    JobParameters jobParameters =
        new JobParametersBuilder().addDate("date", new Date()).toJobParameters();
    
    JobExecution jobExecution = jobLauncherTestUtils.launchJob(jobParameters);
    Assert.assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());
    
  }
  
}

