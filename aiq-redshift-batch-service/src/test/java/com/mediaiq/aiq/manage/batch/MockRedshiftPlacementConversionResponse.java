package com.mediaiq.aiq.manage.batch;

import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.springframework.context.annotation.Configuration;

import com.mediaiq.aiq.plan.redshift.response.RedshiftPlacementConversionResponse;

@Configuration
public class MockRedshiftPlacementConversionResponse {
  
  private List<RedshiftPlacementConversionResponse> responseList;
  
  public List<RedshiftPlacementConversionResponse> getByDate(Date date) {
    responseList = new LinkedList<RedshiftPlacementConversionResponse>();
    
    add(date, 1L, 100L, "PC_CONV", 10L, new BigDecimal(100), new BigDecimal(0));
    add(date, 1L, 100L, "PV_CONV", 20L, new BigDecimal(0), new BigDecimal(30));
    /*
     * date campaignId PixelId PostClickConversions PostClickCost PostViewConversions PostViewCost
     * TotalConversions TotalCost 2015-01-01 1 100 10 100 20 30 30 130
     */
    
    add(date, 1L, 101L, "PC_CONV", 10L, new BigDecimal(100), new BigDecimal(0));
    add(date, 1L, 101L, "PV_CONV", 10L, new BigDecimal(0), new BigDecimal(50));
    
    /*
     * date campaignId PixelId PostClickConversions PostClickCost PostViewConversions PostViewCost
     * TotalConversions TotalCost 2015-01-01 1 101 10 100 10 50 20 150
     */
    return responseList;
  }
  
  private void add(Date date, Long placementId, Long pixelId, String eventType, Long conversions,
      BigDecimal postClickRevenue, BigDecimal postViewRevenue) {
    RedshiftPlacementConversionResponse responseObj = new RedshiftPlacementConversionResponse();
    responseObj.setImpDate(date);
    responseObj.setCampaignId(placementId);
    responseObj.setPixelId(pixelId);
    responseObj.setEventType(eventType);
    responseObj.setConversions(conversions);
    responseObj.setPostClickRevenue(postClickRevenue);
    responseObj.setPostViewRevenue(postViewRevenue);
    responseList.add(responseObj);
  }
}
