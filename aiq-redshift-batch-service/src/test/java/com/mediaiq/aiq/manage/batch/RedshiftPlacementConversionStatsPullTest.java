package com.mediaiq.aiq.manage.batch;

import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class RedshiftPlacementConversionStatsPullTest extends BaseTest {
  
  @Autowired
  @Qualifier("placementConversionStatsLoadJob")
  Job placementConversionStatsLoadJob;
  
  @Before
  public void setup() {
    super.setup();
    jobLauncherTestUtils.setJob(placementConversionStatsLoadJob);
  }
  
  @Test
  public void testCampaignLookupJob() throws Exception {
    
    JobParameters jobParameters =
        new JobParametersBuilder().addDate("date", new Date()).toJobParameters();
    
    // JobExecution jobExecution = jobLauncherTestUtils.launchJob(jobParameters);
    JobExecution jobExecution = jobLauncherTestUtils.launchJob(jobParameters);
    Assert.assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());
    
  }
  
}
