package com.mediaiq.aiq.mediamath.batch.loadjobs.redshift;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;

import com.mediaiq.aiq.mediamath.redshift.response.MediamathImpressionStatsRedshiftResponse;
import com.mediaiq.aiq.mediamath.redshift.service.MediamathImpressionStatsService;

/**
 * @author ishan
 *
 */

public class MediamathImpressionStatsServiceTest extends AbstractRedshiftTest {
  
  @Autowired
  MediamathImpressionStatsService mediamathStatsService;
  
  @Test
  public void test()
      throws DataAccessException, IOException, InterruptedException, ExecutionException {
    Date date = new Date();
    System.out.println("Current Date:" + date);
    Future<List<MediamathImpressionStatsRedshiftResponse>> content =
        mediamathStatsService.fetchImpressionStats(date);
    System.out.println(content.get());
  }
}
