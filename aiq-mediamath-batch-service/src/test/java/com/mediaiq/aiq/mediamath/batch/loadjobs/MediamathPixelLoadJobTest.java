package com.mediaiq.aiq.mediamath.batch.loadjobs;

import org.junit.Test;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * @author ishan
 *
 */
public class MediamathPixelLoadJobTest extends BaseLoadJobTest {
  
  @Autowired
  @Qualifier("mediamathPixelLoadJob")
  Job job;
  
  @Test
  public void Test() throws JobExecutionAlreadyRunningException, JobRestartException,
      JobInstanceAlreadyCompleteException, JobParametersInvalidException {
    super.test(job);
  }
}
