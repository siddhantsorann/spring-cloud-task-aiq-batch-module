package com.mediaiq.aiq.mediamath.batch.loadjobs.IntegrationBatchTest;

import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mediaiq.aiq.mediamath.MediamathBatchServiceApplication;
import com.mediaiq.aiq.mediamath.value.StepAttribute;

/**
 * Created by piyush on 13/1/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@IntegrationTest
@SpringApplicationConfiguration(classes = {MediamathBatchServiceApplication.class})
public class DailyPlacementStatsLoadJobTest extends BaseTest {
  
  @Autowired
  @Qualifier("mediamathDailyPlacementStatsLoadJob")
  private Job dailyPlacementStatsLoadJob;
  
  
  
  @Before
  public void setup() {
    super.setup();
    jobLauncherTestUtils.setJob(dailyPlacementStatsLoadJob);
  }
  
  @Test
  public void testDailyPlacementStatsLoadJob() throws Exception {
    
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder()
        .addLong(StepAttribute.START_ELEMENT, 0L).addDate("date", new Date());
    JobExecution jobExecution =
        jobLauncherTestUtils.launchJob(jobParametersBuilder.toJobParameters());
    Assert.assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());
    System.out.println(" Test Successfully Completed ");
    
  }
  
  
}
