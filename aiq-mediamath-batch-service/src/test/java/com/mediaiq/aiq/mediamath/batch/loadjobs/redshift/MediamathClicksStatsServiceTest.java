package com.mediaiq.aiq.mediamath.batch.loadjobs.redshift;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;

import com.mediaiq.aiq.mediamath.redshift.response.MediamathClickStatsRedshiftResponse;
import com.mediaiq.aiq.mediamath.redshift.service.MediamathClickStatsService;

/**
 * @author ishan
 *
 */

public class MediamathClicksStatsServiceTest extends AbstractRedshiftTest {
  
  @Autowired
  MediamathClickStatsService mediamathClickStatsService;
  
  @Test
  public void test()
      throws DataAccessException, IOException, InterruptedException, ExecutionException {
    Date date = new Date();
    System.out.println("Current Date:" + date);
    Future<List<MediamathClickStatsRedshiftResponse>> list =
        mediamathClickStatsService.fetchClickStats(date);
    System.out.println(list.get());
  }
}
