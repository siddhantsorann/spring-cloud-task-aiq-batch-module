package com.mediaiq.aiq.mediamath.batch.loadjobs;

import java.util.Date;

import org.junit.runner.RunWith;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:mediamath-load-job-context.xml")
public class BaseLoadJobTest {
  
  @Autowired
  protected JobLauncher jobLauncher;
  
  public void test(Job job) throws JobExecutionAlreadyRunningException, JobRestartException,
      JobInstanceAlreadyCompleteException, JobParametersInvalidException {
    
    JobParameters jobParameters =
        new JobParametersBuilder().addDate("date", new Date()).toJobParameters();
    
    jobLauncher.run(job, jobParameters);
    
    System.out.println("Done");
  }
  
}
