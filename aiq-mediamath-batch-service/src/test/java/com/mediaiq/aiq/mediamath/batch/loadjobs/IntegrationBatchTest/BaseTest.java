package com.mediaiq.aiq.mediamath.batch.loadjobs.IntegrationBatchTest;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mediaiq.aiq.mediamath.MediamathBatchServiceApplication;

/**
 * Created by piyush on 13/1/16.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {MediamathBatchServiceApplication.class})
public class BaseTest {
  
  JobLauncherTestUtils jobLauncherTestUtils;
  
  @Autowired
  private JobLauncher jobLauncher;
  
  @Autowired
  private JobRepository jobRepository;
  
  @Before
  public void setup() {
    jobLauncherTestUtils = new JobLauncherTestUtils();
    jobLauncherTestUtils.setJobLauncher(jobLauncher);
    jobLauncherTestUtils.setJobRepository(jobRepository);
  }
}
