package com.mediaiq.aiq.mediamath.batch.loadjobs.redshift;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author ishan
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:mediamath-load-job-context.xml")
public class AbstractRedshiftTest {
  
}
