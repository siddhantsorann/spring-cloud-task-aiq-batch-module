package com.mediaiq.aiq.mediamath.batch.loadjobs.redshift;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Future;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;

import com.mediaiq.aiq.mediamath.redshift.response.MediamathPixelConversionStatsRedshiftResponse;
import com.mediaiq.aiq.mediamath.redshift.service.MediamathPixelConversionStatsService;

/**
 * @author ishan
 *
 */

public class MediamathPixelConversionStatsServiceTest extends AbstractRedshiftTest {
  
  @Autowired
  MediamathPixelConversionStatsService mediamathConversionStatsService;
  
  @Test
  public void test() throws DataAccessException, IOException, InterruptedException, Exception {
    Date date = new Date();
    Future<List<MediamathPixelConversionStatsRedshiftResponse>> list =
        mediamathConversionStatsService.fetchConversionStats(date);
    System.out.println(list.get());
  }
}
