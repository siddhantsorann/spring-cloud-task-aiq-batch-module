package com.mediaiq.aiq.mediamath.batch.loadjobs.redshift;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Future;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;

import com.mediaiq.aiq.mediamath.redshift.response.MediamathConversionStatsRedshiftResponse;
import com.mediaiq.aiq.mediamath.redshift.service.MediamathConversionStatsService;

/**
 * @author ishan
 *
 */

public class MediamathConvStatsServiceTest extends AbstractRedshiftTest {
  
  @Autowired
  MediamathConversionStatsService mediamathConversionStatsService;
  
  @Test
  public void test() throws DataAccessException, IOException, InterruptedException, Exception {
    Date date = new Date();
    Future<List<MediamathConversionStatsRedshiftResponse>> list =
        mediamathConversionStatsService.fetchConversionStats(date);
    System.out.println(list.get());
  }
}
