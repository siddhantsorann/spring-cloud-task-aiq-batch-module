package com.mediaiq.aiq.mediamath.tasklet.redshift;

import java.util.Date;
import java.util.List;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.aiq.mediamath.domain.redshift.MediamathConversionStats;
import com.mediaiq.aiq.mediamath.processor.impl.MediamathRedshiftResponseConversionsConverter;
import com.mediaiq.aiq.mediamath.redshift.response.MediamathConversionStatsRedshiftResponse;
import com.mediaiq.aiq.mediamath.redshift.service.MediamathConversionStatsService;
import com.mediaiq.aiq.mediamath.repo.redshift.MediamathConversionStatsRepo;

/**
 * @author ishan
 *
 */

@Component
@Scope("step")
public class MediamathConversionsStatsLoadTasklet implements Tasklet {
  
  private static final Logger logger =
      LoggerFactory.getLogger(MediamathConversionsStatsLoadTasklet.class);
  
  @Autowired
  MediamathConversionStatsService mediamathConversionStatsService;
  
  @Autowired
  MediamathRedshiftResponseConversionsConverter mediamathResponseConverter;
  
  @Autowired
  MediamathConversionStatsRepo mediamathConversionStatsRepo;
  
  @Override
  public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext)
      throws Exception {
    Future<List<MediamathConversionStatsRedshiftResponse>> conversions =
        mediamathConversionStatsService.fetchConversionStats(new Date());
    List<MediamathConversionStatsRedshiftResponse> conversionsList = conversions.get();
    logger.info("NUmber of conversions fetched:" + conversionsList.size());
    
    List<MediamathConversionStats> convertedList =
        mediamathResponseConverter.convertRedshiftResponse(conversionsList);
    logger.info("Converted List of Conversion Stats: " + convertedList);
    
    mediamathConversionStatsRepo.save(convertedList);
    
    return RepeatStatus.FINISHED;
  }
}
