package com.mediaiq.aiq.mediamath.redshift.impl;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.stereotype.Service;

import com.mediaiq.aiq.mediamath.redshift.service.MediamathStatsService;
import com.mediaiq.aiq.plan.redshift.request.RedshiftMediamathPlacementStatsRequest;

/**
 * @author ishan
 *
 */

@Service
public abstract class MediamathStatsServiceImpl<R> extends MediamathRedshiftServiceImpl<R>
    implements MediamathStatsService<R> {
  
  private final static Logger LOGGER = LoggerFactory.getLogger(MediamathStatsServiceImpl.class);
  
  SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
  
  @Override
  public Future<List<R>> getByDate(Date date) throws DataAccessException, IOException {
    
    LOGGER.info("Sending Request to redshift to fetch mediamath stats");
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    cal.add(Calendar.DATE, -4);
    
    String currentDate = getDateFormat().format(date);
    String olderDate = getDateFormat().format(cal.getTime());
    
    LOGGER.info("StartDate:" + currentDate + ", OlderDate:" + olderDate
        + ", sending request to Redshift, " + " sqlFile:" + getSqlFile());
    return query(getSqlFile(),
        new BeanPropertySqlParameterSource(
            new RedshiftMediamathPlacementStatsRequest(currentDate, olderDate)),
        new BeanPropertyRowMapper<R>(getResponseType()));
  }
  
  public SimpleDateFormat getDateFormat() {
    return dateFormat;
  }
  
  public void setDateFormat(SimpleDateFormat dateFormat) {
    this.dateFormat = dateFormat;
  }
  
  protected abstract String getSqlFile();
}
