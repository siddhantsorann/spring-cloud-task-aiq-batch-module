package com.mediaiq.aiq.mediamath.chunk.mapper;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindException;

import com.mediaiq.aiq.domain.Dsp;
import com.mediaiqdigital.aiq.tradingcenter.domain.DailyInsertionOrderStats;
import com.mediaiqdigital.aiq.tradingcenter.domain.DailyInsertionOrderStatsId;
import com.mediaiqdigital.aiq.tradingcenter.repo.CampaignIoAssociationRepo;

/**
 * Created by piyush on 15/12/15.
 */
public class MediamathDailyInsertionOrderStatsMapper
    implements FieldSetMapper<DailyInsertionOrderStats> {
  
  final static Logger logger =
      LoggerFactory.getLogger(MediamathDailyInsertionOrderStatsMapper.class);
  
  // @Autowired
  // DailyIoPlacementAssociationRepo dailyIoPlacementAssociationRepo;
  
  @Autowired
  CampaignIoAssociationRepo campaignIoAssociationRepo;
  
  @Override
  public DailyInsertionOrderStats mapFieldSet(FieldSet set) throws BindException {
    DailyInsertionOrderStats stats = new DailyInsertionOrderStats();
    Long miqCampaignId = 0L;
    Date start_date = set.readDate("start_date", "yyyy-MM-dd");
    set.readDate("end_date", "yyyy-MM-dd");
    Long insertionOrderId = set.readLong("campaign_id");
    set.readString("campaign_timezone");
    stats.setDailyInsertionOrderStatsId(
        new DailyInsertionOrderStatsId(insertionOrderId, start_date, Dsp.MediaMath));
    stats.setClicks(set.readLong("clicks"));
    stats.setImpressions(set.readLong("impressions"));
    // stats.setMediaCost(set.readDouble("total_spend"));
    // stats.setTotalSpend(set.readInt("total_spend"));
    // stats.setEndDate(end_date);
    // stats.setCampaign_timezone(timezone);
    try {
      
      // fetching the campaign id from campaignIoAssociation table instead of
      // dailyIoPlacementAssociation table
      
      // miqCampaignId =
      // dailyIoPlacementAssociationRepo.findByIdIoId(insertionOrderId).getId().getCampaignId();
      miqCampaignId = campaignIoAssociationRepo
          .findByIdIoIdAndIdDsp(insertionOrderId, Dsp.MediaMath).get(0).getId().getCampaignId();
    } catch (IndexOutOfBoundsException e) {
      logger.info("Miq Campaign Id Not Found");
    }
    if (miqCampaignId != 0L) {
      stats.setCampaignId(miqCampaignId);
    }
    return stats;
  }
}
