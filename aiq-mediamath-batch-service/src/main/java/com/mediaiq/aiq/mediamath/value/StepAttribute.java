package com.mediaiq.aiq.mediamath.value;

/**
 * Created by piyush on 15/12/15.
 */
public interface StepAttribute {
  
  String PAGE_OFFSET = "page_offset";
  String FILE_PATH = "file_path";
  String START_ELEMENT = "start_element";
  String PAGE_SIZE = "page_limit";
  String REPORT_ID = "report_id";
  String REPORT_NAME = "name";
  String TOTAL_ELEMENTS = "total_elements";
}
