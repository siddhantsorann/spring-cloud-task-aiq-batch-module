package com.mediaiq.aiq.mediamath;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import com.mediaiq.aiq.mediamath.service.impl.MediamathBatchServiceImpl;

/**
 * Created by piyush on 14/10/15.
 */
@Component
@ApplicationPath("/api")
public class MediamathBatchJerseyConfig extends ResourceConfig {
  public MediamathBatchJerseyConfig() {
    
    register(MediamathBatchServiceImpl.class);
    
  }
  
}
