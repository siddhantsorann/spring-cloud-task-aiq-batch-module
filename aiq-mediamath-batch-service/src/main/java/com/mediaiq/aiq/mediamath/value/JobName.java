package com.mediaiq.aiq.mediamath.value;

/**
 * @author ishan
 *
 */
public enum JobName {
  mediamathAdvertiserLoadJob, mediamathAgencyLoadJob, mediamathCampaignLoadJob, mediamathStrategyLoadJob, mediamathClicksStatsLoadJob, mediamathImpressionsStatsLoadJob, mediamathConversionsStatsLoadJob, mediamathPixelLoadJob, mediamathPixelConversionsStatsLoadJob, mediamathCreativeStatsLoadJob, mediamathDailyPlacementStatsLoadJob, mediamathDailyInsertionOrderStatsLoadJob, mediamathDailyCampaignPlacementStatsLoadJob, mediamathPlacementLoadJob, mediamathInsertionOrderLoadJob;
}
