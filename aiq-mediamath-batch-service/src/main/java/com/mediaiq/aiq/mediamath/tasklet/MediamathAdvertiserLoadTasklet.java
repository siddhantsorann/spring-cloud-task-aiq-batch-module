package com.mediaiq.aiq.mediamath.tasklet;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.aiq.mediamath.domain.MediamathAdvertiser;
import com.mediaiq.aiq.mediamath.repo.MediamathAdvertiserRepo;
import com.mediaiq.aiq.mediamath.service.client.MediamathRestClient;
import com.mediaiq.aiq.mediamath.service.response.response.MediamathAdvertiserResponse;

@Component
@Scope("step")
public class MediamathAdvertiserLoadTasklet implements Tasklet {
  
  private static final Logger logger =
      LoggerFactory.getLogger(MediamathAdvertiserLoadTasklet.class);
  
  @Autowired
  private MediamathRestClient mediamathClient;
  
  @Autowired
  private MediamathAdvertiserRepo mediamathAdvertiserRepo;
  
  public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext)
      throws Exception {
    
    MediamathAdvertiserResponse advertiserResponse = mediamathClient.getAdvertiserList();
    List<MediamathAdvertiser> advertiserList = advertiserResponse.getEntities().getEntityList();
    logger.info("Loaded Advertiserlist for the Mediamath:" + advertiserList + "@count:"
        + advertiserList.size());
    mediamathAdvertiserRepo.save(advertiserList);
    return RepeatStatus.FINISHED;
  }
}
