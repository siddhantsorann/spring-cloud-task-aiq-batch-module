package com.mediaiq.aiq.mediamath.processor.impl;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.mediaiq.aiq.mediamath.domain.redshift.MediamathPixelConversionStats;
import com.mediaiq.aiq.mediamath.domain.redshift.id.MediamathPixelStatsId;
import com.mediaiq.aiq.mediamath.processor.MediamathRedshiftResponseProcessor;
import com.mediaiq.aiq.mediamath.redshift.response.MediamathPixelConversionStatsRedshiftResponse;


/**
 * @author ishan
 *
 */

@Component
public class MediamathRedshiftResponsePixelConversionsConverter implements
    MediamathRedshiftResponseProcessor<MediamathPixelConversionStats, MediamathPixelConversionStatsRedshiftResponse> {
  
  @Override
  public List<MediamathPixelConversionStats> convertRedshiftResponse(
      List<MediamathPixelConversionStatsRedshiftResponse> redshiftResponseList) {
    
    Map<MediamathPixelStatsId, MediamathPixelConversionStats> convertedMap = new HashMap<>();
    
    for (MediamathPixelConversionStatsRedshiftResponse response : redshiftResponseList) {
      
      MediamathPixelStatsId id = new MediamathPixelStatsId();
      id.setDate(response.getDate());
      id.setPlacementId(response.getPlacementId());
      id.setPixelId(response.getPixelId());
      
      MediamathPixelConversionStats conversionStat = null;
      if (convertedMap.containsKey(id)) {
        conversionStat = convertedMap.get(id);
      } else {
        conversionStat = new MediamathPixelConversionStats();
        conversionStat.setId(id);
        convertedMap.put(id, conversionStat);
      }
      
      if (response.getEventType().equalsIgnoreCase("C")) {
        conversionStat.setPostClickConversions(response.getConversions());
      } else if (response.getEventType().equalsIgnoreCase("V")) {
        conversionStat.setPostViewConversions(response.getConversions());
      }
    }
    return new LinkedList<MediamathPixelConversionStats>(convertedMap.values());
  }
  
}
