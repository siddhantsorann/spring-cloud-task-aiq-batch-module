package com.mediaiq.aiq.mediamath.processor.impl;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.mediaiq.aiq.domain.Cost;
import com.mediaiq.aiq.mediamath.domain.redshift.MediamathImpressionStats;
import com.mediaiq.aiq.mediamath.domain.redshift.id.MediamathStatsId;
import com.mediaiq.aiq.mediamath.processor.MediamathRedshiftResponseProcessor;
import com.mediaiq.aiq.mediamath.redshift.response.MediamathImpressionStatsRedshiftResponse;

/**
 * @author ishan
 *
 */

@Component
public class MediamathRedShiftResponseImpressionsStatsConverter implements
    MediamathRedshiftResponseProcessor<MediamathImpressionStats, MediamathImpressionStatsRedshiftResponse> {
  
  @Override
  public List<MediamathImpressionStats> convertRedshiftResponse(
      List<MediamathImpressionStatsRedshiftResponse> redshiftResponseList) {
    
    List<MediamathImpressionStats> convertedList = new LinkedList<MediamathImpressionStats>();
    
    for (MediamathImpressionStatsRedshiftResponse response : redshiftResponseList) {
      
      MediamathImpressionStats impressionStat = new MediamathImpressionStats();
      MediamathStatsId id = new MediamathStatsId();
      
      // set Id.
      id.setDate(response.getDate());
      id.setPlacementId(response.getPlacementId());
      impressionStat.setId(id);
      
      // set Impressions
      impressionStat.setImpressions(response.getImpressions());
      
      // set Cost
      Cost cost = new Cost();
      cost.setValue(BigDecimal.valueOf(response.getMediaCost()));
      impressionStat.setMediaCost(cost);
      
      convertedList.add(impressionStat);
    }
    return convertedList;
  }
}
