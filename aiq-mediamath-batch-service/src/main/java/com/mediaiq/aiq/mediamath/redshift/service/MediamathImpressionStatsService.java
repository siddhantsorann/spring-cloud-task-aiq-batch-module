package com.mediaiq.aiq.mediamath.redshift.service;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Future;

import org.springframework.dao.DataAccessException;

import com.mediaiq.aiq.mediamath.redshift.response.MediamathImpressionStatsRedshiftResponse;

/**
 * @author ishan
 *
 */

public interface MediamathImpressionStatsService
    extends MediamathStatsService<MediamathImpressionStatsRedshiftResponse> {
  
  Future<List<MediamathImpressionStatsRedshiftResponse>> fetchImpressionStats(Date date)
      throws DataAccessException, IOException;
}
