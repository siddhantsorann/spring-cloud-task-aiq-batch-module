package com.mediaiq.aiq.mediamath.tasklet.Conversion;

import java.util.Set;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.aiq.mediamath.domain.MediamathPlacement;
import com.mediaiq.aiq.mediamath.repo.MediamathPlacementRepo;

/**
 * Created by piyush on 7/4/16.
 */
@Component
@Scope("step")
public class PlacementTimeZoneConversionTasklet implements Tasklet {
  
  
  @Autowired
  MediamathPlacementRepo mediamathPlacementRepo;
  
  @Override
  public RepeatStatus execute(StepContribution stepContribution, ChunkContext context) {
    
    Set<MediamathPlacement> mediamathPlacements = mediamathPlacementRepo.findByTimezone("Etc/GMT");
    for (MediamathPlacement mediamathPlacement : mediamathPlacements) {
      mediamathPlacement.setTimezone("UTC");
      mediamathPlacementRepo.save(mediamathPlacement);
    }
    return RepeatStatus.FINISHED;
  }
}
