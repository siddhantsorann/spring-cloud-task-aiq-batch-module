package com.mediaiq.aiq.mediamath;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.cloud.security.oauth2.resource.EnableOAuth2Resource;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableScheduling
@EnableIntegration
@ComponentScan(basePackages = {"com.mediaiq.aiq.mediamath", "com.mediaiq.aiq.mediamath.firebase",
    "com.mediaiq.spring.batch"})
@ImportResource({"spring-integration-config.xml", "spring-batch-config.xml",
    "classpath:mediamath-load-jobs.xml", "classpath:redshift-warehouse2-config.xml",
    "classpath:redshift-warehouse3-config.xml"})
@EntityScan(basePackages = {"com.mediaiqdigital.aiq.tradingcenter.domain",
    "com.mediaiqdigital.doubleclick.domain", "com.mediaiq.aiq.mediamath.domain",
    "com.mediaiq.aiq.miq.domain", "com.mediaiq.appnexus.domain",
    "com.mediaiqdigital.doubleclick.domain"})


@EnableJpaRepositories(
    basePackages = {"com.mediaiqdigital.aiq.tradingcenter.repo", "com.mediaiq.aiq.mediamath.repo"})
@EnableTransactionManagement
@EnableOAuth2Resource
public class MediamathBatchServiceApplication {
  public static void main(String[] args) {
    
    new SpringApplicationBuilder(MediamathBatchServiceApplication.class).web(true).run(args);
  }
  
}
