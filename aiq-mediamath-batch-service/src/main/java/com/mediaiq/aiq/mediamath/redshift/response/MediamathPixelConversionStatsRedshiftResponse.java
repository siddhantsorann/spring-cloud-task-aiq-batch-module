package com.mediaiq.aiq.mediamath.redshift.response;

/**
 * @author ishan
 *
 */

public class MediamathPixelConversionStatsRedshiftResponse
    extends MediamathConversionStatsRedshiftResponse {
  
  private Long pixelId;
  
  public Long getPixelId() {
    return pixelId;
  }
  
  public void setPixelId(Long pixelId) {
    this.pixelId = pixelId;
  }
  
  @Override
  public String toString() {
    return "MediamathPixelConversionStatsRedshiftResponse [pixelId=" + pixelId + ", placementId="
        + placementId + ", date=" + date + ", conversions=" + conversions + ", eventType="
        + eventType + "]";
  }
  
}
