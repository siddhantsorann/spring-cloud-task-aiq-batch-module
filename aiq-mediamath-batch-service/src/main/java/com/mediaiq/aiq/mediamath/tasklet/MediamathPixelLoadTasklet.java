package com.mediaiq.aiq.mediamath.tasklet;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.aiq.mediamath.domain.MediamathPixel;
import com.mediaiq.aiq.mediamath.repo.MediamathPixelRepo;
import com.mediaiq.aiq.mediamath.service.client.MediamathRestClient;
import com.mediaiq.aiq.mediamath.service.response.response.MediamathPixelResponse;

/**
 * @author ishan
 *
 */
@Component
@Scope("step")
public class MediamathPixelLoadTasklet implements Tasklet {
  
  private static final Logger logger = LoggerFactory.getLogger(MediamathPixelLoadTasklet.class);
  
  @Autowired
  private MediamathRestClient mediamathClient;
  
  @Autowired
  private MediamathPixelRepo mediamathPixelRepo;
  
  public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext)
      throws Exception {
    
    MediamathPixelResponse pixelResponse = mediamathClient.getPixels();
    List<MediamathPixel> pixelList = pixelResponse.getEntities().getEntityList();
    logger.info("Loaded Pixellist for the Mediamath:" + pixelList + "@count:" + pixelList.size());
    mediamathPixelRepo.save(pixelList);
    return RepeatStatus.FINISHED;
  }
}
