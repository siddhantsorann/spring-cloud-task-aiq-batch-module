package com.mediaiq.aiq.mediamath.processor.impl;

import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.mediaiq.aiq.mediamath.domain.redshift.MediamathClickStats;
import com.mediaiq.aiq.mediamath.domain.redshift.id.MediamathStatsId;
import com.mediaiq.aiq.mediamath.processor.MediamathRedshiftResponseProcessor;
import com.mediaiq.aiq.mediamath.redshift.response.MediamathClickStatsRedshiftResponse;

/**
 * @author ishan
 *
 */

@Component
public class MediamathRedshiftResponseClicksConverter implements
    MediamathRedshiftResponseProcessor<MediamathClickStats, MediamathClickStatsRedshiftResponse> {
  
  @Override
  public List<MediamathClickStats> convertRedshiftResponse(
      List<MediamathClickStatsRedshiftResponse> redshiftResponseList) {
    
    List<MediamathClickStats> convertedList = new LinkedList<>();
    for (MediamathClickStatsRedshiftResponse response : redshiftResponseList) {
      
      MediamathClickStats clickStats = new MediamathClickStats();
      
      MediamathStatsId id = new MediamathStatsId();
      id.setDate(response.getDate());
      id.setPlacementId(response.getPlacementId());
      clickStats.setId(id);
      
      clickStats.setClicks(response.getClicks());
      
      convertedList.add(clickStats);
    }
    return convertedList;
  }
}
