package com.mediaiq.aiq.mediamath.tasklet.redshift;

import java.util.Date;
import java.util.List;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.aiq.mediamath.domain.redshift.MediamathClickStats;
import com.mediaiq.aiq.mediamath.processor.impl.MediamathRedshiftResponseClicksConverter;
import com.mediaiq.aiq.mediamath.redshift.response.MediamathClickStatsRedshiftResponse;
import com.mediaiq.aiq.mediamath.redshift.service.MediamathClickStatsService;
import com.mediaiq.aiq.mediamath.repo.redshift.MediamathClickStatsRepo;

/**
 * @author ishan
 *
 */
@Component
@Scope("step")
public class MediamathClicksStatsLoadTasklet implements Tasklet {
  
  private static final Logger logger =
      LoggerFactory.getLogger(MediamathClicksStatsLoadTasklet.class);
  
  @Autowired
  private MediamathClickStatsService mediamathClickStatsService;
  
  @Autowired
  MediamathRedshiftResponseClicksConverter mediamathRedshiftResponseConverter;
  
  @Autowired
  MediamathClickStatsRepo mediamathClickStatsRepo;
  
  public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext)
      throws Exception {
    
    Date date = new Date();
    Future<List<MediamathClickStatsRedshiftResponse>> list =
        mediamathClickStatsService.fetchClickStats(date);
    List<MediamathClickStatsRedshiftResponse> clickList = list.get();
    logger.info("Number of Clicks fetched: " + clickList.size());
    
    List<MediamathClickStats> convertedList =
        mediamathRedshiftResponseConverter.convertRedshiftResponse(clickList);
    logger.info("Converted Click List:" + convertedList);
    
    mediamathClickStatsRepo.save(convertedList);
    
    return RepeatStatus.FINISHED;
  }
  
}

