package com.mediaiq.aiq.mediamath.tasklet.Conversion;

import java.util.Set;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.aiq.mediamath.domain.MediamathInsertionOrder;
import com.mediaiq.aiq.mediamath.repo.MediamathInsertionOrderRepo;

/**
 * Created by piyush on 7/4/16.
 */
@Component
@Scope("step")
public class InsertionOrderTimeZoneConversionTasklet implements Tasklet {
  
  
  @Autowired
  MediamathInsertionOrderRepo mediamathInsertionOrderRepo;
  
  @Override
  public RepeatStatus execute(StepContribution stepContribution, ChunkContext context) {
    
    Set<MediamathInsertionOrder> mediamathInsertionOrders =
        mediamathInsertionOrderRepo.findByTimezone("Etc/GMT");
    for (MediamathInsertionOrder mediamathInsertionOrder : mediamathInsertionOrders) {
      mediamathInsertionOrder.setTimezone("UTC");
      mediamathInsertionOrderRepo.save(mediamathInsertionOrder);
    }
    
    return RepeatStatus.FINISHED;
  }
}
