package com.mediaiq.aiq.mediamath.redshift.response;

import java.util.Date;

/**
 * @author ishan
 *
 */

public class MediamathImpressionStatsRedshiftResponse {
  
  private Long placementId;
  private Date date;
  private Long impressions;
  private Double mediaCost;
  
  public Long getPlacementId() {
    return placementId;
  }
  
  public void setPlacementId(Long placementId) {
    this.placementId = placementId;
  }
  
  public Date getDate() {
    return date;
  }
  
  public void setDate(Date date) {
    this.date = date;
  }
  
  public Long getImpressions() {
    return impressions;
  }
  
  public void setImpressions(Long impressions) {
    this.impressions = impressions;
  }
  
  public Double getMediaCost() {
    return mediaCost;
  }
  
  public void setMediaCost(Double mediaCost) {
    this.mediaCost = mediaCost;
  }
  
  @Override
  public String toString() {
    return "MediamathImpressionStatsRedshiftResponse [placementId=" + placementId + ", date=" + date
        + ", impressions=" + impressions + ", mediaCost=" + mediaCost + "]";
  }
  
}
