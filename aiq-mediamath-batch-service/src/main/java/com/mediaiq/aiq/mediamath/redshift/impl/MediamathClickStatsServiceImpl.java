package com.mediaiq.aiq.mediamath.redshift.impl;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Future;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.mediaiq.aiq.mediamath.redshift.response.MediamathClickStatsRedshiftResponse;
import com.mediaiq.aiq.mediamath.redshift.service.MediamathClickStatsService;

/**
 * @author ishan
 *
 */

@Service
public class MediamathClickStatsServiceImpl
    extends MediamathStatsServiceImpl<MediamathClickStatsRedshiftResponse>
    implements MediamathClickStatsService {
  
  private final String SQL_CLICK_STATS = "/sql/mediamath_placement_clicks_by_date.sql";
  
  @Override
  public Future<List<MediamathClickStatsRedshiftResponse>> fetchClickStats(Date date)
      throws DataAccessException, IOException {
    return super.getByDate(date);
  }
  
  @Override
  protected String getSqlFile() {
    return SQL_CLICK_STATS;
  }
  
  @Override
  protected Class<MediamathClickStatsRedshiftResponse> getResponseType() {
    return MediamathClickStatsRedshiftResponse.class;
  }
  
}
