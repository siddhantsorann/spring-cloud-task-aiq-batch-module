package com.mediaiq.aiq.mediamath.batch.configuration;

import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.JsonMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties
public class RabbitConfig {
  
  @Value("${rabbitmq.host}")
  private String host;
  
  @Value("${rabbitmq.username}")
  private String username;
  
  @Value("${rabbitmq.password}")
  private String password;
  
  protected final String dspPlacementStatsUpdateEventQueue = "dspPlacementStatsUpdateEvent.queue";
  
  @Bean
  public ConnectionFactory connectionFactory() {
    CachingConnectionFactory connectionFactory = new CachingConnectionFactory(host);
    connectionFactory.setUsername(username);
    connectionFactory.setPassword(password);
    return connectionFactory;
  }
  
  @Bean
  public AmqpAdmin amqpAdmin() {
    return new RabbitAdmin(connectionFactory());
  }
  
  @Bean
  public RabbitTemplate rabbitTemplate() {
    RabbitTemplate template = new RabbitTemplate(connectionFactory());
    template.setMessageConverter(new JsonMessageConverter());
    return template;
  }
  
  @Bean(name = "dspPlacementStatsUpdateEventQueue")
  public Queue dspPlacementStatsUpdateEventQueue() {
    return new Queue(this.dspPlacementStatsUpdateEventQueue);
  }
}
