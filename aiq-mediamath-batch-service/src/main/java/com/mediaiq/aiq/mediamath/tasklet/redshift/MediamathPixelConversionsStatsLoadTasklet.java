package com.mediaiq.aiq.mediamath.tasklet.redshift;

import java.util.Date;
import java.util.List;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.aiq.mediamath.domain.redshift.MediamathPixelConversionStats;
import com.mediaiq.aiq.mediamath.processor.impl.MediamathRedshiftResponsePixelConversionsConverter;
import com.mediaiq.aiq.mediamath.redshift.response.MediamathPixelConversionStatsRedshiftResponse;
import com.mediaiq.aiq.mediamath.redshift.service.MediamathPixelConversionStatsService;
import com.mediaiq.aiq.mediamath.repo.redshift.MediamathPixelConversionStatsRepo;

/**
 * @author ishan
 *
 */

@Component
@Scope("step")
public class MediamathPixelConversionsStatsLoadTasklet implements Tasklet {
  
  private static final Logger logger =
      LoggerFactory.getLogger(MediamathPixelConversionsStatsLoadTasklet.class);
  
  @Autowired
  MediamathPixelConversionStatsService mediamathPixelConversionStatsService;
  
  @Autowired
  MediamathRedshiftResponsePixelConversionsConverter mediamathResponseConverter;
  
  @Autowired
  MediamathPixelConversionStatsRepo mediamathPixelConversionStatsRepo;
  
  @Override
  public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext)
      throws Exception {
    
    Future<List<MediamathPixelConversionStatsRedshiftResponse>> conversions =
        mediamathPixelConversionStatsService.fetchConversionStats(new Date());
    List<MediamathPixelConversionStatsRedshiftResponse> conversionsList = conversions.get();
    logger.info("Number of conversions fetched:" + conversionsList.size());
    
    List<MediamathPixelConversionStats> convertedList =
        mediamathResponseConverter.convertRedshiftResponse(conversionsList);
    logger.info("Converted List of Conversion Stats: " + convertedList);
    
    mediamathPixelConversionStatsRepo.save(convertedList);
    
    return RepeatStatus.FINISHED;
  }
}
