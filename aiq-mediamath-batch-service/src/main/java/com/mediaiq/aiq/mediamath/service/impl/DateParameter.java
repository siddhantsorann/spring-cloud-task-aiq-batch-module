package com.mediaiq.aiq.mediamath.service.impl;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by piyush on 10/2/16.
 */
public class DateParameter implements Serializable {
  
  private static final long serialVersionUID = 1L;
  private static final String DATE_FORMAT = "yyyy-MM-dd";
  private static DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
  
  private Date date;
  
  public DateParameter(String inputDate) throws ParseException {
    date = dateFormat.parse(inputDate);
  }
  
  public Date getDate() {
    return date;
  }
  
  public void setDate(Date date) {
    this.date = date;
  }
  
  @Override
  public String toString() {
    return "DateParameter [date=" + date + "]";
  }
}
