package com.mediaiq.aiq.mediamath.redshift.service;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Future;

import org.springframework.dao.DataAccessException;

import com.mediaiq.aiq.mediamath.redshift.base.MediamathRedshiftService;

/**
 * @author ishan
 *
 */

public interface MediamathStatsService<R> extends MediamathRedshiftService<R> {
  
  public Future<List<R>> getByDate(Date date) throws DataAccessException, IOException;
}
