package com.mediaiq.aiq.mediamath.tasklet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.aiq.mediamath.domain.MediamathCreative;
import com.mediaiq.aiq.mediamath.repo.MediamathCreativeRepo;
import com.mediaiq.aiq.mediamath.service.client.MediamathJSONRestClient;
import com.mediaiq.aiq.mediamath.service.response.response.ResponseContainer;

@Component
@Scope("step")
public class MediamathCreativeLoadTasklet
    extends MediamathAbstractPageLoadTasklet<MediamathCreative, MediamathCreativeRepo> {
  
  @Autowired
  private MediamathCreativeRepo mediamathCreativeRepo;
  
  @Override
  public ResponseContainer<MediamathCreative> getPageableResponse(MediamathJSONRestClient client) {
    return client.getJSONCreatives(pageSize, pageOffset);
  }
  
  @Override
  public MediamathCreativeRepo getRepository() {
    return mediamathCreativeRepo;
  }
}
