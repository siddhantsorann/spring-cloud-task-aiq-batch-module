package com.mediaiq.aiq.mediamath.service;

import java.util.Date;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

/**
 * @author ishan
 */
@Path("/load")
public interface MediamathBatchService {
  
  @POST
  @Path("/advertiser")
  public void loadAdvertiserLoadJob(
      @QueryParam("override") @DefaultValue("false") Boolean override);
  
  
  @POST
  @Path("/strategy_full")
  public void loadJSONStrategyLoadJob(
      @QueryParam("override") @DefaultValue("false") Boolean override);
  
  
  @POST
  @Path("/campaign_full")
  public void loadJSONCampaignLoadJob(
      @QueryParam("override") @DefaultValue("false") Boolean override);
  
  @POST
  @Path("/agency")
  public void loadAgencyLoadJob(@QueryParam("override") @DefaultValue("false") Boolean override);
  
  @POST
  @Path("/pixel")
  public void loadPixelLoadJob(@QueryParam("override") @DefaultValue("false") Boolean override);
  
  @POST
  @Path("/clicks-stats")
  public void loadMediamathClickStats();
  
  @POST
  @Path("/impression-stats")
  public void loadMediamathImpressionStats();
  
  @POST
  @Path("/conversion-stats")
  public void loadMediamathConversionStats();
  
  @POST
  @Path("/pixel-conversion-stats")
  public void loadMediamathPixelConversionStats();
  
  @POST
  @Path("/creative")
  public void loadMediamathCreativeStats(
      @QueryParam("override") @DefaultValue("false") Boolean override);
  
  void launchGenerateDspStatsChangeEventJob(Date date);
  
  @POST
  @Path("/daily-io-stats")
  public void launchMediamathDailyInsertionOrderStatsLoadJob();
  
  @POST
  @Path("/daily-placement-stats")
  public void launchMediamathDailyPlacementStatsLoadJob(@QueryParam("duration") String duration);
  
  @POST
  @Path("/daily-campaign-placement-stats")
  public void launchMediamathDailyCampaignPlacementStatsLoadJob();
  
  
}
