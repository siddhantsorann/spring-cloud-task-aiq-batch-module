package com.mediaiq.aiq.mediamath.redshift.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.mediaiq.aiq.mediamath.redshift.base.MediamathRedshiftService;
import com.mediaiq.aiq.plan.redshift.service.impl.AbstractRedshiftServiceImpl;

/**
 * @author ishan
 *
 */

public abstract class MediamathRedshiftServiceImpl<R> extends AbstractRedshiftServiceImpl<R>
    implements MediamathRedshiftService<R> {
  
  @Autowired
  private NamedParameterJdbcTemplate wareHouse2NamedParameterJdbcTemplate;
  
  @Override
  public NamedParameterJdbcTemplate getJdbcTemplate() {
    return wareHouse2NamedParameterJdbcTemplate;
  }
  
}
