package com.mediaiq.aiq.mediamath.redshift.response;

import java.util.Date;

/**
 * @author ishan
 *
 */

public class MediamathConversionStatsRedshiftResponse {
  
  protected Long placementId;
  protected Date date;
  protected Long conversions;
  protected String eventType;
  
  public Long getPlacementId() {
    return placementId;
  }
  
  public void setPlacementId(Long placementId) {
    this.placementId = placementId;
  }
  
  public Date getDate() {
    return date;
  }
  
  public void setDate(Date date) {
    this.date = date;
  }
  
  public Long getConversions() {
    return conversions;
  }
  
  public void setConversions(Long conversions) {
    this.conversions = conversions;
  }
  
  public String getEventType() {
    return eventType;
  }
  
  public void setEventType(String eventType) {
    this.eventType = eventType;
  }
  
  @Override
  public String toString() {
    return "MediamathConversionStatsRedshiftResponse [placementId=" + placementId + ", date=" + date
        + ", conversions=" + conversions + ", eventType=" + eventType + "]";
  }
}
