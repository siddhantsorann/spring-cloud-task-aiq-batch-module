package com.mediaiq.aiq.mediamath.chunk.mapper;

import java.util.Date;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import com.mediaiq.aiq.mediamath.domain.id.PlacementStatsId;
import com.mediaiq.aiq.mediamath.domain.stats.MediamathDailyPlacementCreativeStats;

/**
 * Created by piyush on 8/2/16.
 */
public class MediamathDailyPlacementCreativeStatsMapper
    implements FieldSetMapper<MediamathDailyPlacementCreativeStats> {
  
  @Override
  public MediamathDailyPlacementCreativeStats mapFieldSet(FieldSet set) throws BindException {
    
    com.mediaiq.aiq.mediamath.domain.stats.MediamathDailyPlacementCreativeStats stats =
        new MediamathDailyPlacementCreativeStats();
    Date start_date = set.readDate("start_date", "yyyy-MM-dd");
    Long placementId = set.readLong("strategy_id");
    Long creativeId = set.readLong("creative_id");
    String timezone = set.readString("campaign_timezone");
    if (timezone.equalsIgnoreCase("Etc/GMT"))
      timezone = "UTC";
    stats.setPlacementStatsId(new PlacementStatsId(start_date, placementId, creativeId));
    stats.setClicks(set.readLong("clicks"));
    stats.setInsertionOrderId(set.readLong("campaign_id"));
    stats.setImpressions(set.readLong("impressions"));
    stats.setTotalSpend(set.readDouble("total_spend"));
    stats.setPostClickConversions(set.readLong("post_click_conversions"));
    stats.setPostViewConversions(set.readLong("post_view_conversions"));
    stats.setTimezone(timezone);
    stats.setCampaignCurrencyCode(set.readString("campaign_currency_code"));
    
    return stats;
  }
}


