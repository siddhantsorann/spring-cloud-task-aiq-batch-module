package com.mediaiq.aiq.mediamath.processor;

import java.util.List;

/**
 * @author ishan
 *
 */

public interface MediamathRedshiftResponseProcessor<U, V> {
  
  public List<U> convertRedshiftResponse(List<V> redshiftResponseList);
  
}
