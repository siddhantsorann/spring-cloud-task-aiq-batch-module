package com.mediaiq.aiq.mediamath.tasklet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.aiq.mediamath.domain.MediamathPlacement;
import com.mediaiq.aiq.mediamath.repo.MediamathPlacementRepo;
import com.mediaiq.aiq.mediamath.service.client.MediamathJSONRestClient;
import com.mediaiq.aiq.mediamath.service.response.response.ResponseContainer;

/**
 * Created by piyush on 19/2/16.
 */
@Component
@Scope("step")
public class MediamathPlacementLoadTasklet
    extends MediamathAbstractPageLoadTasklet<MediamathPlacement, MediamathPlacementRepo> {
  
  @Autowired
  private MediamathPlacementRepo mediamathJSONPlacementRepo;
  
  @Override
  public ResponseContainer<MediamathPlacement> getPageableResponse(MediamathJSONRestClient client) {
    return client.getJSONStrategy(pageSize, pageOffset);
  }
  
  @Override
  public MediamathPlacementRepo getRepository() {
    return mediamathJSONPlacementRepo;
  }
  
}
