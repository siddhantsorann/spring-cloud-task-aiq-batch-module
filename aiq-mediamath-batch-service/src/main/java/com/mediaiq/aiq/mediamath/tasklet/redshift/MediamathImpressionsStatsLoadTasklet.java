package com.mediaiq.aiq.mediamath.tasklet.redshift;

import java.util.Date;
import java.util.List;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.aiq.mediamath.domain.redshift.MediamathImpressionStats;
import com.mediaiq.aiq.mediamath.processor.impl.MediamathRedShiftResponseImpressionsStatsConverter;
import com.mediaiq.aiq.mediamath.redshift.response.MediamathImpressionStatsRedshiftResponse;
import com.mediaiq.aiq.mediamath.redshift.service.MediamathImpressionStatsService;
import com.mediaiq.aiq.mediamath.repo.redshift.MediamathImpressionStatsRepo;

/**
 * @author ishan
 *
 */
@Component
@Scope("step")
public class MediamathImpressionsStatsLoadTasklet implements Tasklet {
  
  private static final Logger logger =
      LoggerFactory.getLogger(MediamathImpressionsStatsLoadTasklet.class);
  
  @Autowired
  MediamathImpressionStatsService mediamathImpressionStatsService;
  
  @Autowired
  MediamathRedShiftResponseImpressionsStatsConverter mediamathRedshiftResponseProcessor;
  
  @Autowired
  MediamathImpressionStatsRepo mediamathImpressionStatsRepo;
  
  @Override
  public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext)
      throws Exception {
    Future<List<MediamathImpressionStatsRedshiftResponse>> impressionList =
        mediamathImpressionStatsService.fetchImpressionStats(new Date());
    List<MediamathImpressionStatsRedshiftResponse> impressionsList = impressionList.get();
    logger.info("NUmber of impressions fetched:" + impressionsList.size());
    
    // convert it to the proper format to be saved in manage db
    List<MediamathImpressionStats> convertedList =
        mediamathRedshiftResponseProcessor.convertRedshiftResponse(impressionsList);
    
    logger.info("Converted Impression List:" + convertedList);
    
    mediamathImpressionStatsRepo.save(convertedList);
    
    return RepeatStatus.FINISHED;
  }
}
