package com.mediaiq.aiq.mediamath.tasklet.report;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.aiq.mediamath.service.client.MediamathReportRestClient;
import com.mediaiq.aiq.mediamath.value.StepAttribute;

/**
 * Created by piyush on 8/2/16.
 */

@Component
@Scope("step")
public class MediamathPlacementCreativeReportDownloadTasklet implements Tasklet {
  
  
  final static Logger logger =
      LoggerFactory.getLogger(MediamathPlacementCreativeReportDownloadTasklet.class);
  
  @Value("${report.directory}")
  private String reportDir;
  
  @Value("#{jobParameters['duration']}")
  private String duration;
  
  @Autowired
  private MediamathReportRestClient mediamathRestClient;
  
  @Override
  public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext)
      throws Exception {
    Long startTime = System.currentTimeMillis();
    String fileTarget =
        reportDir + System.getProperty("file.separator") + "mediamathDailyPlacementStats" + ".csv";
    
    System.out.println(" filetarget " + fileTarget);
    InputStream inputStream = mediamathRestClient.getReport("by_day", duration,
        "strategy_id,campaign_id,campaign_timezone,campaign_currency_code,creative_id",
        "clicks,impressions,post_view_conversions,post_click_conversions,total_spend",
        "organization_id=100208").getBody().in();
    
    OutputStream outputStream = new FileOutputStream(fileTarget);
    IOUtils.copy(inputStream, outputStream);
    Long endTime = System.currentTimeMillis();
    ExecutionContext executionContext =
        chunkContext.getStepContext().getStepExecution().getJobExecution().getExecutionContext();
    executionContext.put(StepAttribute.FILE_PATH, fileTarget);
    
    logger.debug("Time taken::" + (endTime - startTime));
    System.out.println(" time taken " + (endTime - startTime));
    
    logger.info("Report {} downloaded.", "report 1");
    
    return RepeatStatus.FINISHED;
  }
}
