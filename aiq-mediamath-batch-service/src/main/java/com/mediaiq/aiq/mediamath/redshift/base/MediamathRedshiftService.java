package com.mediaiq.aiq.mediamath.redshift.base;

import com.mediaiq.aiq.plan.redshift.service.base.AbstractRedshiftService;

/**
 * @author ishan
 *
 */

public interface MediamathRedshiftService<R> extends AbstractRedshiftService<R> {
  
}
