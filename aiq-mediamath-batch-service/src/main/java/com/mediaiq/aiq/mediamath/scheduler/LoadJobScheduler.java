package com.mediaiq.aiq.mediamath.scheduler;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

import com.mediaiq.aiq.mediamath.service.MediamathBatchService;

/**
 * @author ishan
 */
@Configuration
public class LoadJobScheduler {
  
  private static final Logger logger = LoggerFactory.getLogger(LoadJobScheduler.class);
  
  @Autowired
  private MediamathBatchService mediamathBatchService;
  
  @Scheduled(cron = "0 0 1 ? * *")
  private void executeAdvertiserLoadJob() {
    logger.info("Scheduled load job: Executing MediamathAdvertiserLoadJob");
    mediamathBatchService.loadAdvertiserLoadJob(true);
  }
  
  
  @Scheduled(cron = "0 20 1 ? * *")
  private void executeCampaignJsonLoadJob() {
    logger.info("Scheduled load job: Executing MediamathCampaignLoadJob");
    mediamathBatchService.loadJSONCampaignLoadJob(true);
  }
  
  
  @Scheduled(cron = "0 40 1 ? * *")
  private void executeStrategyJsonLoadJob() {
    logger.info("Scheduled load job: Executing MediamathStrategyLoadJob");
    mediamathBatchService.loadJSONStrategyLoadJob(true);
  }
  
  
  @Scheduled(cron = "0 50 1 ? * *")
  private void executeAgencyLoadJob() {
    logger.info("Scheduled load job: Executing MediamathAgencyLoadJob");
    mediamathBatchService.loadAgencyLoadJob(true);
  }
  
  @Scheduled(cron = "0 0 2 ? * *")
  private void executeClickStatsLoadJob() {
    logger.info("Scheduled load job: Executing MediamathClickStatsLoadJob");
    mediamathBatchService.loadMediamathClickStats();
  }
  
  @Scheduled(cron = "0 10 2 ? * *")
  private void executeImpressionStatsLoadJob() {
    logger.info("Scheduled load job: Executing MediamathImpressionStatsLoadJob");
    mediamathBatchService.loadMediamathImpressionStats();
  }
  
  @Scheduled(cron = "0 20 2 * * ?")
  private void executePixelsLoadJob() {
    logger.info("Scheduled load job: Executing MediamathPixelStatsLoadJob");
    mediamathBatchService.loadPixelLoadJob(true);
  }
  
  @Scheduled(cron = "0 30 2 ? * *")
  private void executeConversionsLoadJob() {
    logger.info("Scheduled load job: Executing MediamathConversionStatsLoadJob");
    mediamathBatchService.loadMediamathConversionStats();
  }
  
  @Scheduled(cron = "0 40 2 ? * *")
  private void executePixelConversionsLoadJob() {
    logger.info("Scheduled load job: Executing MediamathConversionStatsLoadJob");
    mediamathBatchService.loadMediamathPixelConversionStats();
  }
  
  @Scheduled(cron = "0 50 2 * * ?")
  private void executeGenerateDspStatsChangeEventJob() {
    logger.info("Scheduled job: Executing GenerateDspStatsChangeEventJob");
    mediamathBatchService.launchGenerateDspStatsChangeEventJob(getOlderDate(1));
    
    mediamathBatchService.launchGenerateDspStatsChangeEventJob(getOlderDate(2));
  }
  
  @Scheduled(cron = "0 20 3/6 * * *")
  private void executeDailyPlacementStatsEventJob() throws ParseException {
    logger.info("Scheduled jon : Executing DailyPlacementStatsEventJob ");
    mediamathBatchService.launchMediamathDailyPlacementStatsLoadJob("yesterday");
  }
  
  private Date getOlderDate(int days) {
    Calendar cal = Calendar.getInstance();
    cal.add(Calendar.DATE, -days);
    cal.add(Calendar.HOUR_OF_DAY, 0);
    Date date = cal.getTime();
    return date;
  }
}
