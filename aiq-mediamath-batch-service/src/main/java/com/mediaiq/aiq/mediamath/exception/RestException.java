package com.mediaiq.aiq.mediamath.exception;


import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class RestException extends WebApplicationException {
  
  private static final long serialVersionUID = 1L;
  
  public RestException(Status status) {
    super(Response.status(status.getCode()).entity(status.getMessage()).build());
  }
  
  public enum Status {
    
    FAILED_TO_LAUNCH_JOB(460, "Error in executing the Mediamath Load Job");
    
    private int code;
    private String message;
    
    private Status(int code, String message) {
      this.code = code;
      this.message = message;
    }
    
    public int getCode() {
      return code;
    }
    
    public String getMessage() {
      return message;
    }
  }
  
}
