package com.mediaiq.aiq.mediamath.firebase;


import com.mediaiq.spring.batch.domain.JobStateDto;

import retrofit.http.Body;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;

public interface FirebaseClient {
  
  @PUT("/job-state/{jobName}.json")
  JobStateDto addJobState(@Path("jobName") String campaignId, @Body JobStateDto jobStateDto,
      @Query("auth") String secret);
}
