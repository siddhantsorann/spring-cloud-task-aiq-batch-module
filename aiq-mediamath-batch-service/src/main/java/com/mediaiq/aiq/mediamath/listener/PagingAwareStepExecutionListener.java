package com.mediaiq.aiq.mediamath.listener;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.stereotype.Component;

import com.mediaiq.aiq.mediamath.value.StepAttribute;

@Component
public class PagingAwareStepExecutionListener implements StepExecutionListener {
  
  @Override
  public void beforeStep(StepExecution stepExecution) {
    // check if step execution context has startElement and numElement,
    // if no populate them
    ExecutionContext context = stepExecution.getExecutionContext();
    
    // populate from job execution context
    JobExecution jobExecution = stepExecution.getJobExecution();
    ExecutionContext jobExecutionContext = jobExecution.getExecutionContext();
    
    Integer startElement = null;
    if (jobExecutionContext.containsKey(StepAttribute.PAGE_OFFSET))
      startElement = jobExecutionContext.getInt(StepAttribute.PAGE_OFFSET);
    
    // else populate from job parameters
    if (startElement == null) {
      JobParameters jobParameters = jobExecution.getJobParameters();
      startElement = Integer.parseInt(jobParameters.getLong(StepAttribute.PAGE_OFFSET).toString());
    }
    
    // else initialize to 0
    if (startElement == null)
      startElement = 1; // mediamath is 1 indexed
      
    context.putInt(StepAttribute.PAGE_OFFSET, startElement);
  }
  
  @Override
  public ExitStatus afterStep(StepExecution stepExecution) {
    // check startElement, numElement and count, and return relevant status
    ExecutionContext context = stepExecution.getExecutionContext();
    int offset = context.getInt(StepAttribute.PAGE_OFFSET);
    int count = context.getInt(StepAttribute.PAGE_SIZE);
    int numElements = context.getInt(StepAttribute.TOTAL_ELEMENTS);
    int remainingElements = numElements - offset;
    
    if (remainingElements <= 0) {
      System.out.println("Task finished.");
      return new ExitStatus("END");
    }
    
    // update execution context
    // context.putInt(StepAttribute.START_ELEMENT, startElement +
    // numElements);
    stepExecution.getJobExecution().getExecutionContext().putInt(StepAttribute.PAGE_OFFSET,
        offset + count);
    
    return new ExitStatus("CONTINUE (Items Remaining: " + remainingElements + ")");
  }
}
