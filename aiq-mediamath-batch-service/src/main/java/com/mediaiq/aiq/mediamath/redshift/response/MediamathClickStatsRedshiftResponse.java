package com.mediaiq.aiq.mediamath.redshift.response;

import java.util.Date;

/**
 * @author ishan
 *
 */

public class MediamathClickStatsRedshiftResponse {
  
  private Long placementId;
  private Date date;
  private Long clicks;
  
  public Long getPlacementId() {
    return placementId;
  }
  
  public void setPlacementId(Long placementId) {
    this.placementId = placementId;
  }
  
  public Date getDate() {
    return date;
  }
  
  public void setDate(Date date) {
    this.date = date;
  }
  
  public Long getClicks() {
    return clicks;
  }
  
  public void setClicks(Long clicks) {
    this.clicks = clicks;
  }
  
  @Override
  public String toString() {
    return "MediamathClickStatsRedshiftResponse [placementId=" + placementId + ", date=" + date
        + ", clicks=" + clicks + "]";
  }
}
