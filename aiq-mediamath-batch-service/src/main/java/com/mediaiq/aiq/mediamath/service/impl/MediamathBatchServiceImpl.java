package com.mediaiq.aiq.mediamath.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Queue;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.mediaiq.aiq.mediamath.domain.redshift.raw.MediamathStats;
import com.mediaiq.aiq.mediamath.repo.MediamathStatsRepo;
import com.mediaiq.aiq.mediamath.service.MediamathBatchService;
import com.mediaiq.aiq.mediamath.value.JobName;
import com.mediaiq.spring.batch.integration.component.JobLaunchGateway;
import com.mediaiq.spring.batch.service.impl.AbstractJobLaunchServiceImpl;

/**
 * @author ishan
 */

@Service
public class MediamathBatchServiceImpl extends AbstractJobLaunchServiceImpl
    implements MediamathBatchService {
  
  private static final Logger logger = LoggerFactory.getLogger(MediamathBatchServiceImpl.class);
  
  @Autowired
  JobLaunchGateway jobLaunchGateway;
  
  @Autowired
  private MediamathStatsRepo mediamathStatsRepo;
  
  @Autowired
  AmqpTemplate rabbitTemplate;
  
  @Autowired
  @Qualifier(value = "dspPlacementStatsUpdateEventQueue")
  Queue dspPlacementStatsUpdateEventQueue;
  
  public void loadAdvertiserLoadJob(Boolean override) {
    logger.info("Starting load job of mediamath advertiser");
    
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.mediamathAdvertiserLoadJob.name(), jobParametersBuilder.toJobParameters());
  }
  
  // public void loadStrategyLoadJob(Boolean override) {
  // logger.info("Starting load job of mediamath strategy");
  // JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
  // jobParametersBuilder.addDate("date",new Date());
  // launchJob(JobName.mediamathStrategyLoadJob.name(),jobParametersBuilder.toJobParameters());
  // }
  
  public void loadJSONStrategyLoadJob(Boolean override) {
    logger.info("Starting load job of mediamath strategy");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.mediamathPlacementLoadJob.name(), jobParametersBuilder.toJobParameters());
  }
  
  // public void loadCampaignLoadJob(Boolean override) {
  // logger.info("Starting load job of mediamath campaign");
  // JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
  // jobParametersBuilder.addDate("date",new Date());
  // launchJob(JobName.mediamathCampaignLoadJob.name(),jobParametersBuilder.toJobParameters());
  // }
  
  public void loadJSONCampaignLoadJob(Boolean override) {
    logger.info("Starting load job of mediamath full campaign");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.mediamathInsertionOrderLoadJob.name(),
        jobParametersBuilder.toJobParameters());
  }
  
  public void loadAgencyLoadJob(Boolean override) {
    logger.info("Starting load job of mediamath agency");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.mediamathAgencyLoadJob.name(), jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public void loadMediamathClickStats() {
    logger.info("Starting load job of fetching click stats");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.mediamathClicksStatsLoadJob.name(), jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public void loadMediamathImpressionStats() {
    logger.info("Starting load job of fetching Impressions stats");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.mediamathImpressionsStatsLoadJob.name(),
        jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public void loadPixelLoadJob(Boolean override) {
    logger.info("Starting load job of fetching Conversion stats");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.mediamathPixelLoadJob.name(), jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public void loadMediamathConversionStats() {
    logger.info("Starting load job of fetching Conversion stats");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.mediamathConversionsStatsLoadJob.name(),
        jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public void loadMediamathPixelConversionStats() {
    logger.info("Starting load job of Pixel Conversion stats");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.mediamathPixelConversionsStatsLoadJob.name(),
        jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public void loadMediamathCreativeStats(Boolean override) {
    
    logger.info("Starting load job of Creative stats");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.mediamathCreativeStatsLoadJob.name(), jobParametersBuilder.toJobParameters());
    
  }
  
  
  @Override
  public void launchGenerateDspStatsChangeEventJob(Date date) {
    
    List<MediamathStats> stats = mediamathStatsRepo.findByIdDate(date);
    List<Long> placementIds = new ArrayList<Long>();
    
    for (MediamathStats stat : stats) {
      placementIds.add(stat.getId().getPlacementId());
    }
  }
  
  @Override
  public void launchMediamathDailyPlacementStatsLoadJob(String duration) {
    
    logger.info("In Method: launchMediamathDailyPlacementStatsLoadJob");
    
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    jobParametersBuilder.addString("duration", duration);
    
    
    launchJob(JobName.mediamathDailyPlacementStatsLoadJob.name(),
        jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public void launchMediamathDailyInsertionOrderStatsLoadJob() {
    
    logger.info("In Method: launchMediamathDailyPlacementStatsLoadJob");
    
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    launchJob(JobName.mediamathDailyInsertionOrderStatsLoadJob.name(),
        jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public void launchMediamathDailyCampaignPlacementStatsLoadJob() {
    
    logger.info("In Method: launchMediamathDailyCampaignPlacementStatsLoadJob");
    
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    
    launchJob(JobName.mediamathDailyCampaignPlacementStatsLoadJob.name(),
        jobParametersBuilder.toJobParameters());
  }
  
}
