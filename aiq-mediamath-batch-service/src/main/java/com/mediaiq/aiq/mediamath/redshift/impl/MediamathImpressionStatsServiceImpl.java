package com.mediaiq.aiq.mediamath.redshift.impl;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Future;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.mediaiq.aiq.mediamath.redshift.response.MediamathImpressionStatsRedshiftResponse;
import com.mediaiq.aiq.mediamath.redshift.service.MediamathImpressionStatsService;

/**
 * @author ishan
 *
 */

@Service
public class MediamathImpressionStatsServiceImpl
    extends MediamathStatsServiceImpl<MediamathImpressionStatsRedshiftResponse>
    implements MediamathImpressionStatsService {
  
  private final String SQL_Impression_Stats = "/sql/mediamath_placement_impression_by_date.sql";
  
  @Override
  public Future<List<MediamathImpressionStatsRedshiftResponse>> fetchImpressionStats(Date date)
      throws DataAccessException, IOException {
    return super.getByDate(date);
  }
  
  @Override
  protected Class<MediamathImpressionStatsRedshiftResponse> getResponseType() {
    return MediamathImpressionStatsRedshiftResponse.class;
  }
  
  @Override
  protected String getSqlFile() {
    return SQL_Impression_Stats;
  }
  
}
