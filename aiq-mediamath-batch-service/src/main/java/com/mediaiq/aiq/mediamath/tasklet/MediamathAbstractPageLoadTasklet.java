package com.mediaiq.aiq.mediamath.tasklet;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;

import com.mediaiq.aiq.mediamath.service.client.MediamathJSONRestClient;
import com.mediaiq.aiq.mediamath.service.response.response.Meta;
import com.mediaiq.aiq.mediamath.service.response.response.ResponseContainer;
import com.mediaiq.aiq.mediamath.value.StepAttribute;

@Scope("step")
public abstract class MediamathAbstractPageLoadTasklet<TYPE, REPO extends JpaRepository<TYPE, ?>>
    implements Tasklet {
  
  private final Logger logger = LoggerFactory.getLogger(this.getClass());
  
  @Autowired
  private MediamathJSONRestClient mediamathJSONRestClient;
  
  
  @Value("#{stepExecutionContext['" + StepAttribute.PAGE_OFFSET + "']}")
  protected Integer pageOffset;
  
  @Value("#{stepExecutionContext['" + StepAttribute.PAGE_SIZE + "']}")
  protected Integer pageSize;
  
  @Override
  public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext)
      throws Exception {
    
    ResponseContainer<TYPE> responseContainer = getPageableResponse(mediamathJSONRestClient);
    List<TYPE> items = responseContainer.getData();
    logger.debug("Response: " + items);
    // fetch items from response
    logger.debug("Items: " + items);
    // save items
    save(items);
    // update execution context
    // increase pageoffset and set to context
    // pageOffset = pageOffset + pageSize;
    // update execution context
    Meta meta = responseContainer.getMeta();
    ExecutionContext executionContext =
        chunkContext.getStepContext().getStepExecution().getExecutionContext();
    executionContext.put(StepAttribute.PAGE_OFFSET, meta.getOffset());
    executionContext.put(StepAttribute.PAGE_SIZE, meta.getCount());
    executionContext.put(StepAttribute.TOTAL_ELEMENTS, meta.getTotal_count());
    return RepeatStatus.FINISHED;
  }
  
  
  public abstract ResponseContainer<TYPE> getPageableResponse(
      MediamathJSONRestClient appnexusClient);
  
  public abstract REPO getRepository();
  
  protected void save(List<TYPE> items) {
    getRepository().save(items);
  }
  
  @Autowired
  public void setMediamathJSONRestClient(MediamathJSONRestClient mediamathJSONRestClient) {
    this.mediamathJSONRestClient = mediamathJSONRestClient;
  }
}
