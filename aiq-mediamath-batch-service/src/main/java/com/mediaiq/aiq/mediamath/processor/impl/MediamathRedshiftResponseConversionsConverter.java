package com.mediaiq.aiq.mediamath.processor.impl;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.mediaiq.aiq.mediamath.domain.redshift.MediamathConversionStats;
import com.mediaiq.aiq.mediamath.domain.redshift.id.MediamathStatsId;
import com.mediaiq.aiq.mediamath.processor.MediamathRedshiftResponseProcessor;
import com.mediaiq.aiq.mediamath.redshift.response.MediamathConversionStatsRedshiftResponse;

/**
 * @author ishan
 *
 */

@Component
public class MediamathRedshiftResponseConversionsConverter implements
    MediamathRedshiftResponseProcessor<MediamathConversionStats, MediamathConversionStatsRedshiftResponse> {
  
  @Override
  public List<MediamathConversionStats> convertRedshiftResponse(
      List<MediamathConversionStatsRedshiftResponse> redshiftResponseList) {
    
    Map<MediamathStatsId, MediamathConversionStats> convertedMap = new HashMap<>();
    
    for (MediamathConversionStatsRedshiftResponse response : redshiftResponseList) {
      
      MediamathStatsId id = new MediamathStatsId();
      id.setDate(response.getDate());
      id.setPlacementId(response.getPlacementId());
      
      MediamathConversionStats conversionStat = null;
      if (convertedMap.containsKey(id)) {
        conversionStat = convertedMap.get(id);
      } else {
        conversionStat = new MediamathConversionStats();
        conversionStat.setId(id);
        convertedMap.put(id, conversionStat);
      }
      
      if (response.getEventType().equalsIgnoreCase("C")) {
        conversionStat.setPostClickConversions(response.getConversions());
      } else if (response.getEventType().equalsIgnoreCase("V")) {
        conversionStat.setPostViewConversions(response.getConversions());
      }
    }
    return new LinkedList<MediamathConversionStats>(convertedMap.values());
  }
  
}
