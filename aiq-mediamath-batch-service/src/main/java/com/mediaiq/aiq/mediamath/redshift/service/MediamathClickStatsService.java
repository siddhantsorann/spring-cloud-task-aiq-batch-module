package com.mediaiq.aiq.mediamath.redshift.service;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Future;

import org.springframework.dao.DataAccessException;

import com.mediaiq.aiq.mediamath.redshift.response.MediamathClickStatsRedshiftResponse;

/**
 * @author ishan
 *
 */

public interface MediamathClickStatsService
    extends MediamathStatsService<MediamathClickStatsRedshiftResponse> {
  
  Future<List<MediamathClickStatsRedshiftResponse>> fetchClickStats(Date date)
      throws DataAccessException, IOException;
}
