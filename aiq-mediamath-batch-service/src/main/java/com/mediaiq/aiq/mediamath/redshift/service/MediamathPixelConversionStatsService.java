package com.mediaiq.aiq.mediamath.redshift.service;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Future;

import org.springframework.dao.DataAccessException;

import com.mediaiq.aiq.mediamath.redshift.response.MediamathPixelConversionStatsRedshiftResponse;

/**
 * @author ishan
 *
 */

public interface MediamathPixelConversionStatsService
    extends MediamathStatsService<MediamathPixelConversionStatsRedshiftResponse> {
  
  Future<List<MediamathPixelConversionStatsRedshiftResponse>> fetchConversionStats(Date date)
      throws DataAccessException, IOException;
  
}
