package com.mediaiq.aiq.mediamath.redshift.impl;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Future;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.mediaiq.aiq.mediamath.redshift.response.MediamathPixelConversionStatsRedshiftResponse;
import com.mediaiq.aiq.mediamath.redshift.service.MediamathPixelConversionStatsService;

/**
 * @author ishan
 *
 */

@Service
public class MediamathPixelConversionStatsServiceImpl
    extends MediamathStatsServiceImpl<MediamathPixelConversionStatsRedshiftResponse>
    implements MediamathPixelConversionStatsService {
  
  private final String SQL_CONVERSION_STATS = "/sql/mediamath_placement_pixel_convs_by_date.sql";
  
  @Override
  public Future<List<MediamathPixelConversionStatsRedshiftResponse>> fetchConversionStats(Date date)
      throws DataAccessException, IOException {
    return super.getByDate(date);
  }
  
  @Override
  protected String getSqlFile() {
    return SQL_CONVERSION_STATS;
  }
  
  @Override
  protected Class<MediamathPixelConversionStatsRedshiftResponse> getResponseType() {
    return MediamathPixelConversionStatsRedshiftResponse.class;
  }
}
