package com.mediaiq.aiq.mediamath.tasklet;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.aiq.mediamath.domain.MediamathInsertionOrder;
import com.mediaiq.aiq.mediamath.repo.MediamathInsertionOrderRepo;
import com.mediaiq.aiq.mediamath.service.client.MediamathJSONRestClient;
import com.mediaiq.aiq.mediamath.service.response.response.ResponseContainer;
import com.mediaiq.aiq.mediamath.value.State;

/**
 * Created by piyush on 19/2/16.
 */
@Component
@Scope("step")
public class MediamathInsertionOrderLoadTasklet
    extends MediamathAbstractPageLoadTasklet<MediamathInsertionOrder, MediamathInsertionOrderRepo> {
  
  @Autowired
  private MediamathInsertionOrderRepo mediamathJSONInsertionOrderRepo;
  
  @Override
  public ResponseContainer<MediamathInsertionOrder> getPageableResponse(
      MediamathJSONRestClient client) {
    return client.getJSONCampaign(pageSize, pageOffset);
  }
  
  @Override
  public MediamathInsertionOrderRepo getRepository() {
    return mediamathJSONInsertionOrderRepo;
  }
  
  @Override
  protected void save(List<MediamathInsertionOrder> items) {
    for (MediamathInsertionOrder mediamathInsertionOrder : items) {
      Boolean status = mediamathInsertionOrder.isActive();
      if (status.equals(true))
        mediamathInsertionOrder.setState(State.active);
      else
        mediamathInsertionOrder.setState(State.inactive);
    }
    super.save(items);
  }
}
