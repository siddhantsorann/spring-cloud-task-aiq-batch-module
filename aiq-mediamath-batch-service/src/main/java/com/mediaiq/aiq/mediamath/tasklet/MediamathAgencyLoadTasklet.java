package com.mediaiq.aiq.mediamath.tasklet;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.aiq.mediamath.domain.MediamathAgency;
import com.mediaiq.aiq.mediamath.repo.MediamathAgencyRepo;
import com.mediaiq.aiq.mediamath.service.client.MediamathRestClient;
import com.mediaiq.aiq.mediamath.service.response.response.MediamathAgencyResponse;

@Component
@Scope("step")
public class MediamathAgencyLoadTasklet implements Tasklet {
  
  private static final Logger logger = LoggerFactory.getLogger(MediamathAgencyLoadTasklet.class);
  
  @Autowired
  private MediamathRestClient mediamathClient;
  
  @Autowired
  private MediamathAgencyRepo mediamathAgencyRepo;
  
  public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext)
      throws Exception {
    
    MediamathAgencyResponse agencyResponse = mediamathClient.getAgencyList();
    List<MediamathAgency> agencyList = agencyResponse.getEntities().getEntityList();
    logger
        .info("Loaded Agencylist for the Mediamath:" + agencyList + "@count:" + agencyList.size());
    mediamathAgencyRepo.save(agencyList);
    return RepeatStatus.FINISHED;
  }
}
