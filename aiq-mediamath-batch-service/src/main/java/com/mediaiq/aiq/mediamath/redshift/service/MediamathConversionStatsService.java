package com.mediaiq.aiq.mediamath.redshift.service;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Future;

import org.springframework.dao.DataAccessException;

import com.mediaiq.aiq.mediamath.redshift.response.MediamathConversionStatsRedshiftResponse;

/**
 * @author ishan
 *
 */

public interface MediamathConversionStatsService
    extends MediamathStatsService<MediamathConversionStatsRedshiftResponse> {
  
  Future<List<MediamathConversionStatsRedshiftResponse>> fetchConversionStats(Date date)
      throws DataAccessException, IOException;
  
}
