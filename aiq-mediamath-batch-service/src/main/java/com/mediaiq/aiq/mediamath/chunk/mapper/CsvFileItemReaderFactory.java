package com.mediaiq.aiq.mediamath.chunk.mapper;

import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

import com.mediaiq.aiq.mediamath.domain.stats.MediamathDailyPlacementCreativeStats;
import com.mediaiq.aiq.mediamath.value.StepAttribute;
import com.mediaiqdigital.aiq.tradingcenter.domain.DailyInsertionOrderStats;

/**
 * Created by piyush on 15/12/15.
 */
@Configuration
public class CsvFileItemReaderFactory {
  
  @Bean(name = "mediamathDailyPlacementStatsReader")
  @Scope("job")
  public FlatFileItemReader<MediamathDailyPlacementCreativeStats> provideT(
      @Value("#{jobExecutionContext['" + StepAttribute.FILE_PATH + "']}") String filePath) {
    String[] names = {"start_date", "end_date", "strategy_id", "campaign_id", "campaign_timezone",
        "campaign_currency_code", "creative_id", "clicks", "impressions", "post_view_conversions",
        "post_click_conversions", "total_spend"};
    
    FlatFileItemReader<MediamathDailyPlacementCreativeStats> reader = new FlatFileItemReader<>();
    DefaultLineMapper<MediamathDailyPlacementCreativeStats> mapper = new DefaultLineMapper<>();
    MediamathDailyPlacementCreativeStatsMapper fieldSetMapper =
        new MediamathDailyPlacementCreativeStatsMapper();
    commonProvider(names, reader, filePath, mapper, fieldSetMapper);
    return reader;
  }
  
  @Bean(name = "mediamathDailyInsertionOrderStatsReader")
  @Scope("step")
  public FlatFileItemReader<DailyInsertionOrderStats> provideTT(
      @Value("#{jobExecutionContext['" + StepAttribute.FILE_PATH + "']}") String filePath) {
    String[] names = {"start_date", "end_date", "campaign_id", "campaign_timezone", "clicks",
        "impressions", "total_spend"};
    
    FlatFileItemReader<DailyInsertionOrderStats> reader = new FlatFileItemReader<>();
    DefaultLineMapper<DailyInsertionOrderStats> mapper = new DefaultLineMapper<>();
    MediamathDailyInsertionOrderStatsMapper fieldSetMapper =
        new MediamathDailyInsertionOrderStatsMapper();
    commonProvider(names, reader, filePath, mapper, fieldSetMapper);
    return reader;
  }
  
  private void commonProvider(String[] names, FlatFileItemReader reader, String filePath,
      DefaultLineMapper mapper, FieldSetMapper fieldSetMapper) {
    System.out.println(filePath);
    Resource resource = new FileSystemResource(filePath);
    reader.setResource(resource);
    reader.setLinesToSkip(1);
    DelimitedLineTokenizer tokenizer = new DelimitedLineTokenizer();
    tokenizer.setNames(names);
    mapper.setLineTokenizer(tokenizer);
    mapper.setFieldSetMapper(fieldSetMapper);
    reader.setLineMapper(mapper);
  }
}
