package com.mediaiq.aiq.mediamath.listener;

import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mediaiq.aiq.mediamath.firebase.BatchFirebaseUtil;

/**
 * Created by chandrahaas on 4/1/16.
 */
@Component
public class BatchJobsNotificationExecutionListener implements JobExecutionListener {
  
  @Autowired
  BatchFirebaseUtil batchFirebaseUtil;
  
  
  @Override
  public void beforeJob(JobExecution jobExecution) {
    String jobName = jobExecution.getJobInstance().getJobName();
    BatchStatus batchStatus = jobExecution.getStatus();
    // Do nothing
    batchFirebaseUtil.postCampaignState(jobNameWithSeat(jobName), batchStatus);
  }
  
  @Override
  public void afterJob(JobExecution jobExecution) {
    String jobName = jobExecution.getJobInstance().getJobName();
    
    BatchStatus batchStatus = jobExecution.getStatus();
    // Do nothing
    batchFirebaseUtil.postCampaignState(jobNameWithSeat(jobName), batchStatus);
    
  }
  
  private String jobNameWithSeat(String jobName) {
    return "mediamath" + "/" + jobName;
  }
}
