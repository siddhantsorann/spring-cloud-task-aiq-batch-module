insert into trading_center.placement_creative_mapping
(placement_id, creative_id, insertion_order_id, last_modified,start_date,
			end_date, dsp)
SELECT p.id, pc.creative_id, ili.insertion_order_id, p.last_modified,
p.last_modified as start_date,
"2100-01-01" as end_date
 'Appnexus'
from
(
SELECT pc.placement_id, pc.creative_id from
appnexus.placement_creatives pc
where placement_id in( :placementIds) union
SELECT p.id, lic.creative_id from appnexus.placement p
join appnexus.line_item_creatives lic
on p.line_item_id = lic.line_item_id where p.id in( :placementIds )
) pc
join
appnexus.placement p on pc.placement_id = p.id join
appnexus.insertion_order_has_line_item ili on ili.line_item_id = p.line_item_id
where
			p.id in(
				select
					placement_id
				from
					trading_center.placement_creative_mapping
					group by dsp,placement_id
				having
					max(end_date) != "2100-01-01" and dsp = "Appnexus"
			)
			or p.id not in(
				select
					d.placement_id
				from
					trading_center.placement_creative_mapping d
				where
					d.dsp = "Appnexus"
			)
			;;
