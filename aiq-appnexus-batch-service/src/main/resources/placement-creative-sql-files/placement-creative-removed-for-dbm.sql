update trading_center.placement_creative_mapping set end_date=
        DATE_SUB(
              CURRENT_DATE,
              INTERVAL 1 DAY
      ) WHERE placement_id,creative_id NOT IN
(select lc.line_item_id,lc.creative_id from appnexus.placement as p join appnexus.line_item as l on p.line_item_id=l.id join appnexus.line_item_creatives as lc on l.id=lc.line_item_id
where p.last_modified>=CURDATE()) ;
