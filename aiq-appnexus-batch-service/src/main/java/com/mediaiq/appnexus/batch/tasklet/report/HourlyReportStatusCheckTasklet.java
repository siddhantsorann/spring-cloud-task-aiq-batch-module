package com.mediaiq.appnexus.batch.tasklet.report;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.batch.exception.ReportNotReadyException;
import com.mediaiq.appnexus.batch.value.StepAttribute;
import com.mediaiq.appnexus.load.client.AppnexusRestClient;
import com.mediaiq.appnexus.load.response.ReportStatusCheckResponse;
import com.mediaiq.appnexus.load.response.ResponseContainer;
import com.mediaiq.spring.batch.aspect.Retry;

@Component
@Scope("step")
public class HourlyReportStatusCheckTasklet implements Tasklet {
  final static Logger logger = LoggerFactory.getLogger(HourlyReportStatusCheckTasklet.class);
  @Autowired
  @Qualifier("JOB")
  private AppnexusRestClient appnexusClient;
  
  @Value("#{jobExecutionContext['" + StepAttribute.REPORT_ID + "']}")
  private String reportId;
  
  private Integer attempt = 1;
  
  @Retry
  @Override
  public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext)
      throws Exception {
    logger.info("Attempt {} to check status of report with ID: {}", attempt, reportId);
    ResponseContainer<ReportStatusCheckResponse> rStatusResponse =
        appnexusClient.getReportStatus(reportId);
    if (!rStatusResponse.getResponse().getExecutionStatus().equalsIgnoreCase("ready")) {
      logger.warn("Report not ready yet. Trying again.");
      attempt++;
      throw new ReportNotReadyException("Invalid attempt");
    }
    return RepeatStatus.FINISHED;
  }
  
}
