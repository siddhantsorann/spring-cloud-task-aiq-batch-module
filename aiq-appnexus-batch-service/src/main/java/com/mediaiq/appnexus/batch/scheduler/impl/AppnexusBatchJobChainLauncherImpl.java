package com.mediaiq.appnexus.batch.scheduler.impl;

import java.text.ParseException;
import java.util.Arrays;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

import com.mediaiq.appnexus.batch.scheduler.AppnexusBatchJobChainLauncher;
import com.mediaiq.appnexus.batch.service.AppnexusBatchService;
import com.mediaiq.appnexus.domain.report.TimeInterval;
import com.mediaiq.appnexus.repo.AppnexusAdvertiserRepo;
import com.mediaiq.appnexus.repo.InsertionOrderRepo;
import com.mediaiq.appnexus.value.AppnexusSeat;

@Configuration
public class AppnexusBatchJobChainLauncherImpl implements AppnexusBatchJobChainLauncher {
  
  @Autowired
  AppnexusBatchService appnexusBatchService;
  
  @Autowired
  InsertionOrderRepo insertionOrderRepo;
  
  @Autowired
  AppnexusAdvertiserRepo appnexusAdvertiserRepo;
  
  @Value("#{'${appnexusSeats}'.split(',')}")
  private List<String> appnexusSeats;
  
  List<AppnexusSeat> appnexusSeatsToBeProcessed;
  
  @Autowired
  private Environment environment;
  
  private String[] activeProfile;
  
  @PostConstruct
  public void init() {
    appnexusSeatsToBeProcessed =
        appnexusSeats.stream().map(AppnexusSeat::valueOf).collect(Collectors.toList());
    activeProfile=this.environment.getActiveProfiles();
  }
  
  
  private final Logger logger = LoggerFactory.getLogger(this.getClass());
  
  @Override
  @Scheduled(cron = "${geo.load.cron.expression}")
  public void launchGeoLoadJob() throws InterruptedException, ExecutionException {
    
    logger.info("Geo Load Job Chain scheduled");
    // Launch CountryLoad -> City Load -> Region Load
    if (appnexusSeatsToBeProcessed.contains(AppnexusSeat.SEAT_MIQ_3)) {
      Future<JobExecution> launchCountryLoadJob =
          appnexusBatchService.launchCountryLoadJob(true, AppnexusSeat.SEAT_MIQ_3);
      launchCountryLoadJob.get();
      
      Future<JobExecution> launchRegionLoadJob =
          appnexusBatchService.launchRegionLoadJob(true, AppnexusSeat.SEAT_MIQ_3);
      launchRegionLoadJob.get();
      Future<JobExecution> launchAllCityLoadJob =
          appnexusBatchService.launchAllCityLoadJob(true, AppnexusSeat.SEAT_MIQ_3);
      launchAllCityLoadJob.get();
    } else {
      Future<JobExecution> launchCountryLoadJob =
          appnexusBatchService.launchCountryLoadJob(true, AppnexusSeat.SEAT_MIQ_1);
      launchCountryLoadJob.get();
      
      Future<JobExecution> launchRegionLoadJob =
          appnexusBatchService.launchRegionLoadJob(true, AppnexusSeat.SEAT_MIQ_1);
      launchRegionLoadJob.get();
      Future<JobExecution> launchAllCityLoadJob =
          appnexusBatchService.launchAllCityLoadJob(true, AppnexusSeat.SEAT_MIQ_1);
      launchAllCityLoadJob.get();
    }
  }
  
  @Override
  @Scheduled(cron = "${domain-list.load.cron.expression}")
  public void launchDomainListLoadJob() {
    logger.info("Domain List Load Job scheduled");
    appnexusSeatsToBeProcessed.forEach(seat -> {
      appnexusBatchService.launchDomainListLoadJob(true, seat, getOlderDate(2));
    });
  }
  
  @Override
  @Scheduled(cron = "${data-usage.load.cron.expression}")
  public void launchDataUsageLoadJob() {
    boolean isdataUsageStatsLoadJobEnabled =
        Arrays.stream(activeProfile).anyMatch(profile -> profile.equalsIgnoreCase("amnet"));
    if (isdataUsageStatsLoadJobEnabled) {
      return;
    }
    logger.info("Data Usage Load Job scheduled");
    Date date = getFutureDate(1);
    if (inCurrentMonth(date, getOlderDate(0)))
      appnexusBatchService.launchDataUsageStatsLoadJob(AppnexusSeat.SEAT_MIQ_1, getFutureDate(1));
    else
      appnexusBatchService.launchDataUsageStatsLoadJob(AppnexusSeat.SEAT_MIQ_1, getOlderDate(0));
  }
  
  private boolean inCurrentMonth(Date date,Date currentDate){
      LocalDate date1 = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
      LocalDate date2 = currentDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
      if(date1.getMonthValue()==date2.getMonthValue() && date1.getYear()==date2.getYear())
        return true;
      return false;
  }
  
  @Override
  @Scheduled(cron = "${content-category.load.cron.expression}")
  public void launchContentCategoryLoadJob() {
    logger.info("Content Category Load Job scheduled");
    if (appnexusSeatsToBeProcessed.contains(AppnexusSeat.SEAT_MIQ_3))
      appnexusBatchService.launchContentCategoryLoadJob(true, AppnexusSeat.SEAT_MIQ_3,
          getOlderDate(2));
    else
      appnexusBatchService.launchContentCategoryLoadJob(true, AppnexusSeat.SEAT_MIQ_1,
          getOlderDate(2));
  }
  
  @Override
  @Scheduled(cron = "${site-domain.load.cron.expression}")
  public void launchSiteDomainReportLoadJob() {
    logger.info("Site Domain Report Load Job scheduled");
    if (appnexusSeatsToBeProcessed.contains(AppnexusSeat.SEAT_MIQ_3))
      appnexusBatchService.launchSiteDomainReportLoadJob(true, AppnexusSeat.SEAT_MIQ_3);
    else
      appnexusBatchService.launchSiteDomainReportLoadJob(true, AppnexusSeat.SEAT_MIQ_1);
  }
  
  @Override
  @Scheduled(cron = "${advertiser-meta.load.cron.expression}")
  public void launchAdvertiserMetadataLoadJob() throws InterruptedException, ExecutionException {
    logger.info("Advertiser Metadata Load Job scheduled");
    appnexusSeatsToBeProcessed.stream().forEach(seat -> {
      try {
        launchAdvertiserMetaLoadJobForSeat(seat);
      } catch (Exception e) {
        logger.error("Error in executing launchAdvertiserMetadataLoadJob: ", e);
      }
    });
  }
  
  @Override
  @Scheduled(cron = "${segment.load.cron.expression}")
  public void launchSegmentLoadJob() {
    logger.info("Segment Load Job scheduled");
    appnexusSeatsToBeProcessed.stream().forEach(seat -> {
      appnexusBatchService.launchSegmentLoadJob(true, seat, getOlderDate(2));
    });
  }
  
  @Override
  @Scheduled(cron = "${browser.load.cron.expression}")
  public void launchBrowserLoadJob() {
    logger.info("Browser Load Job Scheduled ");
    appnexusSeatsToBeProcessed.stream().forEach(seat -> {
      appnexusBatchService.launchBrowserLoadJob(true, seat, getOlderDate(7));
    });
    logger.info("Browser Load Job Complete ");
  }
  
  @Override
  @Scheduled(cron = "${carrier.load.cron.expression}")
  public void launchCarrierLoadJob() {
    logger.info("Carrier Load Job Scheduled ");
    appnexusSeatsToBeProcessed.stream().forEach(seat -> {
      appnexusBatchService.launchCarrierLoadJob(true, seat, getOlderDate(7));
    });
    logger.info("Carrier Load Job Complete ");
  }
  
  @Override
  @Scheduled(cron = "${device.load.cron.expression}")
  public void launchDeviceLoadJob() throws InterruptedException, ExecutionException {
    logger.info("Device Load Job Scheduled ");
    appnexusSeatsToBeProcessed.stream().forEach(seat -> {
      try {
        launchDeviceLoadJobForSeat(seat);
      } catch (Exception e) {
        logger.error("Error in executing launchDeviceLoadJob: ", e);
      }
    });
    logger.info("Device Load Job Completed ");
  }
  
  @Override
  @Scheduled(cron = "${mobile-app-instance.load.cron.expression}")
  public void launchMobileAppInstanceListLoadJob() {
    logger.info("Mobile App Instance List Load Job Scheduled ");
    appnexusSeatsToBeProcessed.stream().forEach(seat -> {
      appnexusBatchService.launchMobileAppInstanceListLoadJob(true, seat, getOlderDate(2));
    });
    logger.info("Mobile App Instance List Load Job Complete ");
  }
  
  @Override
  @Scheduled(cron = "${device.load.cron.expression}")
  public void launchOSLoadJob() throws InterruptedException, ExecutionException {
    logger.info("OS Load Job Scheduled ");
    appnexusSeatsToBeProcessed.stream().forEach(seat -> {
      try {
        launchOSLoadJobForSeat(seat);
      } catch (Exception e) {
        logger.error("Error in executing launchOSLoadJob: ", e);
      }
    });
    logger.info("OS Load Job Completed");
  }
  
  private void launchDeviceLoadJobForSeat(AppnexusSeat seat)
      throws InterruptedException, ExecutionException {
    Future<JobExecution> launchDeviceMakeLoadJob =
        appnexusBatchService.launchDeviceMakeLoadJob(true, seat, getOlderDate(7));
    launchDeviceMakeLoadJob.get();
    
    Future<JobExecution> launchDeviceModelLoadJob =
        appnexusBatchService.launchDeviceModelLoadJob(true, seat, getOlderDate(7));
    launchDeviceModelLoadJob.get();
  }
  
  private void launchAdvertiserMetaLoadJobForSeat(AppnexusSeat seat)
      throws InterruptedException, ExecutionException, ParseException {
    
    Future<JobExecution> launchAdvertiserLoadJob =
        appnexusBatchService.launchAdvertiserLoadJob(true, seat, getOlderDate(2));
    launchAdvertiserLoadJob.get();
    
    Future<JobExecution> launchInsertionOrderLoadJob =
        appnexusBatchService.launchInsertionOrderLoadJob(true, seat, getOlderDate(2));
    launchInsertionOrderLoadJob.get();
    
    Future<JobExecution> launchCreativeLoadJob =
        appnexusBatchService.launchCreativeLoadJob(true, seat, getOlderDate(2));
    launchCreativeLoadJob.get();
    
    Future<JobExecution> launchPixelLoadJob =
        appnexusBatchService.launchPixelLoadJob(true, seat, getOlderDate(2));
    launchPixelLoadJob.get();
    
    Future<JobExecution> launchLineItemLoadJob =
        appnexusBatchService.launchLineItemLoadJob(true, seat, getOlderDate(2));
    launchLineItemLoadJob.get();
    
    Future<JobExecution> launchPlacementLoadJob =
        appnexusBatchService.launchPlacementLoadJob(true, seat, getOlderDate(2), true);
    launchPlacementLoadJob.get();
    
    Future<JobExecution> launchSellerLoadJob =
        appnexusBatchService.launchSellerLoadJob(true, getOlderDate(2));
    launchSellerLoadJob.get();

    Future<JobExecution> launchPlacementCreativeMappingLoadJob =
        appnexusBatchService.launchPlacementCreativeMappingLoadJob(getOlderDate(2), true);
    launchPlacementCreativeMappingLoadJob.get();

  }
  
  private void launchOSLoadJobForSeat(AppnexusSeat seat)
      throws InterruptedException, ExecutionException {
    Future<JobExecution> launchOSFamilyJob =
        appnexusBatchService.launchOperatingSystemFamilyLoadJob(true, seat, getOlderDate(7));
    launchOSFamilyJob.get();
    
    appnexusBatchService.launchOperatingSystemExtendedLoadJob(true, seat, getOlderDate(7));
  }
  
  private Date getOlderDate(int days) {
    Calendar cal = Calendar.getInstance();
    cal.add(Calendar.DATE, -days);
    cal.add(Calendar.HOUR_OF_DAY, 0);
    cal.add(Calendar.MINUTE, 0);
    Date date = cal.getTime();
    return date;
  }
  
  private Date getFutureDate(int days) {
    Calendar cal = Calendar.getInstance();
    cal.add(Calendar.DATE, days);
    cal.add(Calendar.HOUR_OF_DAY, 0);
    cal.add(Calendar.MINUTE, 0);
    Date date = cal.getTime();
    return date;
  }
  
  @Override
  @Scheduled(cron = "${profile.segment.load.cron.expression}")
  public void launchProfileToSegmentMappingLoadJob() {
    logger.info("Profile To SegmentMapping Load Job Scheduled ");
    appnexusSeatsToBeProcessed.stream().forEach(seat -> {
      appnexusBatchService.launchProfileSegmentMappingLoadJob(true, seat, getOlderDate(2));
    });
    logger.info("Profile To SegmentMapping Load Job Completed ");
  }
  
  @Override
  @Scheduled(cron = "${profile.load.cron.expression}")
  public void launchProfileV3LoadJob() {
    logger.info("Profile V3 Load Job Scheduled ");
    appnexusSeatsToBeProcessed.stream().forEach(seat -> {
      appnexusBatchService.launchProfileV3LoadJob(true, seat);
    });
    logger.info("Profile V3 Load Job Completed ");
  }
  
  @Override
  @Scheduled(cron = "${rawstats.load.cron.expression}")
  public void launchRawStatsJob() throws ExecutionException, InterruptedException {
    logger.info("Launching job for raw stats");
    
    Date yesterDay = getOlderDate(1);
    Date tomorrow = getFutureDate(1);
    
    logger.info("Running raw stats for {} to {}", yesterDay, tomorrow);
    appnexusSeatsToBeProcessed.forEach(seat -> {
      logger.info("Launching Raw Stats Job for {}", seat);
      launchForSeat(seat, yesterDay, tomorrow);
    });
  }
  
  @Override
  @Scheduled(cron = "${creative-aggregation.load.cron.expression}")
  public void launchCreativeAggregation() {
    logger.info("Launching job: creative aggregation");
    Date yesterDay = getOlderDate(1);
    Date tomorrow = getFutureDate(1);
    logger.info("Running creative aggregation  for dates : {} to {}", yesterDay, tomorrow);
    appnexusBatchService.launchCreativeAggregationJob(yesterDay, tomorrow);
  }
  
  private void launchForSeat(AppnexusSeat appnexusSeat, Date startDate, Date endDate) {
    appnexusBatchService.launchAppnexusDailyPlacementTimeZoneStatsLoadJob(true, appnexusSeat,
        startDate, endDate, null);
  }
  
  /*
   * @Override // @Scheduled(cron = "0 35 0 ? * SUN") public void
   * launchCreateSegmentsForNewAdvertisersJob() { Date date7DaysBack = getOlderDate(7);
   * appnexusBatchService.launchAddClickerSegmentsForNewAdvertisersJob(true,
   * AppnexusSeat.SEAT_MIQ_1, date7DaysBack); }
   *
   * @Override // @Scheduled(cron = "0 45 0 ? * SUN") public void launchUpdateCreativesJob() { Date
   * date7DaysBack = getOlderDate(7);
   * appnexusBatchService.launchUpdateCreativesWithClickerSegmentJob(true, AppnexusSeat.SEAT_MIQ_1,
   * date7DaysBack);
   *
   * }
   */
  
  @Override
  @Scheduled(cron = "${geo.load.cron.expression}")
  public void launchLanguageLoadJob() {
    logger.info("Languange Load Job scheduled");
    if (appnexusSeatsToBeProcessed.contains(AppnexusSeat.SEAT_MIQ_3))
      appnexusBatchService.launchLanguageLoadJob(true, AppnexusSeat.SEAT_MIQ_3, getOlderDate(2));
    else
      appnexusBatchService.launchLanguageLoadJob(true, AppnexusSeat.SEAT_MIQ_1, getOlderDate(2));
  }
  
  @Override
  @Scheduled(cron = "${appnexus-entity-count.load.cron.expression}")
  public void launchAppnexusEntityCountLoadJob() {
    logger.info("Appnexus Entity Count Load Job Scheduled ");
    appnexusSeatsToBeProcessed.stream().forEach(seat -> {
      appnexusBatchService.launchAppnexusEntityCountLoadJob(true, seat, getOlderDate(3650), true);
    });
    logger.info("Appnexus Entity Count Load Job Complete ");
  }
  
  @Override
  public void launchMobileAppLoadJob() {
    logger.info("Mobile App Load Job Scheduled ");
    appnexusSeatsToBeProcessed.stream().forEach(seat -> {
      appnexusBatchService.launchMobileAppLoadJob(TimeInterval.last_7_days, true, seat);
    });
    
  }
  
  @Override
  @Scheduled(cron = "${update-pixel-to-creative-mapping.cron.expression}")
  public void launchUpdatePixelToCreativeMapping() {
    logger.info("Update  data provider/pixel to creative mapping Job Scheduled ");
    appnexusBatchService.launchUpdatePixelToCreativeMapping();
    logger.info("*** Update  data provider/pixel to creative mapping Job Completed ***");
  }
  
  @Override
  @Scheduled(cron = "${map-dp-to-segments.cron.expression}")
  public void launchMapDpToSegmentsJob() throws InterruptedException, ExecutionException {
    logger.info("Map Data Providers to unmapped Segments Job Scheduled ");
    appnexusBatchService.launchMapDpToSegmentsJob();
    logger.info("*** Map Data Providers to unmapped Segments Job Completed ***");
  }

  @Override
  @Scheduled(cron = "${map-dp-to-appnexus-segments.cron.expression}")
  public void launchSegmentDataPoviderMapping() throws InterruptedException, ExecutionException {
    logger.info("Map Data Providers to mapped to Appnexus Segments Job");
    appnexusBatchService.launchSegmentDataPoviderMapping();
    logger.info("*** Map Data Providers to mapped to Appnexus Segments Job Completed ***");
    
  }
  //Made this part of meta job
  @Override
  //@Scheduled(cron = "${placement-cretive-mappings.cron.expression}")
  public void launchPlacementToCreativeMappingJob()  {
    logger.info("Map Placements to Creatives Job Scheduled ");
    appnexusBatchService.launchPlacementCreativeMappingLoadJob(getOlderDate(2), true);
    logger.info("*** Map Placements to Creatives Job Completed ***");
  }
  
}
