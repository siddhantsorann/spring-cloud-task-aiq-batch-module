package com.mediaiq.appnexus.batch.chunk.mapper;

import java.util.Date;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import com.mediaiq.appnexus.domain.SiteDomain;

public class AppnexusSiteDomainReportMapper implements FieldSetMapper<SiteDomain> {
  
  @Override
  public SiteDomain mapFieldSet(FieldSet fieldSet) throws BindException {
    SiteDomain domain = new SiteDomain();
    domain.setName(fieldSet.readRawString("domain_name"));
    domain.setPlatformAuditedImpressions(fieldSet.readLong("platform_audited_imps"));
    domain.setSecondLevelCategory(fieldSet.readString("top_level_category"));
    domain.setSellerAuditedImpressions(fieldSet.readLong("seller_audited_imps"));
    domain.setTopLevelCategory(fieldSet.readString("top_level_category"));
    domain.setLastModified(new Date());
    return domain;
  }
  
  
  
}
