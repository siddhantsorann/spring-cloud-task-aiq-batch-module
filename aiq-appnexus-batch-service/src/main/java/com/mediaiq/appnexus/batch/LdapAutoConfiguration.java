package com.mediaiq.appnexus.batch;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.ldap.core.ContextSource;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;

@Configuration
@EnableConfigurationProperties
public class LdapAutoConfiguration {
  
  @ConditionalOnProperty(prefix = "ldap.contextSource", name = {"serviceUrl", "base", "password"})
  @Bean
  @ConfigurationProperties(prefix = "ldap.contextSource")
  public LdapContextSource contextSource() {
    LdapContextSource contextSource = new LdapContextSource();
    return contextSource;
  }
  
  @ConditionalOnBean(ContextSource.class)
  @Bean
  public LdapTemplate ldapTemplate(ContextSource contextSource) {
    return new LdapTemplate(contextSource);
  }
}
