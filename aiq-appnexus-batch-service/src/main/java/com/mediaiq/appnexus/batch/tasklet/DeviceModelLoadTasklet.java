package com.mediaiq.appnexus.batch.tasklet;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.domain.DeviceModel;
import com.mediaiq.appnexus.load.client.AppnexusRestClient;
import com.mediaiq.appnexus.load.response.DeviceModelListResponse;
import com.mediaiq.appnexus.load.response.ResponseContainer;
import com.mediaiq.appnexus.repo.DeviceModelRepo;
import com.mediaiq.appnexus.value.AppnexusDate;

@Component
@Scope("step")
public class DeviceModelLoadTasklet extends
    AbstractLastModifiedSeatAwarePageLoadTasklet<DeviceModel, DeviceModelRepo, DeviceModelListResponse> {
  
  @Autowired
  private DeviceModelRepo deviceModelRepo;
  
  @Override
  public ResponseContainer<DeviceModelListResponse> getPageableResponse(
      AppnexusRestClient appnexusClient) {
    return appnexusClient.getDeviceModelList(startElement, new AppnexusDate(lastModified));
  }
  
  @Override
  public DeviceModelRepo getRepository() {
    return deviceModelRepo;
  }
  
  @Override
  public List<DeviceModel> getItemsFromResponse(DeviceModelListResponse response) {
    return response.getDeviceModels();
  }
}
