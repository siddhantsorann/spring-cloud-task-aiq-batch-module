package com.mediaiq.appnexus.batch.tasklet;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.domain.MobileAppInstanceList;
import com.mediaiq.appnexus.load.client.AppnexusRestClient;
import com.mediaiq.appnexus.load.response.MobileAppInstanceListResponse;
import com.mediaiq.appnexus.load.response.ResponseContainer;
import com.mediaiq.appnexus.repo.MobileAppInstanceListRepo;
import com.mediaiq.appnexus.value.AppnexusDate;

@Component
@Scope("step")
public class MobileAppInstanceListLoadTasklet extends
    AbstractLastModifiedSeatAwarePageLoadTasklet<MobileAppInstanceList, MobileAppInstanceListRepo, MobileAppInstanceListResponse> {
  
  @Autowired
  private MobileAppInstanceListRepo mobileAppInstanceListRepo;
  
  @Override
  public ResponseContainer<MobileAppInstanceListResponse> getPageableResponse(
      AppnexusRestClient appnexusClient) {
    return appnexusClient.getMobileAppInstancelList(startElement, new AppnexusDate(lastModified));
  }
  
  @Override
  public MobileAppInstanceListRepo getRepository() {
    return mobileAppInstanceListRepo;
  }
  
  @Override
  public List<MobileAppInstanceList> getItemsFromResponse(MobileAppInstanceListResponse response) {
    return response.getMobileAppInstanceLists();
  }
}
