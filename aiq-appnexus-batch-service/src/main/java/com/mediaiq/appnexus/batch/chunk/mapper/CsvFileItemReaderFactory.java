package com.mediaiq.appnexus.batch.chunk.mapper;


import java.io.IOException;
import java.util.Date;

import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

import com.mediaiq.appnexus.batch.value.StepAttribute;
import com.mediaiq.appnexus.domain.MobileApp;
import com.mediaiq.appnexus.domain.SiteDomain;
import com.mediaiq.appnexus.domain.report.stats.raw.DataUsageStat;
import com.mediaiq.appnexus.domain.report.stats.raw.NetworkLookupStat;

@Configuration
public class CsvFileItemReaderFactory {
  
  @Bean(name = "appnexusDataUsageStatsReader")
  @Scope("job")
  public FlatFileItemReader<DataUsageStat> provideTT(
      @Value("#{jobExecutionContext['" + StepAttribute.FILE_PATH + "']}") String filePath,
      @Value("#{jobParameters['timezone']}") String timezone)
      throws IOException {
    
    String[] names = {"day", "buyer_member_id", "geo_country_code", "campaign_id",
        "data_provider_id", "data_provider_name", "targeted_segmented_ids", "imps",
        "data_costs_buying_currency", "sales_tax"};
    FlatFileItemReader<DataUsageStat> reader = new FlatFileItemReader<>();
    DefaultLineMapper<DataUsageStat> mapper = new DefaultLineMapper<>();
    DataUsageStatsMapper fieldSetMapper = new DataUsageStatsMapper(timezone);
    commonProvider(names, reader, filePath, mapper, fieldSetMapper);
    return reader;
  }
  
  
  @Bean(name = "appnexusDailyPlacementTimeZonePlacementCreativeSellerTimezoneStatsReader")
  @Scope("job")
  public FlatFileItemReader<NetworkLookupStat> provide(
      @Value("#{jobExecutionContext['" + StepAttribute.FILE_PATH + "']}") String filePath,
      @Value("#{jobParameters['timezone']}") String timeZone) {
    String[] names = {"day", "creative_id", "campaign_id", "imps", "clicks", "total_convs",
        "cost_buying_currency", "buying_currency", "insertion_order_id"};
    FlatFileItemReader<NetworkLookupStat> reader = new FlatFileItemReader<>();
    DefaultLineMapper<NetworkLookupStat> mapper = new DefaultLineMapper<>();
    AppnexusDailyPlacementTimeZoneStatsMapper fieldSetMapper =
        new AppnexusDailyPlacementTimeZoneStatsMapper(timeZone);
    commonProvider(names, reader, filePath, mapper, fieldSetMapper);
    return reader;
  }
  
  
  @Bean(name = "siteDomainReportReader")
  @Scope("job")
  public FlatFileItemReader<SiteDomain> provideSiteDomainReader(
      @Value("#{jobExecutionContext['" + StepAttribute.FILE_PATH + "']}") String filePath) {
    String[] names = {"top_level_category", "second_level_category", "domain_name",
        "platform_audited_imps", "seller_audited_imps"};
    FlatFileItemReader<SiteDomain> reader = new FlatFileItemReader<>();
    DefaultLineMapper<SiteDomain> mapper = new DefaultLineMapper<>();
    AppnexusSiteDomainReportMapper fieldSetMapper = new AppnexusSiteDomainReportMapper();
    commonProvider(names, reader, filePath, mapper, fieldSetMapper);
    return reader;
  }
  
  @Bean(name = "mobileAppReader")
  @Scope("job")
  public FlatFileItemReader<MobileApp> provideMobileAppReader(
      @Value("#{jobExecutionContext['" + StepAttribute.FILE_PATH + "']}") String filePath) {
    String[] names =
        {"top_level_category_name", "second_level_category_name", "mobile_application_id",
            "mobile_application_name", "operating_system_id", "operating_system_name"};
    FlatFileItemReader<MobileApp> reader = new FlatFileItemReader<>();
    DefaultLineMapper<MobileApp> mapper = new DefaultLineMapper<>();
    MobileAppReportMapper fieldSetMapper = new MobileAppReportMapper();
    commonProvider(names, reader, filePath, mapper, fieldSetMapper);
    return reader;
  }
  
  @SuppressWarnings({"rawtypes", "unchecked"})
  private void commonProvider(String[] names, FlatFileItemReader reader, String filePath,
      DefaultLineMapper mapper, FieldSetMapper fieldSetMapper) {
    System.out.println(filePath);
    Resource resource = new FileSystemResource(filePath);
    reader.setResource(resource);
    reader.setLinesToSkip(1);
    DelimitedLineTokenizer tokenizer = new DelimitedLineTokenizer();
    tokenizer.setNames(names);
    mapper.setLineTokenizer(tokenizer);
    mapper.setFieldSetMapper(fieldSetMapper);
    reader.setLineMapper(mapper);
  }
  
  @Bean(name = "jobLastRunHourReader")
  @Scope("job")
  public FlatFileItemReader<Date> provideIntegerReader(
      @Value("#{jobExecutionContext['" + StepAttribute.FILE_PATH + "']}") String filePath) {
    String[] names = {"hour"};
    FlatFileItemReader<Date> reader = new FlatFileItemReader<>();
    DefaultLineMapper<Date> mapper = new DefaultLineMapper<>();
    StatsMaxHourMapper fieldSetMapper = new StatsMaxHourMapper();
    commonProvider(names, reader, filePath, mapper, fieldSetMapper);
    return reader;
  }
  
  @Bean(name = "customWriter")
  @Scope("job")
  public CustomWriter getCustomeWriter() {
    CustomWriter customWriter = new CustomWriter();
    return customWriter;
  }
  
  
  @Bean(name = "appnexusDailyPlacementTimeZonePlacementCreativeSellerTimezoneStatsLoader")
  @Scope("job")
  public void provideLoader(
      @Value("#{jobExecutionContext['" + StepAttribute.FILE_PATH + "']}") String filePath,
      @Value("#{jobParameters['timezone']}") String timeZone) {}
}
