package com.mediaiq.appnexus.batch.listener;

import org.apache.commons.lang.StringUtils;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.batch.firebase.BatchFirebaseUtil;

@Component
public class BatchJobsNotificationExecutionListener implements JobExecutionListener {
  
  @Autowired
  BatchFirebaseUtil batchFirebaseUtil;
  
  
  @Override
  public void beforeJob(JobExecution jobExecution) {
    String jobName = jobExecution.getJobInstance().getJobName();
    BatchStatus batchStatus = jobExecution.getStatus();
    String seatName = jobExecution.getJobParameters().getString("appnexus_seat");
    String timeZone = jobExecution.getJobParameters().getString("timezone");
    // Do nothing
    batchFirebaseUtil.postCampaignState(jobNameWithSeat(jobName, seatName, timeZone), batchStatus);
  }
  
  @Override
  public void afterJob(JobExecution jobExecution) {
    String jobName = jobExecution.getJobInstance().getJobName();
    String seatName = jobExecution.getJobParameters().getString("appnexus_seat");
    String timezone = jobExecution.getJobParameters().getString("timezone");
    
    BatchStatus batchStatus = jobExecution.getStatus();
    // Do nothing
    batchFirebaseUtil.postCampaignState(jobNameWithSeat(jobName, seatName, timezone), batchStatus);
    
  }
  
  private String jobNameWithSeat(String jobName, String seat, String timezone) {
    String finalName = "";
    if (seat != null && !seat.isEmpty())
      finalName = seat;
    if (timezone != null && !timezone.isEmpty())
      finalName = finalName + "/" + StringUtils.replace(timezone, "/", "&");
    return "appnexus" + "/" + jobName + "/" + finalName;
  }
  
}
