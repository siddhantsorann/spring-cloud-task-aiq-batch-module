package com.mediaiq.appnexus.batch.service.impl;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

import org.apache.commons.collections4.ListUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mediaiq.appnexus.batch.IoDao;
import com.mediaiq.appnexus.batch.service.AppnexusBatchService;
import com.mediaiq.appnexus.batch.value.JobName;
import com.mediaiq.appnexus.batch.value.StepAttribute;
import com.mediaiq.appnexus.domain.InsertionOrder;
import com.mediaiq.appnexus.domain.Placement;
import com.mediaiq.appnexus.domain.report.TimeInterval;
import com.mediaiq.appnexus.repo.InsertionOrderRepo;
import com.mediaiq.appnexus.repo.PlacementRepo;
import com.mediaiq.appnexus.value.AppnexusSeat;
import com.mediaiq.spring.batch.integration.component.JobLaunchGateway;
import com.mediaiq.spring.batch.service.impl.AbstractJobLaunchServiceImpl;

@Service
public class AppnexusBatchServiceImpl extends AbstractJobLaunchServiceImpl
    implements AppnexusBatchService {
  
  private static final Logger logger = LoggerFactory.getLogger(AppnexusBatchServiceImpl.class);
  
  @Autowired
  JobLaunchGateway jobLaunchGateway;
  
  @Autowired
  @Qualifier("appnexusGson")
  Gson gson;
  
  @Autowired
  InsertionOrderRepo insertionOrderRepo;

  @Autowired
  PlacementRepo placementRepo;
  
  @Autowired
  IoDao insertionOrderJdbcDao;
  
  @Override
  public Future<JobExecution> launchCityLoadJob(String countryCode, Boolean override,
      AppnexusSeat appnexusSeat) {
    
    logger.debug("In Method: launchCityLoadJob");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addString("country_code", countryCode)
        .addString("appnexus_seat", appnexusSeat.name()).toJobParameters();
    
    if (override)
      jobParametersBuilder.addDate("date", new Date());
    
    return launchJob(JobName.cityLoadJob.name(), jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public Future<JobExecution> launchAllCityLoadJob(Boolean override, AppnexusSeat appnexusSeat) {
    logger.info("In Method: launchNewCityJob");
    return launchJobForCityWithStartElement(JobName.cityAllLoadJob.name(), override, appnexusSeat);
  }
  
  public Future<JobExecution> launchJobForCityWithStartElement(String name, Boolean override,
      AppnexusSeat appnexusSeat) {
    logger.info("In Method: launchJobWithStartElement");
    
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    // jobParametersBuilder.addLong("start_element",
    // 0L).addString("appnexus_seat",
    // appnexusSeat.name());
    jobParametersBuilder.addLong("start_element", 0L).addString("appnexus_seat",
        appnexusSeat.name());
    
    if (override)
      jobParametersBuilder.addDate("date", new Date());
    logger.info(" job name " + name);
    return launchJob(name, jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public Future<JobExecution> launchCountryLoadJob(Boolean override, AppnexusSeat appnexusSeat) {
    
    logger.debug("In Method: launchCountryLoadJob");
    return launchJobWithStartElement(JobName.countryLoadJob.name(), override, appnexusSeat, null,
        true);
  }
  
  @Override
  public Future<JobExecution> launchRegionLoadJob(Boolean override, AppnexusSeat appnexusSeat) {
    
    logger.debug("In Method: launchRegionLoadJob");
    return launchJobWithStartElement(JobName.regionLoadJob.name(), override, appnexusSeat, null,
        true);
  }
  
  @Override
  public Future<JobExecution> launchAdvertiserLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      Date lastModified) {
    
    logger.debug("In Method: launchAdvertiserLoadJob");
    return launchJobWithStartElement(JobName.advertiserLoadJob.name(), override, appnexusSeat,
        lastModified, true);
  }
  
  @Override
  public Future<JobExecution> launchContentCategoryLoadJob(Boolean override,
      AppnexusSeat appnexusSeat, Date lastModified) {
    
    logger.debug("In Method: launchContentCategoryLoadJob");
    return launchJobWithStartElement(JobName.contentCategoryLoadJob.name(), override, appnexusSeat,
        lastModified, true);
  }
  
  @Override
  public Future<JobExecution> launchDomainListLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      Date lastModified) {
    
    logger.debug("In Method: launchDomainListLoadJob");
    return launchJobWithStartElement(JobName.domainListLoadJob.name(), override, appnexusSeat,
        lastModified, true);
  }
  
  @Override
  public Future<JobExecution> launchInsertionOrderLoadJob(Boolean override,
      AppnexusSeat appnexusSeat, Date lastModified) {
    
    logger.debug("In Method: launchInsertionOrderLoadJob");
    return launchJobWithStartElement(JobName.insertionOrderLoadJob.name(), override, appnexusSeat,
        lastModified, true);
  }
  
  @Override
  public Future<JobExecution> launchLineItemLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      Date lastModified) {
    
    logger.debug("In Method: launchLineItemLoadJob");
    return launchJobWithStartElement(JobName.lineItemLoadJob.name(), override, appnexusSeat,
        lastModified, true);
  }
  
  @Override
  public Future<JobExecution> launchPixelLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      Date lastModified) {
    
    logger.debug("In Method: launchPixelLoadJob");
    return launchJobWithStartElement(JobName.pixelLoadJob.name(), override, appnexusSeat,
        lastModified, true);
  }
  
  @Override
  public Future<JobExecution> launchPlacementLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      Date lastModified, Boolean showAugmented) {
    
    logger.debug("In Method: launchPlacementLoadJob");
    return launchJobWithStartElement(JobName.placementLoadJob.name(), override, appnexusSeat,
        lastModified, true);
  }
  
  @Override
  public Future<JobExecution> launchSegmentLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      Date lastModified) {
    
    logger.debug("In Method: launchSegmentLoadJob");
    return launchJobWithStartElement(JobName.segmentLoadJob.name(), override, appnexusSeat,
        lastModified, true);
  }
  
  @Override
  public Future<JobExecution> launchSiteDomainReportLoadJob(Boolean override,
      AppnexusSeat appnexusSeat) {
    
    logger.debug("In Method: launchSiteDomainReportLoadJob");
    return launchJobWithStartElement(JobName.siteDomainReportLoadJob.name(), override, appnexusSeat,
        null, true);
  }


  
  @Override
  public void launchAppnexusDailyPlacementTimeZoneStatsLoadJob(Boolean override,
      AppnexusSeat appnexusSeat, Date startDate, Date endDate, String timezone) {
    logger.info(
        "Starting the appnexus daily placement time zone stats load job for {} to {} and seat {}",
        startDate, endDate, appnexusSeat);
    logger.info("Fetching ios and timezones list");
    List<InsertionOrder> ioTimezones = insertionOrderRepo.findBySeat(appnexusSeat);
    logger.info("Fetched ios and timezones list with {} entries", ioTimezones.size());
    logger.info("Mapping ios to timezones");
    Map<String, List<Long>> timezoneIosMap = ioTimezones.stream().map(io -> {
      if (io.getTimezone() == null)
        io.setTimezone("UTC");
      return io;
    }).collect(Collectors.groupingBy(InsertionOrder::getTimezone,
        Collectors.mapping(InsertionOrder::getId, Collectors.toList())));
    logger.info("Mapping done");
    
    // Calculating date for Australian Timezone
    Instant instant = Instant.ofEpochMilli(endDate.getTime());
    LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
    LocalDate localDate = localDateTime.toLocalDate().plusDays(1);
    
    Date dayAfterTomorrow = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
    logger.info("day after tomorrow is " + dayAfterTomorrow);
    
    timezoneIosMap.entrySet().stream().forEach(e -> {
      logger.info("For timezone {}", e.getKey());
      List<List<Long>> ioIdsPartitions = ListUtils.partition(e.getValue(), 5000);
      ioIdsPartitions.stream().forEach(ioIdPartition -> {
        Future<JobExecution> triggerJob;
        if (e.getKey().equalsIgnoreCase("Australia/Sydney")
            || e.getKey().equalsIgnoreCase("Australia/Melbourne")
            || e.getKey().equalsIgnoreCase("Australia/Perth"))
          triggerJob = triggerJob(e.getKey(), ioIdPartition, override, appnexusSeat, startDate,
              dayAfterTomorrow);
        else
          triggerJob =
              triggerJob(e.getKey(), ioIdPartition, override, appnexusSeat, startDate, endDate);
        try {
          triggerJob.get();
        } catch (Exception e1) {
          e1.printStackTrace();
        }
      });
      
    });
  }
  
  @Override
  public void launchDataUsageStatsLoadJob(AppnexusSeat appnexusSeat, Date startDate) {

      logger.info(
          "Starting the appnexus data cost time zone stats load job for {}  and seat {}",
          startDate, appnexusSeat);
      logger.info("Fetching placements and timezones list ");
      List<Placement> placementTimezones = placementRepo.findBySeat(appnexusSeat);
      logger.info("Fetched placements and timezones list with {} entries", placementTimezones.size());
      logger.info("Mapping placements to timezones");
      Map<String, List<Long>> timezoneIosMap = placementTimezones.stream().map(io -> {
          if (io.getTimezone() == null)
              io.setTimezone("UTC");
          return io;
      }).collect(Collectors.groupingBy(Placement::getTimezone,
          Collectors.mapping(Placement::getId, Collectors.toList())));
      logger.info("Mapping done");

      // Calculating date for Australian Timezone
      Instant instant = Instant.ofEpochMilli(startDate.getTime());
      LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
      LocalDate localDate = localDateTime.toLocalDate().plusDays(1);

      Date dayAfterTomorrow = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
      logger.info("day after tomorrow is " + dayAfterTomorrow);

      timezoneIosMap.entrySet().stream().forEach(e -> {
          logger.info("For timezone {}", e.getKey());
          List<List<Long>> placementIdsPartitions = ListUtils.partition(e.getValue(), 5000);
          placementIdsPartitions.stream().forEach(placementIdPartition -> {
              Future<JobExecution> triggerJobForDataCost;
              if (e.getKey().equalsIgnoreCase("Australia/Sydney")
                  || e.getKey().equalsIgnoreCase("Australia/Melbourne")
                  || e.getKey().equalsIgnoreCase("Australia/Perth"))
                  triggerJobForDataCost = triggerJobForDataCost(e.getKey(), placementIdPartition, appnexusSeat, startDate,
                      dayAfterTomorrow);
              else
                  triggerJobForDataCost =
                      triggerJobForDataCost(e.getKey(), placementIdPartition, appnexusSeat, startDate, startDate);
              try {
                  triggerJobForDataCost.get();
              } catch (Exception e1) {
                  e1.printStackTrace();
              }
          });

      });
  }

    private Future<JobExecution> triggerJobForDataCost(String timezone, List<Long> placementList,
        AppnexusSeat appnexusSeat, Date startDate, Date endDate) {

        Calendar cal = Calendar.getInstance();
        cal.setTime(startDate);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        startDate = cal.getTime();
        cal.setTime(endDate);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        endDate = cal.getTime();
        logger.info("Triggering data cost stats job with following parameters: placementList:" + placementList
            + ", appnexusSeat: " + appnexusSeat + "startDate:" + startDate
            + ", endDate:" + endDate);

        String placementJson = gson.toJson(placementList);

        JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
        jobParametersBuilder.addString("appnexus_seat", appnexusSeat.name()).addString("timezone",
            timezone);


        if (startDate != null)
            jobParametersBuilder.addDate("start_date", startDate);
        if (endDate != null)
            jobParametersBuilder.addDate("end_date", endDate);

        if (placementJson != null)
            jobParametersBuilder.addString("campaigns", placementJson);

        jobParametersBuilder.addString("uuid", UUID.randomUUID().toString());

        return launchAppnexusTimezoneDataCostStats(JobName.dataUsageStatsLoadJob,
            jobParametersBuilder.toJobParameters());

    }
  
  private Future<JobExecution> triggerJob(String timezone, List<Long> ioList, Boolean override,
      AppnexusSeat appnexusSeat, Date startDate, Date endDate) {
    
    Calendar cal = Calendar.getInstance();
    cal.setTime(startDate);
    cal.set(Calendar.HOUR_OF_DAY, 0);
    startDate = cal.getTime();
    cal.setTime(endDate);
    cal.set(Calendar.HOUR_OF_DAY, 0);
    endDate = cal.getTime();
    logger.info("Triggering dsp placement stats job with following parameters: ioList:" + ioList
        + ", override:" + override + ", appnexusSeat: " + appnexusSeat + "startDate:" + startDate
        + ", endDate:" + endDate);
    
    String ioJson = gson.toJson(ioList);
    
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addString("appnexus_seat", appnexusSeat.name()).addString("timezone",
        timezone);
    
    if (override)
      jobParametersBuilder.addDate("date", new Date());
    
    if (startDate != null)
      jobParametersBuilder.addDate("start_date", startDate);
    if (endDate != null)
      jobParametersBuilder.addDate("end_date", endDate);
    
    if (ioJson != null)
      jobParametersBuilder.addString("insertion_orders", ioJson);
    
    jobParametersBuilder.addString("uuid", UUID.randomUUID().toString());
    
    return launchAppnexusTimezoneStats("appnexusDailyPlacementCreativeSellerTimeZoneStatsLoadJob",
        jobParametersBuilder.toJobParameters());
    
  }

    public Future<JobExecution> launchAppnexusTimezoneDataCostStats(JobName jobName,
        JobParameters jobParameters) {

        return launchJob(jobName.name(), jobParameters);
    }


    public Future<JobExecution> launchAppnexusTimezoneStats(String jobName,
      JobParameters jobParameters) {
    
    return launchJob(jobName, jobParameters);
  }
  
  @Override
  public Future<JobExecution> launchAppnexusYesterdayPlacementStatsLoadJob(TimeInterval interval,
      Boolean override, AppnexusSeat appnexusSeat) {
    
    logger.debug("In Method: launchAppnexusDailyPlacementStatsLoadJob");
    
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addString("time_interval", interval.name()).addString("appnexus_seat",
        appnexusSeat.name());
    
    if (override)
      jobParametersBuilder.addDate("date", getOlderDate(1));
    
    return launchJob(JobName.appnexusYesterdayPlacementStatsLoadJob.name(),
        jobParametersBuilder.toJobParameters());
  }
  
  private Date getOlderDate(int days) {
    Calendar cal = Calendar.getInstance();
    cal.add(Calendar.DATE, -days);
    Date date = cal.getTime();
    return date;
  }
  
  @Override
  public Future<JobExecution> launchBrowserLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      Date lastModified) {
    
    logger.debug("In Method: launchBrowserLoadJob");
    return launchJobWithStartElement(JobName.browserLoadJob.name(), override, appnexusSeat,
        lastModified, true);
  }
  
  @Override
  public Future<JobExecution> launchCarrierLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      Date lastModified) {
    
    logger.debug("In Method: launchCarrierLoadJob");
    return launchJobWithStartElement(JobName.carrierLoadJob.name(), override, appnexusSeat,
        lastModified, true);
  }
  
  @Override
  public Future<JobExecution> launchDeviceMakeLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      Date lastModified) {
    
    logger.debug("In Method: launchDeviceMakeLoadJob");
    return launchJobWithStartElement(JobName.deviceMakeLoadJob.name(), override, appnexusSeat,
        lastModified, true);
  }
  
  @Override
  public Future<JobExecution> launchDeviceModelLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      Date lastModified) {
    
    logger.debug("In Method: launchDeviceModelLoadJob");
    return launchJobWithStartElement(JobName.deviceModelLoadJob.name(), override, appnexusSeat,
        lastModified, true);
  }
  
  @Override
  public Future<JobExecution> launchMobileAppInstanceListLoadJob(Boolean override,
      AppnexusSeat appnexusSeat, Date lastModified) {
    
    logger.debug("In Method: launchMobileAppInstanceListLoadJob");
    return launchJobWithStartElement(JobName.mobileAppInstanceListLoadJob.name(), override,
        appnexusSeat, lastModified, true);
  }
  
  @Override
  public Future<JobExecution> launchOperatingSystemLoadJob(Boolean override,
      AppnexusSeat appnexusSeat, Date lastModified) {
    
    logger.debug("In Method: launchOperatingSystemLoadJob");
    return launchJobWithStartElement(JobName.operatingSystemLoadJob.name(), override, appnexusSeat,
        lastModified, true);
  }
  
  @Override
  public Future<JobExecution> launchOperatingSystemExtendedLoadJob(Boolean override,
      AppnexusSeat appnexusSeat, Date lastModified) {
    
    logger.debug("In Method: launchOperatingSystemExtendedLoadJob");
    return launchJobWithStartElement(JobName.operatingSystemExtendedLoadJob.name(), override,
        appnexusSeat, lastModified, true);
  }
  
  @Override
  public Future<JobExecution> launchOperatingSystemFamilyLoadJob(Boolean override,
      AppnexusSeat appnexusSeat, Date lastModified) {
    
    logger.debug("In Method: launchOperatingSystemFamilyLoadJob");
    return launchJobWithStartElement(JobName.operatingSystemFamilyLoadJob.name(), override,
        appnexusSeat, lastModified, true);
  }
  
  private Future<JobExecution> launchJobWithStartElement(String name, Boolean override,
      AppnexusSeat appnexusSeat, Date lastModified, Boolean showAugmented) {
    logger.info("In Method: launchJobWithStartElement");
    
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addLong("start_element", 0L).addString("appnexus_seat",
        appnexusSeat.name());
    // if(lastModified==null){
    // throw new IllegalArgumentException();
    // }
    if (lastModified != null)
      jobParametersBuilder.addDate("last_modified", lastModified);
    
    if (override)
      jobParametersBuilder.addDate("date", new Date());
    jobParametersBuilder.addString("show_augmented", showAugmented.toString());
    logger.info(" job name " + name);
    jobParametersBuilder.addString("UUID", UUID.randomUUID().toString());
    return launchJob(name, jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public Future<JobExecution> launchSellerLoadJob(Boolean override, Date lastModified) {
    
    logger.debug("In Method: launchSellerLoadJob");
    return launchJobWithStartElement(JobName.sellerLoadJob.name(), override,
        AppnexusSeat.SEAT_MIQ_1, lastModified, true);
  }
  
  @Override
  public Future<JobExecution> launchDataProviderLoadJob(Boolean override, Date lastModified) {
    logger.debug("In Method: launchSellerLoadJob");
    return launchJobWithStartElement(JobName.dataProvidrLoadJob.name(), override,
        AppnexusSeat.SEAT_MIQ_1, lastModified, true);
  }
  
  protected LocalDate convertToLocalDate(Date date) {
    Instant instant = Instant.ofEpochMilli(date.getTime());
    LocalDate dateLocal = LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).toLocalDate();
    return dateLocal;
  }
  
  @Override
  public Future<JobExecution> launchCreativeLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      Date lastModified) {
    logger.debug("In Method: launchCreativeLoadJob");
    Future<JobExecution> jobExecutionFuture = launchJobWithStartElement(
        JobName.creativeLoadJob.name(), override, appnexusSeat, lastModified, true);
    
    try {
      jobExecutionFuture.get();
    } catch (InterruptedException e) {
      e.printStackTrace();
    } catch (ExecutionException e) {
      e.printStackTrace();
    }
    
    return jobExecutionFuture;
  }
  
  @Override
  public Future<JobExecution> launchCreativeAggregationJob(Date startDate, Date endDate) {
    LocalDate start = startDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    LocalDate end = endDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    
    for (LocalDate localDate = start; localDate.isBefore(end); localDate = localDate.plusDays(1)) {
      logger.info("Launching creative load job for {}", localDate);
      Date date = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
      JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
      jobParametersBuilder.addDate("process_date", date);
      jobParametersBuilder.addDate("date", new Date());
      Future<JobExecution> jobExecutionFuture =
          launchJob(JobName.creativeAggregationJob.name(), jobParametersBuilder.toJobParameters());
      try {
        jobExecutionFuture.get();
      } catch (InterruptedException e) {
        e.printStackTrace();
      } catch (ExecutionException e) {
        e.printStackTrace();
      }
    }
    return null;
    
  }
  
  @Override
  public Future<JobExecution> launchAddClickerSegmentsForNewAdvertisersJob(Boolean override,
      AppnexusSeat appnexusSeat, Date lastModified) {
    
    logger.debug("In Method: launchAddClickerSegmentsForNewAdvertisersJob");
    
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    if (lastModified != null)
      jobParametersBuilder.addDate("last_modified", lastModified);
    
    if (override)
      jobParametersBuilder.addDate("date", new Date());
    
    jobParametersBuilder.addString("appnexus_seat", appnexusSeat.name());
    return launchJob(JobName.createClickerSegmentForNewAdvertisersJob.name(),
        jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public Future<JobExecution> launchUpdateCreativesWithClickerSegmentJob(Boolean override,
      AppnexusSeat appnexusSeat, Date lastModified) {
    logger.debug("In Method: launchUpdateCreativesWithClickerSegmentJob");
    
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    if (lastModified != null)
      jobParametersBuilder.addDate("last_modified", lastModified);
    
    if (override)
      jobParametersBuilder.addDate("date", new Date());
    jobParametersBuilder.addString("appnexus_seat", appnexusSeat.name());
    return launchJob(JobName.updateCreativeJob.name(), jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public Future<JobExecution> launchLanguageLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      Date date) {
    logger.debug("In Method: launchLanguageLoadJob");
    return launchJobWithStartElement(JobName.languageLoadJob.name(), override, appnexusSeat, date,
        true);
  }
  
  @Override
  public Future<JobExecution> launchAppnexusEntityCountLoadJob(boolean override,
      AppnexusSeat appnexusSeat, Date lastModified, Boolean showAugmented) {
    logger.debug("In Method: launchAppnexusEntityCountLoadJob");
    return launchJobWithStartElement(JobName.appnexusEntityCountLoadJob.name(), override,
        appnexusSeat, lastModified, true);
  }
  
  @Override
  public Future<JobExecution> launchMobileAppLoadJob(TimeInterval last7Days, boolean override,
      AppnexusSeat appnexusSeat) {
    logger.debug("In Method: launchMobileAppLoadJob");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    if (override)
      jobParametersBuilder.addDate("date", new Date());
    jobParametersBuilder.addString("appnexus_seat", appnexusSeat.name());
    return launchJob(JobName.mobileAppLoadJob.name(), jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public void launchBuyerSegmentStatLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      Date startDate, Date endDate) throws ExecutionException, InterruptedException {
    logger.info("Starting the buyer segment stats load job for {} to {} and seat {}", startDate,
        endDate, appnexusSeat);
    
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addString("appnexus_seat", appnexusSeat.name());
    if (override)
      jobParametersBuilder.addDate("date", new Date());
    
    if (startDate != null)
      jobParametersBuilder.addDate("start_date", startDate);
    if (endDate != null)
      jobParametersBuilder.addDate("end_date", endDate);
    
    logger.info("Launching job for buyer segment");
    Future<JobExecution> jobExecutionFuture = launchAppnexusTimezoneStats(
        JobName.buyerSegmentStatJob.name(), jobParametersBuilder.toJobParameters());
    jobExecutionFuture.get();
    
  }
  
  @Override
  public Future<JobExecution> launchProfileV3LoadJob(Boolean override, AppnexusSeat appnexusSeat) {
    logger.debug("In Method: launchProfileV3LoadJob");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    if (override)
      jobParametersBuilder.addDate("date", new Date());
    
    jobParametersBuilder.addString("appnexus_seat", appnexusSeat.name());
    jobParametersBuilder.addLong(StepAttribute.START_ELEMENT, 0L);
    return launchJob(JobName.profileLoadJobV3.name(), jobParametersBuilder.toJobParameters());
  }
  
  @Override
  public Future<JobExecution> launchProfileSegmentMappingLoadJob(Boolean override,
      AppnexusSeat appnexusSeat, Date lastModified) {
    logger.debug("In Method: launchProfileV3LoadJob");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    
    jobParametersBuilder.addDate("date", new Date());
    jobParametersBuilder.addString("appnexus_seat", appnexusSeat.name());
    jobParametersBuilder.addLong(StepAttribute.START_ELEMENT, 0l);
    jobParametersBuilder.addString("UUID", UUID.randomUUID().toString());
    if (lastModified != null)
      jobParametersBuilder.addDate("last_modified", lastModified);
    
    Future<JobExecution> jobExecutionFuture = launchJob(JobName.profileSegmentMappingLoadJob.name(),
        jobParametersBuilder.toJobParameters());
    try {
      jobExecutionFuture.get();
    } catch (InterruptedException e) {
      e.printStackTrace();
    } catch (ExecutionException e) {
      e.printStackTrace();
    }
    return jobExecutionFuture;
  }
  
  @Override
  public void launchUpdatePixelToCreativeMapping() {
    logger.debug("In Method: launchUpdatePixelToCreativeMapping");
    
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addDate("date", new Date());
    Future<JobExecution> jobExecutionFuture = launchJob(
        JobName.updatePixelToCreativeMappingJob.name(), jobParametersBuilder.toJobParameters());
    try {
      jobExecutionFuture.get();
    } catch (InterruptedException | ExecutionException e) {
      e.printStackTrace();
    }
  }
  
  @Override
  public void launchMapDpToSegmentsJob() throws ExecutionException, InterruptedException {
    logger.debug("In Method: launchMapDPToSegmentsJob");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    
    jobParametersBuilder.addDate("date", new Date());
    
    Future<JobExecution> jobExecutionFuture = launchJob(JobName.mapDataProviderToSegmentsJob.name(),
        jobParametersBuilder.toJobParameters());
    jobExecutionFuture.get();
    
  }

  @Override
  public void launchSegmentDataPoviderMapping() throws InterruptedException, ExecutionException {
    logger.debug("In Method: launchSegmentDataPoviderMapping");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    
//    jobParametersBuilder.addString("UUID", UUID.randomUUID().toString());
    jobParametersBuilder.addDate("date", new Date());
    
    Future<JobExecution> jobExecutionFuture = launchJob(
        JobName.mapDataProviderToSegment.name(), jobParametersBuilder.toJobParameters());
    
    jobExecutionFuture.get();
    logger.info("completed mapping segments to data provider in segment table");
  }
  
  public Future<JobExecution> launchPlacementCreativeMappingLoadJob(Date lastModified, Boolean override){
    logger.debug("In Method: launchplacementCreativeMappingLoadJob");
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    
    if (lastModified != null) {
      jobParametersBuilder.addDate("last_modified", lastModified);
    } else {
      jobParametersBuilder.addDate("last_modified", getOlderDate(1));
    }

    if (override)
      jobParametersBuilder.addDate("date", new Date());
    
    
    return launchJob(
        JobName.placementCreativeMappingLoadJob.name(), jobParametersBuilder.toJobParameters());
    
  }
  
}
