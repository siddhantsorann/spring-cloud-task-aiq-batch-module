package com.mediaiq.appnexus.batch.tasklet.report.domain;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.batch.tasklet.report.ReportRequestTasklet;
import com.mediaiq.appnexus.domain.report.DateRangeReportRequest;
import com.mediaiq.appnexus.domain.report.ReportFormat;
import com.mediaiq.appnexus.domain.report.ReportRequest;
import com.mediaiq.appnexus.domain.report.ReportType;

@Component
@Scope("step")
public class DspCreativeStatsDailyReportRequestTasklet extends ReportRequestTasklet {
  
  @Value("#{jobParameters['start_date']}")
  private Date startDate;
  
  @Value("#{jobParameters['end_date']}")
  private Date endDate;
  
  @Value("#{jobParameters['time_zone']}")
  private String timeZone;
  
  String[] columns = {"day", "creative_id", "insertion_order_id", "line_item_id", "campaign_id",
      "imps", "clicks", "post_view_convs", "post_click_convs", "total_convs", "ctr", "cost"};
  
  @Override
  protected ReportRequest fetchReportRequest() {
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:MM:SS");
    DateRangeReportRequest reportRequest = new DateRangeReportRequest();
    reportRequest.setReportType(ReportType.network_analytics);
    reportRequest.setStartDate(dateFormat.format(startDate));
    reportRequest.setEndDate(dateFormat.format(endDate));
    reportRequest.setTimezone(timeZone);
    reportRequest.setFormat(ReportFormat.csv);
    reportRequest.setColumns(columns);
    reportRequest.setName("appnexus-daily-dsp-creative-stats");
    return reportRequest;
  }
  
}
