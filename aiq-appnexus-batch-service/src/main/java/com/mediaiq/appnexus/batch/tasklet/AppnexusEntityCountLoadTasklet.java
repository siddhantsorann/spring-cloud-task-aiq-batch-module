package com.mediaiq.appnexus.batch.tasklet;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.batch.value.StepAttribute;
import com.mediaiq.appnexus.domain.AppnexusEntityCount;
import com.mediaiq.appnexus.domain.id.AppnexusEntityCountId;
import com.mediaiq.appnexus.load.client.AppnexusRestClient;
import com.mediaiq.appnexus.load.response.AdvertiserListResponse;
import com.mediaiq.appnexus.load.response.AppnexusProfileListResponse;
import com.mediaiq.appnexus.load.response.BrowserListResponse;
import com.mediaiq.appnexus.load.response.CarrierListResponse;
import com.mediaiq.appnexus.load.response.CountryListResponse;
import com.mediaiq.appnexus.load.response.CreativeListResponse;
import com.mediaiq.appnexus.load.response.DeviceModelListResponse;
import com.mediaiq.appnexus.load.response.InsertionOrderListResponse;
import com.mediaiq.appnexus.load.response.LineItemListResponse;
import com.mediaiq.appnexus.load.response.OperatingSystemListResponse;
import com.mediaiq.appnexus.load.response.PixelListResponse;
import com.mediaiq.appnexus.load.response.PlacementListResponse;
import com.mediaiq.appnexus.load.response.ResponseContainer;
import com.mediaiq.appnexus.load.response.SegmentListResponse;
import com.mediaiq.appnexus.load.response.SellerListsResponse;
import com.mediaiq.appnexus.repo.AppnexusEntityCountRepo;
import com.mediaiq.appnexus.value.AppnexusDate;
import com.mediaiq.appnexus.value.AppnexusSeat;

@Component
@Scope("step")
public class AppnexusEntityCountLoadTasklet implements Tasklet {
  
  
  @Value("#{jobParameters['show_augmented']}")
  protected Boolean showAugmented;
  
  @Value("#{jobParameters['last_modified']}")
  protected Date lastModified;
  
  @Value("#{jobParameters['appnexus_seat']}")
  private AppnexusSeat seat;
  
  private final Logger logger = LoggerFactory.getLogger(this.getClass());
  
  @Autowired
  @Qualifier("JOB")
  private AppnexusRestClient appnexusClient;
  
  @Autowired
  private AppnexusEntityCountRepo appnexusEntityCountRepo;
  
  @Value("#{jobParameters['" + StepAttribute.START_ELEMENT + "']}")
  private Integer startElement;
  
  @Override
  public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext)
      throws Exception {
    ResponseContainer<AdvertiserListResponse> container =
        appnexusClient.getAdvertiserList(startElement, new AppnexusDate(lastModified));
    AdvertiserListResponse response = container.getResponse();
    appnexusEntityCountRepo.save(new AppnexusEntityCount(
        new AppnexusEntityCountId(seat, "advertiser"), response.getCount()));
    logger.info("Saved Advertiser count: {}", response.getCount());
    
    ResponseContainer<CountryListResponse> container2 = appnexusClient.getCountryList(startElement);
    CountryListResponse response2 = container2.getResponse();
    appnexusEntityCountRepo.save(
        new AppnexusEntityCount(new AppnexusEntityCountId(seat, "country"), response2.getCount()));
    logger.info("Saved Country count: {}", response2.getCount());
    
    ResponseContainer<PlacementListResponse> container3 =
        appnexusClient.getPlacementList(startElement, new AppnexusDate(lastModified), true);
    PlacementListResponse response3 = container3.getResponse();
    appnexusEntityCountRepo.save(new AppnexusEntityCount(
        new AppnexusEntityCountId(seat, "placement"), response3.getCount()));
    logger.info("Saved Placement count: {}", response3.getCount());
    
    ResponseContainer<InsertionOrderListResponse> container4 =
        appnexusClient.getInsertionOrderList(startElement, new AppnexusDate(lastModified));
    InsertionOrderListResponse response4 = container4.getResponse();
    appnexusEntityCountRepo.save(new AppnexusEntityCount(
        new AppnexusEntityCountId(seat, "insertion-order"), response4.getCount()));
    logger.info("Saved InsertionOrder count: {}", response4.getCount());
    
    ResponseContainer<LineItemListResponse> container5 =
        appnexusClient.getLineItemList(startElement, new AppnexusDate(lastModified));
    LineItemListResponse response5 = container5.getResponse();
    appnexusEntityCountRepo.save(new AppnexusEntityCount(
        new AppnexusEntityCountId(seat, "line-item"), response5.getCount()));
    logger.info("Saved LineItem count: {}", response5.getCount());
    
    ResponseContainer<PixelListResponse> responseContainer =
        appnexusClient.getPixelList(startElement, new AppnexusDate(lastModified));
    PixelListResponse pixelListResponse = responseContainer.getResponse();
    appnexusEntityCountRepo.save(new AppnexusEntityCount(new AppnexusEntityCountId(seat, "pixel"),
        pixelListResponse.getCount()));
    logger.info("Saved Pixel count: {}", pixelListResponse.getCount());
    
    ResponseContainer<DeviceModelListResponse> container6 =
        appnexusClient.getDeviceModelList(startElement, new AppnexusDate(lastModified));
    DeviceModelListResponse response6 = container6.getResponse();
    appnexusEntityCountRepo.save(new AppnexusEntityCount(
        new AppnexusEntityCountId(seat, "device-model"), response6.getCount()));
    logger.info("Saved DeviceModel count: {}", response6.getCount());
    
    ResponseContainer<BrowserListResponse> container7 =
        appnexusClient.getBrowserList(startElement, new AppnexusDate(lastModified));
    BrowserListResponse response7 = container7.getResponse();
    appnexusEntityCountRepo.save(
        new AppnexusEntityCount(new AppnexusEntityCountId(seat, "browser"), response7.getCount()));
    logger.info("Saved Browser count: {}", response7.getCount());
    
    ResponseContainer<CreativeListResponse> container8 =
        appnexusClient.getCreativeList(startElement, new AppnexusDate(lastModified), 1);
    CreativeListResponse response8 = container8.getResponse();
    appnexusEntityCountRepo.save(
        new AppnexusEntityCount(new AppnexusEntityCountId(seat, "creative"), response8.getCount()));
    logger.info("Saved Creative count: {}", response7.getCount());
    
    ResponseContainer<OperatingSystemListResponse> container9 =
        appnexusClient.getOperatingSystemlList(startElement, new AppnexusDate(lastModified));
    OperatingSystemListResponse response9 = container9.getResponse();
    appnexusEntityCountRepo.save(new AppnexusEntityCount(
        new AppnexusEntityCountId(seat, "operating-system"), response9.getCount()));
    logger.info("Saved OperatingSystem count: {}", response9.getCount());
    
    ResponseContainer<SellerListsResponse> container10 =
        appnexusClient.getSellers(startElement, new AppnexusDate(lastModified));
    SellerListsResponse sellerListResponse = container10.getResponse();
    appnexusEntityCountRepo.save(new AppnexusEntityCount(new AppnexusEntityCountId(seat, "seller"),
        sellerListResponse.getCount()));
    logger.info("Saved Seller count: {}", sellerListResponse.getCount());
    
    ResponseContainer<CarrierListResponse> container11 =
        appnexusClient.getCarrierList(startElement, new AppnexusDate(lastModified));
    CarrierListResponse carrierListResponse = container11.getResponse();
    appnexusEntityCountRepo.save(new AppnexusEntityCount(new AppnexusEntityCountId(seat, "carrier"),
        carrierListResponse.getCount()));
    logger.info("Saved Carrier count: {}", carrierListResponse.getCount());
    
    ResponseContainer<AppnexusProfileListResponse> container12 =
        appnexusClient.getProfileList(startElement, new AppnexusDate(lastModified));
    AppnexusProfileListResponse profileListResponse = container12.getResponse();
    appnexusEntityCountRepo.save(new AppnexusEntityCount(new AppnexusEntityCountId(seat, "profile"),
        profileListResponse.getCount()));
    logger.info("Saved Profile count: {}", profileListResponse.getCount());
    
    ResponseContainer<SegmentListResponse> container13 =
        appnexusClient.getSegmentList(startElement, new AppnexusDate(lastModified));
    SegmentListResponse segmentListResponse = container13.getResponse();
    appnexusEntityCountRepo.save(new AppnexusEntityCount(new AppnexusEntityCountId(seat, "segment"),
        segmentListResponse.getCount()));
    logger.info("Saved Segment count: {}", segmentListResponse.getCount());
    
    return RepeatStatus.FINISHED;
  }
}
