package com.mediaiq.appnexus.batch.tasklet.report;

import java.util.Calendar;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("job")
public class AppnexusPlacementSellerTimezoneStatsReader implements ItemReader<Date> {
  
  final static Logger logger =
      LoggerFactory.getLogger(AppnexusPlacementSellerTimezoneStatsReader.class);
  
  @Value("#{jobParameters['start_date']}")
  protected Date startDate;
  
  @Value("#{jobParameters['end_date']}")
  protected Date endDate;
  
  private Date nextDate;
  private static Calendar calendar = Calendar.getInstance();
  
  @Override
  public Date read()
      throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
    
    logger.info("Starting processor for date: " + nextDate + ", date Range is :" + startDate + "-"
        + endDate);
    
    if (nextDate == null) {
      nextDate = startDate;
    } else {
      calendar.setTime(nextDate);
      calendar.add(Calendar.DATE, 1);
      nextDate = calendar.getTime();
      if (nextDate.equals(endDate)) {
        return null;
      }
    }
    logger.info("Returning date:" + nextDate);
    return nextDate;
  }
  
}
