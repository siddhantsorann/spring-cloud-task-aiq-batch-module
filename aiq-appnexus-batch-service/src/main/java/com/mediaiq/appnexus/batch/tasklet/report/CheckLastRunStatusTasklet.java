package com.mediaiq.appnexus.batch.tasklet.report;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.spring.batch.domain.StatsJobLastRunHour;
import com.mediaiq.spring.batch.repo.StatsJobLastRunHourRepo;

@Component
@Scope("step")
public class CheckLastRunStatusTasklet implements Tasklet {
  
  final static Logger logger = LoggerFactory.getLogger(CheckLastRunStatusTasklet.class);
  
  private static final String APPNEXUS_DAILY_PLACEMENT_STATS_JOB =
      "APPNEXUS_DAILY_PLACEMENT_STATS_JOB";
  
  @Autowired
  StatsJobLastRunHourRepo statsJobLastRunHourRepo;
  
  @Value("#{jobParameters['date']}")
  private Date date;
  
  @Override
  public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext)
      throws Exception {
    
    logger.info("Check if new stats are ready...");
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    StatsJobLastRunHour lastRunInfo = statsJobLastRunHourRepo.findByIdDateAndIdJobName(
        simpleDateFormat.parse(simpleDateFormat.format(date)), APPNEXUS_DAILY_PLACEMENT_STATS_JOB);
    
    // Get from db, last run hour. If its 23, we have pulled all the data
    // for yesterday.
    if (lastRunInfo != null && lastRunInfo.getMaxHour() == 23) {
      logger.info("All the stats are pulled for date " + date + ". Stopping...");
      contribution.setExitStatus(ExitStatus.STOPPED);
    } else {
      contribution.setExitStatus(ExitStatus.COMPLETED);
    }
    
    return RepeatStatus.FINISHED;
    
  }
  
}
