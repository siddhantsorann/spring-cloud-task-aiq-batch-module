package com.mediaiq.appnexus.batch;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.mediaiq.appnexus.value.AppnexusSeat;
import com.mediaiq.appnexus.value.IoTimezone;
import com.mediaiq.appnexus.value.IoTimezoneMapper;


@Repository
public class IoDao {
  
  @Autowired
  NamedParameterJdbcTemplate namedParameterJdbcTemplate;
  
  public List<IoTimezone> findIoTimeList(List<Long> ioList) {
    Map<String, Object> parameters = new HashMap<String, Object>();
    
    parameters.put("ioList", ioList);
    
    return namedParameterJdbcTemplate.query(
        "Select io.id as id, io.timezone as timezone from appnexus.insertion_order  io where io.id in (:ioList)",
        parameters, new IoTimezoneMapper());
    
  }
  
  public List<IoTimezone> findActiveIosWithTiemzone(AppnexusSeat appnexusSeat) {
    Map<String, Object> parameters = new HashMap<String, Object>();
    parameters.put("seat", appnexusSeat);
    
    return namedParameterJdbcTemplate.query(
        "SELECT io.id as id, io.timezone as timezone from appnexus.insertion_order io where io.seat=:seat",
        parameters, new IoTimezoneMapper());
  }
}
