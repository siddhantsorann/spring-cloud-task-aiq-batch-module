package com.mediaiq.appnexus.batch.tasklet;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.domain.Country;
import com.mediaiq.appnexus.load.client.AppnexusRestClient;
import com.mediaiq.appnexus.load.response.CountryListResponse;
import com.mediaiq.appnexus.load.response.ResponseContainer;
import com.mediaiq.appnexus.repo.CountryRepo;

@Component
@Scope("step")
public class CountryLoadTasklet
    extends AbstractPageLoadTasklet<Country, CountryRepo, CountryListResponse> {
  
  @Autowired
  private CountryRepo countryRepo;
  
  @Override
  public List<Country> getItemsFromResponse(CountryListResponse response) {
    return response.getCountries();
  }
  
  @Override
  public ResponseContainer<CountryListResponse> getPageableResponse(
      AppnexusRestClient appnexusClient) {
    return appnexusClient.getCountryList(startElement);
  }
  
  @Override
  public CountryRepo getRepository() {
    return countryRepo;
  }
  
}
