package com.mediaiq.appnexus.batch.tasklet;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecutionException;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.repo.CreativeRepo;
import com.mediaiq.appnexus.repo.DailyPlacementCreativeStatsRepo;

@Component
@Scope("job")
public class UpdatePlacementCreativeStatsTasklet implements Tasklet {
  
  private static final Logger logger =
      LoggerFactory.getLogger(UpdatePlacementCreativeStatsTasklet.class);
  
  @Value("#{jobParameters['process_date']}")
  protected Date date;
  
  @Autowired
  private CreativeRepo creativeRepo;
  
  @Autowired
  private DailyPlacementCreativeStatsRepo dailyPlacementCreativeStatsRepo;
  
  @Override
  public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext)
      throws Exception {
    
    try {
      logger.info("Start deletion of creative stats for : {} ", date);
      dailyPlacementCreativeStatsRepo.deleteByIdDate(date);
      dailyPlacementCreativeStatsRepo.flush();
      logger.info("Deletion finished");
    } catch (Exception e) {
      logger.error("Deletion of creative stats failed for the date {} ", date);
      throw new JobExecutionException("Deletion of creative stats failed due to ", e);
    }
    
    try {
      logger.info("Start insertion of new creative stats for : {} ", date);
      creativeRepo.updateDailyCreativeStats(date);
      logger.info("Insertion finished");
    } catch (Exception e) {
      logger.error("Insertion of Daily Creative stats failed for the date {} ", date);
      throw new JobExecutionException("Creative stats load job failed due to ", e);
    }
    
    return RepeatStatus.FINISHED;
  }
}
