package com.mediaiq.appnexus.batch.chunk.mapper;

import java.util.Date;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

public class StatsMaxHourMapper implements FieldSetMapper<Date> {
  
  @Override
  public Date mapFieldSet(FieldSet set) throws BindException {
    
    Date hour = set.readDate("hour", "yyyy-MM-dd HH:mm");
    return hour;
  }
}
