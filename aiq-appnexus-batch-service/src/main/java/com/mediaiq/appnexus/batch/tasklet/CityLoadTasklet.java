package com.mediaiq.appnexus.batch.tasklet;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.domain.City;
import com.mediaiq.appnexus.load.client.AppnexusRestClient;
import com.mediaiq.appnexus.load.response.CityListResponse;
import com.mediaiq.appnexus.load.response.ResponseContainer;
import com.mediaiq.appnexus.repo.CityRepo;

@Component
@Scope("step")
public class CityLoadTasklet implements Tasklet {
  
  @Autowired
  @Qualifier("JOB")
  private AppnexusRestClient appnexusClient;
  
  
  @Autowired
  private CityRepo cityRepo;
  
  @Value("#{jobParameters['country_code']}")
  private String countryCode;
  
  private static final Logger logger = LoggerFactory.getLogger(CityLoadTasklet.class);
  
  @Override
  public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext)
      throws Exception {
    ResponseContainer<CityListResponse> responseContainer = appnexusClient.getCityList(countryCode);
    List<City> cities = responseContainer.getResponse().getCities();
    logger.debug("Loaded city list for @country " + countryCode + " @count " + cities.size());
    cityRepo.save(cities);
    return RepeatStatus.FINISHED;
  }
  
}
