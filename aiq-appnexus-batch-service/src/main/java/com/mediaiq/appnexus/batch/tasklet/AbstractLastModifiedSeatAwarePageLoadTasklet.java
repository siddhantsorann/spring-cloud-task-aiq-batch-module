package com.mediaiq.appnexus.batch.tasklet;

import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.JpaRepository;

import com.mediaiq.appnexus.load.response.AbstractPagableResponse;
import com.mediaiq.appnexus.value.HasAppnexusSeat;

public abstract class AbstractLastModifiedSeatAwarePageLoadTasklet<TYPE extends HasAppnexusSeat, REPO extends JpaRepository<TYPE, ?>, RESPONSE extends AbstractPagableResponse>
    extends AbstractSeatAwarePageLoadTasklet<TYPE, REPO, RESPONSE> {
  
  @Value("#{jobParameters['last_modified']}")
  protected Date lastModified;
  
  public void setLastModified(@Value("#{jobParameters['last_modified']}") Date lastModified) {
    this.lastModified = lastModified;
  }
  
}
