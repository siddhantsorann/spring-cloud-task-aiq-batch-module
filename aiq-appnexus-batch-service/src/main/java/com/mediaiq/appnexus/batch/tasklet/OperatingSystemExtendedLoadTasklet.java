package com.mediaiq.appnexus.batch.tasklet;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.domain.OperatingSystemExtended;
import com.mediaiq.appnexus.load.client.AppnexusRestClient;
import com.mediaiq.appnexus.load.response.OperatingSystemExtendedListResponse;
import com.mediaiq.appnexus.load.response.ResponseContainer;
import com.mediaiq.appnexus.repo.OperatingSystemExtendedRepo;
import com.mediaiq.appnexus.value.AppnexusDate;

@Component
@Scope("step")
public class OperatingSystemExtendedLoadTasklet extends
    AbstractLastModifiedSeatAwarePageLoadTasklet<OperatingSystemExtended, OperatingSystemExtendedRepo, OperatingSystemExtendedListResponse> {
  
  @Autowired
  private OperatingSystemExtendedRepo operatingSystemExtendedRepo;
  
  @Override
  public ResponseContainer<OperatingSystemExtendedListResponse> getPageableResponse(
      AppnexusRestClient appnexusClient) {
    return appnexusClient.getOperatingSystemExtendedList(startElement,
        new AppnexusDate(lastModified));
  }
  
  @Override
  public OperatingSystemExtendedRepo getRepository() {
    return operatingSystemExtendedRepo;
  }
  
  @Override
  public List<OperatingSystemExtended> getItemsFromResponse(
      OperatingSystemExtendedListResponse response) {
    return response.getOperatingSystemsExtended();
  }
}
