package com.mediaiq.appnexus.batch.tasklet;

import java.io.IOException;
import java.sql.*;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.Date;
import java.util.stream.Collectors;

import com.mediaiq.aiq.domain.Dsp;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.mediaiq.appnexus.domain.Creative;
import com.mediaiq.appnexus.domain.InsertionOrder;
import com.mediaiq.appnexus.domain.LineItem;
import com.mediaiq.appnexus.domain.Placement;
import com.mediaiq.appnexus.repo.InsertionOrderRepo;
import com.mediaiq.appnexus.repo.LineItemRepo;
import com.mediaiq.appnexus.repo.PlacementRepo;
import com.mediaiqdigital.aiq.tradingcenter.domain.PlacementCreativeMapping;
import com.mediaiqdigital.aiq.tradingcenter.repo.PlacementCreativeMappingRepo;

/**
 * Created by prasannachidire on 2/11/17.
 *
 * Maintains the mapping between placement and creative ids from placement-creative table and
 * lineitem-creative table as well.
 */

@Component
@Scope("step")
public class PlacementCreativeMappingLoadTasklet implements Tasklet {
  
  private final Logger LOGGER = LoggerFactory.getLogger(PlacementCreativeMappingLoadTasklet.class);
  
  @Value("#{jobParameters['last_modified']}")
  protected Date lastModified;

  Logger logger = LoggerFactory.getLogger(PlacementCreativeMappingLoadTasklet.class);

  @Autowired JdbcTemplate jdbcTemplate;

  final private String sqlFilePathForAddedPlacements =
        "placement-creative-sql-files/placement-creative-added-for-apn.sql";


  @Autowired
  PlacementRepo placementRepo;

  @Autowired
  LineItemRepo lineItemRepo;

  @Autowired
  InsertionOrderRepo insertionOrderRepo;
  @Autowired
  PlacementCreativeMappingRepo placementCreativeMappingRepo;
  
  @Override
  public RepeatStatus execute(StepContribution arg0, ChunkContext arg1) throws Exception {
    
    
    if (lastModified == null) {
      
      lastModified =
          Date.from(LocalDate.now().minusDays(1).atStartOfDay(ZoneId.systemDefault()).toInstant());
      
    }
    
    List<Placement> placements = placementRepo.findByLastModifiedGreaterThanEqual(lastModified);
    List<LineItem> lineItems = lineItemRepo.findByLastModifiedGreaterThanEqual(lastModified);
    if (!CollectionUtils.isEmpty(lineItems)) {
      List<String> lineItemIdStrings =
          lineItems.stream().map(li -> li.getId().toString()).collect(Collectors.toList());
      Map<Long, LineItem> lineItemIdMap = new HashMap<>();
      List<Placement> placementsFromLI = placementRepo.findByLineItemIdIn(lineItemIdStrings);
      // createMappings(placementsFromLI);
      placements.addAll(placementsFromLI);
    }
    
    createMappings(placements);
    return RepeatStatus.FINISHED;
  }
  
  public void createMappings(List<Placement> placements) throws IOException {
    if (!CollectionUtils.isEmpty(placements)) {
      LOGGER.info("Found {} placements", placements.size());
      List<Long> placementIds =
          placements.stream().map(Placement::getId).collect(Collectors.toList());

    LOGGER.info("New placement creative mapping added ");
    placementCreativeMappingRepo.insertNewPlacementCreativeMappingsForAppnexus(placementIds);


    LOGGER.info("Updating placement creative mapping from placement & lineitem");
    placementCreativeMappingRepo.updateEndDateForRemovedPlacementCreativeMappingsForAppnexus(lastModified);

     } else {
      LOGGER.info("No placements to create mappings");
    }
  }
}
