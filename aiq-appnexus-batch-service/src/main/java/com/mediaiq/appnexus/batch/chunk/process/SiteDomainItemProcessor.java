package com.mediaiq.appnexus.batch.chunk.process;

import java.io.File;
import java.util.Map;

import org.mapdb.DBMaker;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.domain.SiteDomain;

@Component
public class SiteDomainItemProcessor
    implements ItemProcessor<SiteDomain, SiteDomain>, InitializingBean {
  
  @Value("${mapDbDirectory}")
  private String mapDbDirectory;
  
  Map<String, SiteDomain> sitedomainMap;
  
  @Override
  public SiteDomain process(SiteDomain item) throws Exception {
    if (!sitedomainMap.containsKey(item.getName())) {
      sitedomainMap.put(item.getName(), item);
      return item;
    } else if (item.getPlatformAuditedImpressions() < sitedomainMap.get(item.getName())
        .getPlatformAuditedImpressions())
      return null;
    else
      sitedomainMap.put(item.getName(), item);
    return item;
  }
  
  @Override
  public void afterPropertiesSet() throws Exception {
    
    File mapDbDirPath = new File(mapDbDirectory);
    File createTempFile = File.createTempFile("mapdb-temp", "db", mapDbDirPath);
    
    sitedomainMap = DBMaker.newFileDB(createTempFile).deleteFilesAfterClose().closeOnJvmShutdown()
        .transactionDisable().make().getHashMap("temp");
    
  }
}
