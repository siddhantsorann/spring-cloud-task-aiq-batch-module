package com.mediaiq.appnexus.batch.service;

import java.util.Date;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.springframework.batch.core.JobExecution;

import com.mediaiq.appnexus.domain.report.TimeInterval;
import com.mediaiq.appnexus.value.AppnexusSeat;

public interface AppnexusBatchService {
  
  Future<JobExecution> launchCityLoadJob(String countryCode, Boolean override,
      AppnexusSeat appnexusSeat);
  
  Future<JobExecution> launchCountryLoadJob(Boolean override, AppnexusSeat appnexusSeat);
  
  Future<JobExecution> launchAllCityLoadJob(Boolean override, AppnexusSeat appnexusSeat);
  
  Future<JobExecution> launchRegionLoadJob(Boolean override, AppnexusSeat appnexusSeat);
  
  Future<JobExecution> launchAdvertiserLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      Date lastModified);
  
  Future<JobExecution> launchContentCategoryLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      Date lastModified);
  
  Future<JobExecution> launchDomainListLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      Date lastModified);
  
  Future<JobExecution> launchInsertionOrderLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      Date lastModified);
  
  Future<JobExecution> launchLineItemLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      Date lastModified);
  
  Future<JobExecution> launchPixelLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      Date lastModified);
  
  Future<JobExecution> launchPlacementLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      Date lastModified, Boolean showAugmented);
  
  Future<JobExecution> launchSegmentLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      Date lastModified);
  
  Future<JobExecution> launchSiteDomainReportLoadJob(Boolean override, AppnexusSeat appnexusSeat);
  
  
  void launchAppnexusDailyPlacementTimeZoneStatsLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      Date startDate, Date endDate, String timeZone);
  
  void launchDataUsageStatsLoadJob(AppnexusSeat appnexusSeat, Date startDate);
  
  Future<JobExecution> launchAppnexusYesterdayPlacementStatsLoadJob(TimeInterval interval,
      Boolean override, AppnexusSeat appnexusSeat);
  
  Future<JobExecution> launchBrowserLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      Date lastModified);
  
  Future<JobExecution> launchCarrierLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      Date lastModified);
  
  Future<JobExecution> launchDeviceMakeLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      Date lastModified);
  
  Future<JobExecution> launchDeviceModelLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      Date lastModified);
  
  Future<JobExecution> launchMobileAppInstanceListLoadJob(Boolean override,
      AppnexusSeat appnexusSeat, Date lastModified);
  
  Future<JobExecution> launchOperatingSystemLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      Date lastModified);
  
  Future<JobExecution> launchOperatingSystemExtendedLoadJob(Boolean override,
      AppnexusSeat appnexusSeat, Date lastModified);
  
  Future<JobExecution> launchOperatingSystemFamilyLoadJob(Boolean override,
      AppnexusSeat appnexusSeat, Date lastModified);
  
  Future<JobExecution> launchSellerLoadJob(Boolean override, Date lastModified);
  
  Future<JobExecution> launchDataProviderLoadJob(Boolean override, Date lastModified);
  
  Future<JobExecution> launchCreativeLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      Date lastModified);
  
  Future<JobExecution> launchCreativeAggregationJob(Date startDate, Date endDate);
  
  Future<JobExecution> launchAddClickerSegmentsForNewAdvertisersJob(Boolean override,
      AppnexusSeat appnexusSeat, Date date);
  
  Future<JobExecution> launchUpdateCreativesWithClickerSegmentJob(Boolean override,
      AppnexusSeat appnexusSeat, Date date);
  
  Future<JobExecution> launchLanguageLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      Date date);
  
  Future<JobExecution> launchAppnexusEntityCountLoadJob(boolean b, AppnexusSeat seat,
      Date olderDate, Boolean showAugmented);
  
  Future<JobExecution> launchMobileAppLoadJob(TimeInterval last7Days, boolean b, AppnexusSeat seat);
  
  void launchBuyerSegmentStatLoadJob(Boolean override, AppnexusSeat appnexusSeat, Date startDate,
      Date endDate) throws ExecutionException, InterruptedException;
  
  Future<JobExecution> launchProfileV3LoadJob(Boolean override, AppnexusSeat appnexusSeat);
  
  Future<JobExecution> launchProfileSegmentMappingLoadJob(Boolean override,
      AppnexusSeat appnexusSeat, Date lastModified);
  
  void launchMapDpToSegmentsJob() throws ExecutionException, InterruptedException;
  
  void launchUpdatePixelToCreativeMapping();

  void launchSegmentDataPoviderMapping() throws InterruptedException, ExecutionException;
  
  Future<JobExecution> launchPlacementCreativeMappingLoadJob(Date lastModified, Boolean override);
  
}
