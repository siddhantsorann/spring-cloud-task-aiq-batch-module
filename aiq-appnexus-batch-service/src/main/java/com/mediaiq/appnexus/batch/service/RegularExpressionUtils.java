package com.mediaiq.appnexus.batch.service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

/**
 * Created by naveen on 4/4/17.
 */
public class RegularExpressionUtils {
  
  private static final Pattern DOUBLECLICKPATTERN = Pattern.compile(
      "(([http://ad.doubleclick.net|https//:ad.doubleclick.net])|([http://fw.adsafeprotected.com]))([0-9]*);");
  private static final Pattern DCMINS = Pattern.compile("([0-9])+('|\\\\)");
  private static final Pattern FLASHTALKING = Pattern.compile("ftExpTrack_*.([0-9]*) =");
  private static final Pattern ALTAS = Pattern.compile("p=*([0-9]*);");
  private static final Pattern WEBORAMA = Pattern.compile("tracking_element_id  :.([0-9]*)\n");
  private static final Pattern ADITION = Pattern.compile("wp_id=([0-9]*)&");
  private static final Pattern BURSTINGPIPE = Pattern.compile("pli=([0-9]*)&");
  private static final Pattern ADFORM = Pattern.compile("bn=([0-9]*)");
  private static final Pattern EBAY = Pattern.compile("([0-9]){3,100}\\/");
  private static final Pattern QSERVZ = Pattern.compile("\\/([0-9]*)\\?");
  private static final Pattern SIZEM1 = Pattern.compile("DFA_([0-9]*)");
  private static final Pattern SIZEM2 = Pattern.compile("DCM_([0-9]*)");
  private static final Pattern SIZEM3 = Pattern.compile("DFA_DCM_([0-9]*)");
  
  public static String findPattern(String tagScript) {
    
    if (tagScript.contains("https://bs.serving-sys.com")) {
      Matcher matcher = BURSTINGPIPE.matcher(tagScript);
      if (matcher.find()) {
        String sanitized = StringUtils.remove(matcher.group(0), "pli=");
        String formatted = StringUtils.remove(sanitized, "&");
        return formatted;
      }
    }
    
    if (tagScript.contains("https://ad.doubleclick.net")
        || tagScript.contains("http://fw.adsafeprotected.com")) {
      Matcher matcher = DOUBLECLICKPATTERN.matcher(tagScript);
      if (matcher.find()) {
        return matcher.group(1).replace(".", "").replace(";", "");
      }
    }
    
    if (tagScript.contains("data-dcm-placement")) {
      Matcher matcher = DCMINS.matcher(tagScript);
      if (matcher.find()) {
        return matcher.group(0).replace("'", "").replace("\\", "");
      }
    }
    
    if (tagScript.contains("https://servedby.flashtalking.com")) {
      Matcher matcher = FLASHTALKING.matcher(tagScript);
      if (matcher.find()) {
        String sanitized = StringUtils.remove(matcher.group(0), "ftExpTrack_");
        String formatted = StringUtils.remove(sanitized, " =");
        return formatted.trim();
      }
    }
    
    if (tagScript.contains("https://ad.atdmt.com")) {
      Matcher matcher = ALTAS.matcher(tagScript);
      if (matcher.find()) {
        return matcher.group(0).replace("p=", "").replace(";", "");
      }
    }
    
    if (tagScript.contains("http://tearfund.solution.weborama")) {
      Matcher matcher = WEBORAMA.matcher(tagScript);
      if (matcher.find()) {
        String sanitized = StringUtils.remove(matcher.group(0), "tracking_element_id  :");
        return sanitized.trim();
      }
    }
    
    if (tagScript.contains("https://imagesrv.adition.com")) {
      Matcher matcher = ADITION.matcher(tagScript);
      if (matcher.find()) {
        String format = StringUtils.remove(matcher.group(0), "wp_id=");
        String creativeId = StringUtils.remove(format, "&");
        return creativeId;
      }
    }
    
    if (tagScript.contains("https://track.adform.net")) {
      Matcher matcher = ADFORM.matcher(tagScript);
      if (matcher.find()) {
        String sanitized = StringUtils.remove(matcher.group(0), "bn=");
        String formatted = StringUtils.remove(sanitized, "^\"|\"$");
        return formatted;
      }
    }
    
    if (tagScript.contains("https://rover.ebay.com/rover")) {
      Matcher matcher = EBAY.matcher(tagScript);
      if (matcher.find()) {
        String split = StringUtils.remove(matcher.group(0), "/");
        return split.trim();
      }
    }
    
    if (tagScript.contains("https://tags.qservz.com")) {
      Matcher matcher = QSERVZ.matcher(tagScript);
      if (matcher.find()) {
        String format = StringUtils.remove(matcher.group(0), "?");
        String creativeId = StringUtils.remove(format, "/");
        return creativeId;
      }
    }
    
    return StringUtils.EMPTY;
  }
  
  public static String findPatternInName(String name){

    if (name.contains("DFA_")) {
        Matcher matcher = SIZEM1.matcher(name);
        String result = matcher.find() ? StringUtils.remove(matcher.group(0), "DFA_") : StringUtils.EMPTY;
        return result;
    }

    if (name.contains("DCM_")) {
        Matcher matcher = SIZEM2.matcher(name);
        String result = matcher.find() ? StringUtils.remove(matcher.group(0), "DCM_") : StringUtils.EMPTY;
        return result;
    }

    if (name.contains("DFA_DCM_")) {
        Matcher matcher = SIZEM3.matcher(name);
        String result = matcher.find() ? StringUtils.remove(matcher.group(0), "DFA_DCM_") : StringUtils.EMPTY;
        return result;
    }

    return "";
}
}
