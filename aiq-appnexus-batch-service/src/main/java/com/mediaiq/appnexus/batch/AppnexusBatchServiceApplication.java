package com.mediaiq.appnexus.batch;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.cloud.task.configuration.EnableTask;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTask
@EnableScheduling
@EnableIntegration
@ComponentScan(basePackages = {"com.mediaiq.appnexus.load", "com.mediaiqdigital.tc.batch",
    "com.mediaiq.appnexus.batch", "com.mediaiq.spring.batch", "com.mediaiq.aiq.miq.service",
    "com.mediaiq.appnexus.dao", "com.mediaiqdigital.aiq.tradingcenter.repo.impl",
    "com.mediaiq.appnexus.batch.tasklet"})
@EntityScan(basePackages = {"com.mediaiq.spring.batch.domain", "com.mediaiq.appnexus.domain",
    "com.mediaiq.aiq.miq.domain", "com.mediaiq.aiq.mediamath.domain",
    "com.mediaiqdigital.aiq.tradingcenter.domain", "com.mediaiqdigital.doubleclick.domain"})
@EnableJpaRepositories(basePackages = {"com.mediaiq.spring.batch.repo", "com.mediaiq.appnexus.repo",
    "com.mediaiq.aiq.miq.repo", "com.mediaiq.aiq.mediamath.repo",
    "com.mediaiq.appnexus.batch.chunk.mapper", "com.mediaiqdigital.aiq.tradingcenter.repo",
    "com.mediaiqdigital.doubleclick.repo"})
@EnableMongoRepositories(basePackages = "com.mediaiq.appnexus.mongo.repo")
@ImportResource({"classpath:spring-integration-config.xml", "classpath:spring-batch-config.xml",
    "appnexus-batch-jobs.xml", "classpath:spring-batch-retry-config.xml"})
@EnableResourceServer
@EnableTransactionManagement
public class AppnexusBatchServiceApplication {
  
  public static void main(String[] args) {
    new SpringApplicationBuilder(AppnexusBatchServiceApplication.class).web(true).run(args);
  }
  
  @Bean
  public static PropertySourcesPlaceholderConfigurer properties() {
    PropertySourcesPlaceholderConfigurer configurer = new PropertySourcesPlaceholderConfigurer();
    configurer.setIgnoreUnresolvablePlaceholders(true);
    return configurer;
  }
  
}
