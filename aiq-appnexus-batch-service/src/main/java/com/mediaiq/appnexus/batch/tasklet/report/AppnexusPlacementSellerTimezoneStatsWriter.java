package com.mediaiq.appnexus.batch.tasklet.report;

import java.util.List;

import org.springframework.batch.item.ItemWriter;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("job")
public class AppnexusPlacementSellerTimezoneStatsWriter implements ItemWriter<String> {
  
  @Override
  public void write(List<? extends String> arg0) throws Exception {
    
  }
  
  
}
