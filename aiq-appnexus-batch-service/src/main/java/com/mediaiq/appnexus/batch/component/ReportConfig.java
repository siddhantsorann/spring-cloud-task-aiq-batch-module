package com.mediaiq.appnexus.batch.component;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.mediaiq.appnexus.domain.report.ReportRequest;
import com.mediaiq.appnexus.domain.report.ReportType;
import com.mediaiq.appnexus.domain.report.TimeInterval;

@Configuration
public class ReportConfig {
  
  @Bean
  public ReportRequest provideReportRequest() {
    List<String> columns = new ArrayList<>();
    columns.add("day");
    columns.add("imps");
    columns.add("clicks");
    ReportRequest reportRequest =
        new ReportRequest("test", ReportType.network_analytics, columns, TimeInterval.last_7_days);
    return reportRequest;
  }
}
