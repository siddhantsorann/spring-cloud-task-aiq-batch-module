package com.mediaiq.appnexus.batch.listener;

import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.batch.value.StepAttribute;

@Component
public class PagingAwareStepExecutionListener implements StepExecutionListener {
  
  @Override
  public void beforeStep(StepExecution stepExecution) {
    // check if step execution context has startElement and numElement,
    // if no populate them
    ExecutionContext context = stepExecution.getExecutionContext();
    
    // populate from job execution context
    JobExecution jobExecution = stepExecution.getJobExecution();
    ExecutionContext jobExecutionContext = jobExecution.getExecutionContext();
    
    Integer startElement = null;
    if (jobExecutionContext.containsKey(StepAttribute.START_ELEMENT))
      startElement = jobExecutionContext.getInt(StepAttribute.START_ELEMENT);
    
    // else populate from job parameters
    if (startElement == null) {
      JobParameters jobParameters = jobExecution.getJobParameters();
      startElement =
          Integer.parseInt(jobParameters.getLong(StepAttribute.START_ELEMENT).toString());
    }
    
    context.putInt(StepAttribute.START_ELEMENT, startElement);
  }
  
  @Override
  public ExitStatus afterStep(StepExecution stepExecution) {
    
    if (stepExecution.getStatus().equals(BatchStatus.FAILED)
        || stepExecution.getStatus().equals(BatchStatus.ABANDONED)) {
      return stepExecution.getExitStatus();
    }
    
    // check startElement, numElement and count, and return relevant status
    ExecutionContext context = stepExecution.getExecutionContext();
    Object startElement = context.get(StepAttribute.START_ELEMENT);
    if (startElement == null) {
      System.out.println("startElement is null. Task finished.");
      return new ExitStatus("END");
    }
    
    Object count = context.get(StepAttribute.COUNT);
    if (count == null) {
      System.out.println("Count is null. Task finished.");
      return new ExitStatus("END");
    }
    
    Integer numElements = context.getInt(StepAttribute.NUM_ELEMENTS);
    Integer remainingElements = (Integer) count - ((Integer) startElement + numElements);
    
    if (remainingElements <= 0) {
      System.out.println("Task finished.");
      return new ExitStatus("END");
    }
    
    // update execution context
    // context.putInt(StepAttribute.START_ELEMENT, startElement +
    // numElements);
    stepExecution.getJobExecution().getExecutionContext().putInt(StepAttribute.START_ELEMENT,
        ((Integer) startElement + numElements));
    
    return new ExitStatus("CONTINUE (Items Remaining: " + remainingElements + ")");
  }
}
