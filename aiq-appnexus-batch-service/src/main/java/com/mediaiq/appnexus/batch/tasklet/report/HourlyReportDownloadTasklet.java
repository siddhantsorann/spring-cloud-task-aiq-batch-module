package com.mediaiq.appnexus.batch.tasklet.report;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.batch.value.StepAttribute;
import com.mediaiq.appnexus.load.client.AppnexusRestClient;

@Component
@Scope("step")
public class HourlyReportDownloadTasklet implements Tasklet {
  final static Logger logger = LoggerFactory.getLogger(HourlyReportDownloadTasklet.class);
  
  @Value("#{jobExecutionContext['" + StepAttribute.REPORT_ID + "']}")
  private String reportId;
  
  @Value("#{jobExecutionContext['" + StepAttribute.REPORT_NAME + "']}")
  private String reportName;
  
  @Value("${report.directory}")
  private String reportDir;
  
  @Autowired
  @Qualifier("JOB")
  private AppnexusRestClient appnexusRestClient;
  
  @Override
  public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext)
      throws Exception {
    
    Long startTime = System.currentTimeMillis();
    String fileTarget =
        reportDir + System.getProperty("file.separator") + "report-" + reportName + ".csv";
    InputStream inputStream = appnexusRestClient.getReport(reportId).getBody().in();
    
    OutputStream outputStream = new FileOutputStream(fileTarget);
    IOUtils.copy(inputStream, outputStream);
    Long endTime = System.currentTimeMillis();
    ExecutionContext executionContext =
        chunkContext.getStepContext().getStepExecution().getJobExecution().getExecutionContext();
    executionContext.put(StepAttribute.FILE_PATH, fileTarget);
    logger.debug("Time taken::" + (endTime - startTime));
    logger.info("Report {} downloaded.", reportName);
    return RepeatStatus.FINISHED;
  }
}
