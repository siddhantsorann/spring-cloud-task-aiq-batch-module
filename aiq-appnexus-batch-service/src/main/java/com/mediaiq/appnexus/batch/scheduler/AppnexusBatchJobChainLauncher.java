package com.mediaiq.appnexus.batch.scheduler;

import java.util.concurrent.ExecutionException;

public interface AppnexusBatchJobChainLauncher {
  
  void launchGeoLoadJob() throws InterruptedException, ExecutionException;
  
  void launchDomainListLoadJob();
  
  void launchContentCategoryLoadJob();
  
  void launchSiteDomainReportLoadJob();
  
  void launchAdvertiserMetadataLoadJob() throws InterruptedException, ExecutionException;
  
  void launchSegmentLoadJob();
  
  void launchBrowserLoadJob();
  
  void launchLanguageLoadJob();
  
  void launchMobileAppLoadJob();
  
  void launchDataUsageLoadJob();
  
  void launchOSLoadJob() throws InterruptedException, ExecutionException;
  
  void launchCarrierLoadJob();
  
  void launchDeviceLoadJob() throws InterruptedException, ExecutionException;
  
  
  void launchMobileAppInstanceListLoadJob();
  
  void launchRawStatsJob() throws ExecutionException, InterruptedException;
  
  void launchCreativeAggregation();
  
  void launchAppnexusEntityCountLoadJob();
  
  void launchProfileToSegmentMappingLoadJob();
  
  void launchProfileV3LoadJob();
  
  void launchMapDpToSegmentsJob() throws InterruptedException, ExecutionException;
  
  void launchUpdatePixelToCreativeMapping();

  void launchPlacementToCreativeMappingJob();
  
  void launchSegmentDataPoviderMapping() throws InterruptedException, ExecutionException;
  
}
