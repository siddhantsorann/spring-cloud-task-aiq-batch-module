package com.mediaiq.appnexus.batch.tasklet.report;

import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.util.CollectionUtils;

import com.mediaiq.appnexus.batch.value.StepAttribute;
import com.mediaiq.appnexus.domain.report.ReportRequest;
import com.mediaiq.appnexus.load.config.AppnexusRestClientFactory;
import com.mediaiq.appnexus.load.config.AppnexusRestClientSelector;
import com.mediaiq.appnexus.load.request.ReportRequestContainer;
import com.mediaiq.appnexus.load.response.ReportRequestResponse;
import com.mediaiq.appnexus.load.response.ResponseContainer;
import com.mediaiq.appnexus.value.AppnexusSeat;

@Scope("step")
public abstract class ReportRequestTasklet implements Tasklet {
  
  @Autowired
  AppnexusRestClientSelector appnexusRestClientSelector;
  
  @Value("#{jobParameters['appnexus_seat']}")
  private AppnexusSeat appnexusSeat;
  
  final static Logger logger = LoggerFactory.getLogger(ReportRequestTasklet.class);
  
  @Override
  public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext)
      throws Exception {
    
    logger.info("Generating request");
    logger.info("Appnexus seat {}", appnexusSeat);
    
    ReportRequest reportRequest = fetchReportRequest();
    ReportRequestContainer container = new ReportRequestContainer(reportRequest);
    
    if (reportRequest == null)
      throw new IllegalArgumentException("No request object found for this report");
    
    if (!CollectionUtils.isEmpty(reportRequest.getFilters())
        && reportRequest.getFilters().get(0).getValues().isEmpty()) {
      logger.warn("Filters are missing");
      contribution.setExitStatus(ExitStatus.STOPPED);
    }
    
    logger.info("Requesting report:(name= {}) for {}", reportRequest.getName(),
        reportRequest.getReportInterval());
    logger.info(new AppnexusRestClientFactory().provideAppnexusGsonBuilder().toJson(container));
    
    ResponseContainer<ReportRequestResponse> responseContainer =
        this.appnexusRestClientSelector.getClientFor(appnexusSeat).requestReport(container);
    logger.info("Report response recieved with ID: {}",
        responseContainer.getResponse().getReportId());
    ExecutionContext executionContext =
        chunkContext.getStepContext().getStepExecution().getJobExecution().getExecutionContext();
    executionContext.put(StepAttribute.REPORT_NAME, reportRequest.getName());
    executionContext.put(StepAttribute.REPORT_ID, responseContainer.getResponse().getReportId());
    return RepeatStatus.FINISHED;
  }
  protected abstract ReportRequest fetchReportRequest();
  
}
