package com.mediaiq.appnexus.batch.tasklet;

import java.util.List;

import org.apache.commons.lang.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.aiq.domain.value.SellerType;
import com.mediaiq.aiq.miq.domain.MiqSeller;
import com.mediaiq.aiq.miq.repo.MiqSellerRepo;
import com.mediaiq.appnexus.domain.Seller;
import com.mediaiq.appnexus.load.client.AppnexusRestClient;
import com.mediaiq.appnexus.load.response.ResponseContainer;
import com.mediaiq.appnexus.load.response.SellerListsResponse;
import com.mediaiq.appnexus.repo.SellerRepo;
import com.mediaiq.appnexus.value.AppnexusDate;

@Component
@Scope("step")
public class SellerLoadTasklet
    extends AbstractLastModifiedAwarePageLoadTasklet<Seller, SellerRepo, SellerListsResponse> {
  
  @Autowired
  private SellerRepo sellerRepo;
  
  @Autowired
  private MiqSellerRepo miqSellerRepo;
  
  @Override
  public List<Seller> getItemsFromResponse(SellerListsResponse response) {
    return response.getSellers();
  }
  
  @Override
  public ResponseContainer<SellerListsResponse> getPageableResponse(
      AppnexusRestClient appnexusClient) {
    return appnexusClient.getSellers(startElement, new AppnexusDate(lastModified));
  }
  
  @Override
  public SellerRepo getRepository() {
    return sellerRepo;
  }
  
  @Override
  protected void save(List<Seller> items) {
    super.save(items);
    
    for (Seller item : items) {
      MiqSeller seller = miqSellerRepo.findBySellerId(item.getId());
      
      MiqSeller newSeller = new MiqSeller();
      if (seller != null) {
        if (item.getName() != null && !item.getName().equals(seller.getName()))
          seller.setName(item.getName());
        
        if (item.getSellerType() != null && seller.getSellerType() == null)
          updateSellerType(seller, item);
        
        miqSellerRepo.save(seller);
        continue;
      }
      
      newSeller.setId(item.getId());
      newSeller.setName(item.getName());
      newSeller.setSeller(item);
      updateSellerType(newSeller, item);
      
      miqSellerRepo.save(newSeller);
    }
  }
  
  private void updateSellerType(MiqSeller newSeller, Seller item) {
    if (item.getSellerType() != null) {
      switch (item.getSellerType()) {
        case partner:
          newSeller.setSellerType(SellerType.External);
          break;
        case platform:
          newSeller.setSellerType(SellerType.Internal);
          break;
        case adx:
          newSeller.setSellerType(SellerType.Adx);
          break;
        case managed:
          newSeller.setSellerType(SellerType.Managed);
          break;
        default:
          throw new NotImplementedException("Not yet implemented");
      }
    }
  }
  
}
