package com.mediaiq.appnexus.batch.gmail;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.amazonaws.services.s3.AmazonS3;
import com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.Message;
import com.google.api.services.gmail.model.MessagePart;
import com.google.api.services.gmail.model.MessagePartBody;
import com.google.api.services.gmail.model.ModifyThreadRequest;
import com.mediaiq.appnexus.batch.dto.EmailAttachmentDto;

@Service
public class EmailReadService {
  private Logger logger = LoggerFactory.getLogger(EmailReadService.class);
  
  @Autowired
  private GmailService gmailService;
  
  @Autowired
  AmazonS3 s3Client;
  
  @Value("${gmail-attachemnts-path}")
  private String filedirectory;
  
  // can use functions like setMaxResults(1l), setLabelIds(Arrays.asList("UNREAD"))
  public List<EmailAttachmentDto> readEmail(String filter) throws IOException {
    logger.info("Reading Email for filter:" + filter);
    // Build a new authorized API client service.
    Gmail service = gmailService.getGmailService();
    String user = "me";
    Gmail.Users.Messages.List request = service.users().messages().list(user).setQ(filter);
    
    List<Message> email = request.execute().getMessages();
    List<EmailAttachmentDto> filesInEmail = new ArrayList<>();
    // if no unread email by that sender
    if (!CollectionUtils.isEmpty(email)) {
      filesInEmail = getAttachments(service, user, email.get(0).getId());
      markAsRead(service, user, email.get(0).getThreadId());
    }
    return filesInEmail;
  }
  
  
  private void markAsRead(Gmail service, String user, String threadId) throws IOException {
    logger.info("Mark as read for email with ThreadId:" + threadId);
    List<String> labelsToRemove = new ArrayList<>(Arrays.asList("UNREAD"));
    ModifyThreadRequest mods = new ModifyThreadRequest().setRemoveLabelIds(labelsToRemove);
    service.users().threads().modify(user, threadId, mods).execute();
    
  }
  
  
  private List<EmailAttachmentDto> getAttachments(Gmail service, String userId, String messageId)
      throws IOException {
    logger.info("Get Attachment for email with messageId:" + messageId);
    List<EmailAttachmentDto> filesInEmail = new ArrayList<>();
    Message message = service.users().messages().get(userId, messageId).execute();
    List<MessagePart> parts = message.getPayload().getParts();
    parts.forEach(part->{
      if (part.getFilename() != null && part.getFilename().length() > 0 && part.getBody().getSize() > 0) {
        String filename = part.getFilename();
        String attachmentId = part.getBody().getAttachmentId();
        MessagePartBody attachPart = null;
        try {
          attachPart = service.users().messages().attachments().get(userId, messageId, attachmentId).execute();
        } catch (IOException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
        
        Base64 base64Url = new Base64(true);
        byte[] fileByteArray = base64Url.decodeBase64(attachPart.getData());
        ByteArrayInputStream fileInputStream = new ByteArrayInputStream(fileByteArray);
        filesInEmail.add(createEmailAttachmentDto(filename,attachmentId,fileInputStream));
      }
    });
    return filesInEmail;
    
  }


  private EmailAttachmentDto createEmailAttachmentDto(String filename, String attachmentId,
      ByteArrayInputStream fileInputStream) {
    EmailAttachmentDto fileInEmail = new EmailAttachmentDto();
    fileInEmail.setAttachment(fileInputStream);
    fileInEmail.setAttachmentId(attachmentId);
    fileInEmail.setFileName(filename);
    return fileInEmail;
  }
  
  
  
}
