package com.mediaiq.appnexus.batch.tasklet;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.domain.profile.LanguageTarget;
import com.mediaiq.appnexus.load.client.AppnexusRestClient;
import com.mediaiq.appnexus.load.response.LanguageListResponse;
import com.mediaiq.appnexus.load.response.ResponseContainer;
import com.mediaiq.appnexus.repo.LanguageRepo;

@Component
@Scope("step")
public class LanguageLoadTasklet
    extends AbstractPageLoadTasklet<LanguageTarget, LanguageRepo, LanguageListResponse> {
  
  @Autowired
  private LanguageRepo languageRepo;
  
  @Override
  public List<LanguageTarget> getItemsFromResponse(LanguageListResponse response) {
    return response.getLanguages();
  }
  
  @Override
  public ResponseContainer<LanguageListResponse> getPageableResponse(
      AppnexusRestClient appnexusClient) {
    return appnexusClient.getLanguage(startElement);
  }
  
  @Override
  public LanguageRepo getRepository() {
    return languageRepo;
  }
  
}
