package com.mediaiq.appnexus.batch.tasklet;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecutionException;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.scope.context.StepContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.batch.value.StepAttribute;
import com.mediaiq.appnexus.domain.profile.AppnexusProfile;
import com.mediaiq.appnexus.load.client.AppnexusRestClient;
import com.mediaiq.appnexus.load.response.AppnexusProfileListResponse;
import com.mediaiq.appnexus.load.response.ResponseContainer;
import com.mediaiq.appnexus.mongo.repo.AppnexusProfileRepo;
import com.mediaiq.appnexus.repo.PlacementRepo;
import com.mediaiq.appnexus.value.AppnexusSeat;

@Component
@Scope("step")
public class ProfileLoadTaskletV3 implements Tasklet {
  
  Logger logger = LoggerFactory.getLogger(ProfileLoadTaskletV3.class);
  
  @Autowired
  @Qualifier("JOB")
  private AppnexusRestClient appnexusClient;
  
  @Value("#{jobParameters['appnexus_seat']}")
  private AppnexusSeat seat;
  
  @Value("#{jobParameters['" + StepAttribute.START_ELEMENT + "']}")
  private Long startElement;
  
  @Autowired
  private AppnexusProfileRepo profileRepo;
  
  @Autowired
  private PlacementRepo placementRepo;
  
  private Long size = 100L;
  
  @Override
  public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext)
      throws Exception {
    
    Long offset = null;
    StepContext stepContext = chunkContext.getStepContext();
    ExecutionContext executionContext = stepContext.getStepExecution().getExecutionContext();
    
    if (executionContext.containsKey(StepAttribute.START_ELEMENT))
      offset = executionContext.getLong(StepAttribute.START_ELEMENT);
    else {
      offset = startElement;
    }
    
    LocalDate dateLimit = LocalDate.of(2016, 01, 01);
    List<Long> profileIds = placementRepo.findProfileIdsBySeat(offset, size, seat.name(),
        Date.from(dateLimit.atStartOfDay(ZoneId.systemDefault()).toInstant()));
    logger.info("profile ids : {}", profileIds);
    if (profileIds.size() == 0) {
      logger.info("Task finished.");
      return RepeatStatus.FINISHED;
    }
    
    String str = StringUtils.join(profileIds, ",");
    ResponseContainer<AppnexusProfileListResponse> responseContainer =
        appnexusClient.getProfileList(str);
    AppnexusProfileListResponse response = responseContainer.getResponse();
    if (response.getErrorCode() != null || response.getError() != null) {
      logger.error("Error while getting profile list with profile ids :{} and error :{}", str,
          response.getError());
      throw new JobExecutionException("Appnexus Retrofit Error: " + response);
    }
    
    logger.debug("Response: " + response);
    List<AppnexusProfile> items = response.getProfiles();
    
    logger.debug("Items: " + items);
    
    // save items
    save(items);
    
    Thread.sleep(1000);
    
    executionContext.putLong(StepAttribute.START_ELEMENT, offset + size);
    return RepeatStatus.CONTINUABLE;
  }
  
  private void save(List<AppnexusProfile> items) {
    if (items == null)
      return;
    
    for (AppnexusProfile item : items) {
      item.setSeat(seat);
    }
    profileRepo.save(items);
  }
  
}
