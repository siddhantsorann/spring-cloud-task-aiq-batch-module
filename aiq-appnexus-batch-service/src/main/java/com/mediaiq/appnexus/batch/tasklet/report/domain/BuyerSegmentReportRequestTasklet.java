package com.mediaiq.appnexus.batch.tasklet.report.domain;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.batch.tasklet.report.ReportRequestTasklet;
import com.mediaiq.appnexus.domain.report.DateRangeReportRequest;
import com.mediaiq.appnexus.domain.report.ReportFormat;
import com.mediaiq.appnexus.domain.report.ReportRequest;
import com.mediaiq.appnexus.domain.report.ReportType;

@Component
@Scope("step")
public class BuyerSegmentReportRequestTasklet extends ReportRequestTasklet {
  
  Logger logger = LoggerFactory.getLogger(BuyerSegmentReportRequestTasklet.class);
  String[] columns = {"campaign_id", "segment_id"};
  
  @Value("#{jobParameters['start_date']}")
  private Date startDate;
  
  @Value("#{jobParameters['end_date']}")
  private Date endDate;
  
  
  @Override
  protected ReportRequest fetchReportRequest() {
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:MM:SS");
    logger.info("Creating request");
    DateRangeReportRequest reportRequest = new DateRangeReportRequest();
    reportRequest.setReportType(ReportType.buyer_segment_performance);
    reportRequest.setFormat(ReportFormat.csv);
    reportRequest.setStartDate(dateFormat.format(startDate));
    reportRequest.setEndDate(dateFormat.format(endDate));
    reportRequest.setColumns(columns);
    reportRequest.setName("buyer_segment_report");
    logger.info("Return request: {}", reportRequest.toString());
    return reportRequest;
  }
  
}

