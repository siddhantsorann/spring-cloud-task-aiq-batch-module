package com.mediaiq.appnexus.batch.tasklet;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.domain.InsertionOrder;
import com.mediaiq.appnexus.load.client.AppnexusRestClient;
import com.mediaiq.appnexus.load.response.InsertionOrderListResponse;
import com.mediaiq.appnexus.load.response.ResponseContainer;
import com.mediaiq.appnexus.repo.InsertionOrderRepo;
import com.mediaiq.appnexus.value.AppnexusDate;

@Component
@Scope("step")
public class InsertionOrderLoadTasklet extends
    AbstractLastModifiedSeatAwarePageLoadTasklet<InsertionOrder, InsertionOrderRepo, InsertionOrderListResponse> {
  
  @Autowired
  InsertionOrderRepo insertionOrderRepo;
  
  @Override
  public List<InsertionOrder> getItemsFromResponse(InsertionOrderListResponse response) {
    return response.getInsertionOrders();
  }
  
  @Override
  public ResponseContainer<InsertionOrderListResponse> getPageableResponse(
      AppnexusRestClient appnexusClient) {
    ResponseContainer<InsertionOrderListResponse> container =
        appnexusClient.getInsertionOrderList(startElement, new AppnexusDate(lastModified));
    return container;
  }
  
  @Override
  public InsertionOrderRepo getRepository() {
    return insertionOrderRepo;
  }
}
