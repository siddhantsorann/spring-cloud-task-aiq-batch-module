package com.mediaiq.appnexus.batch.tasklet;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecutionException;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;

import com.mediaiq.appnexus.batch.value.StepAttribute;
import com.mediaiq.appnexus.load.client.AppnexusRestClient;
import com.mediaiq.appnexus.load.response.AbstractPagableResponse;
import com.mediaiq.appnexus.load.response.ResponseContainer;

@Scope("step")
public abstract class AbstractPageLoadTasklet<TYPE, REPO extends JpaRepository<TYPE, ?>, RESPONSE extends AbstractPagableResponse>
    implements Tasklet {
  
  private final Logger logger = LoggerFactory.getLogger(this.getClass());
  
  @Autowired
  @Qualifier("JOB")
  private AppnexusRestClient appnexusClient;
  
  @Value("#{stepExecutionContext['" + StepAttribute.START_ELEMENT + "']}")
  protected Integer startElement;
  
  @Override
  public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext)
      throws Exception {
    
    ResponseContainer<RESPONSE> responseContainer = getPageableResponse(appnexusClient);
    RESPONSE response = responseContainer.getResponse();
    if (response.getErrorCode() != null || response.getError() != null)
      throw new JobExecutionException("Appnexus Retrofit Error: " + response);
    
    logger.debug("Response: " + response);
    // fetch items from response
    List<TYPE> items = getItemsFromResponse(response);
    logger.debug("Items: " + items);
    
    // update execution context
    ExecutionContext executionContext =
        chunkContext.getStepContext().getStepExecution().getExecutionContext();
    executionContext.put(StepAttribute.START_ELEMENT, response.getStartElement());
    executionContext.put(StepAttribute.NUM_ELEMENTS, response.getNumElements());
    executionContext.put(StepAttribute.COUNT, response.getCount());
    
    // save items
    save(items);
    return RepeatStatus.FINISHED;
  }
  
  public abstract List<TYPE> getItemsFromResponse(RESPONSE response);
  
  public abstract ResponseContainer<RESPONSE> getPageableResponse(
      AppnexusRestClient appnexusClient);
  
  public abstract REPO getRepository();
  
  protected void save(List<TYPE> items) {
    getRepository().save(items);
  }
  
  @Autowired
  @Qualifier("JOB")
  public void setAppnexusClient(AppnexusRestClient appnexusClient) {
    this.appnexusClient = appnexusClient;
  }
}
