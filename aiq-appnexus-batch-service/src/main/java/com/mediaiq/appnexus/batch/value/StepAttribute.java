package com.mediaiq.appnexus.batch.value;

public interface StepAttribute {
  String START_ELEMENT = "start_element";
  String NUM_ELEMENTS = "num_elements";
  String COUNT = "count";
  String REPORT_ID = "report_id";
  String REPORT_NAME = "name";
  String FILE_PATH = "file_path";
  String LAST_MODIFIED = "last_modified";
  String CAMPAIGN_IDS = "CAMPAIGN_IDS";
  String DATE = "DATE";
  String TIMEZONE = "timezone";
  String OFFSET = "offset";
}
