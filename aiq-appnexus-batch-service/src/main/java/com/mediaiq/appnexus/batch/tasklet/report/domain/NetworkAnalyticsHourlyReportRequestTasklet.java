package com.mediaiq.appnexus.batch.tasklet.report.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.batch.tasklet.report.ReportRequestTasklet;
import com.mediaiq.appnexus.domain.report.ReportFormat;
import com.mediaiq.appnexus.domain.report.ReportRequest;
import com.mediaiq.appnexus.domain.report.ReportType;
import com.mediaiq.appnexus.domain.report.TimeInterval;

@Component
@Scope("step")
public class NetworkAnalyticsHourlyReportRequestTasklet extends ReportRequestTasklet {
  
  String[] columns = {"hour"};
  
  @Override
  protected ReportRequest fetchReportRequest() {
    ReportRequest reportRequest = new ReportRequest();
    reportRequest.setReportType(ReportType.network_analytics);
    reportRequest.setReportInterval(TimeInterval.yesterday);
    reportRequest.setFormat(ReportFormat.csv);
    reportRequest.setColumns(columns);
    reportRequest.setName("appnexus-daily-placement-stats-hourly");
    return reportRequest;
  }
  
}
