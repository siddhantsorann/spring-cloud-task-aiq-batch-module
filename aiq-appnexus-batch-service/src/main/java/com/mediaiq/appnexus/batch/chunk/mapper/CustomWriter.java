package com.mediaiq.appnexus.batch.chunk.mapper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import com.mediaiq.spring.batch.domain.StatsJobLastRunHour;
import com.mediaiq.spring.batch.domain.id.StatsJobLastRunHourId;
import com.mediaiq.spring.batch.repo.StatsJobLastRunHourRepo;

public class CustomWriter implements ItemWriter<Date> {
  
  private static final String APPNEXUS_DAILY_PLACEMENT_STATS_JOB =
      "APPNEXUS_DAILY_PLACEMENT_STATS_JOB";
  
  
  
  @Autowired
  StatsJobLastRunHourRepo statsJobLastRunHourRepo;
  
  @SuppressWarnings("deprecation")
  @Override
  public void write(List<? extends Date> items) throws Exception {
    
    ArrayList<Date> dates = new ArrayList<Date>(items);
    
    // find max of date and store in DB.
    Collections.sort(dates);
    Collections.reverse(dates);
    Date first = dates.get(0);
    
    StatsJobLastRunHour statsJobLastRunHour = statsJobLastRunHourRepo
        .findOne(new StatsJobLastRunHourId(first, APPNEXUS_DAILY_PLACEMENT_STATS_JOB));
    if (statsJobLastRunHour == null)
      statsJobLastRunHour = new StatsJobLastRunHour(
          new StatsJobLastRunHourId(first, APPNEXUS_DAILY_PLACEMENT_STATS_JOB), first.getHours());
    statsJobLastRunHour.setMaxHour(first.getHours());
    statsJobLastRunHourRepo.save(statsJobLastRunHour);
  }
  
}
