package com.mediaiq.appnexus.batch.firebase;

import org.springframework.batch.core.BatchStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.mediaiq.spring.batch.domain.JobStateDto;

/**
 * Created by chandrahaas on 4/1/16.
 */
@Component
public class BatchFirebaseUtil {
  
  @Autowired
  FirebaseClient firebaseClient;
  
  @Value("${firebase.secret}")
  private String secret;
  
  public void postCampaignState(String jobName, BatchStatus campaignState) {
    firebaseClient.addJobState(jobName, new JobStateDto(campaignState), secret);
  }
}
