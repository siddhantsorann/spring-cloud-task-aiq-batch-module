package com.mediaiq.appnexus.batch.tasklet;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.domain.OperatingSystem;
import com.mediaiq.appnexus.load.client.AppnexusRestClient;
import com.mediaiq.appnexus.load.response.OperatingSystemListResponse;
import com.mediaiq.appnexus.load.response.ResponseContainer;
import com.mediaiq.appnexus.repo.OperatingSystemRepo;
import com.mediaiq.appnexus.value.AppnexusDate;

@Component
@Scope("step")
public class OperatingSystemLoadTasklet extends
    AbstractLastModifiedSeatAwarePageLoadTasklet<OperatingSystem, OperatingSystemRepo, OperatingSystemListResponse> {
  
  @Autowired
  private OperatingSystemRepo operatingSystemRepo;
  
  @Override
  public ResponseContainer<OperatingSystemListResponse> getPageableResponse(
      AppnexusRestClient appnexusClient) {
    return appnexusClient.getOperatingSystemlList(startElement, new AppnexusDate(lastModified));
  }
  
  @Override
  public OperatingSystemRepo getRepository() {
    return operatingSystemRepo;
  }
  
  @Override
  public List<OperatingSystem> getItemsFromResponse(OperatingSystemListResponse response) {
    return response.getOperatingSystems();
  }
}
