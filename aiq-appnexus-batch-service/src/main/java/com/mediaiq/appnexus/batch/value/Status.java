package com.mediaiq.appnexus.batch.value;

public enum Status {
  
  INVALID_PARAMS(450,
      "Parameters passed for the request is invalid. Kindly check the request format/payload."), //
  FILE_NOT_FOUND(451, "The specified file was not found."), //
  INVALID_ADVERTISER_PATH(452, "The specified advertiser path is ivvalid."), //
  CANNOT_FIND_LAST_MONTH_REPORTS_DIR(453,
      "Cannot find last month's reports directory."), NO_DATA_IN_BACKEND(403,
          "No data exists in the backend for the selected filters");
  
  private int code;
  private String message;
  
  private Status(int code, String message) {
    this.code = code;
    this.message = message;
  }
  
  public int getCode() {
    return code;
  }
  
  public String getMessage() {
    return message;
  }
}
