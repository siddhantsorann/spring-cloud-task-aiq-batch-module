package com.mediaiq.appnexus.batch.tasklet;

import java.util.HashMap;
import java.util.Map;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

/**
 * Created by mohit on 14/8/17.
 */
@Component
@Scope("step")
public class UpdatePixelToCreativeMappingTasklet implements Tasklet {
  
  @Autowired
  NamedParameterJdbcTemplate namedParameterJdbcTemplate;
  
  private String query = "REPLACE INTO appnexus.third_party_pixel_to_creative_mapping (\n"
      + "  creative_id,\n" + "  data_provider_id,\n" + "  pixel_id) \n"
      + "SELECT tpp.creative_id, dpa.data_provider_id, dpa.aliases\n"
      + "FROM miq.data_provider dp \n" + "JOIN miq.data_provider_aliases dpa \n"
      + "ON dp.id=dpa.data_provider_id\n" + "JOIN appnexus.third_party_pixel tpp\n"
      + "ON dpa.aliases=tpp.third_party_pixel_id\n" + "WHERE dp.type='Pixel';";
  
  @Override
  public RepeatStatus execute(StepContribution arg0, ChunkContext arg1) throws Exception {
    
    Map<String, Object> parameters = new HashMap<String, Object>();
    // parameters.put("", );
    
    namedParameterJdbcTemplate.update(query, parameters);
    
    return RepeatStatus.FINISHED;
  }
}
