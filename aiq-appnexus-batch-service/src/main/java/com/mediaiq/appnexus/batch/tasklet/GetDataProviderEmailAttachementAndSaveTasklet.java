package com.mediaiq.appnexus.batch.tasklet;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.mediaiq.appnexus.batch.dto.AppnexusDataProvider;
import com.mediaiq.appnexus.batch.dto.EmailAttachmentDto;
import com.mediaiq.appnexus.batch.gmail.EmailReadService;
import com.mediaiq.appnexus.domain.Segment;
import com.mediaiq.appnexus.repo.SegmentRepo;


@Component
@Scope("step")
public class GetDataProviderEmailAttachementAndSaveTasklet implements Tasklet {
  
  private Logger logger =
      LoggerFactory.getLogger(GetDataProviderEmailAttachementAndSaveTasklet.class);
  
  @Value("${appnexus-data-provider.email-sender}")
  private String emailSender;
  
  @Value("${appnexus-data-provider.bucket}")
  private String bucket;
  
  @Value("${appnexus-data-provider.path}")
  private String path;
  
  @Autowired
  private SegmentRepo segmentRepo;
  
  @Autowired
  AmazonS3 s3Client;
  
  @Autowired
  private EmailReadService emailAdapter;
  
  @Override
  public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) {
    
    // read email, download csv to a location
    try {
      String sender = emailSender;
      String filter = "in:unread " + "from:" + sender;
      List<EmailAttachmentDto> filesInEmail = emailAdapter.readEmail(filter);
      // if unread email from that sender is present
      if (!CollectionUtils.isEmpty(filesInEmail)) {
        String savedLocation =
            saveFileToS3(filesInEmail.get(0).getFileName(), filesInEmail.get(0).getAttachment());
        InputStream fileInputStream = readFromS3(savedLocation);
        Map<Long, AppnexusDataProvider> segmentDataProviderMap = parseCSV(fileInputStream);
        addsegmentDataProviderMapToTable(segmentDataProviderMap);
      } else {
        logger.info("No unread emails from user: {}", sender);
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    
    return RepeatStatus.FINISHED;
  }
  
  private InputStream readFromS3(String savedLocation) {
    S3Object object = s3Client.getObject(new GetObjectRequest(bucket, savedLocation));
    System.out.println("Content-Type: " + object.getObjectMetadata().getContentType());
    return object.getObjectContent();
  }
  
  private Map<Long, AppnexusDataProvider> parseCSV(InputStream fileInputStream) throws IOException {
    Map<Long, AppnexusDataProvider> segmentDataProviderMap = new HashMap<>();
    String line = "";
    String cvsSplitBy = "(?<!&amp)" // not preceded by &amp
        + "," // split by comma
        + "(?=(?:[^\\\"]*\\\"[^\\\"]*\\\")*(?![^\\\"]*\\\"))"; // not inside ""
    logger.info("Reading and Parsing CSV......");
    try {
      InputStreamReader rd = new InputStreamReader(fileInputStream);
      BufferedReader bufferedReader = new BufferedReader(rd);
      bufferedReader.readLine();
      while ((line = bufferedReader.readLine()) != null) {
        // use comma as separator
        String[] columns = line.split(cvsSplitBy);
        addLineToDataProviderMap(segmentDataProviderMap, columns);
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    
    return segmentDataProviderMap;
  }
  
  private void addLineToDataProviderMap(Map<Long, AppnexusDataProvider> segmentDataProviderMap,
      String[] columns) {
    Long segmentId = new Long(columns[5]);
    Long dataProviderId = new Long(columns[10]);
    String dataProviderName = columns[11];
    
    if (!segmentDataProviderMap.containsKey(segmentId)) {
      AppnexusDataProvider appnexusDataProvider = new AppnexusDataProvider();
      appnexusDataProvider.setId(dataProviderId);
      appnexusDataProvider.setName(dataProviderName);
      segmentDataProviderMap.put(segmentId, appnexusDataProvider);
    }
    
  }
  
  private String saveFileToS3(String fileName, ByteArrayInputStream inputStream) {
    logger.info("Saving file: {} to S3", fileName);
    s3Client.putObject(new PutObjectRequest(bucket, path + fileName, inputStream, null));
    logger.info("Saved to S3");
    return (path + fileName);
  }
  
  private void addsegmentDataProviderMapToTable(
      Map<Long, AppnexusDataProvider> segmentDataProviderMap) {
    logger.info("Adding parsed data to segment table in appnexus db");
    Set<Long> segmentIdsInMap = segmentDataProviderMap.keySet();
    List<Segment> segments = segmentRepo.findAll(segmentIdsInMap);
    
    for (Segment segment : segments) {
      AppnexusDataProvider dataProvider = segmentDataProviderMap.get(segment.getId());
      segment.setDataProviderId(dataProvider.getId());
      segment.setDataProviderName(dataProvider.getName());
    }
    segmentRepo.save(segments);
  }
  
  
  
}
