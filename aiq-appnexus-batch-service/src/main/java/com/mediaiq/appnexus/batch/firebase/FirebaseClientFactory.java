package com.mediaiq.appnexus.batch.firebase;


import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;

import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

/**
 * Created by chandrahaas on 4/1/16.
 */
@Configuration
public class FirebaseClientFactory {
  
  Logger logger = LoggerFactory.getLogger(FirebaseClientFactory.class);
  @Value("${firebase.url}")
  private String firbaseUrl;
  
  private final RestAdapter.Log log = new RestAdapter.Log() {
    
    @Override
    public void log(String message) {
      logger.info(message);;
    }
  };
  
  @Bean
  public FirebaseClient provide() {
    OkHttpClient httpClient = new OkHttpClient();
    httpClient.setReadTimeout(300, TimeUnit.SECONDS);
    RestAdapter restAdapter =
        new RestAdapter.Builder().setEndpoint(firbaseUrl).setClient(new OkClient(httpClient))
            .setConverter(new GsonConverter(provideAppnexusGsonBuilder()))
            
            .setLogLevel(RestAdapter.LogLevel.FULL).setLog(log).build();
    
    return restAdapter.create(FirebaseClient.class);
  }
  
  
  public Gson provideAppnexusGsonBuilder() {
    return new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        .create();
  }
}
