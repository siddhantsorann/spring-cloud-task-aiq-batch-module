package com.mediaiq.appnexus.batch.tasklet;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.domain.AppnexusAdvertiser;
import com.mediaiq.appnexus.domain.Segment;
import com.mediaiq.appnexus.load.client.AppnexusRestClient;
import com.mediaiq.appnexus.load.response.ResponseContainer;
import com.mediaiq.appnexus.load.response.SegmentWrapper;
import com.mediaiq.appnexus.repo.AppnexusAdvertiserRepo;
import com.mediaiq.appnexus.repo.SegmentRepo;
import com.mediaiq.appnexus.value.AppnexusSeat;
import com.mediaiq.appnexus.value.State;

@Component
@Scope("step")
public class ModifiedAdvertiserLoadTasklet implements Tasklet {
  
  @Value("#{jobParameters['last_modified']}")
  protected Date lastModified;
  
  @Autowired
  @Qualifier("JOB")
  private AppnexusRestClient appnexusClient;
  
  @Autowired
  private AppnexusAdvertiserRepo advertiserRepo;
  
  @Autowired
  private SegmentRepo segmentRepo;
  
  private static final Logger logger = LoggerFactory.getLogger(ModifiedAdvertiserLoadTasklet.class);
  
  @Override
  public RepeatStatus execute(StepContribution arg0, ChunkContext arg1) throws Exception {
    
    List<AppnexusAdvertiser> advertisers = advertiserRepo.findByLastModifiedAfterAndStateAndSeat(
        lastModified, State.active, AppnexusSeat.SEAT_MIQ_1);
    
    for (AppnexusAdvertiser advertiser : advertisers) {
      Segment segment = segmentRepo.findByNameAndAdvertiserIdAndSeat(
          "MIQ_CLICKER_" + advertiser.getName(), advertiser.getId(), AppnexusSeat.SEAT_MIQ_1);
      if (segment != null)
        continue;
      
      Segment newSegment = new Segment("MIQ_CLICKER_" + advertiser.getId(),
          "MIQ_CLICKER_" + advertiser.getName(), 668L);
      newSegment.setAdvertiserId(advertiser.getId());
      SegmentWrapper segmentWrapper = new SegmentWrapper(newSegment);
      ResponseContainer<SegmentWrapper> createdSegmentWrapper =
          appnexusClient.createSegment(segmentWrapper);
      
      Segment newCreatedSegment = createdSegmentWrapper.getResponse().getSegment();
      if (newCreatedSegment == null) {
        logger.error("Error creting segment: {}", newSegment);
        continue;
      }
      newCreatedSegment.setSeat(AppnexusSeat.SEAT_MIQ_1);
      newCreatedSegment.setAdvertiserId(advertiser.getId());
      segmentRepo.save(newCreatedSegment);
      logger.info("MIQ Clicker Segment created for new advertiser: {}", advertiser.getId());
    }
    
    return RepeatStatus.FINISHED;
  }
}
