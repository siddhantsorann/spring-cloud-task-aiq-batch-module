package com.mediaiq.appnexus.batch.tasklet;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.domain.profile.AppnexusProfile;
import com.mediaiq.appnexus.load.client.AppnexusRestClient;
import com.mediaiq.appnexus.load.response.AppnexusProfileListResponse;
import com.mediaiq.appnexus.load.response.ResponseContainer;
import com.mediaiq.appnexus.mongo.repo.AppnexusProfileRepo;
import com.mediaiq.appnexus.value.AppnexusDate;

@Component
@Scope("step")
public class ProfileLoadTasklet extends
    AbstractLastModifiedSeatAwarePageLoadTaskletMongo<AppnexusProfile, AppnexusProfileRepo, AppnexusProfileListResponse> {
  
  @Autowired
  private AppnexusProfileRepo profileRepo;
  
  @Override
  public List<AppnexusProfile> getItemsFromResponse(AppnexusProfileListResponse response) {
    return response.getProfiles();
  }
  
  @Override
  public ResponseContainer<AppnexusProfileListResponse> getPageableResponse(
      AppnexusRestClient appnexusClient) {
    return appnexusClient.getProfileList(startElement, new AppnexusDate(lastModified));
  }
  
  @Override
  public AppnexusProfileRepo getRepository() {
    return profileRepo;
  }
}
