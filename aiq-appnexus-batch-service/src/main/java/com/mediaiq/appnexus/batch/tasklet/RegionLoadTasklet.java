package com.mediaiq.appnexus.batch.tasklet;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.domain.Region;
import com.mediaiq.appnexus.load.client.AppnexusRestClient;
import com.mediaiq.appnexus.load.response.RegionListResponse;
import com.mediaiq.appnexus.load.response.ResponseContainer;
import com.mediaiq.appnexus.repo.RegionRepo;

@Component
@Scope("step")
public class RegionLoadTasklet
    extends AbstractPageLoadTasklet<Region, RegionRepo, RegionListResponse> {
  
  @Autowired
  private RegionRepo regionRepo;
  
  @Override
  public ResponseContainer<RegionListResponse> getPageableResponse(
      AppnexusRestClient appnexusClient) {
    return appnexusClient.getRegionList(startElement);
  }
  
  @Override
  public RegionRepo getRepository() {
    return regionRepo;
  }
  
  @Override
  public List<Region> getItemsFromResponse(RegionListResponse response) {
    return response.getRegions();
  }
}
