package com.mediaiq.appnexus.batch.tasklet;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.aiq.domain.value.DataProviderType;
import com.mediaiq.aiq.miq.domain.DataProvider;
import com.mediaiq.aiq.miq.domain.lookup.ThirdPartySegmentsLookup;
import com.mediaiq.aiq.miq.repo.DataProviderRepo;
import com.mediaiq.aiq.miq.repo.lookup.AdvertiserSegmentsLookupRepo;
import com.mediaiq.aiq.miq.repo.lookup.ThirdPartySegmentsLookupRepo;

/**
 * Created by mohit on 14/8/17.
 */
@Component
@Scope("step")
public class MapDPToSegmentsTasklet implements Tasklet {
  
  @Autowired
  private DataProviderRepo dataProviderRepo;
  
  @Autowired
  private AdvertiserSegmentsLookupRepo advertiserSegmentsLookupRepo;
  
  @Autowired
  private ThirdPartySegmentsLookupRepo thirdPartySegmentsLookupRepo;
  
  @Override
  public RepeatStatus execute(StepContribution arg0, ChunkContext arg1) throws Exception {
    
    List<DataProvider> dataProviders = dataProviderRepo.findByType(DataProviderType.DATA_SERVICE);
    // check unmapped segments
    List<ThirdPartySegmentsLookup> unassignedSegments =
        getThirdPartySegments("dataProviderIsNull", null);
    List<ThirdPartySegmentsLookup> mappedSegments =
        mapUnmappedSegments(unassignedSegments, dataProviders);
    save(mappedSegments);
    
    String bufferDpName = "Buffer Cost";
    // check buffer cost mapped segments
    DataProvider bufferDp = dataProviderRepo.findByName(bufferDpName);
    List<ThirdPartySegmentsLookup> bufferCostSegments =
        getThirdPartySegments("dataProvider", bufferDp);
    List<ThirdPartySegmentsLookup> assignedBufferCostSegments =
        mapBufferCostSegments(bufferCostSegments, dataProviders);
    save(assignedBufferCostSegments);
    
    // assign unmapped segments to buffer cost dp
    List<ThirdPartySegmentsLookup> updatedBufferCostSegments = assignBufferCost(bufferDp);
    save(updatedBufferCostSegments);
    
    return RepeatStatus.FINISHED;
  }
  
  private List<ThirdPartySegmentsLookup> mapUnmappedSegments(
      List<ThirdPartySegmentsLookup> unassignedSegments, List<DataProvider> dataProviders) {
    // get unmapped segments
    for (ThirdPartySegmentsLookup t : unassignedSegments) {
      Long dataProviderId = getDataProviderFromSegment(t.getSegment().getName(), dataProviders);
      if (dataProviderId != null) {
        t.setDataProvider(dataProviderRepo.findOne(dataProviderId));
      }
    }
    return unassignedSegments;
  }
  
  private List<ThirdPartySegmentsLookup> mapBufferCostSegments(
      List<ThirdPartySegmentsLookup> bufferCostSegments, List<DataProvider> dataProviders) {
    for (ThirdPartySegmentsLookup t : bufferCostSegments) {
      Long dataProviderId = getDataProviderFromSegment(t.getSegment().getName(), dataProviders);
      if (dataProviderId != null) {
        t.setDataProvider(dataProviderRepo.findOne(dataProviderId));
      }
    }
    return bufferCostSegments;
  }
  
  private List<ThirdPartySegmentsLookup> assignBufferCost(DataProvider bufferCostDataProvider) {
    List<ThirdPartySegmentsLookup> thirdPartySegmentsLookups =
        getThirdPartySegments("dataProviderIsNull", null);
    for (ThirdPartySegmentsLookup t : thirdPartySegmentsLookups) {
      t.setDataProvider(bufferCostDataProvider);
    }
    return thirdPartySegmentsLookups;
    
  }
  
  private Long getDataProviderFromSegment(String segmentName, List<DataProvider> dataProviders) {
    
    if (segmentName == null)
      return null;
    
    Map<String, Long> priorityDP = new HashMap<>();
    
    for (DataProvider dp : dataProviders) {
      List<String> aliases = dp.getAliases();
      if (match(aliases.stream().map(al -> "(" + al + ")").collect(Collectors.toList()),
          segmentName.toLowerCase(), "contains")) {
        priorityDP.put("value", dp.getId());
        priorityDP.put("priority", 1l);
      }
      if (match(aliases, segmentName.toLowerCase(), "startsWith")) {
        if (priorityDP.get("priority") == null || priorityDP.get("priority") > 2) {
          priorityDP.put("value", dp.getId());
          priorityDP.put("priority", 2l);
        }
      }
      if (match(aliases, segmentName.toLowerCase(), "contains")) {
        if (priorityDP.get("priority") == null || priorityDP.get("priority") > 3) {
          priorityDP.put("value", dp.getId());
          priorityDP.put("priority", 3l);
        }
      }
      
    }
    
    return priorityDP.get("value");
    
  }
  
  private Boolean match(List<String> aliases, String segmentName, String matchCase) {
    switch (matchCase) {
      case "contains":
        for (String alias : aliases) {
          if (segmentName.contains(alias.toLowerCase()))
            return true;
        }
      case "startsWith":
        for (String alias : aliases) {
          if (segmentName.startsWith(alias.toLowerCase()))
            return true;
        }
      default:
        return false;
    }
  }
  
  private List<ThirdPartySegmentsLookup> getThirdPartySegments(String condition, DataProvider dp) {
    switch (condition) {
      case "dataProviderIsNull":
        return thirdPartySegmentsLookupRepo.findByDataProviderIdIsNull();
      case "dataProvider":
        return thirdPartySegmentsLookupRepo.findByDataProvider(dp);
      default:
        return null;
    }
  }
  
  private void save(List<ThirdPartySegmentsLookup> segments) {
    thirdPartySegmentsLookupRepo.save(segments);
  }
  
}
