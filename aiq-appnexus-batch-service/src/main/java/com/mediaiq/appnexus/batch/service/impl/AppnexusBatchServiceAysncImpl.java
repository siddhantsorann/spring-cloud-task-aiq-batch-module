package com.mediaiq.appnexus.batch.service.impl;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.springframework.batch.core.JobExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mediaiq.appnexus.batch.service.AppnexusBatchService;
import com.mediaiq.appnexus.batch.service.AppnexusBatchServiceAysnc;
import com.mediaiq.appnexus.batch.service.DateParameter;
import com.mediaiq.appnexus.domain.report.TimeInterval;
import com.mediaiq.appnexus.repo.AppnexusAdvertiserRepo;
import com.mediaiq.appnexus.value.AppnexusSeat;
import com.mediaiq.spring.batch.integration.component.JobLaunchGateway;

@Service
public class AppnexusBatchServiceAysncImpl implements AppnexusBatchServiceAysnc {
  
  @Autowired
  AppnexusBatchService appnexusBatchService;
  
  @Autowired
  JobLaunchGateway jobLaunchGateway;
  
  @Autowired
  private AppnexusAdvertiserRepo appnexusAdvertiserRepo;
  
  @Override
  public void launchCityLoadJob(String countryCode, Boolean override, AppnexusSeat appnexusSeat) {
    appnexusBatchService.launchCityLoadJob(countryCode, override, appnexusSeat);
  }
  
  @Override
  public void launchAllCityLoadJob(Boolean override, AppnexusSeat appnexusSeat) {
    appnexusBatchService.launchAllCityLoadJob(override, appnexusSeat);
  }
  
  @Override
  public void launchCountryLoadJob(Boolean override, AppnexusSeat appnexusSeat) {
    appnexusBatchService.launchCountryLoadJob(override, appnexusSeat);
  }
  
  @Override
  public void launchRegionLoadJob(Boolean override, AppnexusSeat appnexusSeat) {
    appnexusBatchService.launchRegionLoadJob(override, appnexusSeat);
  }
  
  @Override
  public void launchAdvertiserLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      DateParameter lastModified) {
    appnexusBatchService.launchAdvertiserLoadJob(override, appnexusSeat, lastModified.getDate());
  }
  
  @Override
  public void launchContentCategoryLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      DateParameter lastModified) {
    appnexusBatchService.launchContentCategoryLoadJob(override, appnexusSeat,
        lastModified.getDate());
  }
  
  @Override
  public void launchDomainListLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      DateParameter lastModified) {
    appnexusBatchService.launchDomainListLoadJob(override, appnexusSeat, lastModified.getDate());
  }
  
  @Override
  public void launchInsertionOrderLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      DateParameter lastModified) {
    appnexusBatchService.launchInsertionOrderLoadJob(override, appnexusSeat,
        lastModified.getDate());
  }
  
  @Override
  public void launchLineItemLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      DateParameter lastModified) {
    appnexusBatchService.launchLineItemLoadJob(override, appnexusSeat, lastModified.getDate());
  }
  
  @Override
  public void launchPixelLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      DateParameter lastModified) {
    appnexusBatchService.launchPixelLoadJob(override, appnexusSeat, lastModified.getDate());
  }
  
  @Override
  public void launchPlacementLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      DateParameter lastModified, Boolean showAugmented) {
    appnexusBatchService.launchPlacementLoadJob(override, appnexusSeat, lastModified.getDate(),
        showAugmented);
    
  }
  
  @Override
  public void launchSegmentLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      DateParameter lastModified) {
    appnexusBatchService.launchSegmentLoadJob(override, appnexusSeat, lastModified.getDate());
  }
  
  @Override
  public void launchSiteDomainReportLoadJob(Boolean override, AppnexusSeat appnexusSeat) {
    appnexusBatchService.launchSiteDomainReportLoadJob(override, appnexusSeat);
    
  }
  
  
  @Override
  public void launchBrowserLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      DateParameter lastModified) {
    appnexusBatchService.launchBrowserLoadJob(override, appnexusSeat, lastModified.getDate());
  }
  
  @Override
  public void launchCarrierLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      DateParameter lastModified) {
    appnexusBatchService.launchCarrierLoadJob(override, appnexusSeat, lastModified.getDate());
  }
  
  @Override
  public void launchDeviceMakeLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      DateParameter lastModified) {
    appnexusBatchService.launchDeviceMakeLoadJob(override, appnexusSeat, lastModified.getDate());
  }
  
  @Override
  public void launchDeviceModelLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      DateParameter lastModified) {
    appnexusBatchService.launchDeviceModelLoadJob(override, appnexusSeat, lastModified.getDate());
  }
  
  @Override
  public void launchMobileAppInstanceListLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      DateParameter lastModified) {
    appnexusBatchService.launchMobileAppInstanceListLoadJob(override, appnexusSeat,
        lastModified.getDate());
  }
  
  @Override
  public void launchOperatingSystemLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      DateParameter lastModified) {
    appnexusBatchService.launchOperatingSystemLoadJob(override, appnexusSeat,
        lastModified.getDate());
  }
  
  @Override
  public void launchOperatingSystemExtendedLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      DateParameter lastModified) {
    appnexusBatchService.launchOperatingSystemExtendedLoadJob(override, appnexusSeat,
        lastModified.getDate());
  }
  
  @Override
  public void launchOperatingSystemFamilyLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      DateParameter lastModified) {
    appnexusBatchService.launchOperatingSystemFamilyLoadJob(override, appnexusSeat,
        lastModified.getDate());
  }
  
  @Override
  public void launchSellerLoadJob(Boolean override, DateParameter lastModified) {
    appnexusBatchService.launchSellerLoadJob(override, lastModified.getDate());
  }
  
  @Override
  public void launchDataProviderJob(Boolean override, DateParameter lastModified) {
    appnexusBatchService.launchDataProviderLoadJob(override, lastModified.getDate());
    
  }
  
  @Override
  public void launchCreativeLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      DateParameter lastModified) {
    appnexusBatchService.launchCreativeLoadJob(override, appnexusSeat, lastModified.getDate());
  }
  
  @Override
  public void launchAdvertiserMetaLoadJob(AppnexusSeat seat, DateParameter lastModified)
      throws InterruptedException, ExecutionException, ParseException {
    Date lastModifiedDate = lastModified.getDate();
    Future<JobExecution> launchAdvertiserLoadJob =
        appnexusBatchService.launchAdvertiserLoadJob(true, seat, lastModifiedDate);
    launchAdvertiserLoadJob.get();
    
    Future<JobExecution> launchInsertionOrderLoadJob =
        appnexusBatchService.launchInsertionOrderLoadJob(true, seat, lastModifiedDate);
    launchInsertionOrderLoadJob.get();
    
    Future<JobExecution> launchCreativeLoadJob =
        appnexusBatchService.launchCreativeLoadJob(true, seat, lastModifiedDate);
    launchCreativeLoadJob.get();
    
    Future<JobExecution> launchPixelLoadJob =
        appnexusBatchService.launchPixelLoadJob(true, seat, lastModifiedDate);
    launchPixelLoadJob.get();
    
    Future<JobExecution> launchLineItemLoadJob =
        appnexusBatchService.launchLineItemLoadJob(true, seat, lastModifiedDate);
    launchLineItemLoadJob.get();
    
    Future<JobExecution> launchPlacementLoadJob =
        appnexusBatchService.launchPlacementLoadJob(true, seat, lastModifiedDate, true);
    launchPlacementLoadJob.get();
    
    Future<JobExecution> launchSellerLoadJob =
        appnexusBatchService.launchSellerLoadJob(true, lastModifiedDate);
    launchSellerLoadJob.get();

    Future<JobExecution> launchPlacementCreativeMappingLoadJob =
            appnexusBatchService.launchPlacementCreativeMappingLoadJob(lastModifiedDate, true);
  }
  
  
  @Override
  public void launchAppnexusDailyPlacementTimeZoneStatsLoadJob(Boolean override,
      AppnexusSeat appnexusSeat, DateParameter startDate, DateParameter endDate, String timezone) {
    
    appnexusBatchService.launchAppnexusDailyPlacementTimeZoneStatsLoadJob(override, appnexusSeat,
        startDate.getDate(), endDate.getDate(), timezone);
    
    
  }
  
  @Override
  public void launchDataUsageStatsLoadJob(AppnexusSeat appnexusSeat, DateParameter startDate) {
    appnexusBatchService.launchDataUsageStatsLoadJob(appnexusSeat, startDate.getDate());
  }
  
  private Date getOlderDate(int days) {
    Calendar cal = Calendar.getInstance();
    cal.add(Calendar.DATE, -days);
    cal.add(Calendar.HOUR_OF_DAY, 0);
    Date date = cal.getTime();
    return date;
  }
  
  
  @Override
  public void launchCreativeAggregation(DateParameter startDateParam, DateParameter endDateParam) {
    appnexusBatchService.launchCreativeAggregationJob(startDateParam.getDate(),
        endDateParam.getDate());
    
  }
  
  @Override
  public void launchAddClickerSegmentsForNewAdvertisersJob(Boolean override,
      AppnexusSeat appnexusSeat, DateParameter lastModified) {
    appnexusBatchService.launchAddClickerSegmentsForNewAdvertisersJob(override, appnexusSeat,
        lastModified.getDate());
  }
  
  @Override
  public void launchUpdateCreativesWithClickerSegmentJob(Boolean override,
      AppnexusSeat appnexusSeat, DateParameter lastModified) {
    appnexusBatchService.launchUpdateCreativesWithClickerSegmentJob(override, appnexusSeat,
        lastModified.getDate());
    
  }
  
  @Override
  public void launchLanguageLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      DateParameter lastModified) {
    appnexusBatchService.launchLanguageLoadJob(override, appnexusSeat, lastModified.getDate());
  }
  
  @Override
  public void launchAppnexusEntityCountLoadJob(Boolean override, AppnexusSeat appnexusSeat,
      DateParameter lastModified) {
    appnexusBatchService.launchAppnexusEntityCountLoadJob(override, appnexusSeat,
        lastModified.getDate(), true);
    
  }
  
  @Override
  public void launchMobileAppLoadJob(Boolean override, AppnexusSeat seat,
      TimeInterval timeInterval) {
    appnexusBatchService.launchMobileAppLoadJob(timeInterval, override, seat);
    
  }
  
  @Override
  public void launchProfileV3LoadJob(Boolean override, AppnexusSeat appnexusSeat) {
    appnexusBatchService.launchProfileV3LoadJob(override, appnexusSeat);
  }
  
  @Override
  public void launchProfileSegmentMappingJob(Boolean override, AppnexusSeat appnexusSeat,
      DateParameter lastModified) {
    appnexusBatchService.launchProfileSegmentMappingLoadJob(override, appnexusSeat,
        lastModified.getDate());
  }
  
  @Override
  public void launchUpdatePixelToCreativeMapping() {
    appnexusBatchService.launchUpdatePixelToCreativeMapping();
  }
  
  @Override
  public void launchMapDpToSegmentsJob() {
    try {
      appnexusBatchService.launchMapDpToSegmentsJob();
    } catch (ExecutionException | InterruptedException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void launchSegmentDataPoviderMapping() {
    try {
      appnexusBatchService.launchSegmentDataPoviderMapping();
    } catch (InterruptedException | ExecutionException e) {
      e.printStackTrace();
    }
  }
    
  public void launchPlacementCreativeMappingLoadJob(DateParameter lastModified){
      appnexusBatchService.launchPlacementCreativeMappingLoadJob(lastModified.getDate(), true);
  }
}
