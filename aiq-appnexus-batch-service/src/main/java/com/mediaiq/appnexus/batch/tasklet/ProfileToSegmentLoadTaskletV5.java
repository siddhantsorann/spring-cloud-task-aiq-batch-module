package com.mediaiq.appnexus.batch.tasklet;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.scope.context.StepContext;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.batch.value.StepAttribute;
import com.mediaiq.appnexus.domain.Placement;
import com.mediaiq.appnexus.domain.ProfileToSegmentMapping;
import com.mediaiq.appnexus.domain.profile.AppnexusProfile;
import com.mediaiq.appnexus.domain.profile.SegmentGroupTarget;
import com.mediaiq.appnexus.domain.profile.SegmentTarget;
import com.mediaiq.appnexus.load.client.AppnexusRestClient;
import com.mediaiq.appnexus.load.response.AppnexusProfileListResponse;
import com.mediaiq.appnexus.load.response.ResponseContainer;
import com.mediaiq.appnexus.mongo.repo.AppnexusProfileRepo;
import com.mediaiq.appnexus.repo.PlacementRepo;
import com.mediaiq.appnexus.repo.ProfileToSegmentMappingRepo;
import com.mediaiq.appnexus.value.AppnexusSeat;
import com.mediaiq.appnexus.value.TargetAction;

/**
 * Created by naveen on 6/4/17.
 */
@Component
@Scope("step")
public class ProfileToSegmentLoadTaskletV5 extends
    AbstractPageLoadTasklet<ProfileToSegmentMapping, ProfileToSegmentMappingRepo, AppnexusProfileListResponse> {
  
  private static final Logger logger = LoggerFactory.getLogger(ProfileToSegmentLoadTaskletV5.class);
  
  @Value("#{jobParameters['appnexus_seat']}")
  private AppnexusSeat seat;
  
  @Value("#{jobParameters['last_modified']}")
  private Date lastModified;
  
  @Autowired
  private PlacementRepo placementRepo;
  
  @Autowired
  private ProfileToSegmentMappingRepo profileToSegmentMappingRepo;
  
  @Autowired
  private AppnexusProfileRepo appnexusProfileRepo;
  
  private static final Integer SIZE = 1000;
  
  Map<String, Long> profileIdToPlacementIdMap;
  
  private Integer offset;
  
  @Value("#{jobParameters['" + StepAttribute.START_ELEMENT + "']}")
  private int startElement;
  
  @Override
  public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext)
      throws Exception {
    // StepContext stepContext = chunkContext.getStepContext();
    // ExecutionContext executionContext = stepContext.getStepExecution().getExecutionContext();
    //
    // offset = executionContext.getInt(StepAttribute.START_ELEMENT);
    //
    // RepeatStatus repeatstatus = super.execute(stepContribution, chunkContext);
    //
    // executionContext.putInt(StepAttribute.COUNT, Integer.MAX_VALUE);
    // executionContext.put(StepAttribute.START_ELEMENT, offset);
    //
    // return repeatstatus;
    
    int page = 0;
    StepContext stepContext = chunkContext.getStepContext();
    ExecutionContext executionContext = stepContext.getStepExecution().getExecutionContext();
    
    if (executionContext.containsKey(StepAttribute.START_ELEMENT))
      page = executionContext.getInt(StepAttribute.START_ELEMENT);
    else {
      page = startElement;
    }

    PageRequest pageRequest = new PageRequest(page, SIZE);
    logger.info("Saving profile segments page no:{}", page);
    
    Page<AppnexusProfile> profilesPage =
        appnexusProfileRepo.findByLastModifiedAfter(lastModified, pageRequest);
    List<AppnexusProfile> profiles = profilesPage.getContent();
    if (CollectionUtils.isEmpty(profiles)) {
      return RepeatStatus.FINISHED;
    }
    
    List<ProfileToSegmentMapping> newMappings = new ArrayList<ProfileToSegmentMapping>();
    List<String> profileIdStrings =
        profiles.stream().map(p -> p.getId().toString()).collect(Collectors.toList());
    logger.info("Profile Ids {}", profileIdStrings);
    List<Placement> placements = placementRepo.findByProfileIdIn(profileIdStrings);
    logger.info("Total no. of placements found: {}", placements.size());
    
    Map<String, Long> profileIdToPlacementIdMap =
        placements.stream().collect(Collectors.toMap(Placement::getProfileId, Placement::getId));
    logger.info("ProfileId to PlacementId map done");

    List<Long> profileIds = profileIdStrings.stream().map(p-> Long.valueOf(p)).collect(Collectors.toList());

    List<ProfileToSegmentMapping> mappings = profileToSegmentMappingRepo.findByprofileToSegmentIdIdIn(profileIds);

    //If you are alive when this date is reached ... good luck!
    Calendar cal = Calendar.getInstance();
    cal.set(2100, 0, 1, 0, 0, 0);
    Date futureDate = cal.getTime();
    Date currentDate =
        Date.from(LocalDate.now().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    logger.info("current Date {} , future Date {}", currentDate, futureDate);
        
    for (AppnexusProfile profile : profiles) {
      if (profile.getSegmentGroupTargets() == null)
        continue;
      
      for (SegmentGroupTarget segmentGroupTarget : profile.getSegmentGroupTargets()) {
        if (segmentGroupTarget == null || segmentGroupTarget.getSegments() == null)
          continue;
        for (SegmentTarget segmentTarget : segmentGroupTarget.getSegments()) {
          if (segmentTarget == null)
            continue;
          if (profileIdToPlacementIdMap.get(profile.getId().toString()) == null)
            continue;
          ProfileToSegmentMapping profileToSegmentMapping = null;
          List<ProfileToSegmentMapping> oldMap = new ArrayList<>();
          if (!CollectionUtils.isEmpty(mappings)) {
            oldMap = mappings.stream().filter(psm-> psm.getProfileToSegmentId().getId().equals(profile.getId()) && psm.getProfileToSegmentId().getSegmentId().equals(segmentTarget.getSegmentId())).collect(
                Collectors.toList());
          }
          if (CollectionUtils.isEmpty(oldMap)) {
            //New placement to segment map
            profileToSegmentMapping = new ProfileToSegmentMapping(profile.getId(), segmentTarget.getSegmentId(),
                profileIdToPlacementIdMap.get(profile.getId().toString()), profile.getSeat(),
                profile.getLastModified(), profile.getLastModified(), futureDate);
          }
          else {
            profileToSegmentMapping= oldMap.get(0);
            profileToSegmentMapping.setEndDate(futureDate);
          }
          //This contains all the current mappings
          newMappings.add(profileToSegmentMapping);
        }
      }
    }

    //Updating the old mappings end date with current date
    mappings.forEach(mp->{
      List<ProfileToSegmentMapping> map = filterProfileSegmentMappings(newMappings, mp.getProfileToSegmentId().getId(), mp.getProfileToSegmentId().getSegmentId());
      if(CollectionUtils.isEmpty(map)){
        mp.setEndDate(currentDate);
        Long profileId = mp.getProfileToSegmentId().getId();
        if(!CollectionUtils.isEmpty(profiles)){
          Optional<AppnexusProfile> optionalProfile = profiles.stream().filter(p -> p.getId().equals(profileId)).findAny();
          if(optionalProfile.isPresent()){
            mp.setEndDate(optionalProfile.get().getLastModified());
          }
        }
      }
    });

    logger.info("Saving lookup...");
    // save
    mappings.addAll(newMappings);
    profileToSegmentMappingRepo.save(mappings);
    logger.info("Lookup saved");
    
    Thread.sleep(1000);
    
    executionContext.putInt(StepAttribute.START_ELEMENT, page + 1);
    return RepeatStatus.CONTINUABLE;
  }
  
  @Override
  public List<ProfileToSegmentMapping> getItemsFromResponse(AppnexusProfileListResponse response) {
    
    // List<AppnexusProfile> appnexusProfiles = response.getProfiles();
    List<AppnexusProfile> appnexusProfiles =
        appnexusProfileRepo.findByLastModifiedAfter(lastModified);
    logger.debug("Items: " + appnexusProfiles);

    
    List<ProfileToSegmentMapping> profileToSegmentMappings = new ArrayList<>();
    
    for (AppnexusProfile profile : appnexusProfiles) {
      if (CollectionUtils.isEmpty(profile.getSegmentGroupTargets())) {
        continue;
      }
      List<Long> segmentIds = profile.getSegmentGroupTargets().stream()
          .flatMap(
              x -> x.getSegments().stream().filter(s -> s.getAction().equals(TargetAction.exclude)))
          .map(SegmentTarget::getSegmentId).collect(Collectors.toList());
      segmentIds.stream().forEach(x -> {
        ProfileToSegmentMapping profileToSegmentMapping = new ProfileToSegmentMapping(
            profile.getId(), x, profileIdToPlacementIdMap.get(profile.getId().toString()),
            profile.getSeat(), profile.getLastModified(), new Date(), new Date());
        profileToSegmentMappings.add(profileToSegmentMapping);
      });
    }
    if (profileIdToPlacementIdMap != null)
      profileIdToPlacementIdMap.clear();
    
    try {
      Thread.sleep(1000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    
    return profileToSegmentMappings;
  }
  
  @Override
  public ResponseContainer<AppnexusProfileListResponse> getPageableResponse(
      AppnexusRestClient appnexusClient) {
    
    List<Placement> profilesBySeat = placementRepo.findProfilesBySeat(Long.valueOf(offset),
        Long.valueOf(SIZE), seat.name(), lastModified);
    if (profilesBySeat.size() == 0) {
      logger.info("Task finished.");
      offset = null;
      ResponseContainer<AppnexusProfileListResponse> responseContainer =
          new ResponseContainer<AppnexusProfileListResponse>();
      AppnexusProfileListResponse response = new AppnexusProfileListResponse();
      response.setCount(null);
      response.setProfiles(Collections.emptyList());
      responseContainer.setResponse(response);
      return responseContainer;
    }
    
    profileIdToPlacementIdMap = profilesBySeat.stream()
        .collect(Collectors.toMap(Placement::getProfileId, Placement::getId, (p1, p2) -> p1));
    List<String> profileIds =
        profilesBySeat.stream().map(Placement::getProfileId).collect(Collectors.toList());
    String str = StringUtils.join(profileIds, ",");
    logger.info("profile ids : {}", str);
    ResponseContainer<AppnexusProfileListResponse> responseContainer = null;
    
    if (StringUtils.isEmpty(str)) {
      logger.info("Task finished.");
      offset = null;
      responseContainer = new ResponseContainer<AppnexusProfileListResponse>();
      AppnexusProfileListResponse response = new AppnexusProfileListResponse();
      response.setCount(null);
      response.setProfiles(Collections.emptyList());
      responseContainer.setResponse(response);
      return responseContainer;
    }
    
    try {
      responseContainer = appnexusClient.getProfileList(str);
    } catch (Exception e) {
      logger.error("Error in fetching profiles: ", e);
      throw new RuntimeException(e);
    }
    
    return responseContainer;
  }
  
  @Override
  public ProfileToSegmentMappingRepo getRepository() {
    return profileToSegmentMappingRepo;
  }

  private List<ProfileToSegmentMapping> filterProfileSegmentMappings(List<ProfileToSegmentMapping> maps, Long profileId, Long segmentId){
    return maps.stream().filter(psm -> psm.getProfileToSegmentId().getId().equals(profileId) && psm
        .getProfileToSegmentId().getSegmentId().equals(segmentId)).collect(Collectors.toList());
  }
}
