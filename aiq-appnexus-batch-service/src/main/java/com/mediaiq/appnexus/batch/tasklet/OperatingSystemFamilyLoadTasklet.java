package com.mediaiq.appnexus.batch.tasklet;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.domain.OperatingSystemFamily;
import com.mediaiq.appnexus.load.client.AppnexusRestClient;
import com.mediaiq.appnexus.load.response.OperatingSystemFamilyListResponse;
import com.mediaiq.appnexus.load.response.ResponseContainer;
import com.mediaiq.appnexus.repo.OperatingSystemFamilyRepo;
import com.mediaiq.appnexus.value.AppnexusDate;

@Component
@Scope("step")
public class OperatingSystemFamilyLoadTasklet extends
    AbstractLastModifiedAwarePageLoadTasklet<OperatingSystemFamily, OperatingSystemFamilyRepo, OperatingSystemFamilyListResponse> {
  
  @Autowired
  private OperatingSystemFamilyRepo operatingSystemFamilyRepo;
  
  @Override
  public ResponseContainer<OperatingSystemFamilyListResponse> getPageableResponse(
      AppnexusRestClient appnexusClient) {
    return appnexusClient.getOperatingSystemFamilyList(startElement,
        new AppnexusDate(lastModified));
  }
  
  @Override
  public OperatingSystemFamilyRepo getRepository() {
    return operatingSystemFamilyRepo;
  }
  
  @Override
  public List<OperatingSystemFamily> getItemsFromResponse(
      OperatingSystemFamilyListResponse response) {
    return response.getOperatingSystemFamilies();
  }
}
