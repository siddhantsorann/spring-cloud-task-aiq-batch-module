package com.mediaiq.appnexus.batch.jersey.provider;

import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.batch.core.StepExecution;

import com.mediaiq.appnexus.batch.jersey.mixin.StepExecutionMixIn;

@Provider
public class StepExecutionObjectMapperProvider implements ContextResolver<ObjectMapper> {
  
  @Override
  public ObjectMapper getContext(Class<?> type) {
    ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.getSerializationConfig().addMixInAnnotations(StepExecution.class,
        StepExecutionMixIn.class);
    return objectMapper;
  }
  
}
