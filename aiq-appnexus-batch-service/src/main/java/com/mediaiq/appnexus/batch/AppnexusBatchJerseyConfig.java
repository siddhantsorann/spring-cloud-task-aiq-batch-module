package com.mediaiq.appnexus.batch;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.batch.service.impl.AppnexusBatchServiceAysncImpl;
import com.mediaiq.spring.batch.service.impl.JobHistoryServiceImpl;

@Component
@ApplicationPath("/api")
public class AppnexusBatchJerseyConfig extends ResourceConfig {
  
  public AppnexusBatchJerseyConfig() {
    
    register(AppnexusBatchServiceAysncImpl.class);
    register(JobHistoryServiceImpl.class);
  }
  
}
