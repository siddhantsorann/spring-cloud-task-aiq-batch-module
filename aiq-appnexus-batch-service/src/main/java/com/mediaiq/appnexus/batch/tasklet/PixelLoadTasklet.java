package com.mediaiq.appnexus.batch.tasklet;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.domain.Pixel;
import com.mediaiq.appnexus.load.client.AppnexusRestClient;
import com.mediaiq.appnexus.load.response.PixelListResponse;
import com.mediaiq.appnexus.load.response.ResponseContainer;
import com.mediaiq.appnexus.repo.PixelRepo;
import com.mediaiq.appnexus.value.AppnexusDate;

@Component
@Scope("step")
public class PixelLoadTasklet
    extends AbstractLastModifiedAwarePageLoadTasklet<Pixel, PixelRepo, PixelListResponse> {
  
  @Autowired
  PixelRepo pixelRepo;
  
  @Override
  public List<Pixel> getItemsFromResponse(PixelListResponse response) {
    return response.getPixels();
  }
  
  @Override
  public ResponseContainer<PixelListResponse> getPageableResponse(
      AppnexusRestClient appnexusClient) {
    ResponseContainer<PixelListResponse> container =
        appnexusClient.getPixelList(startElement, new AppnexusDate(lastModified));
    return container;
  }
  
  @Override
  public PixelRepo getRepository() {
    return pixelRepo;
  }
  
}
