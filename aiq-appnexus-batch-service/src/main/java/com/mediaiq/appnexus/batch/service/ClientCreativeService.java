package com.mediaiq.appnexus.batch.service;

import com.mediaiq.appnexus.domain.Creative;
import com.mediaiq.appnexus.repo.CreativeRepo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * Created by piyushagarwal on 3/5/18.
 */
@Service
public class ClientCreativeService {

    @Autowired
    CreativeRepo creativeRepo;

    public void clientCreativeFindAndSave(Set<Creative> creatives){

        creatives.stream().forEach(creative -> {
            if (StringUtils.isNotEmpty(creative.getContent())) {
                String clientCreativeId = RegularExpressionUtils.findPattern(creative.getContent());
                if (StringUtils.isNotEmpty(clientCreativeId) && clientCreativeId.matches(".*\\d+.*")) {
                    creative.setClientCreativeId(clientCreativeId);
                }
            }

            if (StringUtils.isNotEmpty(creative.getName())
                    && StringUtils.isEmpty(creative.getClientCreativeId())) {
                String clientCreativeId = RegularExpressionUtils.findPatternInName(creative.getName());
                if (StringUtils.isNotEmpty(clientCreativeId))
                    creative.setClientCreativeId(clientCreativeId);
            }
        });
        creativeRepo.save(creatives);
    }
}
