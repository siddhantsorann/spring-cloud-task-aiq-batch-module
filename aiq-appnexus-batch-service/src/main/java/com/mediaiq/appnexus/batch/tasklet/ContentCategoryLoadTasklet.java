package com.mediaiq.appnexus.batch.tasklet;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.domain.ContentCategory;
import com.mediaiq.appnexus.load.client.AppnexusRestClient;
import com.mediaiq.appnexus.load.response.ContentCategoryListResponse;
import com.mediaiq.appnexus.load.response.ResponseContainer;
import com.mediaiq.appnexus.repo.ContentCategoryRepo;
import com.mediaiq.appnexus.value.AppnexusDate;

@Component
@Scope("step")
public class ContentCategoryLoadTasklet extends
    AbstractLastModifiedAwarePageLoadTasklet<ContentCategory, ContentCategoryRepo, ContentCategoryListResponse> {
  
  @Autowired
  private ContentCategoryRepo contentCategoryRepo;
  
  @Override
  public List<ContentCategory> getItemsFromResponse(ContentCategoryListResponse response) {
    return response.getContentCategories();
  }
  
  @Override
  public ResponseContainer<ContentCategoryListResponse> getPageableResponse(
      AppnexusRestClient appnexusClient) {
    return appnexusClient.getContentCategoryList(startElement, new AppnexusDate(lastModified));
  }
  
  @Override
  public ContentCategoryRepo getRepository() {
    return contentCategoryRepo;
  }
  
}
