package com.mediaiq.appnexus.batch.tasklet.report.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.batch.tasklet.report.ReportRequestTasklet;
import com.mediaiq.appnexus.domain.report.ReportFormat;
import com.mediaiq.appnexus.domain.report.ReportRequest;
import com.mediaiq.appnexus.domain.report.ReportType;
import com.mediaiq.appnexus.domain.report.TimeInterval;

@Component
@Scope("step")
public class MobileAppReportRequestTasklet extends ReportRequestTasklet {
  String[] columns =
      {"top_level_category_name", "second_level_category_name", "mobile_application_id",
          "mobile_application_name", "operating_system_id", "operating_system_name"};
  
  @Override
  protected ReportRequest fetchReportRequest() {
    
    ReportRequest reportRequest = new ReportRequest();
    reportRequest.setReportType(ReportType.network_site_domain_performance);
    reportRequest.setReportInterval(TimeInterval.last_7_days);
    reportRequest.setFormat(ReportFormat.csv);
    reportRequest.setColumns(columns);
    reportRequest.setName("mobile-app-report");
    
    return reportRequest;
  }
  
  
  
}
