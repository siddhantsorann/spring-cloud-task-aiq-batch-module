package com.mediaiq.appnexus.batch.chunk.mapper;

import java.util.Date;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import com.mediaiq.appnexus.domain.report.stats.id.NetworkLookupStatId;
import com.mediaiq.appnexus.domain.report.stats.raw.NetworkLookupStat;


/**
 * Created by piyush on 28/10/15.
 */
public class AppnexusDailyPlacementTimeZoneStatsMapper
    implements FieldSetMapper<NetworkLookupStat> {
  
  private String timezone;
  
  public AppnexusDailyPlacementTimeZoneStatsMapper(String timeZone) {
    this.timezone = timeZone;
  }
  
  @Override
  public NetworkLookupStat mapFieldSet(FieldSet set) throws BindException {
    NetworkLookupStat stats = new NetworkLookupStat();
    
    Date date = set.readDate("day", "yyyy-MM-dd");
    Long placementId = set.readLong("campaign_id");
    String creativeIdString = set.readString("creative_id");
    Long creativeId = 0l;
    if (creativeIdString.equalsIgnoreCase("External Clicks")
        || creativeIdString.equalsIgnoreCase("External Imps"))
      creativeId = -1l;
    else
      creativeId = Long.parseLong(creativeIdString);
    
    
    stats.setId(new NetworkLookupStatId(date, placementId, creativeId));
    stats.setClicks(set.readLong("clicks"));
    stats.setConversions(set.readLong("total_convs"));
    // stats.setMediaCost(set.readDouble("cost_buying_currency"));
    // stats.setCurrency(set.readString("buying_currency"));
    stats.setImpressions(set.readLong("imps"));
    stats.setTimezone(timezone);
    stats.setInsertionOrderId(set.readLong("insertion_order_id"));
    return stats;
  }
  
}
