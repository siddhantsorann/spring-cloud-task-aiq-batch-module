package com.mediaiq.appnexus.batch.tasklet;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.domain.DomainList;
import com.mediaiq.appnexus.load.client.AppnexusRestClient;
import com.mediaiq.appnexus.load.response.DomainListsResponse;
import com.mediaiq.appnexus.load.response.ResponseContainer;
import com.mediaiq.appnexus.repo.DomainListRepo;
import com.mediaiq.appnexus.value.AppnexusDate;

@Component
@Scope("step")
public class DomainListLoadTasklet extends
    AbstractLastModifiedSeatAwarePageLoadTasklet<DomainList, DomainListRepo, DomainListsResponse> {
  
  @Autowired
  private DomainListRepo domainListRepo;
  
  @Override
  public ResponseContainer<DomainListsResponse> getPageableResponse(
      AppnexusRestClient appnexusClient) {
    return appnexusClient.getDomainLists(startElement, new AppnexusDate(lastModified));
  }
  
  @Override
  public DomainListRepo getRepository() {
    return domainListRepo;
  }
  
  @Override
  public List<DomainList> getItemsFromResponse(DomainListsResponse response) {
    return response.getDomainLists();
  }
}
