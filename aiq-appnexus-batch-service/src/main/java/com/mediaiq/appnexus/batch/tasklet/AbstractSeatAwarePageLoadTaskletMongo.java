package com.mediaiq.appnexus.batch.tasklet;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.mediaiq.appnexus.load.response.AbstractPagableResponse;
import com.mediaiq.appnexus.value.AppnexusSeat;
import com.mediaiq.appnexus.value.HasAppnexusSeat;

public abstract class AbstractSeatAwarePageLoadTaskletMongo<TYPE extends HasAppnexusSeat, REPO extends MongoRepository<TYPE, ?>, RESPONSE extends AbstractPagableResponse>
    extends AbstractPageLoadTaskletMongo<TYPE, REPO, RESPONSE> {
  
  @Value("#{jobParameters['appnexus_seat']}")
  private AppnexusSeat seat;
  
  @Override
  protected void save(List<TYPE> items) {
    // TODO: items should not be null
    if (items == null)
      return;
    
    for (TYPE item : items) {
      item.setSeat(seat);
    }
    super.save(items);
  }
  
}
