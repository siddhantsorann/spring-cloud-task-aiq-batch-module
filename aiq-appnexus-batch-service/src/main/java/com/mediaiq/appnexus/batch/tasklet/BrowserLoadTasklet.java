package com.mediaiq.appnexus.batch.tasklet;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.domain.Browser;
import com.mediaiq.appnexus.load.client.AppnexusRestClient;
import com.mediaiq.appnexus.load.response.BrowserListResponse;
import com.mediaiq.appnexus.load.response.ResponseContainer;
import com.mediaiq.appnexus.repo.BrowserRepo;
import com.mediaiq.appnexus.value.AppnexusDate;

@Component
@Scope("step")
public class BrowserLoadTasklet extends
    AbstractLastModifiedSeatAwarePageLoadTasklet<Browser, BrowserRepo, BrowserListResponse> {
  
  @Autowired
  private BrowserRepo browserRepo;
  
  @Override
  public ResponseContainer<BrowserListResponse> getPageableResponse(
      AppnexusRestClient appnexusClient) {
    return appnexusClient.getBrowserList(startElement, new AppnexusDate(lastModified));
  }
  
  @Override
  public BrowserRepo getRepository() {
    return browserRepo;
  }
  
  @Override
  public List<Browser> getItemsFromResponse(BrowserListResponse response) {
    return response.getBrowsers();
  }
}
