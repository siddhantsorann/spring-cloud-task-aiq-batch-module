package com.mediaiq.appnexus.batch.tasklet;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.mediaiq.appnexus.batch.service.ClientCreativeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.domain.AppnexusAdvertiser;
import com.mediaiq.appnexus.domain.Creative;
import com.mediaiq.appnexus.domain.Segment;
import com.mediaiq.appnexus.domain.profile.SegmentTarget;
import com.mediaiq.appnexus.load.client.AppnexusRestClient;
import com.mediaiq.appnexus.load.response.CreativeWrapper;
import com.mediaiq.appnexus.load.response.ResponseContainer;
import com.mediaiq.appnexus.repo.AppnexusAdvertiserRepo;
import com.mediaiq.appnexus.repo.CreativeRepo;
import com.mediaiq.appnexus.repo.SegmentRepo;
import com.mediaiq.appnexus.value.AppnexusSeat;
import com.mediaiq.appnexus.value.SegmentAction;
import com.mediaiq.appnexus.value.State;

@Component
@Scope("step")
public class UpdateCreativesTasklet implements Tasklet {
  
  @Value("#{jobParameters['last_modified']}")
  protected Date lastModified;
  
  @Autowired
  @Qualifier("JOB")
  private AppnexusRestClient appnexusClient;
  
  @Autowired
  private AppnexusAdvertiserRepo advertiserRepo;
  
  @Autowired
  private SegmentRepo segmentRepo;
  
  @Autowired
  private CreativeRepo creativeRepo;

  @Autowired
  ClientCreativeService clientCreativeService;
  
  private static final Logger logger = LoggerFactory.getLogger(UpdateCreativesTasklet.class);
  
  @Override
  public RepeatStatus execute(StepContribution arg0, ChunkContext arg1) throws Exception {
    
    List<AppnexusAdvertiser> advertisers = advertiserRepo.findByLastModifiedAfterAndStateAndSeat(
        lastModified, State.active, AppnexusSeat.SEAT_MIQ_1);
    
    for (AppnexusAdvertiser advertiser : advertisers) {
      logger.info("Updating creatives for advertiser: {}", advertiser.getId());
      
      List<Creative> creatives = creativeRepo.findByAdvertiserIdInAndState(
          Arrays.asList(new Long[] {advertiser.getId()}), State.active);
      
      Segment segment = segmentRepo.findByNameAndAdvertiserIdAndSeat(
          "MIQ_CLICKER_" + advertiser.getName(), advertiser.getId(), AppnexusSeat.SEAT_MIQ_1);
      
      if (segment == null) {
        logger.warn(
            "MIQ_CLICKER_" + advertiser.getName() + " segment not found for advertiser: {} ",
            advertiser.getId());
        continue;
      }
      
      segment.setAction(SegmentAction.add_on_click);
      
      for (Creative creative : creatives) {
        
        logger.info("Updating creative: Id => {}", creative.getId());
        if (lastModified.compareTo(creative.getCreatedOn()) > 0)
          continue;
        
        Set<SegmentTarget> segments = creative.getSegments();
        if (segments == null)
          segments = new HashSet<>();
        
        if (segments.contains(segment)) {
          logger.info(
              "MIQ_CLICKER_" + advertiser.getName() + " Segment already exist in creative: {}",
              creative.getId());
          continue;
        }
        
        // TODO
        // segments.add(segment);
        // creative.setAuditStatus(AuditStatus.pending);
        Creative aNewCreative = new Creative();
        aNewCreative.setSegments(segments);
        
        ResponseContainer<CreativeWrapper> updatedCreative = appnexusClient.updateCreative(
            new CreativeWrapper(aNewCreative), creative.getAdvertiserId(), creative.getId());
        logger.info("Response: " + updatedCreative);
        
        if (updatedCreative.getResponse() != null
            && updatedCreative.getResponse().getCreative() != null) {
          Set<Creative> creativeList = new HashSet<>();
          creativeList.add(updatedCreative.getResponse().getCreative());
          clientCreativeService.clientCreativeFindAndSave(creativeList);
          logger.info("Successfully updated creative: Id => {}", creative.getId());
          // Adding a deloy to avoid limit exceed
          Thread.sleep(500);
        }
      }
    }
    
    return RepeatStatus.FINISHED;
  }
}
