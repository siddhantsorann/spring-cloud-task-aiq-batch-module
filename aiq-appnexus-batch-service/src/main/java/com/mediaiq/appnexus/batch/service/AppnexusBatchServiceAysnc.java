/**
 *
 */
package com.mediaiq.appnexus.batch.service;

import java.text.ParseException;
import java.util.concurrent.ExecutionException;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.mediaiq.appnexus.domain.report.TimeInterval;
import com.mediaiq.appnexus.value.AppnexusSeat;

@Path("/load")
@Produces(MediaType.APPLICATION_JSON)
public interface AppnexusBatchServiceAysnc {
  
  @POST
  @Path("/city")
  void launchCityLoadJob(@QueryParam("country_code") String country_code,
      @QueryParam("override") @DefaultValue("false") Boolean override,
      @QueryParam("appnexus_seat") @DefaultValue("SEAT_MIQ_1") AppnexusSeat appnexusSeat);
  
  @POST
  @Path("/city/all")
  void launchAllCityLoadJob(@QueryParam("override") @DefaultValue("false") Boolean override,
      @QueryParam("appnexus_seat") @DefaultValue("SEAT_MIQ_1") AppnexusSeat appnexusSeat);
  
  @POST
  @Path("/language")
  void launchLanguageLoadJob(@QueryParam("override") @DefaultValue("false") Boolean override,
      @QueryParam("appnexus_seat") @DefaultValue("SEAT_MIQ_1") AppnexusSeat appnexusSeat,
      @QueryParam("last_modified") DateParameter lastModified);
  
  @POST
  @Path("/country")
  void launchCountryLoadJob(@QueryParam("override") @DefaultValue("false") Boolean override,
      @QueryParam("appnexus_seat") @DefaultValue("SEAT_MIQ_1") AppnexusSeat appnexusSeat);
  
  @POST
  @Path("/region")
  void launchRegionLoadJob(@QueryParam("override") @DefaultValue("false") Boolean override,
      @QueryParam("appnexus_seat") @DefaultValue("SEAT_MIQ_1") AppnexusSeat appnexusSeat);
  
  @POST
  @Path("/advertiser")
  void launchAdvertiserLoadJob(@QueryParam("override") @DefaultValue("false") Boolean override,
      @QueryParam("appnexus_seat") @DefaultValue("SEAT_MIQ_1") AppnexusSeat appnexusSeat,
      @QueryParam("last_modified") DateParameter lastModified);
  
  @POST
  @Path("/contentCategory")
  void launchContentCategoryLoadJob(@QueryParam("override") @DefaultValue("false") Boolean override,
      @QueryParam("appnexus_seat") @DefaultValue("SEAT_MIQ_1") AppnexusSeat appnexusSeat,
      @QueryParam("last_modified") DateParameter lastModified);
  
  @POST
  @Path("/domainList")
  void launchDomainListLoadJob(@QueryParam("override") @DefaultValue("false") Boolean override,
      @QueryParam("appnexus_seat") @DefaultValue("SEAT_MIQ_1") AppnexusSeat appnexusSeat,
      @QueryParam("last_modified") DateParameter lastModified);
  
  @POST
  @Path("/insertionOrder")
  void launchInsertionOrderLoadJob(@QueryParam("override") @DefaultValue("false") Boolean override,
      @QueryParam("appnexus_seat") @DefaultValue("SEAT_MIQ_1") AppnexusSeat appnexusSeat,
      @QueryParam("last_modified") DateParameter lastModified);
  
  @POST
  @Path("/lineItem")
  void launchLineItemLoadJob(@QueryParam("override") @DefaultValue("false") Boolean override,
      @QueryParam("appnexus_seat") @DefaultValue("SEAT_MIQ_1") AppnexusSeat appnexusSeat,
      @QueryParam("last_modified") DateParameter lastModified);
  
  @POST
  @Path("/pixel")
  void launchPixelLoadJob(@QueryParam("override") @DefaultValue("false") Boolean override,
      @QueryParam("appnexus_seat") @DefaultValue("SEAT_MIQ_1") AppnexusSeat appnexusSeat,
      @QueryParam("last_modified") DateParameter lastModified);
  
  @POST
  @Path("/placement")
  void launchPlacementLoadJob(@QueryParam("DateParameter") @DefaultValue("false") Boolean override,
      @QueryParam("appnexus_seat") @DefaultValue("SEAT_MIQ_1") AppnexusSeat appnexusSeat,
      @QueryParam("last_modified") DateParameter lastModified,
      @QueryParam("show_augmented") @DefaultValue("true") Boolean showAugmented);
  
  @POST
  @Path("/segment")
  void launchSegmentLoadJob(@QueryParam("override") @DefaultValue("false") Boolean override,
      @QueryParam("appnexus_seat") @DefaultValue("SEAT_MIQ_1") AppnexusSeat appnexusSeat,
      @QueryParam("last_modified") DateParameter lastModified);
  
  @POST
  @Path("/siteDomainReport")
  void launchSiteDomainReportLoadJob(
      @QueryParam("override") @DefaultValue("false") Boolean override,
      @QueryParam("appnexus_seat") @DefaultValue("SEAT_MIQ_1") AppnexusSeat appnexusSeat);
  
  
  @POST
  @Path("/appnexus_raw_stats")
  void launchAppnexusDailyPlacementTimeZoneStatsLoadJob(
      @QueryParam("override") @DefaultValue("false") Boolean override,
      @QueryParam("appnexus_seat") @DefaultValue("SEAT_MIQ_1") AppnexusSeat appnexusSeat,
      @QueryParam("start_date") DateParameter startDate,
      @QueryParam("end_date") DateParameter endDate, @QueryParam("timezone") String timezone);
  
  @POST
  @Path("/data_usage_stats")
  void launchDataUsageStatsLoadJob(
      @QueryParam("appnexus_seat") @DefaultValue("SEAT_MIQ_1") AppnexusSeat appnexusSeat,
      @QueryParam("start_date") DateParameter startDate);
  
  @POST
  @Path("/browser")
  void launchBrowserLoadJob(@QueryParam("override") @DefaultValue("false") Boolean override,
      @QueryParam("appnexus_seat") @DefaultValue("SEAT_MIQ_1") AppnexusSeat appnexusSeat,
      @QueryParam("last_modified") DateParameter lastModified);
  
  @POST
  @Path("/carrier")
  void launchCarrierLoadJob(@QueryParam("override") @DefaultValue("false") Boolean override,
      @QueryParam("appnexus_seat") @DefaultValue("SEAT_MIQ_1") AppnexusSeat appnexusSeat,
      @QueryParam("last_modified") DateParameter lastModified);
  
  @POST
  @Path("/deviceMake")
  void launchDeviceMakeLoadJob(@QueryParam("override") @DefaultValue("false") Boolean override,
      @QueryParam("appnexus_seat") @DefaultValue("SEAT_MIQ_1") AppnexusSeat appnexusSeat,
      @QueryParam("last_modified") DateParameter lastModified);
  
  @POST
  @Path("/deviceModel")
  void launchDeviceModelLoadJob(@QueryParam("override") @DefaultValue("false") Boolean override,
      @QueryParam("appnexus_seat") @DefaultValue("SEAT_MIQ_1") AppnexusSeat appnexusSeat,
      @QueryParam("last_modified") DateParameter lastModified);
  
  @POST
  @Path("/mobileAppInstanceList")
  void launchMobileAppInstanceListLoadJob(
      @QueryParam("override") @DefaultValue("false") Boolean override,
      @QueryParam("appnexus_seat") @DefaultValue("SEAT_MIQ_1") AppnexusSeat appnexusSeat,
      @QueryParam("last_modified") DateParameter lastModified);
  
  @POST
  @Path("/operatingSystem")
  void launchOperatingSystemLoadJob(@QueryParam("override") @DefaultValue("false") Boolean override,
      @QueryParam("appnexus_seat") @DefaultValue("SEAT_MIQ_1") AppnexusSeat appnexusSeat,
      @QueryParam("last_modified") DateParameter lastModified);
  
  @POST
  @Path("/operatingSystemExtended")
  void launchOperatingSystemExtendedLoadJob(
      @QueryParam("override") @DefaultValue("false") Boolean override,
      @QueryParam("appnexus_seat") @DefaultValue("SEAT_MIQ_1") AppnexusSeat appnexusSeat,
      @QueryParam("last_modified") DateParameter lastModified);
  
  @POST
  @Path("/operatingSystemFamily")
  void launchOperatingSystemFamilyLoadJob(
      @QueryParam("override") @DefaultValue("false") Boolean override,
      @QueryParam("appnexus_seat") @DefaultValue("SEAT_MIQ_1") AppnexusSeat appnexusSeat,
      @QueryParam("last_modified") DateParameter lastModified);
  
  @POST
  @Path("/seller")
  void launchSellerLoadJob(@QueryParam("override") @DefaultValue("false") Boolean override,
      @QueryParam("last_modified") DateParameter lastModified);
  
  @POST
  @Path("/dataProvider")
  void launchDataProviderJob(@QueryParam("override") @DefaultValue("false") Boolean override,
      @QueryParam("last_modified") DateParameter lastModified);
  
  
  @POST
  @Path("/profile/v3/")
  void launchProfileV3LoadJob(@QueryParam("override") @DefaultValue("false") Boolean override,
      @QueryParam("appnexus_seat") @DefaultValue("SEAT_MIQ_1") AppnexusSeat appnexusSeat);
  
  @POST
  @Path("/profile-segment-mapping")
  void launchProfileSegmentMappingJob(@QueryParam("override") @DefaultValue("false") Boolean override,
      @QueryParam("appnexus_seat") @DefaultValue("SEAT_MIQ_1") AppnexusSeat appnexusSeat,
      @QueryParam("last_modified") DateParameter lastModified);
  
  @POST
  @Path("/creative")
  void launchCreativeLoadJob(@QueryParam("override") @DefaultValue("false") Boolean override,
      @QueryParam("appnexus_seat") @DefaultValue("SEAT_MIQ_1") AppnexusSeat appnexusSeat,
      @QueryParam("last_modified") DateParameter lastModified);
  
  @POST
  @Path("/advertiser-meta")
  void launchAdvertiserMetaLoadJob(
      @QueryParam("appnexus_seat") @DefaultValue("SEAT_MIQ_1") AppnexusSeat seat,
      @QueryParam("last_modified") DateParameter lastModified)
      throws InterruptedException, ExecutionException, ParseException;
  
  @POST
  @Path("/creative-aggregation")
  void launchCreativeAggregation(@QueryParam("start_date") DateParameter startDate,
      @QueryParam("end_date") DateParameter endDate);
  
  @POST
  @Path("/add-clicker-segments")
  void launchAddClickerSegmentsForNewAdvertisersJob(
      @QueryParam("override") @DefaultValue("false") Boolean override,
      @QueryParam("appnexus_seat") @DefaultValue("SEAT_MIQ_1") AppnexusSeat seat,
      @QueryParam("last_modified") DateParameter lastModified);
  
  @POST
  @Path("/update-creatives-with-clicker-segment")
  void launchUpdateCreativesWithClickerSegmentJob(
      @QueryParam("override") @DefaultValue("false") Boolean override,
      @QueryParam("appnexus_seat") @DefaultValue("SEAT_MIQ_1") AppnexusSeat seat,
      @QueryParam("last_modified") DateParameter lastModified);
  
  @POST
  @Path("/entity-count")
  void launchAppnexusEntityCountLoadJob(
      @QueryParam("override") @DefaultValue("false") Boolean override,
      @QueryParam("appnexus_seat") @DefaultValue("SEAT_MIQ_1") AppnexusSeat seat,
      @QueryParam("last_modified") DateParameter lastModified);
  
  @POST
  @Path("/mobile-app")
  void launchMobileAppLoadJob(@QueryParam("override") @DefaultValue("false") Boolean override,
      @QueryParam("appnexus_seat") @DefaultValue("SEAT_MIQ_1") AppnexusSeat seat,
      @QueryParam("time_interval") @DefaultValue("last_2_days") TimeInterval timeInterval);
  
  @POST
  @Path("/dp-to-segments-map")
  void launchMapDpToSegmentsJob();
  
  @POST
  @Path("/pixel-to-creative-map")
  void launchUpdatePixelToCreativeMapping();

  @POST
  @Path("/placement-creative-mapping")
  void launchPlacementCreativeMappingLoadJob(@QueryParam("last_modified") DateParameter lastModified);
  
  @POST
  @Path("/dp-to-appnexus-segment")
  void launchSegmentDataPoviderMapping();
  
}
