package com.mediaiq.appnexus.batch.tasklet.report.domain;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.batch.value.StepAttribute;
import com.mediaiq.appnexus.repo.DataUsageStatRepo;

/**
 * Created by piyusha on 17/7/17.
 */
@Component
@Scope("step")
public class RawDataUsageStatsUploadTasklet implements Tasklet {
  
  @Value("#{jobExecutionContext['" + StepAttribute.FILE_PATH + "']}")
  String filePath;
  
  @Autowired
  private DataUsageStatRepo dataUsageStatRepo;
  
  @Override
  public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext)
      throws Exception {
    dataUsageStatRepo.uploadFileForDataUsage(filePath);
    return RepeatStatus.FINISHED;
  }
}
