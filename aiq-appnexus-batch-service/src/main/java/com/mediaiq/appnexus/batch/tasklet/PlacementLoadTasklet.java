package com.mediaiq.appnexus.batch.tasklet;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.mediaiq.appnexus.batch.service.ClientCreativeService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.domain.Creative;
import com.mediaiq.appnexus.domain.Placement;
import com.mediaiq.appnexus.domain.PlacementAudit;
import com.mediaiq.appnexus.load.client.AppnexusRestClient;
import com.mediaiq.appnexus.load.response.CreativeListResponse;
import com.mediaiq.appnexus.load.response.CreativeResponse;
import com.mediaiq.appnexus.load.response.PlacementListResponse;
import com.mediaiq.appnexus.load.response.ResponseContainer;
import com.mediaiq.appnexus.repo.CreativeRepo;
import com.mediaiq.appnexus.repo.PlacementAuditRepo;
import com.mediaiq.appnexus.repo.PlacementRepo;
import com.mediaiq.appnexus.value.AppnexusDate;
import com.mediaiq.appnexus.value.AppnexusSeat;

@Component
@Scope("step")
public class PlacementLoadTasklet extends
    AbstractLastModifiedSeatAwarePageLoadTasklet<Placement, PlacementRepo, PlacementListResponse> {
  
  @Value("#{jobParameters['show_augmented']}")
  protected Boolean showAugmented;
  
  @Autowired
  private PlacementRepo placementRepo;
  
  @Autowired
  private CreativeRepo creativeRepo;
  
  @Value("#{jobParameters['appnexus_seat']}")
  private AppnexusSeat seat;
  
  @Autowired
  @Qualifier("JOB")
  private AppnexusRestClient appnexusClient;
  
  @Autowired
  private PlacementAuditRepo placementAuditRepo;

  @Autowired
  ClientCreativeService clientCreativeService;

  private final Logger logger = LoggerFactory.getLogger(this.getClass());
  
  @Override
  public ResponseContainer<PlacementListResponse> getPageableResponse(
      AppnexusRestClient appnexusClient) {
    ResponseContainer<PlacementListResponse> container =
        appnexusClient.getPlacementList(startElement, new AppnexusDate(lastModified), true);
    logger.debug("Container: " + container);
    return container;
  }
  
  @Override
  public PlacementRepo getRepository() {
    return placementRepo;
  }
  
  @Override
  public List<Placement> getItemsFromResponse(PlacementListResponse response) {
    return response.getPlacements();
    
  }
  
  @Override
  protected void save(List<Placement> items) {
    List<Long> creativeIds = new ArrayList<>();
    
    for (Placement placement : items) {
      Set<Creative> creatives = placement.getCreatives();
      if (CollectionUtils.isNotEmpty(creatives)) {
        creatives.stream().forEach(k -> {
          creativeIds.add(k.getId());
        });
        List<Creative> creativeList = creativeRepo.findAll(creativeIds);
        creativeIds
            .remove(creativeList.stream().map(io -> io.getId()).collect(Collectors.toList()));
      }
    }
    
    // fetching missing creatives
    if (!creativeIds.isEmpty()) {
      List<List<Long>> creativePartition = ListUtils.partition(creativeIds, 100);
      creativePartition.forEach(cs -> {
        if (cs.size() == 1) {
          String creativesIdString = StringUtils.join(cs, ",");
          ResponseContainer<CreativeResponse> container1 =
              appnexusClient.getCreative(0, creativesIdString, 1);
          Creative creative = container1.getResponse().getCreative();
          if (creative != null) {
            creative.setSeat(seat);
            Set<Creative> creativeList = new HashSet<>();
            creativeList.add(container1.getResponse().getCreative());
            clientCreativeService.clientCreativeFindAndSave(creativeList);
          }
        } else {
          // handling pagination
          String creativesIdString = StringUtils.join(cs, ",");
          ResponseContainer<CreativeListResponse> container1 =
              appnexusClient.getCreatives(0, creativesIdString, 100);
          if (container1.getResponse().getCreatives() != null) {
            Set<Creative> creativeList = new HashSet<>(container1.getResponse().getCreatives());
            if (creativeList != null) {
              creativeList.stream().forEach(k -> k.setSeat(seat));
              clientCreativeService.clientCreativeFindAndSave(creativeList);
            }
          }
        }
      });
    }
    
    
    getRepository().save(items);
    
    for (Placement placement : items) {
      PlacementAudit placementAudit = new PlacementAudit(placement);
      placementAuditRepo.save(placementAudit);
    }
  }
}
