package com.mediaiq.appnexus.batch.tasklet;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.domain.profile.AppnexusProfile;
import com.mediaiq.appnexus.load.client.AppnexusRestClient;
import com.mediaiq.appnexus.load.response.AppnexusProfileListResponse;
import com.mediaiq.appnexus.load.response.ResponseContainer;
import com.mediaiq.appnexus.mongo.repo.AppnexusProfileRepo;

@Component
@Scope("step")
public class ProfileLoadTaskletV2 extends
    AbstractLastModifiedSeatAwarePageLoadTaskletMongo<AppnexusProfile, AppnexusProfileRepo, AppnexusProfileListResponse> {
  
  @Value("#{jobParameters['min_id']}")
  private Long minId;
  
  @Autowired
  private AppnexusProfileRepo profileRepo;
  
  @Override
  public List<AppnexusProfile> getItemsFromResponse(AppnexusProfileListResponse response) {
    return response.getProfiles();
  }
  
  @Override
  public ResponseContainer<AppnexusProfileListResponse> getPageableResponse(
      AppnexusRestClient appnexusClient) {
    try {
      Thread.sleep(1000);
    } catch (InterruptedException e) {
      // Throttling
      e.printStackTrace();
    }
    return appnexusClient.getProfileList(startElement, minId);
  }
  
  @Override
  public AppnexusProfileRepo getRepository() {
    return profileRepo;
  }
}
