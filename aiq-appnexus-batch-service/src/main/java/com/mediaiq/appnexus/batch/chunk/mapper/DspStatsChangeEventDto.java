package com.mediaiq.appnexus.batch.chunk.mapper;

import java.util.List;

import com.mediaiq.aiq.domain.Dsp;

public class DspStatsChangeEventDto {
  
  public String getDate() {
    return date;
  }
  
  public void setDate(String date) {
    this.date = date;
  }
  
  public Dsp getDsp() {
    return dsp;
  }
  
  public void setDsp(Dsp dsp) {
    this.dsp = dsp;
  }
  
  public List<Long> getPlacementIds() {
    return placementIds;
  }
  
  public void setPlacementIds(List<Long> placementIds) {
    this.placementIds = placementIds;
  }
  
  private List<Long> placementIds;
  private String date;
  private Dsp dsp;
  
  public DspStatsChangeEventDto(List<Long> placementIds, String date, Dsp dsp) {
    super();
    this.placementIds = placementIds;
    this.date = date;
    this.dsp = dsp;
  }
  
}
