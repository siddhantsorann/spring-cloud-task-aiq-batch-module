package com.mediaiq.appnexus.batch.tasklet;

import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.JpaRepository;

import com.mediaiq.appnexus.load.response.AbstractPagableResponse;

public abstract class AbstractLastModifiedAwarePageLoadTasklet<TYPE, REPO extends JpaRepository<TYPE, ?>, RESPONSE extends AbstractPagableResponse>
    extends AbstractPageLoadTasklet<TYPE, REPO, RESPONSE> {
  
  @Value("#{jobParameters['last_modified']}")
  protected Date lastModified;
  
}
