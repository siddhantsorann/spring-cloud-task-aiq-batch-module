package com.mediaiq.appnexus.batch.dto;

/**
 * Created by naveen on 11/4/17.
 */
public class ProfileSegmentDto {
  private Long profileId;
  private Long segmentId;
  
  public ProfileSegmentDto(Long profileId, Long segmentId) {
    this.profileId = profileId;
    this.segmentId = segmentId;
  }
  
  public Long getProfileId() {
    return profileId;
  }
  
  public void setProfileId(Long profileId) {
    this.profileId = profileId;
  }
  
  public Long getSegmentId() {
    return segmentId;
  }
  
  public void setSegmentId(Long segmentId) {
    this.segmentId = segmentId;
  }
  
  @Override
  public String toString() {
    return "ProfileSegmentDto{" + "profileId=" + profileId + ", segmentId=" + segmentId + '}';
  }
}
