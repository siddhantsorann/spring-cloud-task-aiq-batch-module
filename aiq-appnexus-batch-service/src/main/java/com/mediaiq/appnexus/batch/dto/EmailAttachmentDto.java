package com.mediaiq.appnexus.batch.dto;

import java.io.ByteArrayInputStream;

public class EmailAttachmentDto {
  private ByteArrayInputStream attachment;
  private String fileName;
  private String attachmentId;
  
  public String getAttachmentId() {
    return attachmentId;
  }
  public void setAttachmentId(String attachmentId) {
    this.attachmentId = attachmentId;
  }
  public ByteArrayInputStream getAttachment() {
    return attachment;
  }
  public void setAttachment(ByteArrayInputStream attachment) {
    this.attachment = attachment;
  }
  public String getFileName() {
    return fileName;
  }
  public void setFileName(String fileName) {
    this.fileName = fileName;
  }
  
}
