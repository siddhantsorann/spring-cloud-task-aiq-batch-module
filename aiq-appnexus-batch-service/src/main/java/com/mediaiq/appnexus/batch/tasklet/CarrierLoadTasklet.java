package com.mediaiq.appnexus.batch.tasklet;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.domain.Carrier;
import com.mediaiq.appnexus.load.client.AppnexusRestClient;
import com.mediaiq.appnexus.load.response.CarrierListResponse;
import com.mediaiq.appnexus.load.response.ResponseContainer;
import com.mediaiq.appnexus.repo.CarrierRepo;
import com.mediaiq.appnexus.value.AppnexusDate;

@Component
@Scope("step")
public class CarrierLoadTasklet extends
    AbstractLastModifiedSeatAwarePageLoadTasklet<Carrier, CarrierRepo, CarrierListResponse> {
  
  @Autowired
  private CarrierRepo carrierRepo;
  
  @Override
  public ResponseContainer<CarrierListResponse> getPageableResponse(
      AppnexusRestClient appnexusClient) {
    return appnexusClient.getCarrierList(startElement, new AppnexusDate(lastModified));
  }
  
  @Override
  public CarrierRepo getRepository() {
    return carrierRepo;
  }
  
  @Override
  public List<Carrier> getItemsFromResponse(CarrierListResponse response) {
    return response.getCarriers();
  }
}
