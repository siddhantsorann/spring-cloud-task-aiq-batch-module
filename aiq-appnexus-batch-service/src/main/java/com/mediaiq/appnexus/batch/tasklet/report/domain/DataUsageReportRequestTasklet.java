package com.mediaiq.appnexus.batch.tasklet.report.domain;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

import com.mediaiq.appnexus.batch.service.impl.AppnexusBatchServiceImpl;
import com.mediaiq.appnexus.domain.report.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.mediaiq.appnexus.batch.tasklet.report.ReportRequestTasklet;

/**
 * Created by piyusha on 17/7/17.
 */
@Component
@Scope("step")
public class DataUsageReportRequestTasklet extends ReportRequestTasklet {

  private static final Logger logger = LoggerFactory.getLogger(DataUsageReportRequestTasklet.class);

  String[] columns = {"day", "buyer_member_id", "geo_country_code", "campaign_id",
      "data_provider_id", "data_provider_name", "targeted_segment_ids", "imps",
      "data_costs_buying_currency", "sales_tax"};
  
  @Value("#{jobParameters['start_date']}")
  private Date startDate;
  
  @Value("#{jobParameters['end_date']}")
  private Date endDate;

  @Value("#{jobParameters['timezone']}")
  private String timezone;

  @Value("#{jobParameters['campaigns']}")
  private String campaigns;
  
  @Autowired
  @Qualifier("appnexusGson")
  Gson gson;
  
  @Override
  protected ReportRequest fetchReportRequest() {

    startDate=findFirstDate(startDate);
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:MM:SS");
    DateRangeReportRequest reportRequest = new DateRangeReportRequest();
    reportRequest.setReportType(ReportType.buyer_data_usage_analytics);
    reportRequest.setStartDate(dateFormat.format(startDate));
    reportRequest.setEndDate(dateFormat.format(endDate));
    reportRequest.setFormat(ReportFormat.csv);
    reportRequest.setColumns(columns);
    reportRequest.setTimezone(timezone);
    String[] values = gson.fromJson(campaigns, String[].class);
    reportRequest.addFilter(new Filter("campaign_id", values));

    reportRequest.setName("appnexus-daily-usage-stats");
    return reportRequest;
  }


  public Date findFirstDate(Date sDate){
        LocalDate date=sDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        date=date.withDayOfMonth(1);
        return java.sql.Date.valueOf(date);
    }
}
