package com.mediaiq.appnexus.batch.tasklet.report.domain;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.mediaiq.appnexus.batch.tasklet.report.ReportRequestTasklet;
import com.mediaiq.appnexus.domain.report.DateRangeReportRequest;
import com.mediaiq.appnexus.domain.report.Filter;
import com.mediaiq.appnexus.domain.report.ReportFormat;
import com.mediaiq.appnexus.domain.report.ReportRequest;
import com.mediaiq.appnexus.domain.report.ReportType;

@Component
@Scope("step")
public class NetworkAnalyticsTimeZoneReportRequestTasklet extends ReportRequestTasklet {

  String[] columns = {"day", "creative_id", "campaign_id", "imps", "clicks", "total_convs",
      "cost_buying_currency", "buying_currency", "insertion_order_id", "post_click_convs",
      "post_view_convs","view_measured_imps", "imps_viewed"};
  
  @Value("#{jobParameters['start_date']}")
  private Date startDate;
  
  @Value("#{jobParameters['end_date']}")
  private Date endDate;
  
  @Value("#{jobParameters['timezone']}")
  private String timezone;
  
  @Value("#{jobParameters['insertion_orders']}")
  private String insertionOrders;
  
  @Autowired
  @Qualifier("appnexusGson")
  Gson gson;
  
  @Override
  protected ReportRequest fetchReportRequest() {
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    DateRangeReportRequest reportRequest = new DateRangeReportRequest();
    reportRequest.setReportType(ReportType.network_analytics);
    reportRequest.setFormat(ReportFormat.csv);
    reportRequest.setStartDate(dateFormat.format(startDate));
    reportRequest.setEndDate(dateFormat.format(endDate));
    reportRequest.setColumns(columns);
    reportRequest.setTimezone(timezone);
    List<String> orders = new ArrayList<>();
    orders.add("campaign_id");
    orders.add("creative_id");
    orders.add("cost_buying_currency");
    reportRequest.setOrders(orders);
    String[] values = gson.fromJson(insertionOrders, String[].class);
    reportRequest.addFilter(new Filter("insertion_order_id", values));
    reportRequest.setName(
        "appnexus-daily-timezone-placement-stats-" + StringUtils.replace(timezone, "/", "-"));

    return reportRequest;
  }
  
}

