package com.mediaiq.appnexus.batch.chunk.mapper;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import com.mediaiq.appnexus.domain.MobileApp;
import com.mediaiq.appnexus.domain.id.MobileAppId;

public class MobileAppReportMapper implements FieldSetMapper<MobileApp> {
  
  @Override
  public MobileApp mapFieldSet(FieldSet fieldSet) throws BindException {
    MobileApp mobileApp = new MobileApp();
    mobileApp.setId(new MobileAppId(fieldSet.readString("mobile_application_id").toLowerCase(),
        fieldSet.readLong("operating_system_id"),
        fieldSet.readString("top_level_category_name").toLowerCase(),
        fieldSet.readString("second_level_category_name").toLowerCase(),
        fieldSet.readString("operating_system_name").toLowerCase(),
        fieldSet.readRawString("mobile_application_name").toLowerCase()));
    return mobileApp;
  }
  
}
