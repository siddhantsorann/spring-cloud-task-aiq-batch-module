package com.mediaiq.appnexus.batch.tasklet;

import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.mediaiq.appnexus.load.response.AbstractPagableResponse;
import com.mediaiq.appnexus.value.HasAppnexusSeat;

public abstract class AbstractLastModifiedSeatAwarePageLoadTaskletMongo<TYPE extends HasAppnexusSeat, REPO extends MongoRepository<TYPE, ?>, RESPONSE extends AbstractPagableResponse>
    extends AbstractSeatAwarePageLoadTaskletMongo<TYPE, REPO, RESPONSE> {
  
  @Value("#{jobParameters['last_modified']}")
  protected Date lastModified;
  
}
