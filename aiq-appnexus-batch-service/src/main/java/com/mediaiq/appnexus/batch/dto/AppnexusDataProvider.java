package com.mediaiq.appnexus.batch.dto;

public class AppnexusDataProvider {
  
  private Long id;
  private String name;
  
  public Long getId() {
    return id;
  }
  public void setId(Long id) {
    this.id = id;
  }
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }
  
  
}
