package com.mediaiq.appnexus.batch.tasklet.report.domain;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.batch.tasklet.report.ReportRequestTasklet;
import com.mediaiq.appnexus.domain.report.ReportFormat;
import com.mediaiq.appnexus.domain.report.ReportRequest;
import com.mediaiq.appnexus.domain.report.ReportType;
import com.mediaiq.appnexus.domain.report.TimeInterval;

@Component
@Scope("step")
public class NetworkAnalyticsReportRequestTasklet extends ReportRequestTasklet {
  
  String[] columns =
      {"day", "campaign_id", "seller_member_id", "imps", "clicks", "total_convs", "cost"};
  
  @Value("#{jobParameters['time_interval']}")
  private String timeInterval;
  
  @Override
  protected ReportRequest fetchReportRequest() {
    ReportRequest reportRequest = new ReportRequest();
    reportRequest.setReportType(ReportType.network_analytics);
    reportRequest.setReportInterval(TimeInterval.valueOf(timeInterval));
    reportRequest.setFormat(ReportFormat.csv);
    reportRequest.setColumns(columns);
    
    reportRequest.setName("appnexus-daily-placement-stats");
    return reportRequest;
  }
  
}
