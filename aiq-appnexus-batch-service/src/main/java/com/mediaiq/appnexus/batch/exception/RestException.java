package com.mediaiq.appnexus.batch.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class RestException extends WebApplicationException {
  
  private static final long serialVersionUID = 1L;
  
  public RestException(Status status) {
    super(Response.status(status.getCode()).entity(status.getMessage()).build());
  }
  
  public enum Status {
    
    FAILED_TO_LAUNCH_JOB(460,
        "Parameters passed for the request are invalid. Kindly change the request parameters"), FAILED_TO_CREATE_INSERTION_ORDER(
            460, "Error while writing to appnexus"), FAILED_TO_CREATE_LINE_ITEM(460,
                "Error while writing to appnexus"), FAILED_TO_CREATE_PLACEMENT(460,
                    "Error while writing to appnexus"), FAILED_TO_CREATE_PROFILE(460,
                        "Error while writing profile to appnexus");
    
    private int code;
    private String message;
    
    private Status(int code, String message) {
      this.code = code;
      this.message = message;
    }
    
    public int getCode() {
      return code;
    }
    
    public String getMessage() {
      return message;
    }
  }
  
}
