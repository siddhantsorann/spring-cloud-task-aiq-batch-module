package com.mediaiq.appnexus.batch.tasklet;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.domain.City;
import com.mediaiq.appnexus.load.client.AppnexusRestClient;
import com.mediaiq.appnexus.load.response.CityAllListReponse;
import com.mediaiq.appnexus.load.response.ResponseContainer;
import com.mediaiq.appnexus.repo.CityRepo;

/**
 * Created by piyush on 20/6/16.
 */

@Component
@Scope("step")
public class CityAllLoadTasklet
    extends AbstractPageLoadTasklet<City, CityRepo, CityAllListReponse> {
  
  @Autowired
  private CityRepo cityRepo;
  
  @Override
  public List<City> getItemsFromResponse(CityAllListReponse response) {
    return response.getCities();
  }
  
  @Override
  public ResponseContainer<CityAllListReponse> getPageableResponse(
      AppnexusRestClient appnexusClient) {
    System.out.print("**********inside the tasklet*****");
    System.out.print(" startElement " + startElement);
    return appnexusClient.getAllCityList(startElement);
  }
  
  @Override
  public CityRepo getRepository() {
    return cityRepo;
  }
  
  
}
