package com.mediaiq.appnexus.batch.tasklet;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.aiq.domain.value.DataProviderType;
import com.mediaiq.aiq.miq.domain.DataProvider;
import com.mediaiq.aiq.miq.domain.lookup.AdvertiserSegmentsLookup;
import com.mediaiq.aiq.miq.domain.lookup.ThirdPartySegmentsLookup;
import com.mediaiq.aiq.miq.repo.DataProviderRepo;
import com.mediaiq.aiq.miq.repo.lookup.AdvertiserSegmentsLookupRepo;
import com.mediaiq.aiq.miq.repo.lookup.ThirdPartySegmentsLookupRepo;
import com.mediaiq.appnexus.domain.Segment;
import com.mediaiq.appnexus.load.client.AppnexusRestClient;
import com.mediaiq.appnexus.load.response.ResponseContainer;
import com.mediaiq.appnexus.load.response.SegmentListResponse;
import com.mediaiq.appnexus.repo.SegmentRepo;
import com.mediaiq.appnexus.value.AppnexusDate;

// TODO: remove the dependency of miq-repo
@Component
@Scope("step")
public class SegmentLoadTasklet extends
    AbstractLastModifiedSeatAwarePageLoadTasklet<Segment, SegmentRepo, SegmentListResponse> {
  
  @Autowired
  private SegmentRepo segmentRepo;
  
  @Autowired
  private DataProviderRepo dataProviderRepo;
  
  @Autowired
  private AdvertiserSegmentsLookupRepo advertiserSegmentsLookupRepo;
  
  @Autowired
  private ThirdPartySegmentsLookupRepo thirdPartySegmentsLookupRepo;
  
  @Override
  public ResponseContainer<SegmentListResponse> getPageableResponse(
      AppnexusRestClient appnexusClient) {
    return appnexusClient.getSegmentList(startElement, new AppnexusDate(lastModified));
  }
  
  @Override
  public SegmentRepo getRepository() {
    return segmentRepo;
  }
  
  @Override
  public List<Segment> getItemsFromResponse(SegmentListResponse response) {
    return response.getSegments();
  }
  
  @Override
  protected void save(List<Segment> items) {
    super.save(items);
    
    /* JRVS-526 : Allow association of only DATA_SERVICE type data providers */
    List<DataProvider> dataProviders = dataProviderRepo.findByType(DataProviderType.DATA_SERVICE);
    
    for (Segment segment : items) {
      if (segment.getAdvertiserId() != null
          && advertiserSegmentsLookupRepo.findBySegmentId(segment.getId()) == null) {
        
        AdvertiserSegmentsLookup advertiserSegmentsLookup = new AdvertiserSegmentsLookup();
        advertiserSegmentsLookup.setSegment(segment);
        advertiserSegmentsLookupRepo.save(advertiserSegmentsLookup);
        
      }
      if (thirdPartySegmentsLookupRepo.findBySegmentId(segment.getId()) != null)
        continue;
      
      ThirdPartySegmentsLookup thirdPartySegmentsLookup = new ThirdPartySegmentsLookup();
      thirdPartySegmentsLookup.setSegment(segment);
      DataProvider dataProvider = getDataProviderFromSegment(segment, dataProviders);
      if (dataProvider != null) {
        thirdPartySegmentsLookup.setDataProvider(dataProvider);
      }
      thirdPartySegmentsLookupRepo.save(thirdPartySegmentsLookup);
    }
  }
  
  private DataProvider getDataProviderFromSegment(Segment segment,
      List<DataProvider> dataProviders) {
    
    if (segment.getName() == null || segment.getName().toLowerCase().contains("media iq"))
      return null;
    
    for (DataProvider dataProvider : dataProviders) {
      if (segment.getName().toLowerCase()
          .contains("(" + dataProvider.getName().toLowerCase() + ")"))
        return dataProvider;
      
      List<String> aliases = dataProvider.getAliases();
      for (String alias : aliases) {
        if (segment.getName().toLowerCase().contains("(" + alias.toLowerCase() + ")"))
          return dataProvider;
        
        if (segment.getName().toLowerCase().startsWith(alias.toLowerCase()))
          return dataProvider;
      }
    }
    
    return null;
  }
}
