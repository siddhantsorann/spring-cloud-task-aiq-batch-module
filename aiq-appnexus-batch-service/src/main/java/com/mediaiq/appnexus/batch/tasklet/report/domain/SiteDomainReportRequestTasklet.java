package com.mediaiq.appnexus.batch.tasklet.report.domain;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.batch.tasklet.report.ReportRequestTasklet;
import com.mediaiq.appnexus.domain.report.Filter;
import com.mediaiq.appnexus.domain.report.GroupFilter;
import com.mediaiq.appnexus.domain.report.ReportFormat;
import com.mediaiq.appnexus.domain.report.ReportRequest;
import com.mediaiq.appnexus.domain.report.ReportType;
import com.mediaiq.appnexus.domain.report.TimeInterval;

@Component
@Scope("step")
public class SiteDomainReportRequestTasklet extends ReportRequestTasklet {
  
  String[] columns = {"top_level_category", "second_level_category", "domain_name",
      "platform_audited_imps", "seller_audited_imps"};
  
  String[] filterValues = {"US", "GB"};
  
  @Override
  protected ReportRequest fetchReportRequest() {
    List<GroupFilter> grpFilters = getGroupFiltersList();
    List<Filter> filters = getFiltersList();
    List<String> rowPer = getRowPerList();
    List<String> orders = getOrderList();
    ReportRequest reportRequest = new ReportRequest();
    reportRequest.setReportType(ReportType.content_category_volume);
    reportRequest.setReportInterval(TimeInterval.last_7_days);
    reportRequest.setFormat(ReportFormat.csv);
    reportRequest.setColumns(columns);
    reportRequest.setName("appnexus-sitedomain-report");
    reportRequest.setFilters(filters);
    reportRequest.setGroupFilters(grpFilters);
    reportRequest.setRowPer(rowPer);
    reportRequest.setOrders(orders);
    return reportRequest;
  }
  
  private List<Filter> getFiltersList() {
    Filter filter = new Filter("geo_country", filterValues);
    List<Filter> filters = new ArrayList<Filter>();
    filters.add(filter);
    return filters;
  }
  
  private List<GroupFilter> getGroupFiltersList() {
    GroupFilter groupFilter = new GroupFilter("platform_audited_imps", ">", "500");
    List<GroupFilter> grpFilters = new ArrayList<GroupFilter>();
    grpFilters.add(groupFilter);
    return grpFilters;
  }
  
  private List<String> getRowPerList() {
    List<String> rowPer = new ArrayList<String>();
    rowPer.add("top_level_category");
    rowPer.add("second_level_category");
    rowPer.add("domain_name");
    return rowPer;
  }
  
  private List<String> getOrderList() {
    List<String> orders = getRowPerList();
    orders.add("platform_audited_imps");
    orders.add("seller_audited_imps");
    return orders;
  }
  
}
