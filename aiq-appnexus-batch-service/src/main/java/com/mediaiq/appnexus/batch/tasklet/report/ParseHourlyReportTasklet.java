package com.mediaiq.appnexus.batch.tasklet.report;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("step")
public class ParseHourlyReportTasklet implements Tasklet {
  
  final static Logger logger = LoggerFactory.getLogger(ParseHourlyReportTasklet.class);
  
  
  @Override
  public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext)
      throws Exception {
    
    return RepeatStatus.FINISHED;
  }
  
}
