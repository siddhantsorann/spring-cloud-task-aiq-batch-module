package com.mediaiq.appnexus.batch.tasklet.report;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.batch.value.StepAttribute;
import com.mediaiq.appnexus.repo.NetworkLookupStatRepo;

/**
 * Created by chandrahaas on 7/26/16.
 */
@Component
@Scope("step")
public class RawStatsUploadTasklet implements Tasklet {
  
  @Value("#{jobParameters['timezone']}")
  String timezone;
  @Value("#{jobExecutionContext['" + StepAttribute.FILE_PATH + "']}")
  String filePath;
  
  @Autowired
  private NetworkLookupStatRepo rawStatsRepo;
  
  @Override
  public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext)
      throws Exception {
    rawStatsRepo.uploadFile(timezone, filePath);
    return RepeatStatus.FINISHED;
  }
}
