package com.mediaiq.appnexus.batch.tasklet;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.mediaiq.appnexus.batch.service.ClientCreativeService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.domain.Creative;
import com.mediaiq.appnexus.domain.InsertionOrder;
import com.mediaiq.appnexus.domain.LineItem;
import com.mediaiq.appnexus.domain.LineItemAudit;
import com.mediaiq.appnexus.domain.Pixel;
import com.mediaiq.appnexus.load.client.AppnexusRestClient;
import com.mediaiq.appnexus.load.response.CreativeListResponse;
import com.mediaiq.appnexus.load.response.CreativeResponse;
import com.mediaiq.appnexus.load.response.InsertionOrderListResponse;
import com.mediaiq.appnexus.load.response.InsertionOrderResponse;
import com.mediaiq.appnexus.load.response.LineItemListResponse;
import com.mediaiq.appnexus.load.response.PixelListResponse;
import com.mediaiq.appnexus.load.response.PixelResponse;
import com.mediaiq.appnexus.load.response.ResponseContainer;
import com.mediaiq.appnexus.repo.CreativeRepo;
import com.mediaiq.appnexus.repo.InsertionOrderRepo;
import com.mediaiq.appnexus.repo.LineItemAuditRepo;
import com.mediaiq.appnexus.repo.LineItemRepo;
import com.mediaiq.appnexus.repo.PixelRepo;
import com.mediaiq.appnexus.value.AppnexusDate;
import com.mediaiq.appnexus.value.AppnexusSeat;


@Component
@Scope("step")
public class LineItemLoadTasklet extends
    AbstractLastModifiedSeatAwarePageLoadTasklet<LineItem, LineItemRepo, LineItemListResponse> {
  
  @Autowired
  LineItemRepo lineItemRepo;
  
  @Autowired
  LineItemAuditRepo lineItemAuditRepo;
  
  @Autowired
  InsertionOrderRepo insertionOrderRepo;
  
  @Autowired
  PixelRepo pixelRepo;
  
  @Autowired
  CreativeRepo creativeRepo;

  @Autowired
  ClientCreativeService clientCreativeService;
  
  @Value("#{jobParameters['appnexus_seat']}")
  private AppnexusSeat seat;
  
  @Autowired
  @Qualifier("JOB")
  private AppnexusRestClient appnexusClient;
  
  
  @Override
  public List<LineItem> getItemsFromResponse(LineItemListResponse response) {
    return response.getLineItems();
  }
  
  @Override
  public ResponseContainer<LineItemListResponse> getPageableResponse(
      AppnexusRestClient appnexusClient) {
    ResponseContainer<LineItemListResponse> container =
        appnexusClient.getLineItemList(startElement, new AppnexusDate(lastModified));
    return container;
  }
  
  @Override
  public LineItemRepo getRepository() {
    return lineItemRepo;
  }
  
  @Override
  protected void save(List<LineItem> items) {
    List<Long> ioIds = new ArrayList<>();
    List<Long> pixelIds = new ArrayList<>();
    List<Long> creativeIds = new ArrayList<>();
    
    for (LineItem lineItem : items) {
      Set<InsertionOrder> insertionOrderSet = lineItem.getInsertionOrders();
      Set<Pixel> pixels = lineItem.getPixels();
      Set<Creative> creatives = lineItem.getCreatives();
      
      // finding missing Ios,Creatives and Pixels
      
      if (CollectionUtils.isNotEmpty(insertionOrderSet)) {
        insertionOrderSet.stream().forEach(k -> {
          ioIds.add(k.getId());
        });
        List<InsertionOrder> insertionOrderList = insertionOrderRepo.findAll(ioIds);
        ioIds
            .remove(insertionOrderList.stream().map(io -> io.getId()).collect(Collectors.toList()));
      }
      if (CollectionUtils.isNotEmpty(pixels)) {
        pixels.stream().forEach(k -> {
          pixelIds.add(k.getId());
        });
        List<Pixel> pixelList = pixelRepo.findAll(pixelIds);
        pixelIds.remove(pixelList.stream().map(io -> io.getId()).collect(Collectors.toList()));
      }
      if (CollectionUtils.isNotEmpty(creatives)) {
        creatives.stream().forEach(k -> {
          creativeIds.add(k.getId());
        });
        List<Creative> creativeList = creativeRepo.findAll(creativeIds);
        creativeIds
            .remove(creativeList.stream().map(io -> io.getId()).collect(Collectors.toList()));
      }
    }
    // fetching missing Ios
    if (!ioIds.isEmpty()) {
      int count = ioIds.size();
      
      int i = 0;
      
      List<List<Long>> ioIdPartition = ListUtils.partition(ioIds, 100);
      ioIdPartition.forEach(ioPartition -> {
        
        if (ioPartition.size() == 1) {
          String ios = StringUtils.join(ioPartition, ",");
          ResponseContainer<InsertionOrderResponse> container0 =
              appnexusClient.getInsertionOrder(0, ios, 1);
          InsertionOrder insertionOrder = container0.getResponse().getInsertionOrder();
          if (insertionOrder != null) {
            insertionOrder.setSeat(seat);
            insertionOrderRepo.save(container0.getResponse().getInsertionOrder());
          }
        } else {
          // handling pagination
          String ios = StringUtils.join(ioPartition, ",");
          ResponseContainer<InsertionOrderListResponse> responseContainer =
              appnexusClient.getInsertionOrders(i, ios, 100);
          if (responseContainer.getResponse().getInsertionOrders() != null) {
            Set<InsertionOrder> ioList =
                new HashSet<>(responseContainer.getResponse().getInsertionOrders());
            if (ioList != null) {
              ioList.stream().forEach(k -> k.setSeat(seat));
              insertionOrderRepo.save(ioList);
            }
          }
        }
      });
      
      
    }
    
    
    // fetching missing pixels
    if (!pixelIds.isEmpty()) {
      List<List<Long>> pixelPartition = ListUtils.partition(pixelIds, 100);
      pixelPartition.forEach(ps -> {
        if (ps.size() == 1) {
          String pixelIdString = StringUtils.join(ps, ",");
          ResponseContainer<PixelResponse> container = appnexusClient.getPixel(0, pixelIdString, 1);
          if (container.getResponse().getPixel() != null) {
            pixelRepo.save(container.getResponse().getPixel());
          }
        } else {
          // handling pagination
          String pixelIdString = StringUtils.join(ps, ",");
          System.out.println(pixelIdString);
          ResponseContainer<PixelListResponse> container2 =
              appnexusClient.getPixels(0, pixelIdString, 100);
          if (container2.getResponse().getPixels() != null) {
            Set<Pixel> pixelList = new HashSet<>(container2.getResponse().getPixels());
            if (pixelList != null)
              pixelRepo.save(pixelList);
          }
        }
      });
      
    }
    
    
    
    // fetching missing creatives
    if (!creativeIds.isEmpty()) {
      List<List<Long>> creativePartition = ListUtils.partition(creativeIds, 100);
      creativePartition.forEach(cs -> {
        if (cs.size() == 1) {
          String creativesIdString = StringUtils.join(cs, ",");
          ResponseContainer<CreativeResponse> container1 =
              appnexusClient.getCreative(0, creativesIdString, 1);
          Creative creative = container1.getResponse().getCreative();
          if (creative != null) {
            creative.setSeat(seat);
            Set<Creative> creativeList = new HashSet<>();
            creativeList.add(container1.getResponse().getCreative());
            clientCreativeService.clientCreativeFindAndSave(creativeList);
          }
        } else {
          // handling pagination
          String creativesIdString = StringUtils.join(cs, ",");
          ResponseContainer<CreativeListResponse> container1 =
              appnexusClient.getCreatives(0, creativesIdString, 100);
          if (container1.getResponse().getCreatives() != null) {
            Set<Creative> creativeList = new HashSet<>(container1.getResponse().getCreatives());
            if (creativeList != null) {
              creativeList.stream().forEach(k -> k.setSeat(seat));
              clientCreativeService.clientCreativeFindAndSave(creativeList);
            }
          }
        }
      });
    }
    
    getRepository().save(items);
    
    for (LineItem lineItem : items) {
      LineItemAudit lineItemAudit = new LineItemAudit(lineItem);
      lineItemAuditRepo.save(lineItemAudit);
    }
  }
}
