package com.mediaiq.appnexus.batch.tasklet;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecutionException;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.scope.context.StepContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.batch.value.StepAttribute;
import com.mediaiq.appnexus.domain.Placement;
import com.mediaiq.appnexus.domain.ProfileToSegmentMapping;
import com.mediaiq.appnexus.domain.profile.AppnexusProfile;
import com.mediaiq.appnexus.domain.profile.SegmentTarget;
import com.mediaiq.appnexus.load.client.AppnexusRestClient;
import com.mediaiq.appnexus.load.response.AppnexusProfileListResponse;
import com.mediaiq.appnexus.load.response.ResponseContainer;
import com.mediaiq.appnexus.repo.PlacementRepo;
import com.mediaiq.appnexus.repo.ProfileToSegmentMappingRepo;
import com.mediaiq.appnexus.value.AppnexusSeat;
import com.mediaiq.appnexus.value.TargetAction;

/**
 * Created by naveen on 6/4/17.
 */
@Component
@Scope("step")
public class ProfileToSegmentLoadTaskletV4 implements Tasklet {
  
  private static final Logger logger = LoggerFactory.getLogger(ProfileLoadTaskletV3.class);
  
  @Autowired
  @Qualifier("JOB")
  private AppnexusRestClient appnexusClient;
  
  @Value("#{jobParameters['appnexus_seat']}")
  private AppnexusSeat seat;
  
  @Value("#{jobParameters['" + StepAttribute.START_ELEMENT + "']}")
  private Long startElement;
  
  @Autowired
  private PlacementRepo placementRepo;
  
  @Autowired
  private ProfileToSegmentMappingRepo profileToSegmentMappingRepo;
  
  private Long size = 100L;
  
  @Override
  public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext)
      throws Exception {
    Long offset = null;
    StepContext stepContext = chunkContext.getStepContext();
    ExecutionContext executionContext = stepContext.getStepExecution().getExecutionContext();
    
    if (executionContext.containsKey(StepAttribute.START_ELEMENT))
      offset = executionContext.getLong(StepAttribute.START_ELEMENT);
    else {
      offset = startElement;
    }
    
    // List<Long> profileIds = placementRepo.findProfileIdsBySeat(offset, size, seat.name());
    LocalDate dateLimit = LocalDate.of(2016, 01, 01);
    
    List<Placement> profilesBySeat = placementRepo.findProfilesBySeat(offset, size, seat.name(),
        Date.from(dateLimit.atStartOfDay(ZoneId.systemDefault()).toInstant()));
    if (profilesBySeat.size() == 0) {
      logger.info("Task finished.");
      return RepeatStatus.FINISHED;
    }
    
    List<String> profileIds =
        profilesBySeat.stream().map(Placement::getProfileId).collect(Collectors.toList());
    String str = StringUtils.join(profileIds, ",");
    ResponseContainer<AppnexusProfileListResponse> responseContainer =
        appnexusClient.getProfileList(str);
    AppnexusProfileListResponse response = responseContainer.getResponse();
    
    if (response.getErrorCode() != null || response.getError() != null) {
      throw new JobExecutionException("Appnexus Retrofit Error: " + response);
    }
    
    logger.debug("Response: " + response);
    
    Map<String, Long> profileIdToPlacementIdMap = profilesBySeat.stream()
        .collect(Collectors.toMap(Placement::getProfileId, Placement::getId, (p1, p2) -> p1));
    List<AppnexusProfile> appnexusProfiles = response.getProfiles();
    logger.debug("Items: " + appnexusProfiles);
    List<ProfileToSegmentMapping> profileToSegmentMappings = new ArrayList<>();
    
    for (AppnexusProfile profile : appnexusProfiles) {
      if (CollectionUtils.isEmpty(profile.getSegmentGroupTargets())) {
        continue;
      }
      List<Long> segmentIds = profile.getSegmentGroupTargets().stream()
          .flatMap(
              x -> x.getSegments().stream().filter(s -> s.getAction().equals(TargetAction.exclude)))
          .map(SegmentTarget::getSegmentId).collect(Collectors.toList());
      segmentIds.stream().forEach(x -> {
        ProfileToSegmentMapping profileToSegmentMapping = new ProfileToSegmentMapping(
            profile.getId(), x, profileIdToPlacementIdMap.get(profile.getId().toString()),
            profile.getSeat(), profile.getLastModified(), new Date(), new Date());
        profileToSegmentMappings.add(profileToSegmentMapping);
      });
    }
    
    profileToSegmentMappingRepo.save(profileToSegmentMappings);
    profileToSegmentMappings.clear();
    profileIdToPlacementIdMap.clear();
    
    executionContext.putLong(StepAttribute.START_ELEMENT, offset + this.size);
    return RepeatStatus.CONTINUABLE;
  }
}
