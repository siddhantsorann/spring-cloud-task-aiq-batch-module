package com.mediaiq.appnexus.batch.chunk.mapper;

import java.util.Date;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import com.mediaiq.aiq.domain.Cost;
import com.mediaiq.appnexus.domain.report.stats.id.DataUsageStatId;
import com.mediaiq.appnexus.domain.report.stats.raw.DataUsageStat;

/**
 * Created by piyusha on 19/7/17.
 */
public class DataUsageStatsMapper implements FieldSetMapper<DataUsageStat> {

  private String timezone;

  public DataUsageStatsMapper() {}

  public DataUsageStatsMapper(String timezone) {
      this.timezone=timezone;
  }
  
  @Override
  public DataUsageStat mapFieldSet(FieldSet set) throws BindException {
    DataUsageStat stats = new DataUsageStat();
    
    Long buyerMemberId = set.readLong("buyer_member_id");
    Long placementId = set.readLong("campaign_id");
    Long dataProviderId = set.readLong("data_provider_id");
    String targetedSegmentedIds = set.readString("targeted_segmented_ids");
    
    
    Date date = set.readDate("day", "yyyy-MM-dd");
    
    stats.setId(new DataUsageStatId(date, placementId, dataProviderId, targetedSegmentedIds));
    stats.setImpressions(set.readLong("imps"));
    stats.setDataCost(new Cost((set.readBigDecimal("data_costs_buying_currency")), null));
    stats.setDataProviderName(set.readString("data_provider_name"));
    stats.setSalesTaxCost(new Cost(set.readBigDecimal("sales_tax"), null));
    stats.setGeoCountryCode(set.readString("geo_country_code"));
    stats.setBuyerMemberId(buyerMemberId);
    stats.setTimezone(timezone);
    
    return stats;
  }
}
