package com.mediaiq.appnexus.batch.exception;

public class ReportNotReadyException extends RuntimeException {
  
  /**
   * 
   */
  private static final long serialVersionUID = -1217884438234539870L;
  
  /**
   * 
   */
  public ReportNotReadyException() {}
  
  /**
   * @param message
   */
  public ReportNotReadyException(String message) {
    super(message);
  }
  
  /**
   * @param cause
   */
  public ReportNotReadyException(Throwable cause) {
    super(cause);
  }
  
  /**
   * @param message
   * @param cause
   */
  public ReportNotReadyException(String message, Throwable cause) {
    super(message, cause);
  }
  
  /**
   * @param message
   * @param cause
   * @param enableSuppression
   * @param writableStackTrace
   */
  public ReportNotReadyException(String message, Throwable cause, boolean enableSuppression,
      boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
  
}
