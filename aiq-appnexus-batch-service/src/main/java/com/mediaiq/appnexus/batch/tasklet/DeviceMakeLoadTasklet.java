package com.mediaiq.appnexus.batch.tasklet;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.domain.DeviceMake;
import com.mediaiq.appnexus.load.client.AppnexusRestClient;
import com.mediaiq.appnexus.load.response.DeviceMakeListResponse;
import com.mediaiq.appnexus.load.response.ResponseContainer;
import com.mediaiq.appnexus.repo.DeviceMakeRepo;
import com.mediaiq.appnexus.value.AppnexusDate;

@Component
@Scope("step")
public class DeviceMakeLoadTasklet extends
    AbstractLastModifiedSeatAwarePageLoadTasklet<DeviceMake, DeviceMakeRepo, DeviceMakeListResponse> {
  
  @Autowired
  private DeviceMakeRepo deviceMakeRepo;
  
  @Override
  public ResponseContainer<DeviceMakeListResponse> getPageableResponse(
      AppnexusRestClient appnexusClient) {
    return appnexusClient.getDeviceMakeList(startElement, new AppnexusDate(lastModified));
  }
  
  @Override
  public DeviceMakeRepo getRepository() {
    return deviceMakeRepo;
  }
  
  @Override
  public List<DeviceMake> getItemsFromResponse(DeviceMakeListResponse response) {
    return response.getDeviceMakes();
  }
}
