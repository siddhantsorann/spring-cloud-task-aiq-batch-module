package com.mediaiq.appnexus.batch.jersey.mixin;

import org.codehaus.jackson.annotate.JsonBackReference;
import org.springframework.batch.core.JobExecution;

public abstract class StepExecutionMixIn {
  
  @JsonBackReference
  private JobExecution jobExecution;
  
  @JsonBackReference
  abstract public JobExecution getJobExecution();
}
