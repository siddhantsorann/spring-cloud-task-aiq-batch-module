package com.mediaiq.appnexus.batch.tasklet;


import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.mediaiq.appnexus.batch.service.RegularExpressionUtils;
import com.mediaiq.appnexus.domain.Creative;
import com.mediaiq.appnexus.load.client.AppnexusRestClient;
import com.mediaiq.appnexus.load.response.CreativeListResponse;
import com.mediaiq.appnexus.load.response.ResponseContainer;
import com.mediaiq.appnexus.repo.CreativeRepo;
import com.mediaiq.appnexus.value.AppnexusDate;


@Component
@Scope("step")
public class CreativeLoadTasklet extends
    AbstractLastModifiedSeatAwarePageLoadTasklet<Creative, CreativeRepo, CreativeListResponse> {
  
  @Autowired
  private CreativeRepo creativeRepo;
  
  private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
  
  @Override
  public ResponseContainer<CreativeListResponse> getPageableResponse(
      AppnexusRestClient appnexusClient) {
    ResponseContainer<CreativeListResponse> container =
        appnexusClient.getCreativeList(startElement, new AppnexusDate(lastModified), 1);
    try {
      Thread.sleep(1000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    LOGGER.debug("Container: " + container);
    return container;
  }
  
  @Override
  public CreativeRepo getRepository() {
    return creativeRepo;
  }
  
  @Override
  public List<Creative> getItemsFromResponse(CreativeListResponse response) {
    return response.getCreatives();
    
  }
  
  @Override
  protected void save(List<Creative> creatives) {
    
    creatives.stream().forEach(creative -> {
      if (StringUtils.isNotEmpty(creative.getContent())) {
        String clientCreativeId = RegularExpressionUtils.findPattern(creative.getContent());
        if (StringUtils.isNotEmpty(clientCreativeId) && clientCreativeId.matches(".*\\d+.*")) {
          creative.setClientCreativeId(clientCreativeId);
        }
      }
      
      if (StringUtils.isNotEmpty(creative.getName())
          && StringUtils.isEmpty(creative.getClientCreativeId())) {
        String clientCreativeId = RegularExpressionUtils.findPatternInName(creative.getName());
        if (StringUtils.isNotEmpty(clientCreativeId))
          creative.setClientCreativeId(clientCreativeId);
      }
    });
    
    LOGGER.info("Saving creatives with size {} after getting client creative id from the tag ",
        creatives.size());
    super.save(creatives);
    
  }
}
