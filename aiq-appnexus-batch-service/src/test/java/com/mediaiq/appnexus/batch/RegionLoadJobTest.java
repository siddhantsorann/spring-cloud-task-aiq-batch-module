package com.mediaiq.appnexus.batch;

import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.mediaiq.appnexus.batch.value.StepAttribute;

public class RegionLoadJobTest extends BaseTest {
  
  @Autowired
  @Qualifier("regionLoadJob")
  Job regionLoadJob;
  
  @Before
  public void setup() {
    super.setup();
    jobLauncherTestUtils.setJob(regionLoadJob);
  }
  
  @Test
  public void testRegionLoad() throws Exception {
    
    JobParameters jobParameters = new JobParametersBuilder()
        .addLong(StepAttribute.START_ELEMENT, 0L).addDate("date", new Date()).toJobParameters();
    
    JobExecution jobExecution = jobLauncherTestUtils.launchJob(jobParameters);
    Assert.assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());
  }
  
}
