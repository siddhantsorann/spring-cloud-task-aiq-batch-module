package com.mediaiq.appnexus.batch;

import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.mediaiq.appnexus.batch.value.StepAttribute;
import com.mediaiq.appnexus.repo.InsertionOrderRepo;
import com.mediaiq.appnexus.value.AppnexusSeat;

public class InsertionOrderLoadJobTest extends BaseTest {
  
  Date date;
  long countForSeat1 = 0;
  long countForSeat2 = 0;
  
  @Autowired
  @Qualifier("insertionOrderLoadJob")
  Job insertionOrderLoadJob;
  
  @Autowired
  InsertionOrderRepo insertionOrderRepo;
  
  
  @Before
  public void setup() {
    super.setup();
    jobLauncherTestUtils.setJob(insertionOrderLoadJob);
    date = getOlderDate(4);
  }
  
  @Test
  public void testInsertionOrderLoadForSeat1() throws Exception {
    
    JobParameters jobParameters =
        new JobParametersBuilder().addLong(StepAttribute.START_ELEMENT, 0L)
            .addDate("date", new Date()).addDate(StepAttribute.LAST_MODIFIED, date)
            .addString("appnexus_seat", AppnexusSeat.SEAT_MIQ_1.name()).toJobParameters();
    JobExecution jobExecution = jobLauncherTestUtils.launchJob(jobParameters);
    
    
    countForSeat1 = insertionOrderRepo.findByLast(date, AppnexusSeat.SEAT_MIQ_1);
    long jobId = jobExecution.getJobId();
    System.out.println(" job id " + jobId);
    
    List<StepExecution> stepExecutionList = (List<StepExecution>) jobExecution.getStepExecutions();
    StepExecution firstStepExecution = stepExecutionList.get(0);
    Long stepExceutionId = firstStepExecution.getJobExecutionId();
    System.out.println(" executionId " + stepExceutionId);
    
    ExecutionContext executionContext = firstStepExecution.getExecutionContext();
    System.out.println(" context " + executionContext.toString());
    
    int countFromAppnexus = (Integer) executionContext.get("count");
    System.out.println(" count value " + countFromAppnexus);
    
    Assert.assertEquals((long) countFromAppnexus, countForSeat1);
    Assert.assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());
  }
  
  @Test
  public void testInsertionOrderLoadForSeat2() throws Exception {
    
    JobParameters jobParameters =
        new JobParametersBuilder().addLong(StepAttribute.START_ELEMENT, 0L)
            .addDate("date", new Date()).addDate(StepAttribute.LAST_MODIFIED, date)
            .addString("appnexus_seat", AppnexusSeat.SEAT_MIQ_2.name()).toJobParameters();
    JobExecution jobExecution = jobLauncherTestUtils.launchJob(jobParameters);
    
    
    countForSeat2 = insertionOrderRepo.findByLast(date, AppnexusSeat.SEAT_MIQ_2);
    long jobId = jobExecution.getJobId();
    System.out.println(" job id " + jobId);
    
    List<StepExecution> stepExecutionList = (List<StepExecution>) jobExecution.getStepExecutions();
    StepExecution firstStepExecution = stepExecutionList.get(0);
    Long stepExceutionId = firstStepExecution.getJobExecutionId();
    System.out.println(" executionId " + stepExceutionId);
    
    ExecutionContext executionContext = firstStepExecution.getExecutionContext();
    System.out.println(" context " + executionContext.toString());
    
    int countFromAppnexus = (Integer) executionContext.get("count");
    System.out.println(" count value " + countFromAppnexus);
    
    Assert.assertEquals((long) countFromAppnexus, countForSeat2);
    Assert.assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());
  }
  
}
