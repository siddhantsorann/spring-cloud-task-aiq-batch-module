package com.mediaiq.appnexus.batch;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class CreativeAggregationJobTest extends BaseTest {
  
  @Autowired
  @Qualifier("creativeAggregationJob")
  private Job creativeAggregationJob;
  
  private LocalDate localDate;
  
  @Before
  public void init() {
    localDate = LocalDate.of(2017, 2, 2);
    jobLauncherTestUtils.setJob(creativeAggregationJob);
  }
  
  @Test
  public void testCreativeLoadJobAggregation() throws Exception {
    JobParameters jobParameters = new JobParametersBuilder()
        .addDate("process_date",
            Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant()))
        .toJobParameters();
    JobExecution jobExecution = jobLauncherTestUtils.launchJob(jobParameters);
    assertEquals(BatchStatus.COMPLETED, jobExecution.getStatus());
  }
}
