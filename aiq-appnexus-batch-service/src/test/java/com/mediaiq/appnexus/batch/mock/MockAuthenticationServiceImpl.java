package com.mediaiq.appnexus.batch.mock;

import org.springframework.beans.factory.annotation.Value;

import com.mediaiq.appnexus.load.service.AuthenticationService;
import com.mediaiq.appnexus.value.AppnexusSeat;

public class MockAuthenticationServiceImpl implements AuthenticationService {
  
  @Value("${appnexus.api.endpoint}")
  private String appnexusApiEndpoint;
  
  @Override
  public String getAuthenticationToken(AppnexusSeat seat) {
    System.out.println("Appnexus Seat: " + seat);
    return "hbapi:58796:552e46f2d6569:nym2";
  }
}
