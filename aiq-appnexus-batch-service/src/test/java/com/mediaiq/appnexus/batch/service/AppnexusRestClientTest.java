package com.mediaiq.appnexus.batch.service;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.mediaiq.appnexus.batch.BaseTest;
import com.mediaiq.appnexus.load.client.AppnexusRestClient;
import com.mediaiq.appnexus.load.response.ReportStatusCheckResponse;
import com.mediaiq.appnexus.load.response.ResponseContainer;

public class AppnexusRestClientTest extends BaseTest {
  
  @Autowired
  @Qualifier("JOB")
  AppnexusRestClient appnexusRestClient;
  
  @Test
  public void test() {
    ResponseContainer<ReportStatusCheckResponse> reportStatus =
        appnexusRestClient.getReportStatus("965a0030f4c9872274ea9ca8fd2a7a1a");
    System.out.println(reportStatus.getResponse().getExecutionStatus());
  }
}
