package com.mediaiq.appnexus.batch.service;

import java.util.concurrent.ExecutionException;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.mediaiq.appnexus.batch.BaseTest;
import com.mediaiq.appnexus.batch.scheduler.AppnexusBatchJobChainLauncher;

/**
 * Created by chandrahaas on 3/31/16.
 */
public class SchedulerTest extends BaseTest {
  
  @Autowired
  AppnexusBatchJobChainLauncher appnexusBatchJobChainLauncher;
  
  
  @Test
  public void testLaunchForRawStats() throws ExecutionException, InterruptedException {
    appnexusBatchJobChainLauncher.launchRawStatsJob();
  }
}
