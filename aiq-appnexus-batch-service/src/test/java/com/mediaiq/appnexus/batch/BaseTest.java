package com.mediaiq.appnexus.batch;

import java.util.Calendar;
import java.util.Date;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {AppnexusBatchServiceApplication.class})
public class BaseTest {
  
  JobLauncherTestUtils jobLauncherTestUtils;
  
  @Autowired
  private JobLauncher jobLauncher;
  
  @Autowired
  private JobRepository jobRepository;
  
  @Before
  public void setup() {
    jobLauncherTestUtils = new JobLauncherTestUtils();
    jobLauncherTestUtils.setJobLauncher(jobLauncher);
    jobLauncherTestUtils.setJobRepository(jobRepository);
  }
  
  protected Date getOlderDate(int days) {
    Calendar cal = Calendar.getInstance();
    cal.add(Calendar.DATE, -days);
    cal.add(Calendar.HOUR_OF_DAY, 0);
    Date date = cal.getTime();
    return date;
  }
}
