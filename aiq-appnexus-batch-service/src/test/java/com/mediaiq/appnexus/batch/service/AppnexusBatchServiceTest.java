package com.mediaiq.appnexus.batch.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.junit.Test;
import org.springframework.batch.core.JobExecution;
import org.springframework.beans.factory.annotation.Autowired;

import com.mediaiq.appnexus.batch.BaseTest;
import com.mediaiq.appnexus.domain.report.TimeInterval;
import com.mediaiq.appnexus.value.AppnexusSeat;

public class AppnexusBatchServiceTest extends BaseTest {
  
  @Autowired
  private AppnexusBatchService appnexuxBatchService;
  
  // @Autowired
  // private JobHistoryService jobHistoryService;
  
  @Autowired
  private AppnexusBatchServiceAysnc appnexuxBatchServiceAysnc;
  
  @Test
  public void testLaunchCountryLoadJobRequest() throws InterruptedException, ExecutionException {
    
    System.out.println("start");
    
    // jobHistoryService.getQueuedJobs();
    
    Future<JobExecution> launchCountryLoadJob =
        appnexuxBatchService.launchCountryLoadJob(false, AppnexusSeat.SEAT_MIQ_2);
    launchCountryLoadJob.get();
    // jobHistoryService.getQueuedJobs();
    
    System.out.println("end");
  }
  
  @Test
  public void testLaunchRegionLoadJobRequest() {
    
    appnexuxBatchService.launchRegionLoadJob(false, AppnexusSeat.SEAT_MIQ_2);
  }
  
  @Test
  public void testLaunchCityLoadJobRequest() {
    
    appnexuxBatchServiceAysnc.launchCityLoadJob("US", false, AppnexusSeat.SEAT_MIQ_2);
  }
  
  @Test
  public void testLaunchContentCategoryLoadJobRequest() {
    
    appnexuxBatchService.launchContentCategoryLoadJob(false, AppnexusSeat.SEAT_MIQ_2, null);
  }
  
  @Test
  public void testLaunchDomainListLoadJobRequest() {
    
    appnexuxBatchService.launchDomainListLoadJob(false, AppnexusSeat.SEAT_MIQ_2, null);
  }
  
  @Test
  public void testLaunchInsertionOrderLoadJobRequest() {
    LocalDate localDate = LocalDate.of(2017, 02, 15);
    Date date = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
    appnexuxBatchService.launchInsertionOrderLoadJob(false, AppnexusSeat.SEAT_MIQ_1, date);
  }
  
  @Test
  public void testLaunchLineItemLoadJobRequest() {
    
    appnexuxBatchService.launchLineItemLoadJob(false, AppnexusSeat.SEAT_MIQ_2, null);
  }
  
  @Test
  public void testLaunchPixelLoadJobRequest() {
    appnexuxBatchService.launchPixelLoadJob(false, AppnexusSeat.SEAT_MIQ_2, null);
  }
  
  @Test
  public void testLaunchPlacementLoadJobRequest() {
    
    appnexuxBatchService.launchPlacementLoadJob(false, AppnexusSeat.SEAT_MIQ_2, null, true);
  }
  
  @Test
  public void testLaunchSegmentLoadJobRequest() {
    
    appnexuxBatchService.launchSegmentLoadJob(false, AppnexusSeat.SEAT_MIQ_2, null);
  }
  
  @Test
  public void testLaunchSiteDomainReportLoadJobRequest() {
    
    appnexuxBatchService.launchSiteDomainReportLoadJob(false, AppnexusSeat.SEAT_MIQ_2);
  }
  
  
  @Test
  public void testBuyerLoadJob() throws ParseException, ExecutionException, InterruptedException {
    String startDateStr = "20-Dec-2016";
    String endDateStr = "21-Dec-2016";
    DateFormat formatter = new SimpleDateFormat("d-MMM-yyyy");
    Date startDate = formatter.parse(startDateStr);
    Date endDate = formatter.parse(endDateStr);
    appnexuxBatchService.launchBuyerSegmentStatLoadJob(true, AppnexusSeat.SEAT_MIQ_1, startDate,
        endDate);
  }
  
  @Test
  public void testNetworkLookupJob() throws ParseException {
    String startDateStr = "20-Dec-2016";
    String endDateStr = "21-Dec-2016";
    DateFormat formatter = new SimpleDateFormat("d-MMM-yyyy");
    Date startDate = formatter.parse(startDateStr);
    Date endDate = formatter.parse(endDateStr);
    appnexuxBatchService.launchAppnexusDailyPlacementTimeZoneStatsLoadJob(true,
        AppnexusSeat.SEAT_MIQ_1, startDate, endDate, null);
    appnexuxBatchService.launchAppnexusDailyPlacementTimeZoneStatsLoadJob(true,
        AppnexusSeat.SEAT_MIQ_2, startDate, endDate, null);
  }
  
  @Test
  public void testAppnexusYesterdayPlacementStatsLoadJobRequest()
      throws InterruptedException, ExecutionException {
    
    Future<JobExecution> launchAppnexusYesterdayPlacementStatsLoadJob =
        appnexuxBatchService.launchAppnexusYesterdayPlacementStatsLoadJob(TimeInterval.yesterday,
            true, AppnexusSeat.SEAT_MIQ_2);
    launchAppnexusYesterdayPlacementStatsLoadJob.get();
  }
  
}
