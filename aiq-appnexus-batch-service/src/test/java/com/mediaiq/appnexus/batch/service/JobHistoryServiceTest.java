package com.mediaiq.appnexus.batch.service;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.junit.Test;
import org.springframework.batch.core.launch.NoSuchJobException;
import org.springframework.beans.factory.annotation.Autowired;

import com.mediaiq.appnexus.batch.BaseTest;
import com.mediaiq.spring.batch.service.JobHistoryService;

public class JobHistoryServiceTest extends BaseTest {
  
  @Autowired
  private JobHistoryService jobHistoryService;
  
  @Test
  public void testGetJobInstance() throws NoSuchJobException {
    System.out.println(jobHistoryService.getJobInstances("countryLoadJob"));
  }
  
  @Test
  public void testGetJobExectionsForJobInstance()
      throws JsonGenerationException, JsonMappingException, IOException {
    System.out.println(jobHistoryService.getJobExecutionsForJobInstance(228L));
  }
  
  @Test
  public void testGetQueuedJobs() {
    System.out.println(jobHistoryService.getQueuedJobs());
  }
}
