package com.mediaiq.appnexus.batch;

import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.mediaiq.appnexus.batch.value.StepAttribute;
import com.mediaiq.appnexus.value.AppnexusSeat;

public class ProfileLoadJobTest extends BaseTest {
  
  @Autowired
  @Qualifier("profileLoadJob")
  Job profileLoadJob;
  
  @Before
  public void setup() {
    super.setup();
    jobLauncherTestUtils.setJob(profileLoadJob);
  }
  
  @Test
  public void testProfileLoad() throws Exception {
    
    JobParameters jobParameters =
        new JobParametersBuilder().addString("appnexus_seat", AppnexusSeat.SEAT_MIQ_1.name())
            .addLong(StepAttribute.START_ELEMENT, 0L)
            .addDate(StepAttribute.LAST_MODIFIED, getOlderDate(2)).addDate("date", new Date())
            .toJobParameters();
    
    JobExecution jobExecution = jobLauncherTestUtils.launchJob(jobParameters);
    Assert.assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());
  }
  
}
