package com.mediaiq.appnexus.batch;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.mediaiq.appnexus.repo.NetworkLookupStatRepo;

public class FileUploadTest extends BaseTest {
  
  @Autowired
  NetworkLookupStatRepo rawStatsRepo;
  
  @Test
  public void testLoad() {
    rawStatsRepo.uploadFile("Europe/London", "");
  }
  
}
