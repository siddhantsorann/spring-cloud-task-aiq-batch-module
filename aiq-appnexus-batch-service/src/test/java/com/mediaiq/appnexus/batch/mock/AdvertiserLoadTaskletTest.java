package com.mediaiq.appnexus.batch.mock;

import static org.easymock.EasyMock.expect;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.easymock.EasyMockSupport;
import org.junit.Before;
import org.junit.Test;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;

import com.mediaiq.appnexus.batch.tasklet.AdvertiserLoadTasklet;
import com.mediaiq.appnexus.domain.AppnexusAdvertiser;
import com.mediaiq.appnexus.load.client.AppnexusRestClient;
import com.mediaiq.appnexus.load.response.AdvertiserListResponse;
import com.mediaiq.appnexus.load.response.ResponseContainer;
import com.mediaiq.appnexus.value.AppnexusDate;
import com.mediaiq.appnexus.value.AppnexusSeat;
import com.mediaiq.appnexus.value.State;

public class AdvertiserLoadTaskletTest extends EasyMockSupport {
  
  AdvertiserLoadTasklet advertiserLoadTasklet = new AdvertiserLoadTasklet();
  
  private AppnexusRestClient appnexusRestClient;
  private Integer startElement = 0;
  Date date;
  AppnexusDate lastModified;
  
  @Before
  public void setUp() {
    appnexusRestClient = createNiceMock(AppnexusRestClient.class);
    advertiserLoadTasklet.setAppnexusClient(appnexusRestClient);
    date = new Date();
    lastModified = new AppnexusDate(date);
    advertiserLoadTasklet.setLastModified(date);
  }
  
  @Test
  public void testAdvertiserLoadTasklet() {
    
    // Create mock data
    StepContribution stepContribution = null;
    ChunkContext chunkContext = null;
    
    List<AppnexusAdvertiser> advertisers = new ArrayList<AppnexusAdvertiser>();
    AppnexusAdvertiser advertiser1 = new AppnexusAdvertiser(1L, "TestAdvertiser", State.active,
        AppnexusSeat.SEAT_MIQ_1, date, "UTC");
    advertisers.add(advertiser1);
    
    ResponseContainer<AdvertiserListResponse> mockAdvertiserResponse =
        new ResponseContainer<AdvertiserListResponse>();
    mockAdvertiserResponse.setResponse(new AdvertiserListResponse(advertisers));
    
    // record
    expect(appnexusRestClient.getAdvertiserList(startElement, lastModified))
        .andReturn(mockAdvertiserResponse);
    
    // play
    try {
      advertiserLoadTasklet.execute(stepContribution, chunkContext);
    } catch (Exception e) {
      fail("Should not throw exxception");
    }
    
    // Assert
  }
  
}
