package com.mediaiq.appnexus.batch.service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by naveen on 4/4/17.
 */

import org.apache.commons.lang.StringUtils;

public class RegularExpressionTest {
  
  private static final String URL1 =
      "<SCRIPT language='JavaScript1.1' SRC='https://ad.doubleclick.net/ddm/adj/N6198.286450AMNET/B10447346.139528193;sz=160x600;ord=[timestamp];dc_lat=;"
          + "dc_rdid=;tag_for_child_directed_treatment=?'></SCRIPT>"
          + "<NOSCRIPT><A HREF='https://ad.doubleclick.net/ddm/jump/N6198.286450AMNET/B10447346.139528193;sz=160x600;ord=[timestamp]?'>"
          + "<IMG SRC='https://ad.doubleclick.net/ddm/ad/N6198.286450AMNET/B10447346.139528193;sz=160x600;ord=[timestamp];dc_lat=;dc_rdid=;tag_for_child_directed_treatment=?"
          + "BORDER=0 WIDTH=160 HEIGHT=600 ALT='Advertisement'></A></NOSCRIPT>";
  
  private static final String URL2 =
      "<ins class='dcmads' style='display:inline-block;width:160px;height:600px' "
          + "data-dcm-placement='N9238.285985.MEDIAIQ/B10127228.146511793' "
          + "data-dcm-rendering-mode='script' " + "data-dcm-https-only "
          + "data-dcm-resettable-device-id='' "
          + "data-dcm-app-id=''> <script src='https://www.googletagservices.com/dcm/dcmads.js'></script></ins>";
  
  private static final String URL3 =
      "<script src=\"https://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=rsb&c=28&pli=20188487&PluID=0&w=728&h=90&ord=[timestamp]\"></script>\n"
          + "<noscript>\n"
          + "<a href=\"https://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=brd&FlightID=20188487&Page=&PluID=0&Pos=1242670627\" target=\"_blank\"><img src=\"https://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=bsr&FlightID=20188487&Page=&PluID=0&Pos=1242670627\" border=0 width=728 height=90></a>\n"
          + "</noscript>";
  
  private static final String URL4 =
      "<script src=\"https://fw.adsafeprotected.com/rjss/bs.serving-sys.com/75467/13461589/BurstingPipe/"
          + "adServer.bs?cn=rsb&c=28&pli=20357323&PluID=0&w=300&h=250&ord=[timestamp]&ucm=false&adsafe_preview=${IS_PREVIEW}\"></script> <noscript> "
          + "<a href=\"https://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=brd&FlightID=20357323&Page=&PluID=0&Pos=1308904883\" target=\"_blank\">"
          + "<img src=\"https://fw.adsafeprotected.com/rfw/bs.serving-sys.com/75467/13461588/BurstingPipe/adServer.bs?cn=bsr&FlightID=20357323&Page=&PluID=0&"
          + "Pos=1308904883&adsafe_preview=${IS_PREVIEW}\" border=0 width=300 height=250></a> </noscript>";
  
  private static final String URL5 = "<noscript>\n" + "\n" + "\n"
      + "<a href=\"https://servedby.flashtalking.com/click/1/56706;1587734;0;209;0/?ft_width=120&ft_height=600&url=8297538\" target=\"_blank\">\n"
      + "\n" + "\n"
      + "<img border=\"0\" src=\"https://servedby.flashtalking.com/imp/1/56706;1587734;205;gif;MediaIQ;MediaIQBAUAVEUDKEMEA0116RTQuotepageFTCPMDesktopDYN120x600DK/?\"></a>\n"
      + "\n" + "\n" + "</noscript>\n" + "\n" + "\n"
      + "<script language=\"Javascript1.1\" type=\"text/javascript\">\n" + "\n" + "\n"
      + "var ftClick = \"\";\n" + "\n" + "\n" + "var ftExpTrack_1587734 = \"\";\n" + "\n" + "\n"
      + "var ftX = \"\";\n" + "\n" + "\n" + "var ftY = \"\";\n" + "\n" + "\n" + "var ftZ = \"\";\n"
      + "\n" + "\n" + "var ftOBA = 1;\n" + "\n" + "\n" + "var ftContent = \"\";\n" + "\n" + "\n"
      + "var ftCustom = \"\";\n" + "\n" + "\n" + "var ft120x600_OOBclickTrack = \"\";\n" + "\n"
      + "\n" + "var ftRandom = Math.random()*1000000;\n" + "\n" + "\n"
      + "var ftBuildTag1 = \"<scr\";\n" + "\n" + "\n" + "var ftBuildTag2 = \"</\";\n" + "\n" + "\n"
      + "var ftClick_1587734 = ftClick;\n" + "\n" + "\n"
      + "if(typeof(ft_referrer)==\"undefined\"){var ft_referrer=(function(){var r=\"\";if(window==top){r=window.location.href;}else{try{r=window.parent.location.href;}catch(e){}r=(r)?r:document.referrer;}while(encodeURIComponent(r).length>1000){r=r.substring(0,r.length-1);}return r;}());}\n"
      + "\n" + "\n"
      + "var ftDomain = (window==top)?\"\":(function(){var d=document.referrer,h=(d)?d.match(\"(?::q/q/)+([qw-]+(q.[qw-]+)+)(q/)?\".replace(/q/g,decodeURIComponent(\"%\"+\"5C\")))[1]:\"\";return (h&&h!=location.host)?\"&ft_ifb=1&ft_domain=\"+encodeURIComponent(h):\"\";}());\n"
      + "\n" + "\n"
      + "var ftTag = ftBuildTag1 + 'ipt language=\"javascript1.1\" type=\"text/javascript\" ';\n"
      + "\n" + "\n"
      + "ftTag += 'src=\"https://servedby.flashtalking.com/imp/1/56706;1587734;201;js;MediaIQ;MediaIQBAUAVEUDKEMEA0116RTQuotepageFTCPMDesktopDYN120x600DK/?ftx='+ftX+'&fty='+ftY+'&ftadz='+ftZ+'&ftscw='+ftContent+'&ft_custom='+ftCustom+'&ftOBA='+ftOBA+ftDomain+'&ft_referrer='+encodeURIComponent(ft_referrer)+'&cachebuster='+ftRandom+'\" id=\"ftscript_120x600\" name=\"ftscript_120x600\"';\n"
      + "\n" + "\n" + "ftTag += '>' + ftBuildTag2 + 'script>';\n" + "\n" + "\n"
      + "document.write(ftTag);\n" + "\n" + "\n" + "</script>";
  
  private static final String URL6 =
      "<script src='https://ad.atdmt.com/d/a.js;p=11252202173060;a=11252202173224;cod=1;idfa=;aaid=;idfa_lat=;aaid_lat=;"
          + "cache=?click='></script><noscript><iframe frameborder='0' scrolling='no' marginheight='0' marginwidth='0' height='250' width='300' "
          + "src='https://ad.atdmt.com/d/a.html;p=11252202173060;a=11252202173224;cod=1;idfa=;aaid=;idfa_lat=;aaid_lat=;cache=?click=' topmargin='0' leftmargin='0' "
          + "allowtransparency='1'></iframe></noscript>";
  
  private static final String URL7 =
      "<script language=\"javascript\" src=\"https://track.adform.net/adfscript/?bn=11783585\"></script><noscript><a href=\"https://track.adform.net/C/?bn=11783585;C=0\" target=\"_blank\"><img src=\"https://track.adform.net/adfserve/?bn=11783585;srctype=4;ord=[timestamp]\" border=\"0\" width=\"300\" height=\"250\" alt=\"\"/></a></noscript>";
  
  private static final String URL8 = "<script type=\"text/javascript\">\n" + "var adperfobj = {\n"
      + "   account_id           : 2982\n" + "  ,tracking_element_id  : 82\n"
      + "  ,width                : 120\n" + "  ,height               : 600\n"
      + "  ,fullhost             : 'tearfund.solution.weborama.fr'\n"
      + "  ,random               : '[RANDOM]'\n" + "  ,burst                : 'auto'\n"
      + "  ,imptrackers          : []\n" + "  ,clicktrackers        : []\n"
      + "  ,publisherclick       : '[PUBLISHER_TRACKING_URL]'\n" + "};\n"
      + "document.write('<scr'+'ipt language=\"javascript\" src=\"http'+('https:'==document.location.protocol?'s':'')+'://cstatic.weborama.fr/js/advertiserv2/adperf_launch_1.0.0_scrambled.js\"></scr'+'ipt>');\n"
      + "</script>\n"
      + "<noscript><a href=\"http://tearfund.solution.weborama.fr/fcgi-bin/dispatch.fcgi?a.A=cl&a.si=2982&a.te=82&a.bk=1&g.ism=0&a.ra=[RANDOM]&g.lu=\" target=\"_blank\"><img src=\"https://tearfund.solution.weborama.fr/fcgi-bin/dispatch.fcgi?a.A=im&a.si=2982&a.te=82&a.he=600&a.wi=120&a.hr=R&a.bk=1&g.ism=0&a.ra=[RANDOM]\" width=\"120\" height=\"600\" style=\"border:0px\"></a></noscript>";
  
  private static final String URL9 =
      "<script language=\"JavaScript1.1\" src=\"https://rover.ebay.com/ar/1/160198/4?mpt=[CACHEBUSTER]&ff5=[CREATIVE_NAME]&ff7=[CREATIVE+SIZE]&ff8=[MOTIF]&ff9=[DEVICE]&ff18=[SET]&siteid=77&icep_siteid=77&ipn=admain2&adtype=3&size=300x250&pgroup=436667&mpvc=\"></script>\n"
          + "<noscript>\n"
          + "    <a href=\"https://rover.ebay.com/rover/1/160198/4?mpt=[CACHEBUSTER]&ff5=[CREATIVE_NAME]&ff7=[CREATIVE+SIZE]&ff8=[MOTIF]&ff9=[DEVICE]&ff18=[SET]&siteid=77\">\n"
          + "        <img src=\"https://rover.ebay.com/ar/1/160198/4?mpt=[CACHEBUSTER]&ff5=[CREATIVE_NAME]&ff7=[CREATIVE+SIZE]&ff8=[MOTIF]&ff9=[DEVICE]&ff18=[SET]&siteid=77&icep_siteid=77&ipn=admain2&adtype=1&size=300x250&pgroup=436667\" alt=\"Click Here\" border=\"0\">\n"
          + "    </a>\n" + "</noscript>";
  
  private static final String URL10 =
      "<script type='text/javascript' src='https://tags.qservz.com/ct_adj/3606/4203/25580?sz=120x600&li=56512&rnd=${CACHEBUSTER}&click3rd=${CLICK_URL}'></script>\n"
          + "<noscript><a target='_blank' href='https://tags.qservz.com/jump/3606/4203/25580'><img width='120' height='600' src='https://tags.qservz.com/ct_ad/3606/4203/25580?sz=120x600&li=56512&rnd=${CACHEBUSTER}&click3rd=${CLICK_URL}'/></a></noscript>";
  
  private static final String URL11 = "\n"
      + "<script type=\"text/javascript\" src=\"https://imagesrv.adition.com/js/adition.js\"></script>\n"
      + "<script type=\"text/javascript\" src=\"https://adfarm1.adition.com/js?wp_id=2858535&kid=1474057\"></script>\n"
      + "<noscript><a href=\"https://adfarm1.adition.com/click?sid=2858535&ts=[timestamp]\">\n"
      + "<img src=\"https://adfarm1.adition.com/banner?sid=2858535&kid=1474057&ts=[timestamp]\" border=\"0\"></a></noscript>";
  
  private static final String URL12 =
      "<script type=\"text/javascript\" src=\"https://adfarm.mediaplex.com/ad/js/21121-226459-36162-2?mpt=[CACHEBUSTER]&mpvc=\">\n"
          + "</script>\n" + "<noscript>\n"
          + " <a target=\"_blank\" href=\"https://adfarm.mediaplex.com/ad/nc/21121-226459-36162-2?mpt=[CACHEBUSTER]\">\n"
          + " <img src=\"https://adfarm.mediaplex.com/ad/nb/21121-226459-36162-2?mpt=[CACHEBUSTER]\"\n"
          + "alt=\"Click Here\" border=\"0\">\n" + "</a>\n" + "</noscript>";
  
  private static final Pattern DOUBLECLICKPATTERN = Pattern.compile(".([0-9]*);");
  private static final Pattern DCMINS = Pattern.compile("([0-9])+'");
  private static final Pattern SIZEMEK = Pattern.compile("pli=([0-9]*)&");
  private static final Pattern FLASHTALKING = Pattern.compile("ftExpTrack_*.([0-9]*) =");
  private static final Pattern ALTAS = Pattern.compile("p=*([0-9]*);");
  private static final Pattern WEBORAMA = Pattern.compile("tracking_element_id  :.([0-9]*)\n");
  private static final Pattern ADITION = Pattern.compile("wp_id=([0-9]*)&");
  private static final Pattern BURSTINGPIPE = Pattern.compile("pli=([0-9]*)&");
  private static final Pattern ADFORM = Pattern.compile("bn=([0-9]*)");
  private static final Pattern EBAY = Pattern.compile("([0-9]){3,100}\\/");
  private static final Pattern QSERVZ = Pattern.compile("\\/([0-9]*)\\?");
  private static final Pattern MEDIAPLEX = Pattern.compile("-([0-9]*)");
  
  public static void main(String[] args) {
    findPattern(URL6);
  }
  
  public static String findPattern(String tagScript) {
    
    if (tagScript.contains("https://bs.serving-sys.com")
        || tagScript.contains("https://fw.adsafeprotected.com")) {
      Matcher matcher = BURSTINGPIPE.matcher(tagScript);
      if (matcher.find()) {
        String sanitized = StringUtils.remove(matcher.group(0), "pli=");
        String formatted = StringUtils.remove(sanitized, "&");
        System.out.printf("Pattern found %s%n", formatted);
        return formatted;
      }
    }
    
    if (tagScript.contains("https://ad.doubleclick.net")) {
      Matcher matcher = DOUBLECLICKPATTERN.matcher(tagScript);
      if (matcher.find()) {
        System.out.println("Pattern found " + matcher.group(0).replace(".", "").replace(";", ""));
        return matcher.group(0).replace(".", "").replace(";", "");
      }
    }
    
    if (tagScript.contains("data-dcm-placement")) {
      Matcher matcher = DCMINS.matcher(tagScript);
      if (matcher.find()) {
        System.out.println("Pattern found " + matcher.group(0).replace("'", ""));
        return matcher.group(0).replace("'", "");
      }
    }
    
    if (tagScript.contains("https://servedby.flashtalking.com")) {
      Matcher matcher = FLASHTALKING.matcher(tagScript);
      if (matcher.find()) {
        String sanitized = StringUtils.remove(matcher.group(0), "ftExpTrack_");
        String formatted = StringUtils.remove(sanitized, " =");
        System.out.println("Pattern found " + formatted.trim());
        return formatted.trim();
      }
    }
    
    if (tagScript.contains("https://ad.atdmt.com")) {
      Matcher matcher = ALTAS.matcher(tagScript);
      if (matcher.find()) {
        System.out.println("Pattern found " + matcher.group(0).replace("p=", "").replace(";", ""));
        return matcher.group(0).replace("p=", "").replace(";", "");
      }
    }
    
    if (tagScript.contains("http://tearfund.solution.weborama")) {
      Matcher matcher = WEBORAMA.matcher(tagScript);
      if (matcher.find()) {
        String sanitized = StringUtils.remove(matcher.group(0), "tracking_element_id  :");
        System.out.println("Pattern found " + sanitized.trim());
        return sanitized.trim();
      }
    }
    
    if (tagScript.contains("https://imagesrv.adition.com")) {
      Matcher matcher = ADITION.matcher(tagScript);
      if (matcher.find()) {
        String format = StringUtils.remove(matcher.group(0), "wp_id=");
        String creativeId = StringUtils.remove(format, "&");
        System.out.println("Pattern found " + creativeId);
        return creativeId;
      }
    }
    
    if (tagScript.contains("https://track.adform.net")) {
      Matcher matcher = ADFORM.matcher(tagScript);
      if (matcher.find()) {
        String sanitized = StringUtils.remove(matcher.group(0), "bn=");
        String formatted = StringUtils.remove(sanitized, "^\"|\"$");
        System.out.println("Pattern found " + formatted);
        return formatted;
      }
    }
    
    if (tagScript.contains("https://rover.ebay.com/rover")) {
      Matcher matcher = EBAY.matcher(tagScript);
      if (matcher.find()) {
        String split = StringUtils.remove(matcher.group(0), "/");
        System.out.println(split.trim());
        return split.trim();
      }
    }
    
    if (tagScript.contains("https://tags.qservz.com")) {
      Matcher matcher = QSERVZ.matcher(tagScript);
      if (matcher.find()) {
        String format = StringUtils.remove(matcher.group(0), "?");
        String creativeId = StringUtils.remove(format, "/");
        System.out.println("pattern found " + creativeId);
        return creativeId;
      }
    }
    
    // if(URL12.contains("https://adfarm.mediaplex.com")){
    // Matcher matcher = MEDIAPLEX.matcher(URL12);
    // if(matcher.find()){
    // String formatted = matcher.group(1) + matcher.group(2);
    // System.out.println("Pattern found "+ formatted);
    // return formatted;
    // }
    // }
    
    return "";
  }
  
}
