package com.mediaiq.appnexus.batch;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

import com.mediaiq.appnexus.batch.service.AppnexusBatchService;
import com.mediaiq.appnexus.batch.value.StepAttribute;
import com.mediaiq.appnexus.value.AppnexusSeat;

public class ProfileSegmentMappingLoadJobTest extends BaseTest {
  
  
  @Value("#{'${appnexusSeats}'.split(',')}")
  private List<String> appnexusSeats;
  
  @Autowired
  @Qualifier("profileSegmentMappingLoadJob")
  Job profileSegmentMappingLoadJob;
  
  List<AppnexusSeat> appnexusSeatsToBeProcessed;
  
  @Autowired
  private AppnexusBatchService appnexusBatchService;
  
  @Before
  public void setup() {
    super.setup();
    jobLauncherTestUtils.setJob(profileSegmentMappingLoadJob);
    appnexusSeatsToBeProcessed =
        appnexusSeats.stream().map(AppnexusSeat::valueOf).collect(Collectors.toList());
  }
  
  @Test
  public void testProfileSegmentLoadJob() throws Exception {
    
    for (AppnexusSeat appnexusSeat : appnexusSeatsToBeProcessed) {
      JobParameters jobParameters =
          new JobParametersBuilder().addString("appnexus_seat", appnexusSeat.name())
              .addLong(StepAttribute.START_ELEMENT, 185500L)
              .addDate("date",
                  Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant()))
              .toJobParameters();
      
      JobExecution jobExecution = jobLauncherTestUtils.launchJob(jobParameters);
      Assert.assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());
    }
  }
  
  @Test
  public void testAppnexusBatchService() throws Exception {
    Date lastModified = getOlderDate(500);
    appnexusBatchService.launchProfileSegmentMappingLoadJob(false, AppnexusSeat.SEAT_MIQ_1,
        lastModified);
  }
  
}
