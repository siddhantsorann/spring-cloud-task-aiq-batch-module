package com.mediaiq.appnexus.batch;

import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.mediaiq.appnexus.domain.report.TimeInterval;
import com.mediaiq.appnexus.value.AppnexusSeat;

public class MobileAppReportLoadJobTest extends BaseTest {
  
  Date date;
  
  @Autowired
  @Qualifier("mobileAppLoadJob")
  Job mobileAppLoadJob;
  
  @Before
  public void setup() {
    super.setup();
    jobLauncherTestUtils.setJob(mobileAppLoadJob);
    date = getOlderDate(2);
  }
  
  @Test
  public void testMobileAppReportLoadJob() throws Exception {
    JobParameters jobParameters =
        new JobParametersBuilder().addString("appnexus_seat", AppnexusSeat.SEAT_MIQ_1.name())
            .addString("time_interval", TimeInterval.last_7_days.name()).addDate("date", new Date())
            .toJobParameters();
    
    JobExecution jobExecution = jobLauncherTestUtils.launchJob(jobParameters);
    Assert.assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());
    
  }
  
  
}
