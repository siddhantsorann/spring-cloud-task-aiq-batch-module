package com.mediaiq.appnexus.batch;

import java.util.Calendar;
import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mediaiq.appnexus.value.AppnexusSeat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = AppnexusBatchServiceApplication.class)
public class DspCreativeDailyStatsLoadJobTest extends BaseTest {
  
  @Autowired
  @Qualifier("appnexusDailyDspCreativeStatsLoadJob")
  Job appnexusCreativeStatsLoadJob;
  
  @Before
  public void setup() {
    super.setup();
    jobLauncherTestUtils.setJob(appnexusCreativeStatsLoadJob);
  }
  
  @Test
  public void test() {
    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
    jobParametersBuilder.addString("appnexus_seat", AppnexusSeat.SEAT_MIQ_1.name())
        .addString("timezone", "UTC");
    Calendar cal = Calendar.getInstance();
    cal.add(Calendar.DATE, -18);
    cal.set(Calendar.HOUR, 0);
    cal.set(Calendar.MINUTE, 0);
    cal.set(Calendar.SECOND, 0);
    Date startDate = cal.getTime();
    System.out.println(startDate);
    cal.add(Calendar.DATE, 2);
    cal.set(Calendar.HOUR, 0);
    cal.set(Calendar.MINUTE, 0);
    cal.set(Calendar.SECOND, 0);
    Date endDate = cal.getTime();
    if (startDate != null)
      jobParametersBuilder.addDate("start_date", startDate);
    if (endDate != null)
      jobParametersBuilder.addDate("end_date", endDate);
    JobExecution jobExecution = null;
    try {
      jobExecution = jobLauncherTestUtils.launchJob(jobParametersBuilder.toJobParameters());
    } catch (Exception e) {
      e.printStackTrace();
    }
    Assert.assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());
    System.out.println("Saved.............................................");
  }
  
  
}
