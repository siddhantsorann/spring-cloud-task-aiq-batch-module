package com.mediaiq.appnexus.batch;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.mediaiq.appnexus.batch.service.RegularExpressionUtils;
import com.mediaiq.appnexus.domain.Creative;
import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.mediaiq.appnexus.batch.service.AppnexusBatchService;
import com.mediaiq.appnexus.batch.value.StepAttribute;
import com.mediaiq.appnexus.repo.CreativeRepo;
import com.mediaiq.appnexus.value.AppnexusSeat;

public class CreativeLoadJobTest extends BaseTest {
  
  Date date;
  long countForSeat1 = 0;
  long countForSeat2 = 0;
  
  @Autowired
  @Qualifier("creativeLoadJob")
  Job creativeLoadJob;
  
  @Autowired
  CreativeRepo creativeRepo;
  
  private LocalDate localDate;
  
  @Autowired
  private AppnexusBatchService appnexusBatchService;
  
  @Override
  @Before
  public void setup() {
    super.setup();
    // date = getOlderDate(20);
    localDate = LocalDate.of(2017, 04, 10);
    date = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
    jobLauncherTestUtils.setJob(creativeLoadJob);
  }
  
  @Test
  public void testCreativeLoadForSeat1() throws Exception {
    
    JobParameters jobParameters =
        new JobParametersBuilder().addString("appnexus_seat", AppnexusSeat.SEAT_MIQ_1.name())
            .addLong(StepAttribute.START_ELEMENT, 0L).addDate(StepAttribute.LAST_MODIFIED, date)
            .addDate("date", new Date()).toJobParameters();
    System.out.println(" ************ date ********" + date);
    
    JobExecution jobExecution = jobLauncherTestUtils.launchJob(jobParameters);
    
    countForSeat1 = creativeRepo.findByLast(date, AppnexusSeat.SEAT_MIQ_1);
    long jobId = jobExecution.getJobId();
    System.out.println(" job id " + jobId);
    
    List<StepExecution> stepExecutionList = (List<StepExecution>) jobExecution.getStepExecutions();
    StepExecution firstStepExecution = stepExecutionList.get(0);
    Long stepExceutionId = firstStepExecution.getJobExecutionId();
    System.out.println(" executionId " + stepExceutionId);
    
    ExecutionContext executionContext = firstStepExecution.getExecutionContext();
    System.out.println(" context " + executionContext.toString());
    
    int countFromAppnexus = (Integer) executionContext.get("count");
    System.out.println(" count value " + countFromAppnexus);
    
    Assert.assertEquals(countFromAppnexus, countForSeat1);
    Assert.assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());
    
  }
  
  @Test
  public void testCreativeLoadForSeat2() throws Exception {
    
    JobParameters jobParameters =
        new JobParametersBuilder().addString("appnexus_seat", AppnexusSeat.SEAT_MIQ_2.name())
            .addLong(StepAttribute.START_ELEMENT, 0L).addDate(StepAttribute.LAST_MODIFIED, date)
            .addDate("date", new Date()).toJobParameters();
    System.out.println(" ************ date ********" + date);
    
    JobExecution jobExecution = jobLauncherTestUtils.launchJob(jobParameters);
    
    countForSeat2 = creativeRepo.findByLast(date, AppnexusSeat.SEAT_MIQ_2);
    long jobId = jobExecution.getJobId();
    System.out.println(" job id " + jobId);
    
    List<StepExecution> stepExecutionList = (List<StepExecution>) jobExecution.getStepExecutions();
    StepExecution firstStepExecution = stepExecutionList.get(0);
    Long stepExceutionId = firstStepExecution.getJobExecutionId();
    System.out.println(" executionId " + stepExceutionId);
    
    ExecutionContext executionContext = firstStepExecution.getExecutionContext();
    System.out.println(" context " + executionContext.toString());
    
    int countFromAppnexus = (Integer) executionContext.get("count");
    System.out.println(" count value " + countFromAppnexus);
    
    Assert.assertEquals(countFromAppnexus, countForSeat2);
    Assert.assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());
    
  }
  
  @Test
  public void testCreativeLoadJob() throws Exception {
    appnexusBatchService.launchCreativeLoadJob(false, AppnexusSeat.SEAT_MIQ_1, date);
  }

  @Test
  public void testCreativeFinderLoadJob() throws Exception {


    Creative creative1 = new Creative();
    creative1.setContent("document.write('<ins class=\\'dcmads\\' style=\\'display:inline-block;width:120px;height:600px\\'\\r\\n    data-dcm-click-tracker=\\'${CLICK_URL}\\' data-dcm-placement=\\'N5960.285985.MEDIAIQ1/B20920719.218228515\\'\\r\\n    data-dcm-rendering-mode=\\'script\\'\\r\\n    data-dcm-https-only\\r\\n    data-dcm-resettable-device-id=\\'\\'\\r\\n    data-dcm-app-id=\\'\\'\\r\\n    data-dcm-user-defined=\\'${AUCTION_ID}\\'>\\r\\n  <scr' + 'ipt src=\\'https://www.googletagservices.com/dcm/dcmads.js\\'></scr' + 'ipt>\\r\\n</ins>')");
    creative1.setName("wo IAS_120x600_SD_-_Evaluation_Audience_-_35+ Male Audience_DCM_218228515");

    Creative creative2 = new Creative();
    creative2.setContent("document.write('<SCR' + 'IPT language=\\'JavaScript1.1\\' SRC=\\\"https://fw.adsafeprotected.com/rjss/dc/158522/24322341/ddm/adj/N476201.797335XAXIS.CO.UK/B20994192.218716227;sz=300x600;click=${CLICK_URL};ord=${CACHEBUSTER};dc_lat=;dc_rdid=;tag_for_child_directed_treatment=?\\\">\\r\\n</SCR' + 'IPT>');");
    creative2.setName("wi IAS_MediaIQ_Aftersales_Q2_300x600_ValueService_Pros_DCM_218716227");

    List<Creative> creatives = new ArrayList<>();
    creatives.add(creative1);
    creatives.add(creative2);

    creatives.stream().forEach(creative -> {
      if (StringUtils.isNotEmpty(creative.getContent())) {
        String clientCreativeId = RegularExpressionUtils.findPattern(creative.getContent());
        if (StringUtils.isNotEmpty(clientCreativeId) && clientCreativeId.matches(".*\\d+.*")) {
          creative.setClientCreativeId(clientCreativeId);
        }
      }

      if (StringUtils.isNotEmpty(creative.getName())
              && StringUtils.isEmpty(creative.getClientCreativeId())) {
        String clientCreativeId = RegularExpressionUtils.findPatternInName(creative.getName());
        if (StringUtils.isNotEmpty(clientCreativeId))
          creative.setClientCreativeId(clientCreativeId);
      }

    });

    System.out.println("**"+creatives.get(0));


    System.out.println("***"+creatives.get(1));



  }



  }

