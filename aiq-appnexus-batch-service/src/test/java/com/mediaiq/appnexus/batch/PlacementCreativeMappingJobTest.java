package com.mediaiq.appnexus.batch;

import com.mediaiqdigital.aiq.tradingcenter.repo.CampaignKpiPixelRepo;
import org.junit.Before;
import org.junit.Test;
import org.springframework.batch.core.Job;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.mediaiq.appnexus.batch.service.AppnexusBatchService;
import com.mediaiqdigital.aiq.tradingcenter.domain.CampaignKpiPixel;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by prasannachidire on 3/11/17.
 */
public class PlacementCreativeMappingJobTest extends BaseTest {
  
  
  @Autowired
  @Qualifier("placementCreativeMappingLoadJob")
  Job placementCreativeMappingLoadJob;
  
  @Override
  @Before
  public void setup() {
    super.setup();
    jobLauncherTestUtils.setJob(placementCreativeMappingLoadJob);
  }
  
  @Autowired
  private AppnexusBatchService appnexusBatchService;
  
  @Test
  public void testPlacementCreativeMappingLoadJob() throws Exception {
    
    appnexusBatchService.launchPlacementCreativeMappingLoadJob(getOlderDate(10), true);

  }
}
