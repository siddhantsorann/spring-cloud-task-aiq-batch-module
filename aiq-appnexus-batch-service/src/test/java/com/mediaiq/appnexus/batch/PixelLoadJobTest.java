package com.mediaiq.appnexus.batch;

import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.mediaiq.appnexus.batch.value.StepAttribute;
import com.mediaiq.appnexus.repo.PixelRepo;
import com.mediaiq.appnexus.value.AppnexusSeat;

public class PixelLoadJobTest extends BaseTest {
  
  Date date;
  
  @Autowired
  @Qualifier("pixelLoadJob")
  Job pixelLoadJob;
  
  @Autowired
  PixelRepo pixelRepo;
  
  @Before
  public void setup() {
    super.setup();
    jobLauncherTestUtils.setJob(pixelLoadJob);
    date = getOlderDate(1200);
  }
  
  @Test
  public void test() throws Exception {
    
    JobParameters jobParametersForSeat1 =
        new JobParametersBuilder().addLong(StepAttribute.START_ELEMENT, 0L)
            .addDate("date", new Date()).addDate(StepAttribute.LAST_MODIFIED, date)
            .addString("appnexus_seat", AppnexusSeat.SEAT_MIQ_1.name()).toJobParameters();
    
    JobExecution jobExecutionForSeat1 = jobLauncherTestUtils.launchJob(jobParametersForSeat1);
    Assert.assertEquals(ExitStatus.COMPLETED, jobExecutionForSeat1.getExitStatus());
    
    long jobId = jobExecutionForSeat1.getJobId();
    System.out.println(" job id " + jobId);
    
    List<StepExecution> stepExecutionList =
        (List<StepExecution>) jobExecutionForSeat1.getStepExecutions();
    StepExecution firstStepExecution = stepExecutionList.get(0);
    Long stepExceutionId = firstStepExecution.getJobExecutionId();
    System.out.println(" executionId " + stepExceutionId);
    
    ExecutionContext executionContext = firstStepExecution.getExecutionContext();
    System.out.println(" context " + executionContext.toString());
    
    int countFromAppnexus = (Integer) executionContext.get("count");
    System.out.println(" count value " + countFromAppnexus);
    
    JobParameters jobParametersForSeat2 = new JobParametersBuilder()
        .addString("appnexus_seat", AppnexusSeat.SEAT_MIQ_2.name())
        .addDate(StepAttribute.LAST_MODIFIED, date).addDate("date", new Date()).toJobParameters();
    
    
    JobExecution jobExecutionForSeat2 = jobLauncherTestUtils.launchJob(jobParametersForSeat2);
    Assert.assertEquals(ExitStatus.COMPLETED, jobExecutionForSeat2.getExitStatus());
    
    jobId = jobExecutionForSeat2.getJobId();
    System.out.println(" job id " + jobId);
    
    stepExecutionList = (List<StepExecution>) jobExecutionForSeat2.getStepExecutions();
    firstStepExecution = stepExecutionList.get(0);
    stepExceutionId = firstStepExecution.getJobExecutionId();
    System.out.println(" executionId " + stepExceutionId);
    
    executionContext = firstStepExecution.getExecutionContext();
    System.out.println(" context " + executionContext.toString());
    
    countFromAppnexus += (Integer) executionContext.get("count");
    System.out.println(" count value " + countFromAppnexus);
    
    long totalCount = pixelRepo.findByLast(date);
    
    Assert.assertEquals(countFromAppnexus, totalCount);
  }
  
}
