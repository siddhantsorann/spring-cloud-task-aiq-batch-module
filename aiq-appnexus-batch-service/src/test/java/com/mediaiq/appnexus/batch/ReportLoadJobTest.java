package com.mediaiq.appnexus.batch;

import org.junit.Test;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mediaiq.appnexus.batch.component.ReportConfig;
import com.mediaiq.appnexus.domain.report.Filter;
import com.mediaiq.appnexus.domain.report.GroupFilter;
import com.mediaiq.appnexus.domain.report.ReportRequest;
import com.mediaiq.appnexus.domain.report.utils.FilterSerializer;
import com.mediaiq.appnexus.domain.report.utils.GroupFilterSerializer;
import com.mediaiq.appnexus.load.request.ReportRequestContainer;
import com.mediaiq.appnexus.value.AppnexusSeat;

public class ReportLoadJobTest {
  
  @Test
  public void test() throws JobExecutionAlreadyRunningException, JobRestartException,
      JobInstanceAlreadyCompleteException, JobParametersInvalidException {
    
    String[] springConfig = {"classpath:appnexus-batch-service-test-context.xml",
        "classpath:jobs/appnexus-daily-placement-stats-load-job.xml"};
    
    @SuppressWarnings("resource")
    ApplicationContext context = new ClassPathXmlApplicationContext(springConfig);
    
    JobLauncher jobLauncher = (JobLauncher) context.getBean("jobLauncher");
    Job job = (Job) context.getBean("appnexusDailyPlacementStatsLoadJob");
    
    JobParameters jobParameters2 =
        new JobParametersBuilder().addString("appnexus_seat", AppnexusSeat.SEAT_MIQ_2.name())
            .addString("salt", "32").toJobParameters();
    
    jobLauncher.run(job, jobParameters2);
    
    System.out.println("Done");
  }
  
  @Test
  public void jsonTest() {
    GsonBuilder builder = new GsonBuilder();
    builder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
    builder.registerTypeAdapter(Filter.class, new FilterSerializer());
    builder.registerTypeAdapter(GroupFilter.class, new GroupFilterSerializer());
    Gson gson = builder.create();
    ReportRequest reportRequest = new ReportConfig().provideReportRequest();
    ReportRequestContainer container = new ReportRequestContainer(reportRequest);
    System.out.println(gson.toJson(container));
  }
}
