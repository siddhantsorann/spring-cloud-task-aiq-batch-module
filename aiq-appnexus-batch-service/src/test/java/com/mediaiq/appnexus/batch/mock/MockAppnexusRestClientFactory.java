package com.mediaiq.appnexus.batch.mock;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mediaiq.appnexus.domain.report.Filter;
import com.mediaiq.appnexus.domain.report.GroupFilter;
import com.mediaiq.appnexus.domain.report.utils.FilterSerializer;
import com.mediaiq.appnexus.domain.report.utils.GroupFilterSerializer;
import com.mediaiq.appnexus.domain.utils.GsonDateDeserializer;
import com.mediaiq.appnexus.load.client.AppnexusRestClient;
import com.mediaiq.appnexus.load.config.AppnexusRestClientFactory;
import com.mediaiq.appnexus.load.service.AuthenticationService;
import com.mediaiq.appnexus.value.AppnexusSeat;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RestAdapter.Log;
import retrofit.RestAdapter.LogLevel;
import retrofit.converter.GsonConverter;

@Configuration
public class MockAppnexusRestClientFactory {
  
  @Autowired
  private AuthenticationService authenticationService;
  
  private final Logger logger = LoggerFactory.getLogger(AppnexusRestClientFactory.class);
  
  private final Log log = new Log() {
    
    @Override
    public void log(String message) {
      logger.info(message);
    }
  };
  
  @Value("${appnexus.api.endpoint}")
  private String appnexusApiEndpoint;
  
  @Bean(name = "JOB1")
  // @Qualifier("JOB")
  @Scope("job1")
  public AppnexusRestClient appnexusRestClient(
      
      Gson gson) {
    
    RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(appnexusApiEndpoint)
        .setRequestInterceptor(new RequestInterceptor() {
          @Override
          public void intercept(RequestFacade request) {
            request.addHeader("Authorization",
                authenticationService.getAuthenticationToken(AppnexusSeat.SEAT_MIQ_2));
          }
        }).setConverter(new GsonConverter(gson)).setLogLevel(LogLevel.FULL).setLog(log).build();
    
    return restAdapter.create(AppnexusRestClient.class);
  }
  
  @Bean
  public Gson provideAppnexusGsonBuilder() {
    return new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        .registerTypeAdapter(Date.class, new GsonDateDeserializer())
        .registerTypeAdapter(Filter.class, new FilterSerializer())
        .registerTypeAdapter(GroupFilter.class, new GroupFilterSerializer()).create();
  }
}
