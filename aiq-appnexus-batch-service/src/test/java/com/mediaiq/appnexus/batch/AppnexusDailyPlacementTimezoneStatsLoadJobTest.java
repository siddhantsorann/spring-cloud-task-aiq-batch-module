package com.mediaiq.appnexus.batch;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.springframework.batch.core.Job;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.mediaiq.appnexus.batch.service.AppnexusBatchService;
import com.mediaiq.appnexus.value.AppnexusSeat;

public class AppnexusDailyPlacementTimezoneStatsLoadJobTest extends BaseTest {
  
  @Autowired
  @Qualifier("appnexusDailyPlacementCreativeSellerTimeZoneStatsLoadJob")
  Job appnexusDailyPlacementCreativeSellerTimeZoneStatsLoadJob;
  
  @Override
  @Before
  public void setup() {
    super.setup();
    jobLauncherTestUtils.setJob(appnexusDailyPlacementCreativeSellerTimeZoneStatsLoadJob);
  }
  
  @Autowired
  private AppnexusBatchService appnexusBatchService;
  
  @Test
  public void testProcessingStep() throws Exception {
    
    String startDateStr = "10-Apr-2017";
    String endDateStr = "11-Apr-2017";
    
    DateFormat formatter = new SimpleDateFormat("d-MMM-yyyy");
    Date startDate = formatter.parse(startDateStr);
    Date endDate = formatter.parse(endDateStr);
    
    // JobParameters jobParameters = new
    // JobParametersBuilder().addDate("start_date", startDate)
    // .addDate("end_date", endDate).addString("timezone", "Europe/London")
    // .addString("appnexus_seat",
    // AppnexusSeat.SEAT_MIQ_1.name()).addDate("date", new Date())
    // .toJobParameters();
    //
    // JobExecution jobExecution =
    // jobLauncherTestUtils.launchJob(jobParameters);
    //
    // Assert.assertEquals(ExitStatus.COMPLETED,
    // jobExecution.getExitStatus());
    
    appnexusBatchService.launchAppnexusDailyPlacementTimeZoneStatsLoadJob(true,
        AppnexusSeat.SEAT_MIQ_1, startDate, endDate, null);
  }
  
}
